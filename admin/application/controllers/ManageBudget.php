<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManageBudget extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		
		}
		$this->load->model('Budget_model');
		
 	}
	
	public function index() {

		if(isset($_POST) && !empty($_POST)){

			$budget_data = $_POST;
			$budget_add = $this->Budget_model->addBudget($budget_data);
			if($budget_add == true){
				$this->session->set_flashdata('message', array('message' => 'Successfully Added', 'title' => 'Success !', 'class' => 'success'));
			}
			else{
				$this->session->set_flashdata('message', array('message' => 'Error Occured Budget Already Exist', 'title' => 'Error !', 'class' => 'error'));
			}
		}
		$all_budget = $this->Budget_model->get_all_budget();	

		$template['page'] = "ManageBudget/addBudget";
		$template['page_title'] = "Manage Budget Page";
		$template['data'] = $all_budget;
		$this->load->view('template', $template);
	}

	function budget_delete($id){
		$delete_data = $this->Budget_model->delete_budget($id);
		if($delete_data){
			$this->session->set_flashdata('message', array('message' => 'Successfully Deleted', 'title' => 'Success !', 'class' => 'success'));
			redirect(base_url().'ManageBudget');
		}
	}
	function budget_edit(){
        $id = $this->uri->segment(3);
        //print_r($id);exit();
        if($id == ''){
        	redirect(base_url().'ManageBudget/index');
        } 
        else{

			$budget_data = $this->Budget_model->get_single_budget($id);
			if($budget_data != ''){
				$template['page'] = "ManageBudget/editBudget";
				$template['page_title'] = "Manage Budget Page";
				$template['data'] = $budget_data;
				if(isset($_POST) && !empty($_POST)){
					$data=$_POST;
					$success_update = $this->Budget_model->update_budget($data, $id);
					if($success_update == true){
						$this->session->set_flashdata('message', array('message' => 'Successfully Updated', 'title' => 'Success !', 'class' => 'success'));
					redirect(base_url().'ManageBudget');
					}else{
						$this->session->set_flashdata('message', array('message' => 'Sorry Updation Failed. Exams Already Exist', 'title' => 'Error', 'class' => 'error'));
					redirect(base_url().'ManageBudget');
					}
				}
	   		}else{
	   			redirect(base_url().'ManageBudget/index');
	   		}
		}
		$this->load->view('template', $template);
	}
}