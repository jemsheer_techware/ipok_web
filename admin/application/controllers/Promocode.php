<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promocode extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
		$this->load->model('Promocode_model');
		
 	}
	
	public function index() {

		if(isset($_POST) && !empty($_POST)){
			$data = $_POST;
			$fileName =$_FILES['image']['name'];
			$fileName = str_replace('%','a',$fileName);
			$config = set_upload_options('../assets/uploads/promocode'); 
         	$config['file_name'] = $fileName;
         	$this->load->library('upload', $config);
	        if ( ! $this->upload->do_upload('image')) {
	            $error = array('error' => $this->upload->display_errors('', '')); 
	            $res = array(
						"status"=> "error",
						"error"=> "Upload Error",
						"message"=> "Sorry! Promocode Image not uploaded".$error['error']
					);	
	            $promocode_add =$res;
	        }	
	        else { 				         	
	            $imagedata = $this->upload->data(); 
	            $data['image']='assets/uploads/promocode/'.$imagedata['file_name'];
			}
			if(isset($data['image'])) {	
				$validdate = explode(' - ',$data['valid_from']);
				unset($data['valid_from']);
				$data['valid_from'] = strtotime($validdate[0]);
				$data['valid_to'] = strtotime($validdate[1]);
				$promocode_add = $this->Promocode_model->addPromocode($data);
			}
			if($promocode_add['status'] == 'success'){
				$this->session->set_flashdata('message', array('message' => 'Successfully Added', 'title' => 'Success !', 'class' => 'success'));
			}
			else{
				$this->session->set_flashdata('message', array('message' => $promocode_add['message'], 'title' => 'Error !', 'class' => 'error'));
			}
		}
		$all_problems = $this->Promocode_model->get_all_doctors();	
       
		$template['page'] = "ManagePromocode/addPromocode";
		$template['page_title'] = "Manage Promocode";
		$template['data'] = $all_problems;
		$this->load->view('template', $template);
	}
	function promocode_delete(){
		$id = $_GET['id'];
		$delete_data = $this->Promocode_model->delete_promocode($id);
		if($delete_data){
			$this->session->set_flashdata('message', array('message' => 'Successfully Deleted', 'title' => 'Success !', 'class' => 'success'));
			redirect(base_url().'Promocode/promocode_view');
		}
	}
	
	function promocode_view(){
		$all_promocodes = $this->Promocode_model->get_all_promocodes();	

		$template['page'] = "ManagePromocode/viewPromocode";
		$template['page_title'] = "View Promocode";
		$template['data'] = $all_promocodes;
		$this->load->view('template', $template);
	}
	function promocode_edit(){
        $id = $_GET['id'];
        if($id == ''){
        	redirect(base_url().'Promocode/promocode_view');
        } 
        else{
			$promocode_data = $this->Promocode_model->get_single_promocode($id);
            $all_doctors = $this->Promocode_model->get_all_doctors();
			if($promocode_data != ''){
				$template['page'] = "ManagePromocode/editPromocode";
				$template['page_title'] = "Edit Promocode Page";

				$valid_to = date('m/d/Y',$promocode_data->valid_to);
		        $valid_from = date('m/d/Y',$promocode_data->valid_from);
		        $new_date = $valid_from."-".$valid_to;
		        unset($promocode_data->valid_to);
		        unset($promocode_data->valid_from);
		        $promocode_data->valid_from = $new_date;
		        $promocode_data->doctor_id = explode(',',$promocode_data->doctor_id);

		        $template['data'] = $promocode_data;
		        $template['datas'] = $all_doctors;

				if(isset($_POST) && !empty($_POST)){
					$data=$_POST;
					if($_FILES['image']['name'] == ''){
						$data['image'] = $promocode_data->image;
					}else{
						$fileName =$_FILES['image']['name'];
						$fileName = str_replace('%','a',$fileName);
						$config = set_upload_options('../assets/uploads/promocode'); 
			         	$config['file_name'] = $fileName;
			         	$this->load->library('upload', $config);
				        if ( ! $this->upload->do_upload('image')) {
				            $error = array('error' => $this->upload->display_errors('', '')); 
				            $res = array(
									"status"=> "error",
									"error"=> "Upload Error",
									"message"=> "Sorry! Promocode Image not uploaded. ".$error['error']
								);	
				            $promocode_edit =$res;
				        }	
				        else { 				         	
				            $imagedata = $this->upload->data(); 
				            $data['image']='assets/uploads/promocode/'.$imagedata['file_name'];
						}
					}
						if(isset($data['image'])) {	
							$validdate = explode('-',$data['valid_from']);
							unset($data['valid_from']);
							$data['valid_from'] = strtotime($validdate[0]);
							$data['valid_to'] = strtotime($validdate[1]);
							$promocode_edit = $this->Promocode_model->update_promocode($data,$id);
						}
					if($promocode_edit['status'] == 'success'){
						$this->session->set_flashdata('message', array('message' => 'Successfully Updated', 'title' => 'Success !', 'class' => 'success'));
					redirect(base_url().'Promocode/promocode_view');
					}else{
							$this->session->set_flashdata('message', array('message' => $promocode_edit['message'], 'title' => 'Error !', 'class' => 'error'));
					redirect(base_url().'Promocode/promocode_view');
					}
				}
	   		}else{
	   			redirect(base_url().'Promocode/promocode_view');
	   		}
		}
		$this->load->view('template', $template);
	}
}