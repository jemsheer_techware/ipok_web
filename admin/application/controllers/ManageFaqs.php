<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManageFaqs extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		
		}
		$this->load->model('Faq_model');
		
 	}
	
	public function index() {

		if(isset($_POST) && !empty($_POST)){

			$faq_data = $_POST;
			$faq_add = $this->Faq_model->addFaq($faq_data);
			if($faq_add == true){
				$this->session->set_flashdata('message', array('message' => 'Successfully Added', 'title' => 'Success !', 'class' => 'success'));
			}
			else{
				$this->session->set_flashdata('message', array('message' => 'Error Occured! Faq Not Inserted', 'title' => 'Error !', 'class' => 'error'));
			}
		}	

		$template['page'] = "ManageFaqs/addFaq";
		$template['page_title'] = "Manage Faq Page";
		$this->load->view('template', $template);
	}

	function faq_view(){
		$template['page'] = "ManageFaqs/viewFaqs";
		$template['page_title'] = "Manage Faq Page";
		$template['data'] = $this->Faq_model->get_all_faqs();
		$this->load->view('template', $template);
	}

	function faq_edit(){
        $id = $_GET['id'];
        if($id == ''){
        	redirect(base_url().'ManageFaqs/faq_view');
        } 
        else{

			$faq_data = $this->Faq_model->get_single_faq($id);
			if($faq_data != ''){
				$template['page'] = "ManageFaqs/editFaq";
				$template['page_title'] = "Manage Faq Page";
				$template['data'] = $faq_data;
				if(isset($_POST) && !empty($_POST)){
					$data=$_POST;
					$success_update = $this->Faq_model->update_faq($data, $id);
					if($success_update == true){
						$this->session->set_flashdata('message', array('message' => 'Successfully Updated', 'title' => 'Success !', 'class' => 'success'));
					redirect(base_url().'ManageFaqs/faq_view');
					}else{
						$this->session->set_flashdata('message', array('message' => 'Sorry Updation Failed.', 'title' => 'Error', 'class' => 'error'));
					redirect(base_url().'ManageFaqs/faq_view');
					}
				}
	   		}else{
	   			redirect(base_url().'ManageFaqs/faq_view');
	   		}
		}
		$this->load->view('template', $template);
	}

	public function faq_doctor_add() {

		if(isset($_POST) && !empty($_POST)){

			$faq_data = $_POST;
			$faq_add = $this->Faq_model->addFaqDoctor($faq_data);
			if($faq_add == true){
				$this->session->set_flashdata('message', array('message' => 'Successfully Added', 'title' => 'Success !', 'class' => 'success'));
			}
			else{
				$this->session->set_flashdata('message', array('message' => 'Error Occured! Faq Not Inserted', 'title' => 'Error !', 'class' => 'error'));
			}
		}	

		$template['page'] = "ManageFaqsDoctor/addFaq";
		$template['page_title'] = "Manage Faq Page";
		$this->load->view('template', $template);
	}

	function faq_view_doctor(){
		$template['page'] = "ManageFaqsDoctor/viewFaqs";
		$template['page_title'] = "Manage Faq Page";
		$template['data'] = $this->Faq_model->get_all_faqs_doctor();
		$this->load->view('template', $template);
	}

	function faq_delete(){
		$id = $_GET['id'];
		$delete_data = $this->Faq_model->delete_faq($id);
		if($delete_data){
			$this->session->set_flashdata('message', array('message' => 'Successfully Deleted', 'title' => 'Success !', 'class' => 'success'));
			redirect(base_url().'ManageFaqs/faq_view');
		}
	}

	function faq_delete_doctor(){
		$id = $_GET['id'];
		$delete_data = $this->Faq_model->delete_faq($id);
		if($delete_data){
			$this->session->set_flashdata('message', array('message' => 'Successfully Deleted', 'title' => 'Success !', 'class' => 'success'));
			redirect(base_url().'ManageFaqs/faq_view_doctor');
		}
	}

/*	function faq_edit(){
        $id = $_GET['id'];
        if($id == ''){
        	redirect(base_url().'ManageFaqs/faq_view');
        } 
        else{

			$faq_data = $this->Faq_model->get_single_faq($id);
			if($faq_data != ''){
				$template['page'] = "ManageFaqs/editFaq";
				$template['page_title'] = "Manage Faq Page";
				$template['data'] = $faq_data;
				if(isset($_POST) && !empty($_POST)){
					$data=$_POST;
					$success_update = $this->Faq_model->update_faq($data, $id);
					if($success_update == true){
						$this->session->set_flashdata('message', array('message' => 'Successfully Updated', 'title' => 'Success !', 'class' => 'success'));
					redirect(base_url().'ManageFaqs/faq_view');
					}else{
						$this->session->set_flashdata('message', array('message' => 'Sorry Updation Failed.', 'title' => 'Error', 'class' => 'error'));
					redirect(base_url().'ManageFaqs/faq_view');
					}
				}
	   		}else{
	   			redirect(base_url().'ManageFaqs/faq_view');
	   		}
		}
		$this->load->view('template', $template);
	}*/

	function faq_edit_doctor(){
        $id = $_GET['id'];
        if($id == ''){
        	redirect(base_url().'ManageFaqs/faq_view_doctor');
        } 
        else{

			$faq_data = $this->Faq_model->get_single_faq($id);
			if($faq_data != ''){
				$template['page'] = "ManageFaqsDoctor/editFaq";
				$template['page_title'] = "Manage Faq Page";
				$template['data'] = $faq_data;
				if(isset($_POST) && !empty($_POST)){
					$data=$_POST;
					$success_update = $this->Faq_model->update_faq($data, $id);
					if($success_update == true){
						$this->session->set_flashdata('message', array('message' => 'Successfully Updated', 'title' => 'Success !', 'class' => 'success'));
					redirect(base_url().'ManageFaqs/faq_view_doctor');
					}else{
						$this->session->set_flashdata('message', array('message' => 'Sorry Updation Failed.', 'title' => 'Error', 'class' => 'error'));
					redirect(base_url().'ManageFaqs/faq_view_doctor');
					}
				}
	   		}else{
	   			redirect(base_url().'ManageFaqs/faq_view_doctor');
	   		}
		}
		$this->load->view('template', $template);
	}
}