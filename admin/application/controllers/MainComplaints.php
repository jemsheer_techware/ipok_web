<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MainComplaints extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
		$this->load->model('Complaint_model');
		
 	}
	
	public function index() {

		if(isset($_POST) && !empty($_POST)){
           // print_r($_POST);exit();
			$complaint_data = $_POST;
			$medicine_add = $this->Complaint_model->addComplaint($complaint_data);
			if($medicine_add == true){
				$this->session->set_flashdata('message', array('message' => 'Successfully Added', 'title' => 'Success !', 'class' => 'success'));
			}
			else{
				$this->session->set_flashdata('message', array('message' => 'Error Occured. Complaint Already Exist', 'title' => 'Error !', 'class' => 'error'));
			}
		}
		$all_complaints = $this->Complaint_model->get_all_complaints();	

		$template['page'] = "MainComplaints/addComplaints";
		$template['page_title'] = "Main Complaints Page";
		$template['data'] = $all_complaints;
		$this->load->view('template', $template);
	}
	function complaint_delete($id){
		$delete_data = $this->Complaint_model->delete_complaint($id);
		if($delete_data){
			$this->session->set_flashdata('message', array('message' => 'Successfully Deleted', 'title' => 'Success !', 'class' => 'success'));
			redirect(base_url().'MainComplaints/index');
		}
	}
	function complaint_edit(){
        $id = $this->uri->segment(3);
        if($id == ''){
        	redirect(base_url().'MainComplaints/index');
        } 
        else{
			$complaint_data = $this->Complaint_model->get_single_complaint($id);
			if($complaint_data != ''){
				$template['page'] = "MainComplaints/editComplaint";
				$template['page_title'] = "Main Complaint Page";
				$template['data'] = $complaint_data;
				if(isset($_POST) && !empty($_POST)){
					$datas=$_POST;
					//date_default_timezone_set("Asia/Kolkata");			
					//$data['modified_date']=date("Y-m-d h:i:sa");
					$success_update = $this->Complaint_model->update_complaint($datas, $id);
					if($success_update == true){
						$this->session->set_flashdata('message', array('message' => 'Successfully Updated', 'title' => 'Success !', 'class' => 'success'));
					redirect(base_url().'MainComplaints');
					}else{
						$this->session->set_flashdata('message', array('message' => 'Error Occured. Complaint already Exist', 'title' => 'error !', 'class' => 'error'));
					redirect(base_url().'MainComplaints');
					}
				}
	   		}else{
	   			redirect(base_url().'MainComplaints/index');
	   		}
		}
		$this->load->view('template', $template);
	}
}