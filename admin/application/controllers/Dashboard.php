<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
 	}
	
	public function index() {
		$template['page'] = "dashboard";
		$template['page_title'] = "Dashboard Page";
		$template['data'] = "Dashboard Page";
		$this->load->view('template', $template);
		
	}
}
