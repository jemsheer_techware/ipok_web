<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManageRole extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//date_default_timezone_set("Asia/Kolkata");
		if(!can_access_page()) {
			redirect(base_url()."error");
		}		
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
		$this->load->model('Role_model');
 	}
	
	function index() {

		if(isset($_POST) and !empty($_POST)) {
			$roledata=$_POST;
//			date_default_timezone_set("Asia/Kolkata");
			$roledata['created_date']=date("Y-m-d h:i:sa");
			$roledata['modified_date']=date("Y-m-d h:i:sa");
			$result=$this->Role_model->rolemng_users($roledata);
			if($result==true){					
					$this->session->set_flashdata('message', array('message' => 'Role saved successfully', 'title' => 'Success !', 'class' => 'success'));				
				}
				else if($result==false){
					$this->session->set_flashdata('message', array('message' => 'Role already exist', 'title' => 'Warning !', 'class' => 'warning'));
				}
				else{					
					$this->session->set_flashdata('message', array('message' => 'Sorry,Error Occured', 'title' => 'Error !', 'class' => 'danger'));
				}				
		}
		
		$template['page'] = "ManageRole/addRole";
		$template['page_title'] = "Add Role Page";
		$template['roles'] = $this->Role_model->get_roles();
		$this->load->view('template', $template);
	}

	public function role_edit(){
		$roleid = $this->uri->segment(3);
		if($roleid == ''){
			redirect(base_url().'ManageRole');
		}else{
			$template['page'] = "ManageRole/editRole";
			$template['page_title'] = "Edit Role Page";
			$template['data'] = $this->Role_model->get_singleroles($roleid);
			if($template['data'] == ''){
               redirect(base_url().'ManageRole');
			}else{
				if(isset($_POST) and !empty($_POST)){
					$data=$_POST;
		//			date_default_timezone_set("Asia/Kolkata");			
					$data['modified_date']=date("Y-m-d h:i:sa");
					$this->Role_model->update_role($data, $roleid);
				}
			}
		}
		$this->load->view('template', $template);
	}
	public function role_delete($roleid){
		$this->Role_model->delete_role($roleid);
	}
}
