<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManageDoctors extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(!can_access_page()) {
			redirect(base_url()."error");
		}if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
		$this->load->model('Webservice_model');
		$this->load->model('Doctor_model');
		$this->load->model('Clinic_model');
		//date_default_timezone_set("Asia/Kolkata");
		$this->logData = $this->session->userdata('logged_in');
 	}
	
	public function index() {
		$specialization = $this->Doctor_model->get_specializations();
		if(isset($_POST) && !empty($_POST)){
			//print_r($_POST);exit();
			$Newdata =array();
			$data = $_POST;			
			$Newdata['email'] = $data['reg_pat_email'];
			$Newdata['rg'] = $data['reg_pat_rg'];
			$Newdata['cpf'] = $data['reg_pat_cpf'];
    		$Newdata['crm'] = $data['reg_pat_crm'];
    		$Newdata['dob'] = strtotime($data['reg_pat_dob'].'0:00:00');
    		$Newdata['gender'] = $data['reg_pat_gender'];
    		$Newdata['cep'] = $data['reg_pat_cep'];
    		$Newdata['street_address'] = $data['reg_pat_streetadd'];
    		$Newdata['locality'] = $data['reg_pat_locality'];
    		$Newdata['number'] = $data['reg_pat_number'];
    		$Newdata['complement'] = $data['reg_pat_complement'];
    		$Newdata['name'] = $data['reg_pat_name'];
    		$Newdata['username'] = $data['reg_pat_username'];
    		$Newdata['password'] = $data['reg_pat_password'];
    		$Newdata['about'] = $data['reg_pat_biography'];  
    		$Newdata['specialization'] = $data['specialization'];  
    		$Newdata['price'] = $data['price'];  			    	
			$res = $this->main_Registration($Newdata);
			if($res['status'] == 'success'){
				$this->session->set_flashdata('message', array('message' => 'Successfully Registered', 'title' => 'Success !', 'class' => 'success'));
			}
			else{
				$this->session->set_flashdata('message', array('message' => $res['message'], 'title' => 'Warning !', 'class' => 'warning'));
			}
		}
		//$allDoctors = $this->Doctor_model->get_all_doctor();	
		$template['page'] = "ManageDoctors/addDoctors";
		$template['page_title'] = "Add Doctors Page";
		$template['data'] = $specialization;
		$this->load->view('template', $template);
	}
	function main_Registration($data){

				$check_result = $this->Webservice_model->dataExist_doctor($data);
				if($check_result['message'] == 'success')
				{	
					$data['password'] = md5($data['password']);
					$result = $this->Webservice_model->register_doctor($data);
					if($result['status'] == 'success'){
						$fileName = $result['userdata']['id'].'_'.$_FILES['profile_pic']['name'];
						$config = set_upload_options('../assets/uploads/profilepic/doctors'); 
			         	$config['file_name'] = $fileName;
			         	$this->load->library('upload', $config);
				         if ( ! $this->upload->do_upload('profile_pic')) {
				            $error = array('error' => $this->upload->display_errors('', '')); 
				            $res = array(
									"status"=> "error",
									"error"=> "Upload Error",
									"message"=> "Sorry! Profile Photo not uploaded".$error['error']
								);	
				            $this->Webservice_model->delete_registration_doctor($result['userdata']['id']);
				         }	
				         else { 				         	
				            $imagedata = $this->upload->data(); 
				            $fullfilepath='assets/uploads/profilepic/doctors/'.$imagedata['file_name'];	            
				            $res=$this->mainFn($result,$fullfilepath,'');				              
						}
					}
					else{
						$res = array(
										"status"=> "error",
										"error"=> "Insertion is failed",
										"message"=> "Sorry! Insertion is failed"
									);			
					}
				}
				else{
					$res = array(
									"status"=> "error",
									"error"=> "already exist",
									"message"=> $check_result['message']
								);
				}
				return $res;
	 		}

		 	function mainFn($result,$fullfilepath,$fullfilepath_bystander){

				//date_default_timezone_set("Asia/Kolkata");
				$static_string = 'IPOK_Doctor'.time();
				$authToken = uniqid($static_string);
				$result_authtoken = $this->Webservice_model->authtoken_registration_doctor($authToken,$result['userdata']['id']);
				if($result_authtoken){
					$picdata = array('profile_pic'=>$fullfilepath);
					$this->Webservice_model->updatePic_doctor($picdata,$result['userdata']['id']);					
					$res = array(
						"status"=> "success",
						"data"=>array(
							"auth_token"=>$authToken,
							"user"=>array(							
								
							)
						)
					);
				}
				return $res;
			}
	function exist(){
		//echo "<script type='text/javascript'>alert('mmmmm');</script>";die();
		$data = $_POST;
		$check_result = $this->Webservice_model->dataExist_doctor($data);

		print json_encode($check_result);
	}
	function view(){
		$allDoctors = $this->Doctor_model->get_all_doctor();
		if(isset($this->logData['uType'])){
			
		if($this->logData['uType'] == 'clinic'){

			$all_existdoctors = $this->Clinic_model->get_exist_doctor($this->logData['id']);
			$template['data_existDoctor'] = $all_existdoctors;
		}
		}
		$template['page'] = "ManageDoctors/viewDoctors";
		$template['page_title'] = "Add Doctors Page";
		$template['data'] = $allDoctors;
		
		$this->load->view('template', $template);
	}

	public function check_cep()
	{
		$data = $_POST;
		$result= check_cep_viacep($data['cep']); //common helper
		print ($result);
	}
	
	function assign_clinic_doctor($doctorid){
		//print_r($doctorid);
		//print_r($this->logData['id']);die();

								
				$template['page'] = "ManageClinic/addDoctors";
				$template['page_title'] = "Assign Doctor Page";
				$template['data_doctorId'] = $doctorid;
				$template['data_id'] = $this->logData['id'];
				$this->load->view('template', $template);
	}
	function bookingList(){
					
					$booking_list = $this->Doctor_model->get_bookinglist($this->logData['id']);
					//print_r($booking_list);die();
					$template['page'] = "ManageDoctors/bookingList";
					$template['page_title'] = "Booking List Page";
					$template['data'] = $booking_list;
					$this->load->view('template', $template);
	}

	public function status(){
		$id = $this->uri->segment(3);
		$s=$this->Doctor_model->update_status($id);
		$this->session->set_flashdata('message', array('message' => 'Doctor Status enable Successfully','class' => 'success'));
		redirect(base_url().'ManageDoctors/view');
	}

     public function status_active(){
	  $id = $this->uri->segment(3);
 	  $s=$this->Doctor_model->enable_status($id);
	  $this->session->set_flashdata('message', array('message' => 'Doctor Status disable Successfully','class' => 'success'));
	  redirect(base_url().'ManageDoctors/view');
	 }	
}