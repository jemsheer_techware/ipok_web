<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MajorProblems extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
		$this->load->model('MajorProblem_model');
		
 	}
	/********************Major Problem Functionalities*****************************/
	
	public function index() {

		if(isset($_POST) && !empty($_POST)){
			$data = $_POST;
			$result = $this->MajorProblem_model->addProblems($data);
			if($result == true){
				$this->session->set_flashdata('message', array('message' => 'Successfully Added', 'title' => 'Success !', 'class' => 'success'));
			}
			else{
				$this->session->set_flashdata('message', array('message' => 'Error Occured. Complaint Already Exist', 'title' => 'Error !', 'class' => 'error'));
			}
		}
		$all_problems = $this->MajorProblem_model->get_all_Problems();	

		$template['page'] = "MajorProblems/addProblems";
		$template['page_title'] = "Add Major Problem";
		$template['data'] = $all_problems;
		$this->load->view('template', $template);
	}
	function problem_delete($id){
		$delete_data = $this->MajorProblem_model->delete_problem($id);
		if($delete_data){
			$this->session->set_flashdata('message', array('message' => 'Successfully Deleted', 'title' => 'Success !', 'class' => 'success'));
			redirect(base_url().'MajorProblems/index');
		}
	}
	function problem_edit(){
        $id = $this->uri->segment(3);
        if($id == ''){
        	redirect(base_url().'MajorProblems/index');
        } 
        else{
			$problem_data = $this->MajorProblem_model->get_single_Problem($id);
			if($problem_data != ''){
				$template['page'] = "MajorProblems/editProblem";
				$template['page_title'] = "Major Problem";
				$template['data'] = $problem_data;
				if(isset($_POST) && !empty($_POST)){
					$datas=$_POST;

					$success_update = $this->MajorProblem_model->update_problem($datas, $id);
					if($success_update == true){
						$this->session->set_flashdata('message', array('message' => 'Successfully Updated', 'title' => 'Success !', 'class' => 'success'));
					redirect(base_url().'MajorProblems');
					}else{
						$this->session->set_flashdata('message', array('message' => 'Error Occured. Problem already Exist', 'title' => 'error !', 'class' => 'error'));
					redirect(base_url().'MajorProblems');
					}
				}
	   		}else{
	   			redirect(base_url().'MajorProblems/index');
	   		}
		}
		$this->load->view('template', $template);
	}


	/************************** Major Sub Problem functionalities ***************************/

	function subproblem_index(){
		if(isset($_POST) && !empty($_POST)){
			$data = $_POST;
			$result = $this->MajorProblem_model->addSubProblems($data);
			if($result == true){
				$this->session->set_flashdata('message', array('message' => 'Successfully Added', 'title' => 'Success !', 'class' => 'success'));
			}
			else{
				$this->session->set_flashdata('message', array('message' => 'Error Occured. Problem Already Exist in this Category', 'title' => 'Error !', 'class' => 'error'));
			}
		}
		$all_problems = $this->MajorProblem_model->get_all_Problems();
		$all_subproblems = $this->MajorProblem_model->get_all_subProblems();	

		$template['page'] = "ManageSubProblems/addsubProblems";
		$template['page_title'] = "Add Major SubProblem";
		$template['datas'] = $all_subproblems;
		$template['data'] = $all_problems;
		$this->load->view('template', $template);
	}

	public function subproblem_edit(){
		$id = $this->uri->segment(3);
        if($id == ''){
        	redirect(base_url().'MajorProblems/subproblem_index');
        } 
        else{
        	$all_problems = $this->MajorProblem_model->get_all_Problems();
        	$template['data'] = $all_problems;

			$subproblem_data = $this->MajorProblem_model->get_single_subProblem($id);
			if($subproblem_data != ''){
				$template['page'] = "ManageSubProblems/editsubProblems";
				$template['page_title'] = "Major Problem";
				$template['datas'] = $subproblem_data;
				if(isset($_POST) && !empty($_POST)){
					$datas=$_POST;

					$success_update = $this->MajorProblem_model->update_subProblem($datas, $id);
					if($success_update == true){
						$this->session->set_flashdata('message', array('message' => 'Successfully Updated', 'title' => 'Success !', 'class' => 'success'));
					redirect(base_url().'MajorProblems/subproblem_index');
					}else{
						$this->session->set_flashdata("message", array("message" => "Error Occured. ".ucfirst(strtolower($datas['subproblem_name']))." in this category already Exist", "title" => "error !", "class" => "error"));
					redirect(base_url().'MajorProblems/subproblem_index');
					}
				}
	   		}else{
	   			redirect(base_url().'MajorProblems/subproblem_index');
	   		}
		}
		$this->load->view('template', $template);
	}

	public function subproblem_delete($id){
		$delete_data = $this->MajorProblem_model->delete_subproblem($id);
		if($delete_data == true){
			$this->session->set_flashdata('message', array('message' => 'Successfully Deleted', 'title' => 'Success !', 'class' => 'success'));
			redirect(base_url().'MajorProblems/subproblem_index');
		}
	}
}