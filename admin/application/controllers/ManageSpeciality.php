<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManageSpeciality extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
//		date_default_timezone_set("Asia/Kolkata");
		$this->load->helper(array( 'url')); 
		$this->load->model('Speciality_model');
		$this->load->library('form_validation');
			
 	}
 	public function index(){
 		
 		if(isset($_POST) && !empty($_POST)){
 			$data = $_POST;
 			$result = $this->Speciality_model->addSpeciality($data);
 			if($result['status'] == 'success'){
 				$this->session->set_flashdata('message', array('message' => 'Speciality Added successfully', 'title' => 'Success !', 'class' => 'success'));
 			}
 			else{
 				$this->session->set_flashdata('message', array('message' => 'Error', 'title' => 'Error !', 'class' => 'danger'));
 			}
 		}
 			$get_speciality = $this->Speciality_model->get_speciality();
 			$template['page'] = "ManageSpeciality/addSpeciality";
			$template['page_title'] = "speciality Page";
			$template['data'] =  $get_speciality;
			$this->load->view('template', $template);
 	}
 	public function delete($id){
 		$result = $this->Speciality_model->delete_speciality($id);
 		if($result['status'] == 'success'){
 			$this->session->set_flashdata('message', array('message' => 'Speciality deleted successfully', 'title' => 'Success !', 'class' => 'success'));
 			redirect(base_url().'ManageSpeciality');
 		}
 		else if($result['status'] == 'error'){
 			$this->session->set_flashdata('message', array('message' => 'This Speciality is not allowed to delete ', 'title' => 'Error !', 'class' => 'danger'));
 			redirect(base_url().'ManageSpeciality');
 		}
 	}
 	public function edit(){
 		$id = $this->uri->segment(3);
 		if($id == ''){
          redirect(base_url().'ManageSpeciality');
 		}else{
	 		$get_single_speciality = $this->Speciality_model->get_single_speciality($id);
	 		if($get_single_speciality == ''){
                 redirect(base_url().'ManageSpeciality');
	 		}
	 		else{
		 		if(isset($_POST) && !empty($_POST)){
		 			
		 			$newData = $_POST;
		 			$result = $this->Speciality_model->edit_speciality($newData,$id);
			 		if($result){
			 			$this->session->set_flashdata('message', array('message' => 'Speciality Updated successfully', 'title' => 'Success !', 'class' => 'success'));
			 			redirect(base_url().'ManageSpeciality');
			 		}
		 		}
		 			$template['page'] = "ManageSpeciality/editSpeciality";
					$template['page_title'] = "speciality Page";
					$template['data'] =  $get_single_speciality;
					$this->load->view('template', $template);
			}
 		}		
 	}
}