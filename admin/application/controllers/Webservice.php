<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

 	if (isset($_SERVER['HTTP_ORIGIN'])) {
       header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
       header('Access-Control-Allow-Credentials: true');
       header('Access-Control-Max-Age: 86400');    // cache for 1 day
   	}    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");        exit(0);
    }

class Webservice extends CI_Controller {

	
	public function __construct() {
		parent::__construct();
		
//		date_default_timezone_set("Asia/Kolkata");
		header('Content-Type: application/json');
		$this->load->model('Webservice_model');
		$this->load->helper(array('form', 'url')); 
		$this->load->library('email');
		
		global  $string;  
		$this->load->helper('file');
		$string = read_file('commonData.php');
		$this->string = json_decode($string);
		
 	}
/*................................... IPOK PATIENT API ....................................*/

/*.............................. For User Registration .................................*/

 	function main_Registration($data){
 		$type = 'user';
		$check_result = $this->Webservice_model->dataExist($data,$type);
		//print_r($check_result);exit();
		if($check_result['message'] == 'success')
		{	
			if(isset($data['cpf'])){
				$ress = $this->cpf_valid($data['cpf']);	

				if((isset($ress)) && ($ress == 1)){
					/*$is_cpf = $this->Webservice_model->is_cpf_exist($data['cpf']);
					if($is_cpf['status'] == 'success'){*/
						$res = $this->registration_processing_user($data,$type);
					/*}else{
						$res = array(
								"status"=> "error",
								"error"=> "CPF Exist",
								"message"=> "CPF Already Exist"
							);
					}*/
				}else{
					$res = array(
								"status"=> "error",
								"error"=> "Invalid CPF",
								"message"=> "Invalid CPF"
							);
				}
			}else{
				$res = $this->registration_processing_user($data,$type);
			}
		}
		else{
			$res = array(
							"status"=> "error",
							"error"=> "already exist",
							"message"=> $check_result['message']
						);
		}
		return $res;
 	}
	
	public function registration_processing_user($data,$type){
		$data['password'] = md5($data['password']);
		$data['dob'] = $data['dob'];
		$new = array();
		
		if(isset($data['bystander_name']) || isset($data['bystander_relation'])  || isset($data['bystander_cpf']) || isset($data['bystander_dob']) ){
			if(isset($data['bystander_name'])){	
				$new['dependent_name'] = $data['bystander_name'];
				unset($data['bystander_name']);
			}
			if(isset($data['bystander_relation'])){	
				$new['relation'] = $data['bystander_relation'];
				unset($data['bystander_relation']);
			}
			if(isset($data['bystander_cpf'])){	
				$new['cpf'] = $data['bystander_cpf'];
				unset($data['bystander_cpf']);
			}
			if(isset($data['bystander_dob'])){	
				$new['dob'] = strtotime($data['bystander_dob']);
				unset($data['bystander_dob']);
			}
	
			$result = $this->Webservice_model->registration($data,$type);
			$this->Webservice_model->insert_bystander_registration($new,$result);
		}else{
			$result = $this->Webservice_model->registration($data,$type);
		}
		

		if($result['status'] == 'success'){
			$fileName = $result['userdata']['id'].'_'.$_FILES['profile_photo']['name'];
			$config = set_upload_options('../assets/uploads/profilepic/patient/'); 
			$config['file_name'] = $fileName;
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('profile_photo')) {
				$error = array('error' => $this->upload->display_errors('', '')); 
				$res = array(
						"status"=> "error",
						"error"=> "Upload Error",
						"message"=> "Sorry! Profile Photo not uploaded".$error['error']
					);	
				$this->Webservice_model->delete_registration($result['userdata']['id'],$type);
			}	
			else { 
				
				$imagedata = $this->upload->data(); 
				$fullfilepath='assets/uploads/profilepic/patient/'.$imagedata['file_name'];
				if(isset($_FILES['bystander_profile_photo'])){
					$fileName = $result['userdata']['id'].'_'.$_FILES['bystander_profile_photo']['name'];
					$config = set_upload_options('../assets/uploads/profilepic/patient_dependent/'); 
					$config['file_name'] = $fileName;
					$this->upload->initialize($config);
					$this->load->library('upload', $config);
					if(! $this->upload->do_upload('bystander_profile_photo')){
						 $error = array('error' => $this->upload->display_errors('', '')); 
						$res = array(
								"status"=> "error",
								"error"=> "Upload Error",
								"message"=> "Sorry! ByStander Profile Photo not uploaded".$error['error']
							);	    
						$this->Webservice_model->delete_registration($result['userdata']['id'],$type);
						$this->Webservice_model->delete_patient_registration($result['userdata']['id']);  
					}
					else{
						$imagedata_bystander = $this->upload->data(); 
						$fullfilepath_bystander = 'assets/uploads/profilepic/patient_dependent/'.$imagedata_bystander['file_name'];
						$res=$this->mainFn($result,$fullfilepath,$fullfilepath_bystander);
					}
				}
				else{
					$res=$this->mainFn($result,$fullfilepath,'');
				}    
			}
		}
		else{
			$res = array(
							"status"=> "error",
							"error"=> "Insertion is failed",
							"message"=> "Sorry! Insertion is failed"
						);			
		}
		return $res;
	}

 	function mainFn($result,$fullfilepath,$fullfilepath_bystander){

//		date_default_timezone_set("Asia/Kolkata");
		$static_string = 'IPOK_User'.time();
		$authToken = uniqid($static_string);
		$result_authtoken = $this->Webservice_model->authtoken_registration($authToken,$result['userdata']['id']);
		if($result_authtoken){
			$picdata = array('profile_photo'=>$fullfilepath);
			$type = 'user';
			$this->Webservice_model->updatePic($picdata,$result['userdata']['id'],$type);
			if($fullfilepath_bystander){
				$picdata_bystander = array('image'=>$fullfilepath_bystander);
				$bystander_data = $this->Webservice_model->updatePicbystander($picdata_bystander,$result['userdata']['id']);
				$bystander_name = $bystander_data->dependent_name;
				$bystander_relation = $bystander_data->relation;
				$bystander_cpf = $bystander_data->cpf;
				$bystander_dob =$bystander_data->dob;
				$bystander_profile_photo = $bystander_data->image;
			}else{
				$bystander_name ='';
				$bystander_relation = '';
				$bystander_cpf = '';
				$bystander_dob ='';
				$bystander_profile_photo = '';
			}
			if($result['userdata']['cpf'] == ''){
				$result['userdata']['cpf'] = '';
			}
			if($result['userdata']['rg'] == ''){
				$result['userdata']['rg'] = '';
			}
			if($result['userdata']['weight'] == ''){
				$result['userdata']['weight'] = '';
			}
			if($result['userdata']['height'] == ''){
				$result['userdata']['height'] = '';
			}
			if($result['userdata']['blood_group'] == ''){
				$result['userdata']['blood_group'] = '';
			}
			if($result['userdata']['landmark'] == ''){
				$result['userdata']['landmark'] = '';
			}
			$res = array(
				"status"=> "success",
				"data"=>array(
					"auth_token"=>$authToken,
					"user"=>array(
						"user_id"=> $result['userdata']['id'],
						"name"=> $result['userdata']['name'],
						"username"=> $result['userdata']['username'],
						"email"=> $result['userdata']['email'],
						"password" => $result['userdata']['password'],
						"cpf" => $result['userdata']['cpf'],
						"rg" => $result['userdata']['rg'],
						"dob" => $result['userdata']['dob'],
						"gender" => $result['userdata']['gender'],
						"weight" => $result['userdata']['weight'] ,
						"height" => $result['userdata']['height'], 
						"blood_group" => $result['userdata']['blood_group'],
						"zip_code" => $result['userdata']['zip_code'],
						"street_address" => $result['userdata']['street_address'],
						"locality" => $result['userdata']['locality'],
						"number" => $result['userdata']['number'],
						"landmark" =>$result['userdata']['landmark'],
						"profile_photo" => $fullfilepath,
						"bystander_name" =>$bystander_name,
						"bystander_relation" => $bystander_relation,
						"bystander_cpf" => $bystander_cpf,
						"bystander_dob" => $bystander_dob,
						"bystander_profile_photo" => $bystander_profile_photo
					)
				)
			);
		}
		return $res;
	}


	public function registration(){
		header('Content-type: application/json');
	//	$data =(array) json_decode(file_get_contents("php://input"));
		$data = $_POST;		
		//print_r($data);die();
		if(isset($data['name']) && strlen($data['name']) && isset($data['username']) && strlen($data['username']) && isset($data['email']) && strlen($data['email']) && isset($data['password']) && strlen($data['password']) && isset($data['dob']) && strlen($data['dob']) && isset($data['gender']) && strlen($data['gender']) && isset($data['zip_code']) && strlen($data['zip_code']) && isset($data['street_address']) && strlen($data['street_address']) && isset($data['locality']) && strlen($data['locality']) && isset($data['number']) && strlen($data['number']) && isset($_FILES['profile_photo']))
		{
			$res = $this->main_Registration($data);
		}
		else{
			$res = array(
								"status"=> "error",
								"error"=> "required",
								"message"=> "Sorry! Registration is failed"
							);
		}			
		print json_encode($res);
	}
	
/*.........................................................................................*/	
/*.................................. User Login ...........................................*/


	function login(){
		header('Content-type: application/json');
		$data =(array) json_decode(file_get_contents("php://input"));
		//$data=$_POST;

		if(isset($data['username']) && strlen($data['username']) && isset($data['password']) && strlen($data['password']))
		{
			$result=$this->Webservice_model->login($data);
			if($result['status']=='success'){
			$bystander_data=$this->Webservice_model->get_patient_depend($result['userdata']['id']);
			if($result['userdata']['cpf'] == ''){
				$result['userdata']['cpf'] = '';
			}
			if($result['userdata']['rg'] == ''){
				$result['userdata']['rg'] = '';
			}
			if($result['userdata']['weight'] == ''){
				$result['userdata']['weight'] = '';
			}
			if($result['userdata']['height'] == ''){
				$result['userdata']['height'] = '';
			}
			if($result['userdata']['blood_group'] == ''){
				$result['userdata']['blood_group'] = '';
			}
			if($result['userdata']['landmark'] == ''){
				$result['userdata']['landmark'] = '';
			}
				$res = array(
							"status"=> "success",
							"data"=>array(
								"auth_token"=>$result['userdata']['authtoken'],
								"user"=>array(
									"user_id"=> $result['userdata']['userid'],
									"name"=> $result['userdata']['name'],
									"username"=> $result['userdata']['username'],
									"email"=> $result['userdata']['email'],
									"password" => $result['userdata']['password'],
									"cpf" => $result['userdata']['cpf'],
									"rg" => $result['userdata']['rg'],
									"dob" => $result['userdata']['dob'],
									"gender" => $result['userdata']['gender'],
									"weight" => $result['userdata']['weight'] ,
									"height" => $result['userdata']['height'], 
									"blood_group" => $result['userdata']['blood_group'],
									"zip_code" => $result['userdata']['zip_code'],
									"street_address" => $result['userdata']['street_address'],
									"locality" => $result['userdata']['locality'],
									"number" => $result['userdata']['number'],
									"landmark" =>$result['userdata']['landmark'],
									"profile_photo" => $result['userdata']['profile_photo'],
									"bystander_details"=>$bystander_data
									

									)
								)
							);
			}
			else{
				$res = array(
							"status"=> "error",
							"error"=> "Login Failed",
							"message"=> "Invalid username or Password"
							);
			}
		}
		else{
			$res = array(
						"status"=> "error",
						"error"=> "Login Failed",
						"message"=> "Invalid username or Password"
					);
		}
		print json_encode($res);

	}

/*........................................................................................... */
/*................................... Facebook Registration/Login ........................... */


	function facebook_login() {
		header('Content-type: application/json');
		//$data =(array) json_decode(file_get_contents("php://input"));
		$data=$_POST;
		if(isset($data['uid']) && strlen($data['uid']) && isset($data['access_token']) && strlen($data['access_token']) && isset($data['name']) && strlen($data['name']))
		{

			$status=false;
			$check_result = $this->Webservice_model->is_uid_exist_patient($data['uid']);
				//print_r($check_result);exit();
			if($check_result['status'] == 'exist'){
				$userData = $this->Webservice_model->get_userdata_patient_by_uid($data['uid']);
//print_r($userData);exit();
				if($userData['status']=='success'){
					$bystander_data=$this->Webservice_model->get_patient_depend($userData['userdata']['id']);
					$result_1 = $this->get_patient_fulldata($userData,$bystander_data);
					$status=true;

					if(isset($data['email']) && strlen($data['email'])){
						$check_result = $this->Webservice_model->email_exist($data['email']);				
						if($check_result['status'] == 'exist'){
							$userData = $this->Webservice_model->get_userdata($data['email']);
							//print_r($userData);
							if($userData['status']=='success'){
								$bystander_data=$this->Webservice_model->get_patient_depend($userData['userdata']['id']);
								$result_2 = $this->get_patient_fulldata($userData,$bystander_data);
								//print_r($result_1);echo"<br>";
								//print_r($result_2);echo"<br>";
								//print_r($result_1['data']['user']['user_id']);echo "<br>";
									//print_r($result_2['data']['user']['user_id']);exit();
								if($result_1['data']['user']['user_id'] == $result_2['data']['user']['user_id']){
									$this->Webservice_model->update_patient_fbuid($result_2['data']['user']['user_id'],$data);
									$res = $result_2;
								}else{
									$res = $result_1;
								}
							}
							else if($userData['status'] == 'fail'){					
								$res=$result_1;
							}
						}
						else if($check_result['status'] == 'not exist'){					
							$res=$result_1;
						}
					}else{
						$res=$result_1;
					}
					

				}
				else if($userData['status'] == 'fail'){
					$status=false;
				}
			}else if($check_result['status'] == 'not exist'){
				$status=false;

				if(isset($data['email']) && strlen($data['email'])){
					$check_result = $this->Webservice_model->email_exist($data['email']);
				
					if($check_result['status'] == 'exist'){
						$userData = $this->Webservice_model->get_userdata($data['email']);
						if($userData['status']=='success'){
							$status=true;
							$bystander_data=$this->Webservice_model->get_patient_depend($userData['userdata']['id']);
							$res = $this->get_patient_fulldata($userData,$bystander_data);
						}
						else if($userData['status'] == 'fail'){				
							$status=false;
						}
					}
					else if($check_result['status'] == 'not exist'){					
						$status=false;
					}
				}

			}

			if(!$status){
				$res = $this->get_insert_patient_uid_data($data);
			}
		}
		else{
			$res = array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Facebook Login failed. All Field is required"
						);
		}
		print json_encode($res);
	}


/*........................................................................................ */

/*........................ Check Email and Username Availability ........................... */


	function check_availability(){
		header('Content-type: application/json');

		$data =(array) json_decode(file_get_contents("php://input"));
		//$data = $_POST;
		//print json_encode($data);die();

	 	//print_r($data);die();
		// if(isset($data['username']) && strlen($data['username'])){
			
		// }
		 if((isset($data['username']) && strlen($data['username'])) || (isset($data['email']) && strlen($data['email']))){
		// {
			$type = 'user';
			$check_result = $this->Webservice_model->dataExist($data,$type);
			//print_r($check_result);exit();
			if(!isset($data['username'])){
				$data['username'] = '';
			}
			if(!isset($data['email'])){

				$data['email'] = '';
			}
			$res = '';

			if($check_result['message'] == 'success'){
				if(isset($data['email']) && strlen(trim($data['email']," ")) > 0){
					$return_data["email"] = $data['email'];
					$return_data["is_email_avaliable"] = true;
				}else if(isset($data['username']) && strlen(trim($data['username']," ")) > 0){
					$return_data["username"] = $data['username'];
					$return_data["is_username_avaliable"] = true;
				}
			}
			else if( $check_result['message'] == 'username and email are already exist'){
				$return_data["email"] = $data['email'];
				$return_data["username"] = $data['username'];
				$return_data["is_email_avaliable"] = false;
				$return_data["is_username_avaliable"] = false;

			}
			else if( $check_result['message'] == 'username already exist'){
				$return_data["username"] = $data['username'];
				$return_data["is_username_avaliable"] = false;

			}
			else if( $check_result['message'] == 'email already exist'){
				$return_data["email"] = $data['email'];
				$return_data["is_email_avaliable"] = false;
				
			}
			else{
				$res = array(
									"status"=> "error",
									"error"=> "required",
									"message"=> "Data is required for checking"
								);
			}
			if($res == ''){
				$res = array(
						"status"=> "success",
						"data"=> $return_data
						);
			}
		}else{
			$res = array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "Data is required for checking"
						);
		}
		print json_encode($res);

	}
/*....................................................................................... */

/*............................... Update User Profile   ................................. */

	 function facebook_registration(){
	 	header('Content-type: application/json');

	 	//$data =(array) json_decode(file_get_contents("php://input"));
	 	$data = $_POST;
	 	if(isset($data['uid']) && strlen($data['uid']) && isset($data['access_token']) && strlen($data['access_token']) && isset($data['email']) && strlen($data['email'])  && isset($data['dob']) && strlen($data['dob']) && isset($data['gender']) && strlen($data['gender']) && isset($data['zip_code']) && strlen($data['zip_code']) && isset($data['street_address']) && strlen($data['street_address']) && isset($data['locality']) && strlen($data['locality']) && isset($data['number']) && strlen($data['number']) && isset($_FILES['profile_photo']))
		{
			$type = 'user';
			$result = $this->Webservice_model->is_this_uid_exit_patient($data);
			if($result['status'] == 'success'){
				$fileName = $result['userdata']['id'].'_'.$_FILES['profile_photo']['name'];
				$config = set_upload_options('../assets/uploads/profilepic/patient/'); 
	         	$config['file_name'] = $fileName;
	         	$this->load->library('upload', $config);
		        if ( ! $this->upload->do_upload('profile_photo')) {
		            $error = array('error' => $this->upload->display_errors('', '')); 
		            $res = array(
							"status"=> "error",
							"error"=> "Upload Error",
							"message"=> "Sorry! Profile Photo not uploaded".$error['error']
						);	
		            $this->Webservice_model->delete_registration($result['userdata']['id'],$type);
		        }	
		        else { 
		         	
		            $imagedata = $this->upload->data(); 
		            $fullfilepath='assets/uploads/profilepic/patient/'.$imagedata['file_name'];
		            if(isset($_FILES['bystander_profile_photo'])){
		            	$fileName = $result['userdata']['id'].'_'.$_FILES['bystander_profile_photo']['name'];
		            	$config = set_upload_options('../assets/uploads/profilepic/patient_dependent/'); 
			         	$config['file_name'] = $fileName;
			         	$this->upload->initialize($config);
			         	$this->load->library('upload', $config);
			        	if(! $this->upload->do_upload('bystander_profile_photo')){
			        		 $error = array('error' => $this->upload->display_errors('', '')); 
				            $res = array(
									"status"=> "error",
									"error"=> "Upload Error",
									"message"=> "Sorry! ByStander Profile Photo not uploaded".$error['error']
								);
				            $this->Webservice_model->delete_registration($result['userdata']['id'],$type);
				            $this->Webservice_model->delete_patient_registration($result['userdata']['id']);  
			        	}
			        	else{
			        		$imagedata_bystander = $this->upload->data(); 
		            		$fullfilepath_bystander = 'assets/uploads/profilepic/patient_dependent/'.$imagedata_bystander['file_name'];
		            		$picdata_bystander = array('image'=>$fullfilepath_bystander);
							$bystande = $this->Webservice_model->updatePicbystander($picdata_bystander,$result['userdata']['id']);
							$userData = $this->Webservice_model->get_userdata($data['email']);
							$bystander_data=$this->Webservice_model->get_patient_depend($userData['userdata']['id']);
							 $res = $this->get_patient_fulldata($userData,$bystander_data);
			        	}
		            }
		            else{
		            	$picdata = array('profile_photo'=>$fullfilepath);
						$this->Webservice_model->updatePic($picdata,$result['userdata']['id'],$type);
						$userData = $this->Webservice_model->get_userdata($data['email']);
						$bystander_data=$this->Webservice_model->get_patient_depend($userData['userdata']['id']);
						$res = $this->get_patient_fulldata($userData,$bystander_data);
		            }    
				}
			}elseif ($result['status'] == 'fail') {
				$res = array('status'=>'error','message'=>$result['message']);
			}

			else{
				$res = array(
							"status"=> "error",
							"error"=> "error",
							"message"=> "Sorry! Registration is failed.Try Again"
							);
			}
			       
		}
		else{
			$res = array(
								"status"=> "error",
								"error"=> "required",
								"message"=> "Sorry! Registration is failed. All Field is required"
							);
		}
		print json_encode($res);
	

	 }
/*............................................................................................*/

/*............................ ... Update User Profile   ................................ . */


	/*function forgot_password(){
		header('Content-type: application/json');

		//$data =(array) json_decode(file_get_contents("php://input"));
	 	$data = $_POST;

	 	if(isset($data['email']) && strlen($data['email'])){

	 		$check_result = $this->Webservice_model->email_exist($data['email']);
	 		if($check_result['status'] == 'exist'){
	 			  $configs = array(
                        'protocol'=>'smtp',
                        'smtp_host'=>"mail.techlabz.in",
                        'smtp_user'=>'no-reply@techlabz.in',
                        'smtp_pass'=>'k4$_a4%eD?Hi',
                        'smtp_port'=>'587'
                        ); 

                 $this->email->initialize($configs);
				
				$this->email->from('no-reply@techlabz.in', 'Reeba');
				$this->email->to('reebamary0@gmail.com');


				$this->email->subject('Email Test');
				$this->email->message('Testing the email class.');
				//print_r($this->email);die();

				 if($this->email->send()) {        
                  $res =  "EmailSend";
                  }
                  else
                  {
                  $res =  "error";
                  }
                  echo $this->email->print_debugger();
	 		}
	 		else if($check_result['status'] == 'not exist'){

	 			$res = array(
								"status"=> "error",
								"error"=> "Not Exist",
								"message"=> "Sorry! Email is not exist"
							);			

	 		}

	 	}
	 	print json_encode($res);

	}*/

/*............................................................................................*/

/*.............................. ... Home Details   ....................................... . */

	function home_details(){
		header('Content-type: application/json');

		$headers = apache_request_headers();
		//$data =  $_GET['auth_token']; 
		//print_r(apache_request_headers());  exit;

		if(isset($headers['Auth']) && strlen($headers['Auth'])){

			$check_result = $this->Webservice_model->get_auth_token($headers['Auth']);
			if($check_result['status'] == 'success'){
				$result = $this->Webservice_model->get_home_details($check_result['userdata']['id']);
				$res = array("status"=> "success",
								"data"=> array(
									"name" => decrypt_data($check_result['userdata']['name']),
									"profile_photo" => $check_result['userdata']['profile_photo'],
									"no_of_professionals"=> $result['doctor_count'],
									"no_of_clinics"=> $result['clinic_count'],
									"no_of_schedules" => $result['booking_count'],
									"no_of_moreservices" =>  $result['charity_count'],
									"no_of_notification" => $result['notification_count']
								)		
						);
			}
			else{
				$res = array(
								"status"=> "error",
								"error"=> "Not Exist",
								"message"=> "Authtoken is not exist"
							);			

			}
		}
		else{
			$res = array(
								"status"=> "error",
								"error"=> "required",
								"message"=> "Authtoken is required"
							);
		}
		print json_encode($res);
	}


/*............................................................................................*/

/*............................ Update User Location   ....................................... . */


	function location_update(){	
	header('Content-type: application/json');	
		$data =(array) json_decode(file_get_contents("php://input"));
		//$data= $_GET;
		$headers = apache_request_headers();
		
		if(isset($headers['Auth']) && strlen($headers['Auth'])){

			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			
			if($check_authToken){

				if(isset($data['location_name']) && strlen($data['location_name']) && isset($data['location_latitude']) && strlen($data['location_latitude']) && isset($data['location_longitude']) && strlen($data['location_longitude'])){
		
    				$result = $this->Webservice_model->location_update($data,$headers);
    				
    				if($result['status'] == 'success'){
    						$res = array(
    										"status" => "success",
    										"data" => array(
    														  "location_name"=>$data['location_name'],
    														  "location_latitude"=>$data['location_latitude'],
    														  "location_longitude"=>$data['location_longitude']
    														)
    									);
    				}
    				else if($result['status'] == 'fail'){
    					$res = array(
    									"status"=> "error",
    									"error"=> "Updation is failed",
    									"message"=> "Location Updation is Failed"
    								);
    				}
				}
				else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "location name or latitude or longitude is required"
						);
				}
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res = array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "Authtoken is required"
						);
		}
		print json_encode($res);
	}

/*...........................................................................................*/

/*.....................................  Find Location ..................................... */

	function location()
	{
		header('Content-type: application/json');
		$data = array();
		$headers = apache_request_headers();
		$postData = $_GET;

		
		if(isset($headers['Auth']) && strlen($headers['Auth'])){
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($postData['latitude']) && strlen($postData['latitude']) && isset($postData['longitude']) && strlen($postData['longitude'])){

						$result = $this->Webservice_model->get_closest_location($postData['latitude'],$postData['longitude']);
						if(empty($result)){
							$res = array(
										"status"=> "success",
										"data"=>array('location'=>[])
									);
						}
						else{
							foreach ($result as $key => $value) {
								
								$res = array(	"status"=> "success");

								$data1 =	array(
													//"location_id"=>$key,
													"location_name"=>$value['location_name'],
													"location_latitude"=>$value['location_lattitude'],
													"location_longitude"=>$value['location_longitude']
												);
													
											
											
								array_push($data,$data1);
							}
							$res['data']['location'] = array_values(array_unique($data,SORT_REGULAR));

						}				
				}
				else
				{
						$res = array(
										"status"=> "error",
										"error"=> "required",
										"message"=> "latitude or longitude is required"
									);
				}
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res = array(
									"status"=> "error",
									"error"=> "required",
									"message"=> "Authtoken is required"
								);
		}
		print json_encode($res);
	}

/*............................................................................................*/

/*............................... Update FCM Token   ........................................ */

	function update_fcm_token(){
		header('Content-type: application/json');

		//$data = $_POST;
		$headers =  apache_request_headers();
		$data =(array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])){
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($data['fcm_token']) && strlen($data['fcm_token'])){
					$result = $this->Webservice_model->update_fcm_token($data,$headers['Auth']);
					if($result['status'] == 'success'){
						$res = array("status"=>'success');
					}
					else if($result['status'] == 'fail'){
						$res = array(
										"status"=> "error",
										"error"=> "updation failed",
										"message"=> "updation failed"
									);
					}
				}else{
					$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "FCM token is Required"
					);
				}
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res = array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "Authtoken is required"
						);
		}
		print json_encode($res);
	}

/*...........................................................................................*/

/*................................. Doctor specialization   ................................ */


	function doctor_specialisation(){
		header('Content-type: application/json');
		$headers =  apache_request_headers();
		if(isset($headers['Auth'])){
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				$result_doctor_specialisation = $this->Webservice_model->get_doctor_specialisation();
				if($result_doctor_specialisation['status'] == 'success'){
					//print_r($result_doctor_specialisation['data']);
					$specialization = array();
					foreach ($result_doctor_specialisation['data'] as $key => $value) 
					{
							$specialisation_data = 	array(
								"specialization_id"=>$value['specialization_id'],
								"specialization_name" => $value['specialization_name']
								);							
							array_push($specialization, $specialisation_data);
					}
					$res = 	array(
						"status"=> "success",
						"data"=>array(
							"specialization"=>$specialization
							)
						);
				}
				else{

					$res =  array(
						"status"=> "error",
						"error"=> "No data",
						"message"=> "No data"
						);
				}
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res =  array(
					"status"=> "error",
					"error"=> "Required",
					"message"=> "Auth token is required"
					);
		}
		print json_encode($res);
	}

/*..........................................................................................*/

/*....................................... Doctor profile   ................................. */


	function doctor_profile(){
		header('Content-type: application/json');
		$headers =  apache_request_headers();

		//$data = (array) json_decode(file_get_contents("php://input"));
		$data = $_GET;
		//print_r($data);die();
		$value_photos = array();
		$value_phone = array();
		
		if(isset($headers['Auth'])){
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){	
				if(isset($data['doctor_id']) && strlen($data['doctor_id'])/* && isset($data['clinic_id']) && strlen($data['clinic_id'])*/){

					$result = $this->Webservice_model->get_doctor_profile($data['doctor_id']);
					if($result['status'] == 'success'){
						if($result['data']['online_status'] == 0){
							$online = false;
						}
						else if($result['data']['online_status'] == 1){
							$online = true;
						}
						$new[0] = $result['data']['telephone'];
						/*foreach (explode(',',$result['data']['phone']) as $key => $value) {
										$dd_phn = $value;
										 array_push($value_phone, $dd_phn);
									}*/
						foreach (explode(',',$result['data']['photos']) as $key => $value) {
										$dd_pic = $value;
										 array_push($value_photos, $dd_pic);
									}
						$res = array(
							"status" => "success",
							"data" => array(
								"doctor_id" => $result['data']['id'],
								"doctor_name" => $result['data']['name'],
								"doctor_specialization" => $result['data']['specialization_name'],
								"doctor_photo" => $result['data']['profile_pic'],
								"no_of_reviews" => $result['data']['reviewcount'],
								"online"=> $online,
								"fare"=> $result['data']['price'],

								"latitude" => $result['data']['location_lattitude'],
								"longitude" => $result['data']['location_longitude'],
								"about" => $result['data']['about'],
								"address" => $result['data']['street_address'].','.$result['data']['locality'].','.$result['data']['number'],

								"phone" => $new,
								"specialization" => explode(',',$result['data']['sub_name']),
								"photos" =>$value_photos
								
								)
							);
					}
					else{
						$res =  array(
									"status"=> "error",
									"error"=> "No data",
									"message"=> "No data"
								);
					}
				}
				else
				{
					$res =  array(
								"status"=> "error",
								"error"=> "required",
								"message"=> "Doctor id or clinic id is required"
							);
				}
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res =  array(
					"status"=> "error",
					"error"=> "Required",
					"message"=> "Auth token is required"
					);
		}
		print json_encode($res);
	}


/*............................................................................................*/

/*....................................  Filter Others  .....................................  */

	function filter_others(){
		header('Content-type: application/json');


		$headers =  apache_request_headers();
		if(isset($headers['Auth'])){
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				$filterData = filteredData();
				$res = array("status" => "success",
					"data" => array("others" => $filterData
						)
					);
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res =  array(
					"status"=> "error",
					"error"=> "Required",
					"message"=> "Auth token is required"
					);
		}

		
		print json_encode($res);
	}
/*...........................................................................................*/

/*................................  List Reviews  ........................................  */


	function review_list(){
		header('Content-type: application/json');
		//$data = (array) json_decode(file_get_contents("php://input"));
		$data = $_GET;
		$headers =  apache_request_headers();
		$per_page = 20;
		if(isset($headers['Auth'])){
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){	
				if((isset($data['doctor_id']) && strlen($data['doctor_id']))|| (isset($data['clinic_id']) && strlen($data['clinic_id']))){

					if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
						$currentpage =  (int)$_GET['page'];
						$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
						$start = $currentpage * $per_page;
						$page_no = $_GET['page'];
					}
					else{
						$currentpage = 0;
						$start = 0;
						$page_no = 0;
					}	
					if(isset($data['doctor_id']) && strlen($data['doctor_id'])){

						$result = $this->Webservice_model->review_list($data['doctor_id'],$start,$per_page);
					}
					else if(isset($data['clinic_id']) && strlen($data['clinic_id'])){
						$result = $this->Webservice_model->review_list_clinic($data['clinic_id'],$start,$per_page);
					}

					$reviews = array();
					if($result != false){
						$total = count($result);	
					}else{
						$total = 0;
					}
					if($total >= $per_page){

						$totalPages = $total / $per_page;
					}
					else{
						$totalPages = $total;
					}
					if($result != ''){
						foreach (array_slice($result,$start,$per_page) as $key => $value) {
							$pushData = array('id'=>$value['review_id'],
								'name' => $value['name'],
								'date' => floatval($value['date']),
								'review' => $value['review'],
								'profile_photo' => $value['profile_photo']);
							array_push($reviews, $pushData);

						}
					}
					
					if($result != false){	

						$res = array('status' => 'success',
							'data' => array(
								'reviews' => $reviews
								),
							'meta' => array('total_pages' => $totalPages,
								'total' => $total,
								'current_page' =>$currentpage+1,
								'per_page' => $per_page)
							);
					}
					else{
						$res = array(
								"status"=> "success",
								'data' => array(
										'reviews' =>[]
								),
								'meta' => array('total_pages' => $totalPages,
								'total' => $total,
								'current_page' =>$currentpage+1,
								'per_page' => $per_page)
							);
					}
				}
				else{
						$res = array(
								"status"=> "error",
								"error"=> "required",
								"message"=> "Doctor id is required"
							);
				}
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res =  array(
					"status"=> "error",
					"error"=> "Required",
					"message"=> "Auth token is required"
					);
		}
		print json_encode($res);

	}

/*...........................................................................................*/

/*.....................................  Doctor Booking  ...................................*/


	function doctor_booking(){
		header('Content-type: application/json');
		//$data = $_POST;
		$headers =  apache_request_headers();
		$data = (array) json_decode(file_get_contents("php://input"));
        //print_r($data);exit();
		if(isset($headers['Auth']) && strlen($headers['Auth'])){
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($data['doctor_id']) && strlen($data['doctor_id']) && isset($data['clinic_id']) && strlen($data['clinic_id']) && isset($data['date']) && strlen($data['date']) && isset($data['time']) && strlen($data['time']) && isset($data['amount']) && strlen($data['amount']) /*&& isset($data['booking_status']) && strlen($data['booking_status'])*/&& isset($data['payment_mode']) && strlen($data['payment_mode'])) {

					$result = $this->Webservice_model->doctor_booking($data,$headers['Auth']);
					if($result['status'] == 'success'){
						$res = array('status' => 'success','data' => $result['data']);
					}
					else if($result['status'] == 'fail'){
						$res = array(
								"status"=> "error",
								"error"=> "insertion error",
								"message"=> $result['msg']
							);
					}
					else if($result['status'] == 'failed'){
						$res = array(
								"status"=> "success",
								"data"=> $result['data']
							);
					}
				}
				else{
					$res = array(
								"status"=> "error",
								"error"=> "required",
								"message"=> "All Field is required"
							);
				}
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res =  array(
					"status"=> "error",
					"error"=> "Required",
					"message"=> "Auth token is required"
					);
		}
		print json_encode($res);
	}


/*............................................................................................*/

/*........................................ clinic specialization ............................*/


	function clinic_specialisation(){
		header('Content-type: application/json');

		//$data = (array) json_decode(file_get_contents('php://input'));
		$data = $_POST;
		$headers =  apache_request_headers();
		if(isset($headers['Auth'])){
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
		
				$result_clinic_specialisation = $this->Webservice_model->get_clinic_specialisation();
				if($result_clinic_specialisation['status'] == 'success'){					
					$specialization = array();
					foreach ($result_clinic_specialisation['data'] as $key => $value) 
					{
							$specialisation_data = 	array(
								"specialization_id"=>$value['id'],
								"specialization_name" => $value['specialization_name']
								);							
							array_push($specialization, $specialisation_data);
					}
					$res = 	array(
						"status"=> "success",
						"data"=>array(
							"specialization"=>$specialization
							)
						);
				}
				else{

					$res =  array(
						"status"=> "error",
						"error"=> "No data",
						"message"=> "No data"
						);
				}
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res =  array(
					"status"=> "error",
					"error"=> "Required",
					"message"=> "Auth token is required"
					);
		}
		print json_encode($res);



	}
/*.........................................................................................*/

/*..................................... Search doctor ......................................*/

 	function search_doctor(){
 		header('Content-type: application/json');
 		// $data = json_decode(file_get_contents('php://input'));
 		$headers =  apache_request_headers();
 		$data = $_GET;
 		
		$per_page = 20;
		if(isset($headers['Auth'])){
		 	$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){	
		 		/*if((isset($data['query']) && strlen($data['query'])) || (isset($data['speciality']) && strlen($data['speciality'])) || (isset($data['clinic_id']) && strlen($data['clinic_id'])) || (isset($data['date']) && strlen($data['date'])) || (isset($data['others']) && strlen($data['others'])) || (isset($data['location_name']) && strlen($data['location_name']) && isset($data['location_latitude']) && strlen($data['location_latitude']) && isset($data['location_longitude']) && strlen($data['location_longitude'])))
		 		{*/
		 		if(!empty($data)){
		 			if(isset($data['page']) && strlen($data['page'])){
						$currentpage =  $data['page'];
						$start = ($currentpage * 10) + (($currentpage - 2)*10);
					}
					else{
						$currentpage = 1;
						$start = 0;
					}
					$allDoctors = array(); 
			 		$search_query_result = $this->search_doctor_query($data,$headers);
			 		if($search_query_result['status'] == 'success'){
			 			foreach ($search_query_result['data'] as $key => $value) {
			 				if($value['online'] == 0){
			 					
			 					$search_query_result['data'][$key]['online'] = false;
			 				}
			 				else if($value['online'] == 1){
			 					
			 					$search_query_result['data'][$key]['online'] = true;
			 				}
			 			
			 			}
				 			$doctorsArray = array();
							$total = count($search_query_result['data']);
							if($total >= $per_page){

								$totalPages = (int)($total / $per_page);
							}
							else{
								$totalPages = 1;
							}
							
							foreach (array_slice($search_query_result['data'],$start,$per_page) as $key => $value) {
								
								array_push($doctorsArray, $value);

							}
				 		$res = array('status' => 'success',
								'data' => array(
									'doctor' => $doctorsArray
									),
								'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage,
									'per_page' => $per_page)
								);
			 		}	 				 			
			 		else if($search_query_result['status'] == 'fail'){
			 			$res =  array(
									"status"=> "success",
									"data"=> array('doctor'=>[])
									//"message"=> "No data"
								);
			 		}
			 		else if($search_query_result == null){
			 			$res =  array(
									"status"=> "error",
									"error"=> "Can't fetch your Location",
									"message"=> "Location error"
								);
			 		}

			 	}
		 		else{

		 			$res =  array(
						"status"=> "success",
						"data"=> array('doctor'=>[])
						);
		 		}
		 	}
		 	else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
	 	}
	 	else{
	 		$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
					);
	 	}
 		print json_encode($res);
 	}


 	function search_doctor_query($data,$headers){
 		
 		$get_location = $this->Webservice_model->get_location($headers['Auth']);
 		if($get_location['status'] == 'success'){
			//print_r($get_location);
	 		$result = $this->Webservice_model->search_doctor_query($data,$get_location['data']['location_latitude'],$get_location['data']['location_longitude']);

 		}
 		else{
 			$result = null;
 		}


 		return $result;

 	}
 	
 /*...................................................................................*/

/*..................................... Search clinic .......................................*/

 	function search_clinic(){
 		header('Content-type: application/json');

 		// $data = json_decode(file_get_contents('php://input'));
 		$headers =  apache_request_headers();
 		$data = $_GET;
 		$res =array('status'=>'success');

		if(isset($headers['Auth']) && strlen($headers['Auth'])){

			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){

		 		if((isset($data['query']) && strlen($data['query'])) || (isset($data['speciality']) && strlen($data['speciality'])) || (isset($data['date']) && strlen($data['date'])) || (isset($data['others']) && strlen($data['others'])) || (isset($data['location_name']) && strlen($data['location_name']) && isset($data['location_latitude']) && strlen($data['location_latitude']) && isset($data['location_longitude']) && strlen($data['location_longitude'])))
		 		{
	 				$res['data']['clinic'] = array();


			 		$search_query_result = $this->search_clinic_query($data,$headers);
			 			if($search_query_result['status'] == 'success'){	
					 			foreach ($search_query_result['data'] as $key => $value) {
			 				
			 						array_push($res['data']['clinic'], $value);
			 					}
			 				
					 			$res['data']['no_of_clinics'] = count($res['data']['clinic']);
			 			}
					 	else if($search_query_result['status'] == 'fail'){
					 		$res =  array(
										"status"=> "success",
										"data" =>array('clinic'=>[],'no_of_clinics'=>'0')
									);
					 	}			 						 		
			 	}
		 		else{

		 			$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Atleast one parameter is required"
						);
		 		}
		 		
		 		//$res['data']['clinic'] = array_unique($res['data']['clinic'],SORT_REGULAR); // for contain unique data in array
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}	 		
	 	}
	 	else{
	 		$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
					);
	 	}

 		print json_encode($res);
 	}


 	function search_clinic_query($data,$headers){
 		
 		$get_location = $this->Webservice_model->get_location($headers['Auth']);
 		if($get_location['status'] == 'success'){

	 		$result = $this->Webservice_model->search_clinic_query($data,$get_location['data']['location_latitude'],$get_location['data']['location_longitude']);

 		}
 		else{
 			$result = null;
 		}

 		return $result;

 	}
 	
 	function search_clinic_lowervalue($data,$headers,$field){
 		
 		$get_location = $this->Webservice_model->get_location($headers['Auth']);
 		if($get_location['status'] == 'success'){

	 		$result = $this->Webservice_model->search_clinic_lowervalue($get_location['data']['location_latitude'],$get_location['data']['location_longitude'],$field);

 		}
 		else{
 			$result = null;
 		}
 		return $result;

 	}



/*......................................*******..............................................*/
/*............................. Doctors Availability....................................*/

	function doctor_availability(){

        header('Content-type: application/json');
		$headers =  apache_request_headers();
		$data = $_GET;
/*$value = "05:30";
$time = explode(':',$value);
$ms = $time[0]*60000 + $time[1]*1000;

print_r($ms);echo "<br>";
print_r(date('H:i',$ms));exit();*/

		/*$time = 19800;
		print_r(date('H:i:sT',$time));die();*/

		if(isset($headers['Auth']) && strlen($headers['Auth'])){		

			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($data['doctor_id']) && strlen($data['doctor_id']) && isset($data['clinic_id']) && strlen($data['clinic_id']) && isset($data['date']) && strlen($data['date']) && isset($data['offset']) && strlen($data['offset'])){
					$result_availability = $this->Webservice_model->doctor_availability($data);
					if($result_availability['data']['active_schedule'] == '0'){
						$schedule = $result_availability['data']['date'];
					}else{
						$schedule = $result_availability['data']['date_secondary'];
					}
					if($result_availability['status'] == 'success' && ($schedule!= '""' )){
						$day = date('D',$data['date']);					
                      
						$res = array('status' => 'success',
							'data' => array('time_interval' =>array()));
						$schedule = json_decode($schedule,true);
						$time = $data['offset'];
                        $new_time = date('H:i',$time);
						//print_r($time);exit();
                        $new_time = explode(':',$new_time);
				        $nowin_server = date("Y-m-d TH:i:s");
				        if($data['offset'] > 0){
                            $nowin_server_addoffset = date('Y-m-d H:i:s',strtotime('+'.$new_time[0].' hour +'.$new_time[1].' minutes',strtotime($nowin_server)));
				        }else{
				            $nowin_server_addoffset = date('Y-m-d H:i:s',strtotime('-'.$new_time[0].' hour -'.$new_time[1].' minutes',strtotime($nowin_server)));
				        }

						foreach ($schedule as $key => $value) {
							
							if($value['day'] == strtolower($day)){							
								$start = date('Y-m-d',$data['date']).' '.$value['time']['start'];
					            $end = date('Y-m-d',$data['date']).' '.$value['time']['end'];
								$interval_time = $result_availability['data']['duration']*60;
								$start_time = strtotime($start);
								$end_time =  strtotime($end);
								$break_from = strtotime($value['time']['break_from']);
								$break_to = strtotime($value['time']['break_to']);
						
								for ($i=$start_time; $i <= $end_time; $i=$i+$interval_time) { 
									
									$initial = $i;
									$end = $i+$interval_time;
									if(isset($value['time']['break_from']) && isset($value['time']['break_to']) && ($value['time']['break_from'] != 'null' ) && ($value['time']['break_to'] != 'null') && strlen($value['time']['break_from']) && strlen($value['time']['break_to'])){
									 
										if(!((($initial < strtotime($value['time']['break_from'])) &&($end > strtotime($value['time']['break_from']))) || (($initial < strtotime($value['time']['break_to'])) &&($end > strtotime($value['time']['break_to']))))){
											if($end <= $end_time && ($initial > strtotime($nowin_server_addoffset))){
												$time = date('h:i a',$initial).' - '.date('h:i a',$end);
												$result = $this->Webservice_model->is_waiting_list_used($data,$time);
												if($result == false || count($result) <= 1){
													array_push($res['data']['time_interval'], array('time'=>date('h:i a',$initial).' - '.date('h:i a',$end)));
												}
											}
										}
									}else{
									    if($end <= $end_time && ($initial > strtotime($nowin_server_addoffset))){
											$time = date('h:i a',$initial).' - '.date('h:i a',$end);
											$result = $this->Webservice_model->is_waiting_list_used($data,$time);
											if($result == false || count($result) <= 1){
												array_push($res['data']['time_interval'], array('time'=>date('h:i a',$initial).' - '.date('h:i a',$end)));
											}
										}
									}
								}
							}
						}						
						$res['data']['time_interval'] = array_values(array_unique($res['data']['time_interval'],SORT_REGULAR));
					}else{
						$res =  array(
						"status"=> "success",
						"data"=>array('time_interval'=>[])
						);
					}
				}
				else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Doctor id or clinic id is required"
						);
				}
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
					);
		}
		print json_encode($res);
	}
/*...........................................................................................*/
/*.................................... Clinic Profile ..................................*/

	function clinic_profile(){
        header('Content-type: application/json');
		$data = $_GET;
		$headers = apache_request_headers();

		if(isset($headers['Auth']) && strlen($headers['Auth'])){		

			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if (isset($data['clinic_id']) && strlen($data['clinic_id'])) {
					
					$result_data = $this->Webservice_model->clinic_profile($data['clinic_id']);
					if($result_data['status'] == 'success'){						
						$result_data['data']['address'] = $result_data['data']['cep'].','.$result_data['data']['street_address'].','.$result_data['data']['locality'];
						unset($result_data['data']['cep']);
						unset($result_data['data']['street_address']);
						unset($result_data['data']['locality']);
						unset($result_data['data']['number']);
						$res = array("status" => "success",
									"data"=>$result_data['data']);
					}
					else if($result_data['status'] == 'fail'){
						$res =  array(
										"status"=> "error",
										"error"=> "No data",
										"message"=> "No data"
									);
					}					
				}
				else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "clinic id is required"
						);
				}
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
					);
		}
		print json_encode($res);
	}
/*............................................................................................*/
/*.................................. Doctor available dates ..................................*/


	/*function available_dates(){
		header('Content-type: application/json');

		$data = $_GET;
		$headers = apache_request_headers();
		
		if(isset($headers['Auth']) && strlen($headers['Auth'])){		

			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($data['doctor_id']) && strlen($data['doctor_id']) && isset($data['month']) && strlen($data['month']) && isset($data['year']) && strlen($data['year']) && isset($data['clinic_id']) && strlen($data['clinic_id'])){
					$result_booked = $this->Webservice_model->get_doctor_dates($data);					
					if($result_booked['status'] == 'success'){
						$res = array("status" => "success",
							"data" => array("booked"=>array(),"not_available" =>array()));						
						foreach ($result_booked['data'] as $key => $value) {
							if(!in_array( date('j',$value['date']), $res['data']['booked'], true)){
								array_push($res['data']['booked'], date('j',$value['date']));
							}
							if(!is_null($value['start_date']) && !is_null($value['end_date'])){

								if(date('F',$value['start_date']) == $data['month']){

									array_push($res['data']['not_available'], date('j',$value['date']));
								}
								if(date('F',$value['end_date']) == $data['month']){
									array_push($res['data']['not_available'], date('j',$value['date']));
								}
							}							
						}
					}
					else if($result_booked['status'] == 'fail'){
						$res =  array(
										"status"=> "error",
										"error"=> "No data",
										"message"=> "No data"
									);
					}					
				}
				else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "doctor id or month or year or time_interval is required"
						);
				}
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
					);
		}
		print json_encode($res);
	}
*/



	function available_dates(){
		header('Content-type: application/json');

		$data = $_GET;
		$headers = apache_request_headers();
		
		if(isset($headers['Auth']) && strlen($headers['Auth'])){		
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($data['doctor_id']) && strlen($data['doctor_id']) && isset($data['month']) && strlen($data['month']) && isset($data['year']) && strlen($data['year']) && isset($data['clinic_id']) && strlen($data['clinic_id'])){
					$result_booked = $this->Webservice_model->get_doctor_dates($data);	
					//print_r($result_booked);exit();
					if($result_booked['status'] == 'success'){
						$res = array("status" => "success",
							"data" => array("available"=>array(),"not_available" =>array()));		
							$db_day_array = array();	
							//print_r($result_booked['data']);	
							if($result_booked['data'] != '""'){
								$schedule_data = json_decode($result_booked['data']);
								foreach ($schedule_data as $key => $value) {
									$db_day_array[] = $value->day;
								}
							}else{
								$db_day_array[] = [];
							}
						$time = strtotime($data['month']. $data['year']);
						//print_r($time);exit();
						$search_form = date('Y-m-01',$time);
				 		$search_to =  date('Y-m-t',$time);
						$req_month_number = date('m',strtotime($data['month']));
						$tod_month_number = date('m');
						$month_today = date('F');
				 		$year_today = date('Y');
						$date_today = date('Y-m-d');
						
				 		for($i = $search_form; $i <= $search_to ; $i++){
							if((($tod_month_number <= $req_month_number)&& ($year_today == $data['year'])) || ((($req_month_number <= $tod_month_number) || ($req_month_number >= $tod_month_number))&& ($year_today <= $data['year']))){
								$day = strtolower(date('D',strtotime($i)));
								//print_r($day);
								if(in_array($day, $db_day_array)){
									if(count($result_booked['leave']) > 0){
										foreach ($result_booked['leave'] as $key => $value) {
											if((date('j',$value->start_date) <= date('j',strtotime($i))) && (date('j',$value->end_date) >= date('j',strtotime($i))) &&(date('F',$value->start_date) == $data['month']) && (date('F',$value->end_date) == $data['month']) && (date('Y',$value->start_date) == $data['year']) && (date('Y',$value->end_date) == $data['year'])) {
												array_push($res['data']['not_available'], date('j',strtotime($i)));
											}else{
												//array_push($res['data']['available'], date('j',strtotime($i)));
												if(strtotime($i) < strtotime($date_today)){
													array_push($res['data']['not_available'], date('j',strtotime($i)));
												}else{
													array_push($res['data']['available'], date('j',strtotime($i)));
												}
											}
										}
									}else{
										//array_push($res['data']['available'], date('j',strtotime($i)));
										if(strtotime($i) < strtotime($date_today)){
											array_push($res['data']['not_available'], date('j',strtotime($i)));
										}else{
											array_push($res['data']['available'], date('j',strtotime($i)));
										}
									}
								}else{
									array_push($res['data']['not_available'], date('j',strtotime($i)));
								}
							}else{
					 			array_push($res['data']['not_available'], date('j',strtotime($i)));
					 		}
				 		}	
					}
					else if($result_booked['status'] == 'fail'){
						$res =  array(
										"status"=> "error",
										"error"=> "Not available",
										"message"=> "Doctor is not available in that clinic"
									);
					}					
				}
				else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "doctor id or month or year or clinic id is required"
						);
				}
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
					);
		}
		print json_encode($res);
	}
/*..........................................................................................*/
/*....................................... booking list.......................................*/
	
    
	function booking_list(){
    	header('Content-type: application/json');

		$data = $_GET;
		$headers = apache_request_headers();
        

		if(isset($headers['Auth']) && strlen($headers['Auth'])){
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				$result_booking_list = $this->Webservice_model->get_booking_list($headers['Auth']);
				if($result_booking_list['status'] == 'success'){
					$res = array("status"=>'success',
						'data'=>array('booking_list'=>$result_booking_list['data']));
				}
				else{
				    	$res =  array(
					"status"=> "success",
					//"error"=> "No Data",
					"data"=> array('booking_list'=>[])
					);
				}
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
					);
		}
		print json_encode($res);
	}
/***************************jensa**********************************************/
/*..........................................................................................*/
/*....................................... doctor review.......................................*/
	public function doctor_review(){
		header('Content-type: application/json');

		$headers = apache_request_headers();
		$data = (array) json_decode(file_get_contents("php://input"));
		if((isset($headers['Auth'])) && strlen($headers['Auth'])){
        	$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if((isset($data['doctor_id'])) && strlen($data['doctor_id']) && isset($data['review']) && strlen($data['review']) && isset($data['medical_record_id']) && strlen($data['medical_record_id'])){
					$result = $this->Webservice_model->doctor_review($data,$headers['Auth']);
					if($result['status'] == 'success'){
						$res = array("status"=> "success");
					}else{
						$res = array(
							"status"=> "error",
							"error"=>"insertion error",
							"message"=>$result['msg']
						);
					}
				}else{
					$res = array(
								"status"=>"error",
								"error"=>"required",
								"message"=>"doctor id or review required"
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
        print json_encode($res);
	}

	/*........................................................................................*/
/*....................................... change booking date.................................*/

	/*public function change_date(){
		header('Content-type: application/json');
		$headers = apache_request_headers();
		$data = (array) json_decode(file_get_contents("php://input"));

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
        	$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if((isset($data['booking_id'])) && strlen(trim($data['booking_id']," ")) > 0 && isset($data['date']) && strlen(trim($data['date']," ")) > 0){
					$result = $this->Webservice_model->update_bookingdate($data);
					if($result == false){
						$res = array(
							"status"=>"success",
							"error"=>"Updation Failed",
							"message"=> "Updation Failed"
						);
					}else{
						$res = array(
							"status"=>"success",
							"data"=>$result
						);
					}

				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "booking id or date is required"
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		 print json_encode($res);
	}*/
/*..........................................................................................*/
/*.......................................add patient dependent................................*/

	public function add_dependent(){
		header('Content-type: application/json');
		$headers = apache_request_headers();
		//$data = (array) json_decode(file_get_contents("php://input"));
		$data = $_POST;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if((isset($data['dependent_name'])) && strlen(trim($data['dependent_name']," ")) > 0 && isset($data['dependent_relation']) && strlen(trim($data['dependent_relation']," ")) > 0 && isset($data['dependent_dob']) && strlen(trim($data['dependent_dob']," ")) > 0 && (isset($_FILES['dependent_image']) && isset($data['dependent_cpf']) && strlen(trim($data['dependent_cpf']," ")) > 0)){
						// $dob = date('Y-m-d', strtotime($data['dependent_age'] . ' years ago'));
						// $data['dob'] = strtotime($dob);
						$ress = $this->cpf_valid($data['dependent_cpf']);
						if($ress == '1'){
							$auth_result = $this->Webservice_model->get_userid_frm_authtoken($headers['Auth']);
							$fileName =$auth_result->userid.'_'.$_FILES['dependent_image']['name'];
							$fileName = str_replace('%','a',$fileName);
							$config = set_upload_options('../assets/uploads/profilepic/patient_dependent/'); 
				         	$config['file_name'] = $fileName;
				         	$this->load->library('upload', $config);
					        if ( ! $this->upload->do_upload('dependent_image')) {
					            $error = array('error' => $this->upload->display_errors('', '')); 
					            $res = array(
										"status"=> "error",
										"error"=> "Upload Error",
										"message"=> "Sorry! Profile Photo not uploaded".$error['error']
									);
					        }	
					        else { 
					            $imagedata = $this->upload->data(); 
					            $data['image']='assets/uploads/profilepic/patient_dependent/'.$imagedata['file_name'];
					        }
	                        $result = $this->Webservice_model->add_dependent($auth_result->userid,$data);
	                        if($result == false){
	                        	$res =  array(
										"status"=> "error",
										"error"=> "No data",
										"message"=> "No data"
									);
	                        }else{
	                        	$new_data = array();
	                        	foreach ($result as $key => $value) {
	                        		//$dob=date('Y-m-d',$value->dob);
							    	//$diff = (date('Y') - date('Y',strtotime($dob)));
							    	//print_r($diff);exit();
							    	$new_data[$key]['dependent_id'] =$value->id;
							    	$new_data[$key]['dependent_name'] =$value->dependent_name;
							    	$new_data[$key]['dependent_relation'] =$value->relation;
							    	$new_data[$key]['dependent_image'] =$value->image;
							    	$new_data[$key]['dependent_cpf'] =$value->cpf;
							    	$new_data[$key]['age'] =$value->dob;
	                        	}
	                        	$res = array(
	                        				'status'=>'success',
	                        				'data' =>array('dependent_list'=>$new_data)
		                        		);
	                    	}
	                    }else{
	                    	$res = array(
								"status"=> "error",
								"error"=> "error",
								"message"=> "Invalid CPF"
							);
	                    }
				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "All field is required"
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		 print json_encode($res);
	}

/*..........................................................................................*/
/*.............................. get patient dependent list.................................*/

	public function dependent_list(){
		header('Content-type: application/json');
		$headers = apache_request_headers();
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
            	$result = $this->Webservice_model->get_dependent_list_users($headers['Auth']);
            	if($result['status'] == 'success'){
            		$res =  array(
						"status"=> "success",
						"data"=> array('dependent_list'=>$result['data'])
						
					);
            	}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

	}

//......................edit bystander details of patient

	public function edit_dependent(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = $_POST;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if((isset($data['dependent_name'])) && strlen(trim($data['dependent_name']," ")) > 0 && isset($data['dependent_relation']) && strlen(trim($data['dependent_relation']," ")) > 0 && isset($data['dependent_age']) && strlen(trim($data['dependent_age']," ")) > 0 && (isset($_FILES['dependent_image']) && isset($data['dependent_id']) && strlen(trim($data['dependent_id']," ")) > 0) && isset($data['dependent_cpf']) && strlen(trim($data['dependent_cpf']," ")) > 0){
					$ress = $this->cpf_valid($data['dependent_cpf']);
					if($ress == '1'){
						$auth_result = $this->Webservice_model->get_userid_frm_authtoken($headers['Auth']);
						$fileName = $auth_result->userid.'_'.$_FILES['dependent_image']['name'];
						$config = set_upload_options('../assets/uploads/profilepic/patient_dependent/'); 
			         	$config['file_name'] = $fileName;
			         	$this->load->library('upload', $config);
				        if ( ! $this->upload->do_upload('dependent_image')) {
				            $error = array('error' => $this->upload->display_errors('', '')); 
				            $res = array(
									"status"=> "error",
									"error"=> "Upload Error",
									"message"=> "Sorry! Profile Photo not uploaded".$error['error']
								);	
				        }	
				        else { 
				         	
				            $imagedata = $this->upload->data(); 
				            $data['image'] ='assets/uploads/profilepic/patient_dependent/'.$imagedata['file_name'];   
						}
						$result = $this->Webservice_model->edit_dependent($data,$auth_result->userid);
						if($result == false){
		                	$res =  array(
									"status"=> "error",
									"error"=> "No data",
									"message"=> "No data"
								);
		                }else{
		                	$new_data = array();
		                	foreach ($result as $key => $value) {
						    	$new_data[$key]['dependent_id'] =$value->id;
						    	$new_data[$key]['dependent_name'] =$value->dependent_name;
						    	$new_data[$key]['dependent_relation'] =$value->relation;
						    	$new_data[$key]['dependent_image'] =$value->image;
						    	$new_data[$key]['dependent_cpf'] =$value->cpf;
						    	$new_data[$key]['age'] =$value->dob;
		                	}
		                	$res = array(
		                				'status'=>'success',
		                				'data' =>array('dependent_list'=>$new_data)
		                    		);
		            	}
		            }else{
		            	$res = array(
								"status"=> "error",
								"error"=> "error",
								"message"=> "Invalid CPF"
							);
		            }
				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "All field is required"
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

/*..........................................................................................*/
/*.................................Get upcomming consultation................................*/

	public function upcoming_consultation(){

		$id = $_GET['booking_id'];
		$headers = apache_request_headers();

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($id) && strlen(trim($id," ")) > 0){
					$result = $this->Webservice_model->get_upcoming_consultation($id);
					if($result['status'] == 'success'){
						$res = array(
							"status"=> "success",
							"data"=> $result['data']
						);
					}else{
						$res =  array(
									"status"=> "error",
									"error"=> "No data",
									"message"=> "No data"
								);
					}

				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Booking Id is required"
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}

		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

/*..........................................................................................*/
/*.......................................change user password................................*/

	public function change_password(){
		header('Content-type: application/json');
		$headers = apache_request_headers();
		$data = (array) json_decode(file_get_contents("php://input"));

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($data['current_password']) && strlen(trim($data['current_password']," ")) > 0 && isset($data['new_password']) && strlen(trim($data['new_password']," ")) > 0){
					$auth_result = $this->Webservice_model->get_userid_frm_authtoken($headers['Auth']);
					$result = $this->Webservice_model->change_patient_password($auth_result->userid,$data);
					if($result['status'] == 'success'){
						$res =  array(
							"status"=> "success"
						);
					}else{
						$res =  array(
							"status"=> "error",
							"error" => "Not Updated",
							"message" => $result['msg']
						);
					}
				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Current Password and New Password is required"
					);
				}

			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}

		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

/*..........................................................................................*/
/*.......................................get promocode list................................*/

	public function get_promocode_list(){
		$headers = apache_request_headers();

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				$result = $this->Webservice_model->get_promocode_list();
				if($result['status'] == 'success'){
					$res =  array(
							"status"=> "success",
							"data" => array('promocodes'=>$result['data'])
						);
				}else{
					$res =  array(
							"status"=> "success",
							"data" => array('promocodes'=>[])
						);
				}
               
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

	}

/*..........................................................................................*/
/*..................................cancel booking by patient................................*/

	public function cancel_booking(){
		header('Content-type: application/json');
		$headers = apache_request_headers();
		$data = (array) json_decode(file_get_contents("php://input"));

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($data['booking_id']) && strlen(trim($data['booking_id']," ")) > 0 && isset($data['offset']) && strlen(trim($data['offset']," ")) > 0){
					$result = $this->Webservice_model->cancel_booking($data);
					if($result['status'] == 'success'){
						$res =  array(
							"status"=> "success"
					
						);
					}else{
						$res =  array(
							"status"=> "error",
					        "error" =>"error",
					        "message"=>$result['message']
						);
					}
				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Booking Id is required"
					);
				}

			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

/*..........................................................................................*/
/*..................................get promocode hospital list.............................*/

	public function promocode_hospital_list(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$id = $_GET['promocode_id'];
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($id) && strlen(trim($id," ")) > 0){
					$promo_distance_result = $this->promo_distance_query($id,$headers);
				//	echo"<pre>"; print_r($promo_distance_result); echo "</pre>";exit();
					if($promo_distance_result['status'] == 'success'){
						$res =  array(
							"status"=> "success",
							"data"=> array('doctor_list'=>$promo_distance_result['data'])
						);
					}else{
						$res =  array(
							"status"=> "success",
							"data"=> array('doctor_list'=>[])
						);
					}
				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Promocode Id is required"
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}


	public function promo_distance_query($id,$headers){
				$get_location = $this->Webservice_model->get_location($headers['Auth']);
 		if($get_location['status'] == 'success'){

	 		$result = $this->Webservice_model->get_promocode_hospital_list($id,$get_location['data']['location_latitude'],$get_location['data']['location_longitude']);

 		}
 		else{
 			$result = null;
 		}


 		return $result;
	}

/*..........................................................................................*/
/*......................................set push status.....................................*/

	public function set_push_status(){
		header('Content-type: application/json');
		$headers = apache_request_headers();
		$data = (array) json_decode(file_get_contents("php://input"));

		//$data = $_POST;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($data['status']) && strlen(trim($data['status']," ")) > 0){
					$auth_result = $this->Webservice_model->get_userid_frm_authtoken($headers['Auth']);
					$result = $this->Webservice_model->set_push_status($auth_result->userid,$data['status']);
					if($result['status'] == 'success'){
						$res =  array(
						"status"=> "success",
						"data"=> array("status"=>$data['status'])
						);
					}else{
						$res =  array(
						"status"=> "error",
						"error"=> "Not Updated",
						"message"=> "Status not updated"
					);
					}

				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Status Id is required"
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

/*..........................................................................................*/
/*.......................................get push status....................................*/

	public function get_push_status(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				$auth_result = $this->Webservice_model->get_userid_frm_authtoken($headers['Auth']);
				$result = $this->Webservice_model->get_push_status($auth_result->userid);
				if($result['status'] == 'success'){
					$res =  array(
						"status"=> "success",
						"data"=> $result['data']
					);
				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "Not Exist",
						"message"=> "User Not Exist"
					);
				}

			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

/*..........................................................................................*/
/*.......................................App Feedback........................................*/

	public function app_feedback(){
		header('Content-type: application/json');
		$headers = apache_request_headers();
		$data = (array) json_decode(file_get_contents("php://input"));

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($data['feedback_type']) && strlen(trim($data['feedback_type']," ")) > 0 && isset($data['feedback_content']) && strlen(trim($data['feedback_content']," ")) > 0){
					$auth_result = $this->Webservice_model->get_userid_frm_authtoken($headers['Auth']);
					$data['user_id'] = $auth_result->userid;
					$result = $this->Webservice_model->insert_app_feedback($data);
					if($result['status'] == 'success'){
						$res =  array(
							"status"=> "success"
						);
					}else{
						$res =  array(
							"status"=> "error",
							"error"=> "Not Inserted",
							"message"=> "Insertion Error"
						);
					}
				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Feedback type and Feedback Content is required"
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

/*..........................................................................................*/
/*.......................................FAQ List........................................*/

	public function faq(){
		header('Content-type: application/json');
		$headers = apache_request_headers();
		//$data = (array) json_decode(file_get_contents("php://input"));
		//$page = $_GET['page'];
		$per_page=20;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
					$currentpage =  (int)$_GET['page'];
					$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
					$start = $currentpage * $per_page;
					$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}

				
				$result = $this->Webservice_model->get_faqs($start,$per_page);
				$faqArray = array();
				if($result != false){
					$total = count($result);
				}else{
					$total = 0;
				}
				
				if($total >= $per_page){

					$totalPages = (int)($total / $per_page);
				}
				else{
					$totalPages = 1;
				}
				if($result != ''){
					foreach (array_slice($result,$start,$per_page) as $key => $value) {
						
						array_push($faqArray, $value);

					}
				}

				if($result != false){
					$res = array('status' => 'success',
								'data' => array(
									'faq_list' => $faqArray
									),
								'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
								);

				}else{
					$res = array(
						"status"=>"success",
						"data"=>array('faq_list'=>[]),
						'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

/*..........................................................................................*/
/*.......................................Medical Records........................................*/

	public function completed_consultation(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$booking_id = $_GET['booking_id'];
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($booking_id) && strlen(trim($booking_id," ")) > 0 ){
					$result = $this->Webservice_model->get_medical_record($booking_id);
					if($result['status'] == 'success'){
						$res =  array(
							"status"=> "success",
							"data"=>$result['data']
						);		
					}else{
						$res =  array(
							"status"=> "error",
							"error"=> "error",
							"message"=> $result['msg']
						);
					}
				}else{
				$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Booking Id is required"
				);
			}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
 	}

/*..........................................................................................*/
/*.......................................More Service........................................*/

 	public function more_service(){
 		header('Content-type: application/json');
		$headers = apache_request_headers();

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				$result = $this->Webservice_model->more_service();
				if($result['status'] == 'success'){
					$res =  array(
						"status"=> "success",
						"data"=> $result['data']
					);
				}else{
					$res =  array(
						"status"=> "success",
						"data"=> array('more_service'=>[])
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
 	}

/*..........................................................................................*/
/*..............................Donation hospital list .....................................*/

 	public function donation_hospital_list(){
 		header('Content-type: application/json');
		$headers = apache_request_headers();

		$service_id = $_GET['service_id'];
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($service_id) && strlen(trim($service_id," ")) > 0 ){
					$result = $this->Webservice_model->donation_hospital_list($service_id);
					if($result['status'] == 'success'){
						$res = array(
							"status"=>"success",
							"data"=>$result['data']
						);
					}else{
						$res = array(
							"status"=>"success",
							"data"=>array('hospital_list'=>[])
						);
					}
				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "Not Valid",
						"message"=> "Service Id is required"
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
 	}

/*..........................................................................................*/
/*.....................................Add Save card ........................................*/

 	public function save_card(){
 		header('Content-type: application/json');
		$headers = apache_request_headers();
		$data = (array) json_decode(file_get_contents("php://input"));

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($data['card_number']) && strlen(trim($data['card_number']," ")) > 0 && isset($data['card_holder_name']) && strlen(trim($data['card_holder_name']," ")) > 0 && isset($data['card_expiry_month']) && strlen(trim($data['card_expiry_month']," ")) > 0 && isset($data['card_expiry_year']) && strlen(trim($data['card_expiry_year']," ")) > 0 && isset($data['card_cvv']) && strlen(trim($data['card_cvv']," ")) > 0){
					$auth_result = $this->Webservice_model->get_userid_frm_authtoken($headers['Auth']);
					$data['user_id'] = $auth_result->userid;
					$acnt_no = str_split($data['card_number'], 4);
					$data['hash_account_numer'] = 'xxxx-xxxx-xxxx-'.$acnt_no[3];
					$data['card_number'] = encrypt_data($data['card_number']);
					$data['card_holder_name'] = encrypt_data($data['card_holder_name']);
					$data['card_expiry_month'] = encrypt_data($data['card_expiry_month']);
					$data['card_expiry_year'] = encrypt_data($data['card_expiry_year']);
					$data['card_cvv'] = encrypt_data($data['card_cvv']);
					$result = $this->Webservice_model->save_card($data);
					if($result['status'] == 'success'){
						$res =  array(
							"status"=> "success"
						);
					}else{
						$res =  array(
							"status"=> "error",
							"error"=> "error",
							"message"=> "Insertion Failure"
						);
					}

				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "All Field is required"
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
 	}

/*..........................................................................................*/
/*.....................................Save card List........................................*/

 	public function saved_card_list(){
 		header('Content-type: application/json');
		$headers = apache_request_headers();

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				$auth_result = $this->Webservice_model->get_userid_frm_authtoken($headers['Auth']);
				$result = $this->Webservice_model->get_saved_card_list($auth_result->userid);
				if($result['status'] == 'success'){
					$res =  array(
						"status"=> "success",
						"data"=> $result['data']
					);
				}else{
					$res =  array(
						"status"=> "success",
						"data"=> $result['data']
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

 	}
	
	public function edit_save_card(){
 		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($data['card_number']) && strlen(trim($data['card_number']," ")) > 0 && isset($data['card_holder_name']) && strlen(trim($data['card_holder_name']," ")) > 0 && isset($data['card_expiry_month']) && strlen(trim($data['card_expiry_month']," ")) > 0 && isset($data['card_expiry_year']) && strlen(trim($data['card_expiry_year']," ")) > 0 && isset($data['card_cvv']) && strlen(trim($data['card_cvv']," ")) > 0 && isset($data['card_id']) && strlen(trim($data['card_id']," ")) > 0){
					$auth_result = $this->Webservice_model->get_userid_frm_authtoken($headers['Auth']);
					$data['user_id'] = $auth_result->userid;
					$acnt_no = str_split($data['card_number'], 4);
					$data['hash_account_numer'] = 'xxxx-xxxx-xxxx-'.$acnt_no[3];
					$data['card_number'] = encrypt_data($data['card_number']);
					$data['card_holder_name'] = encrypt_data($data['card_holder_name']);
					$data['card_expiry_month'] = encrypt_data($data['card_expiry_month']);
					$data['card_expiry_year'] = encrypt_data($data['card_expiry_year']);
					$data['card_cvv'] = encrypt_data($data['card_cvv']);
					$result = $this->Webservice_model->edit_save_card($data);
					if($result){
						$res = array('status'=>'success');
					}else{
						$res = array(
									'status'=>'error',
									'error'=>'Not Updated',
									'message'=>'Sorry Updation Failed..Try again Later'
									);
					}
				}else{
					$res =  array(
								"status"=> "error",
								"error"=> "required",
								"message"=> "All Field is required"
							);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

 	}
	
	 	public function notification(){
 		header('Content-type: application/json');
		$headers = apache_request_headers();

		$per_page=20;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				$auth_result = $this->Webservice_model->get_userid_frm_authtoken($headers['Auth']);
				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
					$currentpage =  (int)$_GET['page'];
					$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
					$start = $currentpage * $per_page;
					$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}

				$result = $this->Webservice_model->notification($auth_result->userid,$start,$per_page);
				$count = $this->Webservice_model->notification_list_unread_count($auth_result->userid);
				if($count){
					$total_count = $count->count;
				}else{
					$total_count = 0;
				}
				$notiArray = array();
				if($result != false){
					$total = count($result);
				}else{
					$total =0;
				}
				
				if($total >= $per_page){

					$totalPages = (int)($total / $per_page);
				}
				else{
					$totalPages = 0;
				}
				if($result != false){
					foreach (array_slice($result,$start,$per_page) as $key => $value) {
						
						array_push($notiArray, $value);

					}
				}

				if($result != false){

					$res = array('status' => 'success',
								'data' => array(
									'unread_count'=>$total_count,
									'notification_list' => $notiArray
									),
								'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
								);

				}else{
					$res = array(
						"status"=>"success",
						"data"=>array('unread_count'=>$total_count,'notification_list'=>[]),
						'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
					);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

 	}
	
	public function get_patient_recent_chat_list(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$per_page=20;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				$auth_result = $this->Webservice_model->get_userid_frm_authtoken($headers['Auth']);
				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
					$currentpage =  (int)$_GET['page'];
					$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
					$start = $currentpage * $per_page;
					$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}
				$type = 'user';
				$result = $this->Webservice_model->get_recent_chat_list($auth_result->userid,$start,$per_page,$type);
				$chatArray = array();
				if($result != false){
					$total = count($result);
				}else{
					$total =0;
				}
				
				if($total >= $per_page){

					$totalPages = (int)($total / $per_page);
				}
				else{
					$totalPages = 1;
				}
				if($result != ''){
					foreach (array_slice($result,$start,$per_page) as $key => $value) {
						array_push($chatArray, $value);
					}
				}
				if($result != false){
					$res = array('status' => 'success',
								'data' => array(
									'recent_chats' => $chatArray
									),
								'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
								);

				}else{
					$res = array(
						"status"=>"success",
						"data"=>array('recent_chats'=>[]),
						'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
					);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}
	
	public function set_patient_recent_chat(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($data['chat_list_id']) && strlen(trim($data['chat_list_id']," ")) > 0 && isset($data['patient_id']) && strlen(trim($data['patient_id']," ")) > 0 && isset($data['doctor_id']) && strlen(trim($data['doctor_id']," ")) > 0 && isset($data['sender_type']) && strlen(trim($data['sender_type']," ")) > 0 && isset($data['type']) && strlen(trim($data['type']," ")) > 0 && isset($data['time']) && strlen(trim($data['time']," ")) > 0) {
					
					$result = $this->Webservice_model->set_recent_chat($data);
					if($result){
						$res = array('status'=>'success');
					}else{
						$res = array(
									'status'=>'error',
									'error'=>'Not Updated',
									'message'=>'Sorry Not Updated'
								);
					}
				}else{
					$res = array(
								'status'=>'error',
								'error'=>'required',
								'message'=>'All Field is required'
							);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}
	
		public function chat_patient_file_upload(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = $_POST;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if($check_authToken){
				if(isset($data['chat_list_id']) && strlen(trim($data['chat_list_id']," ")) > 0 && isset($data['chat_id']) && strlen(trim($data['chat_id']," ")) > 0 && isset($data['type']) && strlen(trim($data['type']," ")) > 0 && isset($_FILES['file'])) {
					$auth_result = $this->Webservice_model->get_userid_frm_authtoken($headers['Auth']);
					$fileName = $auth_result->userid.'_'.$_FILES['file']['name'];
					$config = set_upload_options('../assets/uploads/profilepic/chat/'); 
		         	$config['file_name'] = $fileName;
		         	$this->load->library('upload', $config);
			        if ( ! $this->upload->do_upload('file')) {
			            $error = array('error' => $this->upload->display_errors('', '')); 
			            $res = array(
								"status"=> "error",
								"error"=> "Upload Error",
								"message"=> "Sorry! Profile Photo not uploaded".$error['error']
							);	
			        }	
			        else{ 
			         	
			            $imagedata = $this->upload->data(); 
			            if($data['type'] == 0){
			            	$image_url='assets/uploads/profilepic/chat/'.$imagedata['file_name'];
			            }else{
			            	$image_url='assets/uploads/profilepic/chat/'.$imagedata['file_name'];
			            }
			        }

			        $result = $this->Webservice_model->chat_file_upload($data,$image_url);
			        if($result['status'] == 'success'){
			        	$res = array(
			        				'status'=>'success',
			        				'data'=>array(
			        							'chat_list_id'=>$data['chat_list_id'],
			        							'chat_id'=>$data['chat_id'],
			        							'file_url'=>$image_url
			        						)
			        			);
			        }else{
			        	$res = array(
			        				'status'=>'error',
			        				'error'=>'error',
			        				'message'=>$result['message']
			        			);
			        }
				}else{
					$res = array(
								'status'=>'error',
								'error'=>'required',
								'message'=>'All Field is required'
							);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}
	
		public function forgot_password_user(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));
		if(isset($data['email']) && strlen(trim($data['email']," ")) > 0) {
			$type = 'user';
			$result = $this->Webservice_model->forgot_password($data,$type);
			if($result['status'] == 'success'){
				$res =  array(
					"status"=> "success"
				);
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Invalid email",
					"message"=> $result['message']
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Email is required"
				);
		}
			
		print json_encode($res);
	}





/******************************Ipok API Doc End********************************************/
/**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/


/********************************Medicos API doc*******************************************/



	public function main_Registration_doctor($data){
		$type = 'doctor';
		$check_result = $this->Webservice_model->dataExist($data,$type);
		//print_r($check_result);exit();
		if($check_result['message'] == 'success')
		{	
			$data['password'] = md5($data['password']);
		//	$data['dob'] = strtotime($data['dob']);
			
			$data['cep'] = $data['zip_code'];
			$data['complement'] = $data['landmark'];
			$data['about'] = $data['biodata'];
			$data['specialization'] = $data['specialization_id'];
			$data['gender'] = strtoupper($data['gender']);

			unset($data['zip_code']);
			unset($data['landmark']);
			unset($data['biodata']);
			unset($data['specialization_id']);
			
			$result = $this->Webservice_model->registration($data,$type);
			
			if($result['status'] == 'success'){
				$fileName = $result['userdata']['id'].'_'.$_FILES['profile_photo']['name'];
				$config = set_upload_options('../assets/uploads/profilepic/doctors/'); 
	         	$config['file_name'] = $fileName;
	         	$this->load->library('upload', $config);
		        if ( ! $this->upload->do_upload('profile_photo')) {
		            $error = array('error' => $this->upload->display_errors('', '')); 
		            $res = array(
							"status"=> "error",
							"error"=> "Upload Error",
							"message"=> "Sorry! Profile Photo not uploaded".$error['error']
						);	
		            $this->Webservice_model->delete_registration($result['userdata']['id'],$type);
		        }	
		        else { 
		         	
		            $imagedata = $this->upload->data(); 
		            $fullfilepath='assets/uploads/profilepic/doctors/'.$imagedata['file_name'];
		            $res=$this->mainFn_doctor($result,$fullfilepath);
		              
				}
			}
			else{
				$res = array(
							"status"=> "error",
							"error"=> "Insertion is failed",
							"message"=> "Sorry! Insertion is failed"
							);			
			}
		}
		else{
			$res = array(
						"status"=> "error",
						"error"=> "already exist",
						"message"=> $check_result['message']
						);
		}
		return $res;
 	}

 	function mainFn_doctor($result,$fullfilepath){
		$static_string = 'IPOK_Doctor'.time();
		$authToken = uniqid($static_string);
		$result_authtoken = $this->Webservice_model->authtoken_registration_doctor($authToken,$result['userdata']['id']);
		if($result_authtoken){
			$picdata = array('profile_pic'=>$fullfilepath);
			$type = 'doctor';
			$this->Webservice_model->updatePic($picdata,$result['userdata']['id'],$type);
			$pic = $this->Webservice_model->get_updatePic_doctor($result['userdata']['id']);
			$result['userdata']['profile_pic'] = $pic->profile_pic;
			$res = $this->get_doctor_fulldata($result,$authToken);
		}
		return $res;
	}



	public function doctor_registration(){
		header('Content-type: application/json');
	//	$data =(array) json_decode(file_get_contents("php://input"));
		$data = $_POST;		
		//print_r($data);die();
		if(isset($data['name']) && strlen($data['name']) && isset($data['username']) && strlen($data['username']) && isset($data['email']) && strlen($data['email']) && isset($data['password']) && strlen($data['password']) && isset($data['dob']) && strlen($data['dob']) && isset($data['gender']) && strlen($data['gender']) && isset($data['zip_code']) && strlen($data['zip_code']) && isset($data['street_address']) && strlen($data['street_address']) && isset($data['locality']) && strlen($data['locality']) && isset($data['number']) && strlen($data['number']) && isset($_FILES['profile_photo']) && isset($data['cpf']) && strlen($data['cpf']) && isset($data['rg']) && strlen($data['rg']) && isset($data['crm']) && strlen($data['crm']) && isset($data['landmark']) && strlen($data['landmark']) && isset($data['biodata']) && strlen($data['biodata']) &&isset($data['specialization_id']) && strlen($data['specialization_id']))
		{
			$res = $this->main_Registration_doctor($data);
		}
		else{
			$res = array(
								"status"=> "error",
								"error"=> "required",
								"message"=> "Sorry! Registration is failed. All Field is required"
							);
		}			
		print json_encode($res);
	}

	public function cpf_validation(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$cpf = $_GET['cpf'];
		if(isset($cpf) && strlen(trim($cpf," ")) > 0 ){
			$ress = $this->cpf_valid($cpf);
			if($ress == '1'){
				$res = array(
						"status"=> "success",
						"data"=>array("is_valid"=>true)
						);
			}else{
				$res = array(
						"status"=> "success",
						"data"=>array("is_valid"=>false)
						);
			}
		}else{
			$res = array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "CPF is required"
						);
		}			
		print json_encode($res);
	}

	public function cpf_valid($cpf){
		
		$cpf = preg_replace('/[^0-9]/', '', (string) $cpf);
	// Valida tamanho
	if (strlen($cpf) != 11)
		return false;
	// Calcula e confere primeiro dígito verificador
	for ($i = 0, $j = 10, $soma = 0; $i < 9; $i++, $j--)
		$soma += $cpf{$i} * $j;
	$resto = $soma % 11;
	if ($cpf{9} != ($resto < 2 ? 0 : 11 - $resto))
		return false;
	// Calcula e confere segundo dígito verificador
	for ($i = 0, $j = 11, $soma = 0; $i < 10; $i++, $j--)
		$soma += $cpf{$i} * $j;
	$resto = $soma % 11;
	return $cpf{10} == ($resto < 2 ? 0 : 11 - $resto);
	}

	public function check_email_availability(){
		header('Content-type: application/json');
		$headers = apache_request_headers();
		$data = (array) json_decode(file_get_contents("php://input"));

		if(isset($data['email']) && strlen(trim($data['email']," ")) > 0){
			$type='doctor';
			$result = $this->Webservice_model->dataExist($data,$type);
			if($result['message'] == 'success'){
				$res = array(
						"status"=> "success",
						"data"=>array(
										"email"=>$data['email'],
										"is_email_available"=>true)
						);
			}else{
				$res = array(
						"status"=> "success",
						"data"=>array(
										"email"=>$data['email'],
										"is_email_available"=>false)
						);
			}
		}else{
			$res = array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Username is required"
						);
		}			
		print json_encode($res);
	}

	public function check_username_availability(){
		header('Content-type: application/json');
		$headers = apache_request_headers();
		$data = (array) json_decode(file_get_contents("php://input"));

		if(isset($data['username']) && strlen(trim($data['username']," ")) > 0){
			$type='doctor';
			$result = $this->Webservice_model->dataExist($data,$type);
			if($result['message'] == 'success'){
				$res = array(
						"status"=> "success",
						"data"=>array(
										"username"=>$data['username'],
										"is_username_available"=>true)
						);
			}else{
				$res = array(
						"status"=> "success",
						"data"=>array(
										"username"=>$data['username'],
										"is_username_available"=>false)
						);
			}
		}else{
			$res = array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Username is required for checking"
						);
		}			
		print json_encode($res);
	}

	function doctor_login(){
		header('Content-type: application/json');
		$data =(array) json_decode(file_get_contents("php://input"));
		//$data=$_POST;

		if(isset($data['username']) && strlen($data['username']) && isset($data['password']) && strlen($data['password']))
		{
			$result=$this->Webservice_model->doctor_login($data);
			//$result['userdata']['specialization_id'] = $result['userdata']['specialization'];
			//echo"<pre>";print_r($result);echo"</pre>";exit();
			if($result['status']=='success'){
				$res = $this->get_doctor_fulldata($result,'');
			}
			else{
				$res = array(
							"status"=> "error",
							"error"=> "Login Failed",
							"message"=> "Invalid username or Password"
							);
			}
		}
		else{
			$res = array(
						"status"=> "error",
						"error"=> "Login Failed",
						"message"=> "Invalid username or Password"
						);
		}
		print json_encode($res);

	}

	public function facebook_registration_doctor(){
		header('Content-type: application/json');
	//	$data =(array) json_decode(file_get_contents("php://input"));
		$data = $_POST;		
		//print_r($data);exit()
		if(isset($data['uid']) && strlen($data['uid']) && isset($data['access_token']) && strlen($data['access_token']) && isset($data['email']) && strlen($data['email']) && isset($data['dob']) && strlen($data['dob']) && isset($data['gender']) && strlen($data['gender']) && isset($data['zip_code']) && strlen($data['zip_code']) && isset($data['street_address']) && strlen($data['street_address']) && isset($data['locality']) && strlen($data['locality']) && isset($data['number']) && strlen($data['number']) && isset($_FILES['profile_photo']) && isset($data['cpf']) && strlen($data['cpf']) && isset($data['rg']) && strlen($data['rg']) && isset($data['crm']) && strlen($data['crm']) && isset($data['landmark']) && strlen($data['landmark']) && isset($data['biodata']) && strlen($data['biodata']) &&isset($data['specialization_id']) && strlen($data['specialization_id']))
		{
			$result = $this->Webservice_model->is_this_uid_exit_doctor($data);
			//echo"<pre>";print_r($result);echo"</pre>";exit();
			if($result['status'] == 'success'){
				$fileName = $result['userdata']['id'].'_'.$_FILES['profile_photo']['name'];
				$config = set_upload_options('../assets/uploads/profilepic/doctors/'); 
	         	$config['file_name'] = $fileName;
	         	$this->load->library('upload', $config);
		        if ( ! $this->upload->do_upload('profile_photo')) {
		            $error = array('error' => $this->upload->display_errors('', '')); 
		            $res = array(
							"status"=> "error",
							"error"=> "Upload Error",
							"message"=> "Sorry! Profile Photo not uploaded".$error['error']
						);	
		            $type='doctor';
		            $this->Webservice_model->delete_registration($result['userdata']['id'],$type);
		        }	
		        else { 
		            $imagedata = $this->upload->data(); 
		            $fullfilepath='assets/uploads/profilepic/doctors/'.$imagedata['file_name'];
		            $picdata = array('profile_pic'=>$fullfilepath);
					$type = 'doctor';
		            $this->Webservice_model->updatePic($picdata,$result['userdata']['id'],$type);
		            $result['userdata']['profile_pic'] = $fullfilepath;
		            $res = $this->get_doctor_fulldata($result,'');

		        }
			}elseif ($result['status'] == 'fail') {
				$res = array('status'=>'error','message'=>$result['message']);
			}

			else{
				$res = array(
							"status"=> "error",
							"error"=> "error",
							"message"=> "Sorry! Registration is failed.Try Again"
							);
			}
		}
		else{
			$res = array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Sorry! Registration is failed. All Field is required"
							);
		}			
		print json_encode($res);
	}

	public function facebook_login_doctor(){
		header('Content-type: application/json');
		//$data =(array) json_decode(file_get_contents("php://input"));
		$data=$_POST;
		if(isset($data['uid']) && strlen($data['uid']) && isset($data['access_token']) && strlen($data['access_token']) && isset($data['name']) && strlen($data['name']))
		{

			$status=false;
			$check_result = $this->Webservice_model->is_uid_exist($data['uid']);
				//print_r($check_result);exit();
			if($check_result['status'] == 'exist'){
				$userData = $this->Webservice_model->get_userdata_doctor_by_uid($data['uid']);
//print_r($userData);exit();
				if($userData['status']=='success'){
					$result_1 = $this->get_doctor_fulldata($userData,'');
					$status=true;

					if(isset($data['email']) && strlen($data['email'])){
						$check_result = $this->Webservice_model->email_exist_doctor($data['email']);				
						if($check_result['status'] == 'exist'){
							$userData = $this->Webservice_model->get_userdata_doctor($data['email']);
							//print_r($userData);
							if($userData['status']=='success'){
								$result_2 = $this->get_doctor_fulldata($userData,'');
								if($result_1['data']['doctor_id'] == $result_2['data']['doctor_id']){
									$this->Webservice_model->update_doctor_fbuid($result_2['data']['doctor_id'],$data);
									$res = $result_2;
								}else{
									$res = $result_1;
								}
							}
							else if($userData['status'] == 'fail'){					
								$res=$result_1;
							}
						}
						else if($check_result['status'] == 'not exist'){					
							$res=$result_1;
						}
					}else{
						$res=$result_1;
					}
					

				}
				else if($userData['status'] == 'fail'){
					$status=false;
				}
			}else if($check_result['status'] == 'not exist'){
				$status=false;

				if(isset($data['email']) && strlen($data['email'])){
					$check_result = $this->Webservice_model->email_exist_doctor($data['email']);
				
					if($check_result['status'] == 'exist'){
						$userData = $this->Webservice_model->get_userdata_doctor($data['email']);
						if($userData['status']=='success'){
							$status=true;
							$res = $this->get_doctor_fulldata($userData,'');
						}
						else if($userData['status'] == 'fail'){				
							$status=false;
						}
					}
					else if($check_result['status'] == 'not exist'){					
						$status=false;
					}
				}

			}

			if(!$status){
				$res = $this->get_insert_uid_data($data);
			}
		}
		else{
			$res = array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Facebook Login failed. All Field is required"
						);
		}
		print json_encode($res);
	}

	public function get_doctor_fulldata($userData,$authtoken){
		if(isset($authtoken) && $authtoken != ''){
			$auth =$authtoken;
		}else{
			$auth = $userData['userdata']['authtoken'];
		}
		//echo"<pre>";print_r($userData);echo"</pre>";exit();

		if($userData['userdata']['specialization_id'] == ''){
			$userData['userdata']['specialization_id'] = '';
		}
		if($userData['userdata']['specialization_name'] == ''){
			$userData['userdata']['specialization_name'] = '';
		}
		$res = array(
					"status"=> "success",
					"data"=>array(
						"auth_token"=>$auth,
						"doctor_id"=> $userData['userdata']['id'],
						"name"=> $userData['userdata']['name'],
						"username"=> $userData['userdata']['username'],
						"email"=> $userData['userdata']['email'],
						"cpf" => $userData['userdata']['cpf'],
						"rg" => $userData['userdata']['rg'],
						"dob" => $userData['userdata']['dob'],
						"gender" => $userData['userdata']['gender'],
						"crm" => $userData['userdata']['crm'] ,
						"profile_photo" => $userData['userdata']['profile_pic'],
						"zip_code" => $userData['userdata']['cep'],
						"street_address" => $userData['userdata']['street_address'],
						"locality" => $userData['userdata']['locality'],
						"number" => $userData['userdata']['number'],
						"landmark" => $userData['userdata']['complement'],
						"biodata" => $userData['userdata']['about'],
						"specialization" =>array(
												'id' =>$userData['userdata']['specialization_id'],
												'name'=>$userData['userdata']['specialization_name']
												),
						"is_registration_completed"=>(boolean)$userData['userdata']['is_registration_completed']	
						)
					);
		return $res;
	}

	public function get_patient_fulldata($userData,$bystander_data){
		if($userData['userdata']['cpf'] == ''){
			$userData['userdata']['cpf'] = '';
		}
		if($userData['userdata']['rg'] == ''){
			$userData['userdata']['rg'] = '';
		}
		if($userData['userdata']['weight'] == ''){
			$userData['userdata']['weight'] = '';
		}
		if($userData['userdata']['height'] == ''){
			$userData['userdata']['height'] = '';
		}
		if($userData['userdata']['blood_group'] == ''){
			$userData['userdata']['blood_group'] = '';
		}
		if($userData['userdata']['number'] == ''){
			$userData['userdata']['number'] = '';
		}
		if($userData['userdata']['landmark'] == ''){
			$userData['userdata']['landmark'] = '';
		}

		$res = array(
				"status"=> "success",
				"data"=>array(
					"auth_token"=>$userData['userdata']['authtoken'],
					"user"=>array(
						"user_id"=> $userData['userdata']['userid'],
						"name"=> $userData['userdata']['name'],
						"username"=> $userData['userdata']['username'],
						"email"=> $userData['userdata']['email'],
						"password" => $userData['userdata']['password'],
						"cpf" => $userData['userdata']['cpf'],
						"rg" => $userData['userdata']['rg'],
						"dob" => $userData['userdata']['dob'],
						"gender" => $userData['userdata']['gender'],
						"weight" => $userData['userdata']['weight'] ,
						"height" => $userData['userdata']['height'], 
						"blood_group" => $userData['userdata']['blood_group'],
						"zip_code" => $userData['userdata']['zip_code'],
						"street_address" => $userData['userdata']['street_address'],
						"locality" => $userData['userdata']['locality'],
						"number" => $userData['userdata']['number'],
						"landmark" =>$userData['userdata']['landmark'],
						"profile_photo" => $userData['userdata']['profile_photo'],
						"bystander_details"=>$bystander_data,
                        "is_registration_completed" =>(boolean) $userData['userdata']['is_registration_completed']

						)
					)
				);
		return $res;
	}

	public function get_insert_uid_data($data){

		$result = $this->Webservice_model->insert_uid_doctor($data);
		$static_string = 'IPOK_Doctor'.time();
		$authToken = uniqid($static_string);
		$result_authtoken = $this->Webservice_model->authtoken_registration_doctor($authToken,$result['userdata']['id']);
		if($result_authtoken){
			$res = array(
					"status"=> "success",
					"data"=>array(
					"auth_token"=>$authToken,
					"doctor_id"=> $result['userdata']['id'],
					"name"=> "",
					"username"=>"",
					"email"=> $result['userdata']['email'],
					"cpf" =>"",
					"rg" =>"",
					"dob" =>"",
					"gender" =>"",
					"crm" =>"",
					"profile_photo" =>"",
					"zip_code" =>"",
					"street_address" =>"",
					"locality" =>"",
					"number" => "",
					"landmark" =>"",
					"biodata" =>"",
					"specialization" =>array(
											'id' =>"",
											'name'=>""
											),
					"is_registration_completed"=>(boolean)$result['userdata']['is_registration_completed']
					)
				);
		}
		return $res;
	}

	public function get_insert_patient_uid_data($data){

		$result = $this->Webservice_model->insert_uid_patient($data);
		$static_string = 'IPOK_User'.time();
		$authToken = uniqid($static_string);
		$result_authtoken = $this->Webservice_model->authtoken_registration($authToken,$result['userdata']['id']);
		if($result_authtoken){
			$res = array(
				"status"=> "success",
				"data"=>array(
					"auth_token"=>$authToken,
					"user"=>array(
						"user_id"=> $result['userdata']['id'],
						"name"=> "",
						"username"=> "",
						"email"=> $result['userdata']['email'],
						"password" => "",
						"cpf" =>"",
						"rg" =>"",
						"dob" => "",
						"gender" =>"",
						"weight" => "" ,
						"height" =>"", 
						"blood_group" =>"",
						"zip_code" =>"",
						"street_address" =>"",
						"locality" =>"",
						"number" =>"",
						"landmark" =>"",
						"profile_photo" =>"",
						"bystander_details"=>[],
                        "is_registration_completed" =>(boolean)$result['userdata']['is_registration_completed']

						)
					)
				);
		}
		return $res;
	}

	public function get_specializations(){
		header('Content-type: application/json');

		$result = $this->Webservice_model->get_specialisation();
		if($result['status'] == 'success'){
			$res = array(
					'status'=>'success',
					'data'=>$result['data'] 
					);
		}else{
			$res = array(
					'status'=>'error',
					'error'=>'No data',
					'message'=>'Specialization could not be fetched' 
					);
		}
		print json_encode($res);
	}

	public function update_fcm_token_doctor(){
		header('Content-type: application/json');

		//$data = $_POST;
		$headers =  apache_request_headers();
		//$data =(array) json_decode(file_get_contents("php://input"));
		$data = (array) json_decode(file_get_contents("php://input"));;
		if( isset($headers['Auth']) && strlen($headers['Auth'])){
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['fcm_token']) && strlen($data['fcm_token'])){
				$result = $this->Webservice_model->update_fcm_token_doctor($data,$headers['Auth']);
					if($result['status'] == 'success'){
						$res = array("status"=>'success');
					}
					else if($result['status'] == 'fail'){
						$res = array(
										"status"=> "error",
										"error"=> "updation failed",
										"message"=> "updation failed"
									);
					}
				}else{
					$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Fcm token is required"
					);
				}
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res = array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "FCM Token or Authtoken is required"
						);
		}
		print json_encode($res);
	}

	public function configure_consultation(){
		header('Content-type: application/json');
		$headers =  apache_request_headers();

		//$data = $_POST;
		$data =(array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				//print_r($data);exit();
				if(isset($data['consultation_duration']) && strlen(trim($data['consultation_duration']," ")) > 0 && isset($data['consultation_fee']) && strlen(trim($data['consultation_fee']," ")) > 0 && isset($data['is_home_consultation_available']) && isset($data['is_return_consultation_available'])){
					$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					$result = $this->Webservice_model->configure_consultation($data,$authtoken->doctor_id);
					if($result['status'] == 'success'){
						$res = array(
								'status'=>'success',
								'data'=>$result['data']
									);
					}else{
						$res = array(
								'status'=>'success',
								'error'=>'error',
								'message'=>$result['message']
									);
					}
				}else{
					$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "All Field is required"
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

	}

	public function set_doctor_vacation(){
		header('Content-type: application/json');
		$headers =  apache_request_headers();

		$data =(array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['clinic_id']) && strlen(trim($data['clinic_id']," ")) > 0 && isset($data['start_date']) && strlen(trim($data['start_date']," ")) > 0 && isset($data['end_date']) && strlen(trim($data['end_date']," ")) > 0){
					$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					$result = $this->Webservice_model->set_doctor_vacation($data,$authtoken->doctor_id);
					if($result == true){
						$res = array(
									'status'=>'success'
									);
					}else{
						$res = array(
									'status'=>'error',
									'error'=>'error',
									'message'=>'Insertion Failure'
									);
					}
				}else{
					$res =  array(
								"status"=> "error",
								"error"=> "required",
								"message"=> "All field Required"
					);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
					);
			}
		}else{
			$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function get_doctor_agenda(){
		header('Content-type: application/json');
		$headers =  apache_request_headers();

		$clinic_id = $_GET['clinic_id'];
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($clinic_id) && strlen(trim($clinic_id," ")) > 0){
					$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					$result = $this->Webservice_model->get_doctor_agenda($clinic_id,$authtoken->doctor_id);
					if($result['status'] == 'success'){
						$res = array(
									'status'=>'success',
									'data'=>$result['data']
									);
					}else{
						$res = array(
									'status'=>'error',
									'message'=>$result['message']
									);
					}
				}else{
					$res =  array(
								"status"=> "error",
								"error"=> "required",
								"message"=> "All field Required"
					);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
					);
			}
		}else{
			$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function set_doctor_primary_agenda(){
		header('Content-type: application/json');
		$headers =  apache_request_headers();

		$data =(array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['clinic_id']) && strlen(trim($data['clinic_id']," ")) > 0 && isset($data['schedules']) && isset($data['activated_schedule']) && strlen(trim($data['activated_schedule']," ")) > 0 ){
					$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					$type = '0';
					$result = $this->Webservice_model->set_doctor_primary_agenda($data,$authtoken->doctor_id,$type);
					if($result['status'] == 'success'){
						$res = array('status'=>'success');
					}else{
						$res = array(
								'status'=>'error',
								'error'=>'error',
								'message'=>$result['message']
									);
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "All Field is required"
					);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
					);
			}
		}else{
			$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function set_doctor_secondary_agenda(){
		header('Content-type: application/json');
		$headers =  apache_request_headers();

		$data =(array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['clinic_id']) && strlen(trim($data['clinic_id']," ")) > 0 && isset($data['schedules']) && isset($data['activated_schedule']) && strlen(trim($data['activated_schedule']," ")) > 0){
					$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					$type = '1';
					$result = $this->Webservice_model->set_doctor_primary_agenda($data,$authtoken->doctor_id,$type);
					if($result['status'] == 'success'){
						$res = array('status'=>'success');
					}else{
						$res = array(
								'status'=>'error',
								'error'=>'error',
								'message'=>$result['message']
									);
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "All Field is required"
					);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
					);
			}
		}else{
			$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function get_clinic_list(){
		header('Content-type: application/json');
		$headers =  apache_request_headers();

		
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($_GET['id']) &&  strlen(trim($_GET['id']," ")) > 0 ){
					$result = $this->Webservice_model->get_clinic_list($_GET['id']);
					if($result['status'] == 'success'){
						$res =  array(
								"status"=> "success",
								"data"=> $result['data']
						);
					}else{
						$res =  array(
								"status"=> "success",
								"data"=> $result['data']
						);
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "Doctor id is required"
					);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
					);
			}
		}else{
			$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function get_doctor_review_list(){
		header('Content-type: application/json');
		$headers =  apache_request_headers();

		//$page = $_GET['page'];
		$per_page=20;

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);

				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
					$currentpage =  (int)$_GET['page'];
					$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
					$start = $currentpage * $per_page;
					$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}

				$result = $this->Webservice_model->get_doctor_review_list($authtoken->doctor_id,$start,$per_page);

				$revArray = array();
				if($result != false){
					$total = count($result);
				}else{
					$total =0;
				}
				
				if($total >= $per_page){

					$totalPages = (int)($total / $per_page);
				}
				else{
					$totalPages = 1;
				}
				if($result != ''){
					foreach (array_slice($result,$start,$per_page) as $key => $value) {
						
						array_push($revArray, $value);

					}
				}

				if($result != false){

					$res = array('status' => 'success',
								'data' => array(
									'reviews' => $revArray
									),
								'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
								);

				}else{
					$res = array(
						"status"=>"success",
						"data"=>array('reviews'=>[]),
						'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
					);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
					);
			}
		}else{
			$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function get_doctor_faq_list(){
		header('Content-type: application/json');
		$headers = apache_request_headers();
		//$data = (array) json_decode(file_get_contents("php://input"));
		//$page = $_GET['page'];
		$per_page=20;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
					$currentpage =  (int)$_GET['page'];
					$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
					$start = $currentpage * $per_page;
					$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}

				
				$result = $this->Webservice_model->get_doctor_faq_list($start,$per_page);
				$faqArray = array();
				if($result != false){
					$total = count($result);
				}else{
					$total = 0;
				}
				
				if($total >= $per_page){

					$totalPages = (int)($total / $per_page);
				}
				else{
					$totalPages = 1;
				}
				if($result != ''){
					foreach (array_slice($result,$start,$per_page) as $key => $value) {
						
						array_push($faqArray, $value);

					}
				}
				if($result != false){
					$res = array('status' => 'success',
								'data' => array(
									'faq_list' => $faqArray
									),
								'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' =>$per_page)
								);

				}else{
					$res = array(
						"status"=>"success",
						"data"=>array('faq_list'=>[]),
						'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' =>$currentpage+1,
									'per_page' =>$per_page)
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function get_patient_list(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		//$page = $_GET['page'];
		$per_page=20;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
					$currentpage =  (int)$_GET['page'];
					$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
					$start = $currentpage * $per_page;
					$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}
				$result = $this->Webservice_model->get_patient_list($authtoken->doctor_id,$start,$per_page);

				$patientArray = array();
				if($result != false){
					$total = count($result);
				}else{
					$total = 0;
				}
				
				if($total >= $per_page){

					$totalPages = (int)($total / $per_page);
				}
				else{
					$totalPages = 1;
				}
				if($result != ''){
					foreach (array_slice($result,$start,$per_page) as $key => $value) {
						
						array_push($patientArray, $value);

					}
				}
				if($result != false){
					$res = array('status' => 'success',
								'data' => array(
									'patients' => $patientArray
									),
								'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
								);

				}else{
					$res = array(
						"status"=>"success",
						"data"=>array('patients'=>[]),
						'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function get_patient_anniversary_list(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		//$page = $_GET['page'];
		$per_page=20;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
					$currentpage =  (int)$_GET['page'];
					$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
					$start = $currentpage * $per_page;
					$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}
				$result = $this->Webservice_model->get_patient_anniversary_list($authtoken->doctor_id,$start,$per_page);
				$annArray = array();
				if($result != false){
					$total = count($result);
				}else{
					$total = 0;
				}
				
				if($total >= $per_page){

					$totalPages = (int)($total / $per_page);
				}
				else{
					$totalPages = 1;
				}
				if($result != ''){
					foreach (array_slice($result,$start,$per_page) as $key => $value) {
						
						array_push($annArray, $value);

					}
				}
				if($result != false){
					$res = array('status' => 'success',
								'data' => array(
									'anniversaries' => $annArray
									),
								'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' =>$currentpage+1,
									'per_page' => $per_page)
								);

				}else{
					$res = array(
						"status"=>"success",
						"data"=>array('anniversaries'=>[]),
						'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' =>$currentpage+1,
									'per_page' => $per_page)
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

		public function doctor_app_feedback(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['feedback_type']) && strlen(trim($data['feedback_type']," ")) > 0 && isset($data['feedback_content']) && strlen(trim($data['feedback_content']," ")) > 0){
					$auth_result = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					
					$data['doctor_id'] = $auth_result->doctor_id;
					$result = $this->Webservice_model->insert_app_feedback($data);
					if($result['status'] == 'success'){
						$res =  array(
							"status"=> "success"
						);
					}else{
						$res =  array(
							"status"=> "error",
							"error"=> "Not Inserted",
							"message"=> "Insertion Error"
						);
					}
				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Feedback type and Feedback Content is required"
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function set_push_status_doctor(){
		header('Content-type: application/json');
		$headers = apache_request_headers();
		$data = (array) json_decode(file_get_contents("php://input"));

		//$data = $_POST;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['status']) ){
					$auth_result = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					$result = $this->Webservice_model->set_push_status_doctor($auth_result->doctor_id,$data['status']);
					if($result['status'] == 'success'){
						$res =  array(
						"status"=> "success"
						);
					}else{
						$res =  array(
						"status"=> "error",
						"error"=> "Not Updated",
						"message"=> "Status not updated"
					);
					}

				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Status Id is required"
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

/*..........................................................................................*/
/*.......................................get push status....................................*/

	public function get_push_status_doctor(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$auth_result = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				$result = $this->Webservice_model->get_push_status_doctor($auth_result->doctor_id);
				if($result['status'] == 'success'){
					$res =  array(
						"status"=> "success",
						"data"=> $result['data']
					);
				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "Not Exist",
						"message"=> "User Not Exist"
					);
				}

			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function doctor_change_password(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['old_password']) && strlen(trim($data['old_password']," ")) > 0 && isset($data['new_password']) && strlen(trim($data['new_password']," ")) > 0){
					$auth_result = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					$result = $this->Webservice_model->doctor_change_password($auth_result->doctor_id,$data);
					if($result['status'] == 'success'){
						$res =  array(
							"status"=> "success"
						);
					}else{
						$res =  array(
							"status"=> "error",
							"error" => "Not Updated",
							"message" => $result['msg']
						);
					}
				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Current Password and New Password is required"
					);
				}

			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}

		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function get_patient_medical_record_list(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$id = $_GET['patient_id'];
		$per_page=20;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($id) && strlen(trim($id," ")) > 0){
					$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
						$currentpage =  (int)$_GET['page'];
						$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
						$start = $currentpage * $per_page;
						$page_no = $_GET['page'];
					}
					else{
						$currentpage = 0;
						$start = 0;
						$page_no = 0;
					}

					$result = $this->Webservice_model->get_patient_medical_record_list($authtoken->doctor_id,$start,$per_page,$id);
					//print_r($result);exit()
					$medicalArray = array();
					if(count($result['medical']) > 0){
						$total = count($result['medical']);
					}else{
						$total = 0;
					}
					if($total >= $per_page){

						$totalPages = (int)($total / $per_page);
					}
					else{
						$totalPages = 1;
					}
					if($result != ''){
						foreach (array_slice($result['medical'],$start,$per_page) as $key => $value) {
							
							array_push($medicalArray, $value);

						}
					}
					if($result['status'] == 'success'){
						$result['data']['medical_records'] = $medicalArray;

						$res = array(
									'status'=>'success',
									'data'=>$result['data'],
									'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' =>$currentpage+1,
									'per_page' => $per_page)
									);
					}else{
						$res = array(
									'status'=>'success',
									'data'=>$result['data'],
									'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' =>$currentpage+1,
									'per_page' => $per_page)
									);
					}
				}else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "Patient Id is required"
					);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}

		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function get_doctor_upcoming_consultation_list(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$per_page=10;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
						$currentpage =  (int)$_GET['page'];
						$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
						$start = $currentpage * $per_page;
						$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}
				$type='0';
				$result = $this->Webservice_model->get_doctor_upcoming_consultation_list($authtoken->doctor_id,$start,$per_page,$type);

				$upcomingArray = array();
				if($result != false){
					$total = count($result);
				}else{
					$total = 0;
				}
				
				if($total >= $per_page){

					$totalPages = (int)($total / $per_page);
				}
				else{
					$totalPages = 1;
				}
				if($result != ''){
					foreach (array_slice($result,$start,$per_page) as $key => $value) {
						array_push($upcomingArray, $value);
					}
				}
				if($result != false){
					$result['data']['consultations'] = $upcomingArray;
					$res = array(
								'status'=>'success',
								'data'=>$result['data'],
								'meta' => array('total_pages' => $totalPages,
								'total' => $total,
								'current_page' =>$currentpage+1,
								'per_page' => $per_page)
							);
				}else{
					$res = array(
								'status'=>'success',
								'data'=>array('consultations'=>[]),
								'meta' => array('total_pages' => $totalPages,
								'total' => $total,
								'current_page' =>$currentpage+1,
								'per_page' => $per_page)
							);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function get_doctor_today_consultation_list(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$per_page=20;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
						$currentpage =  (int)$_GET['page'];
						$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
						$start = $currentpage * $per_page;
						$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}
				$type='1';
				$result = $this->Webservice_model->get_doctor_upcoming_consultation_list($authtoken->doctor_id,$start,$per_page,$type);

					$upcomingArray = array();
					if($result != false){
						$total = count($result);
					}else{
						$total = 0;
					}
					
					if($total >= $per_page){

						$totalPages = (int)($total / $per_page);
					}
					else{
						$totalPages = 1;
					}
					if($result != ''){
						foreach (array_slice($result,$start,$per_page) as $key => $value) {
							array_push($upcomingArray, $value);
						}
					}
				if($result != false){

					
					$result['data']['consultations'] = $upcomingArray;
					$res = array(
								'status'=>'success',
								'data'=>$result['data'],
								'meta' => array('total_pages' => $totalPages,
								'total' => $total,
								'current_page' =>$currentpage+1,
								'per_page' => $per_page)
							);
				}else{
					$res = array(
								'status'=>'success',
								'data'=>array('consultations'=>[]),
								'meta' => array('total_pages' => $totalPages,
								'total' => $total,
								'current_page' =>$currentpage+1,
								'per_page' => $per_page)
							);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function get_doctor_next_week_consultation_list(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$per_page=20;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
						$currentpage =  (int)$_GET['page'];
						$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
						$start = $currentpage * $per_page;
						$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}
				$type='2';
				$result = $this->Webservice_model->get_doctor_upcoming_consultation_list($authtoken->doctor_id,$start,$per_page,$type);

					$upcomingArray = array();
					if($result != false){
						$total = count($result);
					}else{
						$total = 0;
					}
					
					if($total >= $per_page){

						$totalPages = (int)($total / $per_page);
					}
					else{
						$totalPages = 1;
					}
					if($result != ''){
						foreach (array_slice($result,$start,$per_page) as $key => $value) {
							array_push($upcomingArray, $value);
						}
					}
				if($result != false){
					$result['data']['consultations'] = $upcomingArray;
					$res = array(
								'status'=>'success',
								'data'=>$result['data'],
								'meta' => array('total_pages' => $totalPages,
								'total' => $total,
								'current_page' => $currentpage+1,
								'per_page' => $per_page) 
							);
				}else{
					$res = array(
								'status'=>'success',
								'data'=>array('consultations'=>[]),
								'meta' => array('total_pages' => $totalPages,
								'total' => $total,
								'current_page' => $currentpage+1,
								'per_page' => $per_page) 
							);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function get_patient_medical_record_details(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data =$_GET;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['patient_id']) && strlen(trim($data['patient_id']," ")) > 0 && isset($data['medical_record_id']) && strlen(trim($data['medical_record_id']," ")) > 0) {
					$result = $this->Webservice_model->get_patient_medical_record_details($data);
					if($result['status'] == 'success'){
						$res = array(
									'status'=>'success',
									'data'=>$result['data']
									);
					}else{
						$res = array(
									'status'=>'error',
									'error'=>'error',
									'message'=>$result['message']
									);
					}
				}else{
					$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Patient Id and Medical Record Id is required"
				);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function get_doctor_notification_list(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$per_page = 20;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
					$currentpage =  (int)$_GET['page'];
					$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
					$start = $currentpage * $per_page;
					$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}
				$result = $this->Webservice_model->get_doctor_notification_list($authtoken->doctor_id,$start,$per_page);
				$count = $this->Webservice_model->get_doctor_notification_list_unread_count($authtoken->doctor_id);
				if($count){
					$total_count = $count->count;
				}else{
					$total_count = 0;
				}
					$notifArray = array();
					if(count($result) > 0){
						$total = count($result);
					}else{
						$total = 0;
					}
					if($total >= $per_page){

						$totalPages = (int)($total / $per_page);
					}
					else{
						$totalPages = 1;
					}
					//print_r($result);exit;
					if($result != ''){
						foreach (array_slice($result,$start,$per_page) as $key => $value) {
							$value->read_status = $value->read_status!=0 ? true : false; 
							array_push($notifArray, $value);
						}
					}
					
				if($result != false){

					$res = array(
								'status'=>'success',
								'data'=>array('unread_count'=>$total_count,'notifications'=>$notifArray),
								'meta' => array('total_pages' => $totalPages,
								'total' => $total,
								'current_page' =>$currentpage+1,
								'per_page' => $per_page) 
							);
				}else{
					$res = array(
								'status'=>'success',
								'data'=>array('unread_count'=>$total_count,'notifications'=>[]),
								'meta' => array('total_pages' => $totalPages,
								'total' => $total,
								'current_page' =>$currentpage+1,
								'per_page' => $per_page)
							);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

	}

	public function set_doctor_notification_status(){
		header('Content-type: application/json');
		$headers = apache_request_headers();
		$data = $_GET;

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($_GET['notification_id']) && strlen(trim($_GET['notification_id']," ")) > 0  && isset($_GET['status']) && strlen(trim($_GET['status']," ")) > 0 ) {
					$result = $this->Webservice_model->set_doctor_notification_status($data);
					if($result == true){
						$res =  array(
							"status"=> "success"
						);
					}else{
						$res =  array(
							"status"=> "error",
							"error"=> "error",
							"message"=> "Sorry not updated"
						);
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "All Field is required"
						);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

	}

	public function  cancel_booking_doctor(){
		header('Content-type: application/json');
		$headers = apache_request_headers();
		$data = (array) json_decode(file_get_contents("php://input"));

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['booking_id']) && strlen(trim($data['booking_id']," ")) > 0){
					$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					$result = $this->Webservice_model->cancel_booking_doctor($authtoken->doctor_id,$data);
					if($result['status'] == 'success'){
						$res =  array(
							"status"=> "success"
						);
					}else{
						$res =  array(
							"status"=> "error",
							'error'=>'error',
							'message'=>$result['message']
						);
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "All Field is required"
						);
				}

			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

	}

	public function get_doctor_consultation_configuration(){
		header('Content-type: application/json');
		$headers = apache_request_headers();
		
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				$result = $this->Webservice_model->get_doctor_consultation_configuration($authtoken->doctor_id);
				if($result['status'] == 'success'){
					$res = array(
								'status'=>'success',
								'data'=>$result['data']
							);
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "error",
							"message"=> $result['message']
						);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function start_consultation_doctor(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['booking_id']) && strlen(trim($data['booking_id']," ")) > 0){
					$result = $this->Webservice_model->start_consultation_doctor($data);
					if($result['status'] == 'success'){
						$res = array('status'=>'success');
					}else{
						$res = array(
								'status'=>'error',
								'error'=>'error',
								'message'=>$result['message']
							);
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "All Field is required"
						);
				}
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function get_doctor_recent_chat_list(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$per_page=20;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
					$currentpage =  (int)$_GET['page'];
					$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
					$start = $currentpage * $per_page;
					$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}
				$type = 'doctor';
				$result = $this->Webservice_model->get_recent_chat_list($authtoken->doctor_id,$start,$per_page,$type);
				$chatArray = array();
				if($result != false){
					$total = count($result);
				}else{
					$total =0;
				}
				
				if($total >= $per_page){

					$totalPages = (int)($total / $per_page);
				}
				else{
					$totalPages = 1;
				}
				if($result != ''){
					foreach (array_slice($result,$start,$per_page) as $key => $value) {
						
						array_push($chatArray, $value);

					}
				}

				if($result != false){

					$res = array('status' => 'success',
								'data' => array(
									'recent_chats' => $chatArray
									),
								'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
								);

				}else{
					$res = array(
						"status"=>"success",
						"data"=>array('recent_chats'=>[]),
						'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
					);
				}

			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function set_doctor_recent_chat(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['chat_list_id']) && strlen(trim($data['chat_list_id']," ")) > 0 && isset($data['patient_id']) && strlen(trim($data['patient_id']," ")) > 0 && isset($data['doctor_id']) && strlen(trim($data['doctor_id']," ")) > 0 && isset($data['sender_type']) && strlen(trim($data['sender_type']," ")) > 0 && isset($data['type']) && strlen(trim($data['type']," ")) > 0 && isset($data['time']) && strlen(trim($data['time']," ")) > 0) {
					
					$result = $this->Webservice_model->set_recent_chat($data);
					if($result){
						$res = array('status'=>'success');
					}else{
						$res = array(
									'status'=>'error',
									'error'=>'Not Updated',
									'message'=>'Sorry Not Updated'
								);
					}
				}else{
					$res = array(
								'status'=>'error',
								'error'=>'required',
								'message'=>'All Field is required'
							);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function chat_doctor_file_upload(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = $_POST;

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['chat_list_id']) && strlen(trim($data['chat_list_id']," ")) > 0 && isset($data['chat_id']) && strlen(trim($data['chat_id']," ")) > 0 && isset($data['type']) && strlen(trim($data['type']," ")) > 0 && isset($_FILES['file'])) {
					$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					$fileName = $authtoken->doctor_id.'_'.$_FILES['file']['name'];
					$config = set_upload_options('../assets/uploads/profilepic/chat/'); 
		         	$config['file_name'] = $fileName;
		         	$this->load->library('upload', $config);
			        if ( ! $this->upload->do_upload('file')) {
			            $error = array('error' => $this->upload->display_errors('', '')); 
			            $res = array(
								"status"=> "error",
								"error"=> "Upload Error",
								"message"=> "Sorry! Profile Photo not uploaded".$error['error']
							);	
			        }	
			        else{ 
			         	
			            $imagedata = $this->upload->data(); 
			            $image_url='assets/uploads/profilepic/chat/'.$imagedata['file_name'];
			        }

			        $result = $this->Webservice_model->chat_file_upload($data,$image_url);
			        if($result['status'] == 'success'){
			        	$res = array(
			        				'status'=>'success',
			        				'data'=>array(
			        							'chat_list_id'=>$data['chat_list_id'],
			        							'chat_id'=>$data['chat_id'],
			        							'file_url'=>$image_url
			        						)
			        			);
			        }else{
			        	$res = array(
			        				'status'=>'error',
			        				'error'=>'error',
			        				'message'=>$result['message']
			        			);
			        }
				}else{
					$res = array(
								'status'=>'error',
								'error'=>'required',
								'message'=>'All Field is required'
							);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function get_consultation_basic_data(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$result = $this->Webservice_model->get_consultation_basic_data();
				if($result['status'] == 'success'){
					$res = array(
								'status'=>'success',
								'data'=>$result['data']
								);
				}else{
					$res= array('status'=>'error','error'=>'error','message'=>'No data Found');
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function set_anamnese(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['booking_id']) && strlen(trim($data['booking_id']," ")) > 0 && isset($data['main_problem']) && strlen(trim($data['main_problem']," ")) > 0 && isset($data['has_hepatitis']) && isset($data['is_pregnant']) && isset($data['has_diabetes']) && isset($data['has_healing_problems'])) {
					$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					$result = $this->Webservice_model->set_anamnese($data,$authtoken->doctor_id);
					if($result['status'] == 'success'){
						$res = array('status'=>'success');
					}else{
						$res = array('status'=>'error','error'=>'error','message'=>$result['message']);
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "All field is required"
						);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

	}

	public function encrypt_data(){
		$data = '24048';
		$new = encrypt_data($data);
		$neww = decrypt_data($new);
		print_r($new);echo"<br>";
		print_r($neww);
		exit();
	}

	public function set_prescriptions(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['booking_id']) && strlen(trim($data['booking_id']," ")) > 0 && isset($data['prescriptions'])) {
					$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					$result = $this->Webservice_model->set_prescriptions($data,$authtoken->doctor_id);
					if($result['status'] == 'success'){
						$res = array('status'=>'success');
					}else{
						$res =  array(
							"status"=> "error",
							"error"=> "Not updated",
							"message"=>$result['message']
						);
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "All field is required"
						);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

	}

	public function get_medicine_basic_data(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = $_GET;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['medicine']) && strlen(trim($data['medicine']," ")) > 0) {
					$result = $this->Webservice_model->get_medicine_basic_data($data);
					if($result['status'] == 'success'){
						$res = array(
									'status'=>'success',
									'data'=>$result['data']
								);
					}else{
						$res = array(
									'status'=>'success',
									'data'=>[]
								);
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "Required",
							"message"=> "Medicine is required"
						);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

	}

	public function get_budget_basic_data(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = $_GET;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['item']) && strlen(trim($data['item']," ")) > 0) {
					$result = $this->Webservice_model->get_budget_basic_data($data);
					if($result['status'] == 'success'){
						$res = array(
									'status'=>'success',
									'data'=>$result['data']
								);
					}else{
						$res = array(
								'status'=>'success',
								'data'=>[]
							);
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "Required",
							"message"=> "Item is required"
						);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function set_exams_procedures(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['booking_id']) && strlen(trim($data['booking_id']," ")) > 0 && isset($data['exams'])) {
					$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					$result = $this->Webservice_model->set_exams_procedures($data,$authtoken->doctor_id);
					if($result['status'] == 'success'){
						$res = array('status'=>'success');
					}else{
						$res =  array(
							"status"=> "error",
							"error"=> "Not updated",
							"message"=>$result['message']
						);
					}	
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "Required",
							"message"=> "Booking Id and Exams is required"
						);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

	}

	public function set_budgets(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['booking_id']) && strlen(trim($data['booking_id']," ")) > 0 && isset($data['items'])) {
					$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					$result = $this->Webservice_model->set_budgets($data,$authtoken->doctor_id);
					if($result['status'] == 'success'){
						$res = array('status'=>'success');
					}else{
						$res =  array(
							"status"=> "error",
							"error"=> "Not updated",
							"message"=>$result['message']
						);
					}	
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "Required",
							"message"=> "Booking Id and Budget is required"
						);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

	}

	public function forgot_password_doctor(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));
		if(isset($data['email']) && strlen(trim($data['email']," ")) > 0) {
			$type = 'doctor';
			$result = $this->Webservice_model->forgot_password($data,$type);
			if($result['status'] == 'success'){
				$res =  array(
					"status"=> "success"
				);
			}else{
				$res =  array(
					"status"=> "error",
					"error"=> "Invalid email",
					"message"=> $result['message']
				);
			}
		}else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Email is required"
				);
		}
			
		print json_encode($res);
	}

	public function finalize_medical_record(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = $_POST;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['booking_id']) && strlen(trim($data['booking_id']," ")) > 0 ) {
					$new = array();
					$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					if(isset($_FILES['photo'])){
						if (!is_dir("./../assets/uploads/medicalrecord/booking".$data['booking_id'])) {
							mkdir("./../assets/uploads/medicalrecord/booking".$data['booking_id'], "0777",true);
						}
						$files = $_FILES;
						$config = set_upload_options('./../assets/uploads/medicalrecord/booking'.$data['booking_id']); 
						$this->load->library('upload', $config);

						for ($i=0; $i < count($files['photo']['name']) ; $i++) 
						{ 
					
							$_FILES['photo']['name'] = $files['photo']['name'][$i];
							$_FILES['photo']['type'] = $files['photo']['type'][$i];
							$_FILES['photo']['tmp_name'] = $files['photo']['tmp_name'][$i];
							$_FILES['photo']['error'] = $files['photo']['error'][$i];
							$_FILES['photo']['size'] = $files['photo']['size'][$i];    
							if($this->upload->do_upload('photo'))
							{
								$imagedata= $this->upload->data();
								$new[$i] = 'assets/uploads/medicalrecord/booking'.$data['booking_id'].'/'.$imagedata['file_name'];
							}
							else
							{
								$display_error = array('error' => $this->upload->display_errors('', '')); 
								$res = array(
								"status"=> "error",
								"error"=> "Upload Error",
								"msg"=> "Sorry! Images not uploaded. ".$display_error['error']
								);	
								print json_encode($res);
								exit();
								
							}

						}
						$data['images'] =json_encode($new);
					}
					$result = $this->Webservice_model->finalize_medical_record($data,$authtoken->doctor_id);
					if($result['status'] == 'success'){
						$res =  array(
							"status"=> "success",
							"data"=>array(
										'booking_id'=>$data['booking_id'],
										'photos'=>$new
									)
						);
					}else{
						$res =  array(
							"status"=> "error",
							"error"=> "Not Updated",
							"message"=> $result['message']
						);
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "All Field is required"
						);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function set_medical_certificate(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['booking_id']) && strlen(trim($data['booking_id']," ")) > 0 && isset($data['is_letter_with_cid']) && isset($data['days']) && strlen(trim($data['days']," ")) > 0 && isset($data['medical_certificate']) &&  strlen(trim($data['medical_certificate']," ")) > 0) {
					$result = $this->Webservice_model->set_medical_certificate($data);
					if($result['status'] == 'success'){
						$res = array('status'=>'success');
					}else{
						$res = array(
									'status'=>'error',
									'error'=>'error',
									'message'=>$request['message']
								);
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "All Field is required"
						);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}

	public function get_wallet_details(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				$result = $this->Webservice_model->get_wallet_details($authtoken->doctor_id);
				if($result['status'] == 'success'){
					$res = array(
								'status'=>'success',
								'data'=>$result['data']
								);
				}else{
					$res =array(
								'status'=>'success',
								'data'=>$result['data']
								);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}
	
	public function get_completed_consultation_transaction_list(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$per_page = 20;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);

				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
					$currentpage =  (int)$_GET['page'];
					$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
					$start = $currentpage * $per_page;
					$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}
				$type = '1';
				$result = $this->Webservice_model->get_consultation_transaction_list($authtoken->doctor_id,$start,$per_page,$type);
				$transArray = array();
				if($result['status'] != 'failed'){
					$total = count($result['data']);
				}else{
					$total = 0;
				}
				
				if($total >= $per_page){

					$totalPages = (int)($total / $per_page);
				}
				else{
					$totalPages = 1;
				}
				if($result != ''){
					foreach (array_slice($result['data'],$start,$per_page) as $key => $value) {
						
						array_push($transArray, $value);

					}
				}

				if($result != false){
					$res = array('status' => 'success',
								'data' => array(
									'consultation' => $transArray
									),
								'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
								);

				}else{
					$res = array(
						"status"=>"success",
						"data"=>array('consultation'=>[]),
						'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
					);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}
	
	public function get_future_consultation_transaction_list(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$per_page = 20;
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);

				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
					$currentpage =  (int)$_GET['page'];
					$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
					$start = $currentpage * $per_page;
					$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}
				$type = '2';
				$result = $this->Webservice_model->get_consultation_transaction_list($authtoken->doctor_id,$start,$per_page,$type);
				$transArray = array();
				if($result['status'] == 'success'){
					$total = count($result['data']);
				}else{
					$total = 0;
				}
				
				if($total >= $per_page){

					$totalPages = (int)($total / $per_page);
				}
				else{
					$totalPages = 1;
				}
				if($result['status'] == 'success'){
					foreach (array_slice($result['data'],$start,$per_page) as $key => $value) {
						
						array_push($transArray, $value);

					}
				}

				if($result['status'] == 'success'){
					$res = array('status' => 'success',
								'data' => array(
									'consultation' => $transArray
									),
								'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
								);

				}else{
					$res = array(
						"status"=>"success",
						"data"=>array('consultation'=>[]),
						'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
					);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}
	
	public function add_bank(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				$data['type_id'] = $authtoken->doctor_id;
				if(isset($data['bank_name']) && strlen(trim($data['bank_name']," ")) > 0 && isset($data['agency'])  && strlen(trim($data['agency']," ")) > 0 && isset($data['account']) && strlen(trim($data['account']," ")) > 0 && isset($data['name']) &&  strlen(trim($data['name']," ")) > 0) {
					$data['type'] = '1';
					$data['account_no'] = encrypt_data($data['account']);
					$data['account_holder'] = encrypt_data($data['name']);
					unset($data['account']);
					unset($data['name']);
					$result = $this->Webservice_model->add_bank($data);
					if($result['status'] == 'success'){
						$res = array(
									'status'=>'success',
									'data'=>$result['data']
									);
					}else{
						$res = array('status'=>'error','error'=>'error','message'=>'Something Went Wrong.. Try Again Later!');
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "All Field is required"
						);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}
	
	public function edit_bank(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				if(isset($data['id']) &&  strlen(trim($data['id']," ")) > 0) {
					
					if(isset($data['account']) && strlen(trim($data['account']," ")) > 0){	
						$data['account_no'] = encrypt_data($data['account']);
						unset($data['account']);
					}
					if(isset($data['name']) &&  strlen(trim($data['name']," ")) > 0){	
						$data['account_holder'] = encrypt_data($data['name']);
						unset($data['name']);
					}

					$result = $this->Webservice_model->edit_bank($data);
					if($result['status'] == 'success'){
						$res = array(
									'status'=>'success',
									'data'=>$result['data']
									);
					}else{
						$res = array('status'=>'error','error'=>'error','message'=>$result['message']);
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "Id is required"
						);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}
	
	public function delete_bank(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				if(isset($data['id']) && strlen(trim($data['id']," ")) > 0) {
					$result = $this->Webservice_model->delete_bank($data);
					if($result['status'] == 'success'){
						$res = array(
									'status'=>'success'
									);
					}else{
						$res = array('status'=>'error','error'=>'error','message'=>$result['message']);
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "All Field is required"
						);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}
	
		public function get_bank_list(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$per_page = 20;
		$data = (array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
					$currentpage =  (int)$_GET['page'];
					$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
					$start = $currentpage * $per_page;
					$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}
				$result = $this->Webservice_model->get_bank_list($authtoken->doctor_id,$start,$per_page);
				$bankArray = array();
				if($result['status'] != 'failed'){
					$total = count($result['data']);
				}else{
					$total = 0;
				}
				
				if($total >= $per_page){

					$totalPages = (int)($total / $per_page);
				}
				else{
					$totalPages = 1;
				}
				if($result['status'] != 'failed'){
					foreach (array_slice($result['data'],$start,$per_page) as $key => $value) {
						
						array_push($bankArray, $value);

					}
				}

				if($result['status'] != 'failed'){
					$res = array('status' => 'success',
								'data' => array(
									'banks' => $bankArray
									),
								'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
								);

				}else{
					$res = array(
						"status"=>"success",
						"data"=>array('banks'=>[]),
						'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
					);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}
	
		public function get_withdrawal_history(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$per_page = 20;
		$data = (array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				if(isset($_GET['page']) && strlen(trim($_GET['page']," ")) > 0 ) {
					$currentpage =  (int)$_GET['page'];
					$currentpage = $currentpage==0 ? $currentpage : $currentpage-1;
					$start = $currentpage * $per_page;
					$page_no = $_GET['page'];
				}
				else{
					$currentpage = 0;
					$start = 0;
					$page_no = 0;
				}
				$result = $this->Webservice_model->get_withdrawal_history($authtoken->doctor_id,$start,$per_page);
				$withdrawArray = array();
				if($result != false){
					$total = count($result);
				}else{
					$total = 0;
				}
				
				if($total >= $per_page){

					$totalPages = (int)($total / $per_page);
				}
				else{
					$totalPages = 1;
				}
				if($result != ''){
					foreach (array_slice($result,$start,$per_page) as $key => $value) {
						
						array_push($withdrawArray, $value);

					}
				}

				if($result != false){
					$res = array('status' => 'success',
								'data' => array(
									'withdrawal_history' => $withdrawArray
									),
								'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' =>$currentpage+1,
									'per_page' => $per_page)
								);

				}else{
					$res = array(
						"status"=>"success",
						"data"=>array('withdrawal_history'=>[]),
						'meta' => array('total_pages' => $totalPages,
									'total' => $total,
									'current_page' => $currentpage+1,
									'per_page' => $per_page)
					);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);
	}
	
		public function get_doctor_profile(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
				if(isset($data['doctor_id'])){
					$id = $data['doctor_id'];
				}else{
					$id = $authtoken->doctor_id;
				}
				$result = $this->Webservice_model->doctor_profile($id);
				if($result['status'] == 'success'){
					$res = array('status'=>'success','data'=>$result['data']);
				}else{
					$res = array('status'=>'failed');
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

	}
	
	/*public function push_sents() {
	
		$data1 = "SELECT * FROM settings WHERE id = '0' ";

		$query1 = $this->db->query($data1);

		$rs = $query1->row();
		$key = $rs->api_key;
		
		$data = "{ \"notification\": { \"title\": \"hi\", \"text\": \"hellonewmessage\" , \"sound\": \"default\" }, \"time_to_live\": 60, \"data\" : {\"response\" : {\"status\" : \"success\", \"data\" : {\"id\" : \"12\"}}}, \"collapse_key\" : \"trip\", \"priority\":\"high\", \"to\" : \"fS8vuhzQKwg:APA91bEJbAw_Hgy8AfMv42GWJEow71yToWBzhhRnT5uOcFmrsLl4Ir3ONZqgvPKoksK5lCbKT8e9DS2oOOylm1VEUoCH4MntbYcdodqj1PcQhdxv2fJdMJzK0kBU4eCyYXC6wx-U8tOw\"}";
		
		$ch = curl_init("https://fcm.googleapis.com/fcm/send"); 
		$header = array('Content-Type: application/json', 'Authorization: key='.$key);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		$out = curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);

		curl_close($ch);

    }*/
	
	 public function get_vacation_dates(){
		header('Content-type: application/json');

		$data = $_GET;
		$headers = apache_request_headers();
		
		if(isset($headers['Auth']) && strlen($headers['Auth'])){		

			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['month']) && strlen($data['month']) && isset($data['year']) && strlen($data['year']) && isset($data['clinic_id']) && strlen($data['clinic_id'])){
					$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					$data['doctor_id'] = $authtoken->doctor_id;
					$result_booked = $this->Webservice_model->get_doctor_dates($data);					
					if($result_booked['status'] == 'success'){
						$res = array("status" => "success",
							"data" => array(/*"available"=>array(),*/"vacation_dates" =>array()));		
							$db_day_array = array();
							if($result_booked['data'] != '""'){
								$schedule_data = json_decode($result_booked['data']);
								foreach ($schedule_data as $key => $value) {
									$db_day_array[] = $value->day;
								}
							}else{
								$db_day_array[] = [];
							}
						$date_month = $this->get_month($data['month']);
						$time = strtotime($date_month. $data['year']);
						$search_form = date('Y-m-01',$time);
				 		$search_to =  date('Y-m-t',$time);
				 		for($i = $search_form; $i <= $search_to ; $i++){
				 			$day = strtolower(date('D',strtotime($i)));
				 			if(in_array($day, $db_day_array)){
				 				if(count($result_booked['leave']) > 0){
					 				foreach ($result_booked['leave'] as $key => $value) {

					 					if((date('j',$value->start_date) <= date('j',strtotime($i))) && (date('j',$value->end_date) >= date('j',strtotime($i))) ) {
					 						array_push($res['data']['vacation_dates'], date('j',strtotime($i)));
					 					}/*else{
					 						array_push($res['data']['available'], date('j',strtotime($i)));
					 					}*/
					 				}
					 			}/*else{
									array_push($res['data']['available'], date('j',strtotime($i)));
								}*/
				 			}else{
				 				array_push($res['data']['vacation_dates'], date('j',strtotime($i)));
				 			}
				 		}	
					}
					else if($result_booked['status'] == 'fail'){
						$res =  array(
										"status"=> "error",
										"error"=> "Not available",
										"message"=> "Doctor is not available in that clinic"
									);
					}					
				}
				else{
					$res =  array(
						"status"=> "error",
						"error"=> "required",
						"message"=> "doctor id or month or year or clinic id is required"
						);
				}
			}
			else{
				$res =  array(
					"status"=> "error",
					"error"=> "Not Valid",
					"message"=> "Authtoken is not valid"
					);
			}
		}
		else{
			$res =  array(
					"status"=> "error",
					"error"=> "required",
					"message"=> "Authtoken is required"
					);
		}
		print json_encode($res);
	}
											
	function get_month($day){
		if($day == '0'){$dayid = 'January';}elseif ($day == '1') {$dayid = 'February';}elseif ($day == '2') {$dayid = 'March';}elseif ($day == '3') {$dayid = 'April';}elseif ($day == '4') {$dayid = 'May';}elseif ($day == '5') {$dayid = 'June';}elseif ($day == '6') {$dayid = 'July';}elseif ($day == '7') {$dayid = 'August';}elseif ($day == '8') {$dayid = 'September';}elseif ($day == '9') {$dayid = 'October';}elseif ($day == '10') {$dayid = 'November';}elseif ($day == '11') {$dayid = 'December';}
		return $dayid;
	}
	
	public function check_wait_listed(){
		header('Content-type: application/json');

		$data = $_GET;
		$headers = apache_request_headers();

		if (isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if ($check_authToken) {
				if(isset($data['date']) && strlen($data['date']) && isset($data['time']) && strlen($data['time'])&& isset($data['doctor_id']) && strlen($data['doctor_id']) && isset($data['clinic_id']) && strlen($data['clinic_id'])){
					$authtoken = $this->Webservice_model->get_userid_frm_authtoken($headers['Auth']);
					$data['user_id'] = $authtoken->userid;

					$result = $this->Webservice_model->check_wait_listed($data);
					if($result['status'] == 'success'){
						$res = array('status'=>'success','data'=>array('is_booked'=>$result['booked'],'is_return_avail'=>$result['is_return_avail']));
					}else{
						$res = array('status'=>'error','error'=>'error');
					}
				} else {
					$res = array(
							"status" => "error",
							"error" => "required",
							"message" => "Date and Time is required");
				}
			}else {
				$res = array(
						"status" => "error",
						"error" => "Not Valid",
						"message" => "Authtoken is not valid");
			}
		}else {
			$res = array(
					"status" => "error",
					"error" => "required",
					"message" => "Authtoken is required");
		}
		print json_encode($res);
	}
	
	public function set_patient_notification_status(){
		header('Content-type: application/json');

		$data = (array)json_decode(file_get_contents("php://input"));
		$headers = apache_request_headers();
		if (isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if ($check_authToken) {
				$result = $this->Webservice_model->set_patient_notification_status($data);
				if($result == true){
					$res = array('status'=>'success');
				}else{
					$res = array('status'=>'failed','error'=>'error','message'=>'Sorry Not Updated..Something Went Wrong.. Try Again Later');
				}
			}else {
				$res = array(
						"status" => "error",
						"error" => "Not Valid",
						"message" => "Authtoken is not valid");
			}
		}else {
			$res = array(
					"status" => "error",
					"error" => "required",
					"message" => "Authtoken is required");
		}
		print json_encode($res);
		
	}

	public function withdrawal_request(){
		header('Content-type: application/json');
		$headers = apache_request_headers();

		$data = (array) json_decode(file_get_contents("php://input"));
		if(isset($headers['Auth']) && strlen($headers['Auth'])) {
			$check_authToken = $this->Webservice_model->check_auth_token_doctor($headers['Auth']);
			if($check_authToken){
				if(isset($data['bank_id']) && strlen(trim($data['bank_id']," ")) > 0 && isset($data['amount'])  && strlen(trim($data['amount']," ")) > 0) {
					$authtoken = $this->Webservice_model->get_doctorid_frm_authtoken($headers['Auth']);
					$data['doctor_id'] = $authtoken->doctor_id;
					$result = $this->Webservice_model->withdrawal_request($data);
					if($result['status'] == 'success'){
						$res = array('status'=>'success','data'=>$result['data']);
					}else{
						$res = array('status'=>'error','error'=>'error','message'=>$result['message']);
					}
				}else{
					$res =  array(
							"status"=> "error",
							"error"=> "required",
							"message"=> "All Field is required"
						);
				}
			}else{
				$res =  array(
							"status"=> "error",
							"error"=> "Not Valid",
							"message"=> "Authtoken is not valid"
						);
			}
		}else{
			$res =  array(
				"status"=> "error",
				"error"=> "required",
				"message"=> "Authtoken is required"
			);
		}
		print json_encode($res);

	}
	
	Public function payment_patient() {
		header('Content-type: application/json');

		$data = (array)json_decode(file_get_contents("php://input"));
		$headers = apache_request_headers();

		if (isset($headers['Auth']) && strlen($headers['Auth'])) {

			$check_authToken = $this->Webservice_model->check_auth_token($headers['Auth']);
			if ($check_authToken) {
				if (isset($data['month']) && strlen($data['month']) && isset($data['year']) && strlen($data['year']) && isset($data['first_name']) && strlen($data['first_name']) && isset($data['last_name']) && strlen($data['last_name']) && isset($data['card_number']) && strlen($data['card_number']) && isset($data['cvv']) && strlen($data['cvv']) && isset($data['booking_id']) && strlen($data['booking_id'])) {
					$authtoken = $this->Webservice_model->get_userid_frm_authtoken($headers['Auth']);
					$data['user_id'] = $authtoken->userid;
					$payed_data = $this->Webservice_model->get_booked_payed_data($data['booking_id']);
					if($payed_data['status'] == 'success' && ($payed_data['is_result'] == '0' || $payed_data['is_result'] == '1')){
						$check_markbooking = $this->Webservice_model->checkBooking($data['booking_id']);
						$booking_details = $this->Webservice_model->get_booking_details($data['booking_id']);
						$patient_data = $this->Webservice_model->get_single_patient($booking_details['patient_id']);
						$doctor_data = $this->Webservice_model->get_single_doctor($booking_details['doctor_id']);

						$time = $data['offset'];
						$new_time = date('H:i', $time);
						$new_time = explode(':', $new_time);
						$nowin_server = date("Y-m-d TH:i:s");
						if ($data['offset'] > 0) {

							$nowin_server_addoffset = date('Y-m-d H:i:s', strtotime('+'.$new_time[0].' hour +'.$new_time[1].' minutes', strtotime($nowin_server)));

						} else {

							$nowin_server_addoffset = date('Y-m-d H:i:s', strtotime('-'.$new_time[0].' hour -'.$new_time[1].' minutes', strtotime($nowin_server)));

						}

						$payment_token = array('account_id' => $this->config->item('id'), 'method' => 'credit_card', 'test' => true, );
						$payment_token['data'] = array('number' => $data['card_number'], 'verification_value' => $data['cvv'], 'first_name' => $data['first_name'], 'last_name' => $data['last_name'], 'month' => $data['month'], 'year' => $data['year']);

						$request = load_curl('https://api.iugu.com/v1/payment_token', $payment_token);
						$token = json_decode($request);

						if ($check_markbooking['count'] != 1) {
							$res = array('status' => 'error', 'payment_status' => '0', 'message' => 'No Booking Found');
						}
						elseif(!empty($token->errors->number)) {
							$res = array('status' => 'error', 'payment_status' => '0', 'message' => 'Invalid Card Provided');
						}
						else {
							if (empty($patient_data['pt_customer_id'])) {
								$create_customer = array('email' => $patient_data['pt_email'], 'name' => $patient_data['pt_name'], /*'cpf_cnpj' => $patient_data['pt_cpf'],*/ 'zip_code' => $patient_data['pt_zip_code'], 'street' => $patient_data['pt_street_add'], 'number' => $patient_data['pt_number'], 'complement' => $patient_data['pt_complement']);
								$request = load_curl('https://api.iugu.com/v1/customers', $create_customer);
								$customer = json_decode($request);
								$this->Webservice_model->update_profile($patient_data['patientid'], array('customer_id' => $customer->id));
								$patient_data['pt_customer_id'] = $customer->id;
							}

							$create_payment_method = array('description' => 'Booking Payment', 'token' => $token->id);
							$request = load_curl('https://api.iugu.com/v1/customers/'.$patient_data['pt_customer_id'].'/payment_methods', $create_payment_method);
							$payment_method = json_decode($request);

							$create_charge = array('customer_payment_method_id' => $payment_method->id, 'customer_id' => $patient_data['pt_customer_id'], 'email' => $patient_data['pt_email']);

							$items = array('description' => 'Doctor Visit', 'price_cents' => $booking_details['total_sum'] * 100, 'quantity' => 1);

							$create_charge['items'] = $items;
							$create_charge['payer'] = array('cpf_cnpj' => $patient_data['pt_cpf'], 'name' => $patient_data['pt_name'], 'email' => $patient_data['pt_email']);
							$create_charge['payer']['address'] = array('zip_code' => $patient_data['pt_zip_code'], 'street' => $patient_data['pt_street_add'], 'number' => $patient_data['pt_number'], 'complement' => $patient_data['pt_complement']);
							//print_r($create_charge);die();
							$request = load_curl('https://api.iugu.com/v1/charge', $create_charge);
							$charge = json_decode($request);

							if (isset($charge->success)and isset($charge->LR)and($charge->success == true)and($charge->LR == '00')) {
								/*CODE FOR DOCTOR NOTIFICATION ON CONFIRMED CONSULTATION*/

								/*CODE FOR WALLET INSERTION*/

								/*------------------------------------------------*/

								$wallet = $this->Webservice_model->get_wallet_for_doctor($booking_details['doctor_id']);

								if (empty($wallet)) {
									$wallet = array('reedem_earn' => 0, 'future_earn' => 0, 'total_earn' => 0);
								}
								$earn = $booking_details['total_sum'] - (($booking_details['total_sum'] * $booking_details['ipok_fee']) / 100);
								$wallet['future_earn'] = $wallet['future_earn'] + $earn;
								$wallet['total_earn'] = $wallet['total_earn'] + $earn;
								$this->Webservice_model->update_wallet($booking_details['doctor_id'], $wallet);

								if ($booking_details['booking_status'] == 0) {
									/*------------------------------------------------*/
									/*CODE FOR SENTING WAITING LIST NOTIFICATION FOR PAID VISIT - PATIENT NOTIFICATION*/

									$text_pat = 'Your appointment was scheduled in the system as waiting, on '.date('d.m.Y', $booking_details['time_start']).' at '.date('H:i a', $booking_details['time_start']).', doctor '.$doctor_data['dr_name'];

									$notification_pat = array('patient_id' => $booking_details['patient_id'], 'type' => 0, 'message' => $text_pat, 'read_status' => 0, 'time' => strtotime($nowin_server), 'booking_id' => $_POST['booking_id']);

									$patient_insert_id = $this->Webservice_model->insert_notification_patient($notification_pat);

									$fcm_user = $this->Webservice_model->get_patient_fcm($booking_details['patient_id']);

									if (!empty($fcm_user['fcm_token'])) {
										$pat_push_obj['id'] = $patient_insert_id;
										$pat_push_obj['type'] = "Waiting List";
										$pat_push_obj['booking_id'] = $_POST['booking_id'];
										$pat_push_obj['booking_date'] = $booking_details['date'];
										$pat_push_obj['doctor_id'] = $booking_details['doctor_id'];
										$pat_push_obj['doctor_name'] = $doctor_data['dr_name'];
										$pat_push_obj['doctor_specialization'] = $doctor_data['dr_specialization'];
										$pat_push_obj['message'] = $text_pat;
										$pat_push_obj['time'] = strtotime($nowin_server);
										$pat_push_obj['to'] = $fcm_user['fcm_token'];
										$user_type = '1'; //patient push
										$push_status = $this->Webservice_model->push_sent($pat_push_obj, $user_type);

									}
									/*------------------------------------------------*/
								} else {

									/*CODE FOR SENTING NOTIFICATION - DOCTOR NOTIFICATION*/

									/*------------------------------------------------*/

									$text = 'A new appointment was scheduled in the system, on '.date('d.m.Y', $booking_details['time_start']).' at '.date('H:i a', $booking_details['time_start']).', patient '.$patient_data['pt_name'];

									$notification = array('doctor_id' => $booking_details['doctor_id'], 'type' => 2, 'message' => $text, 'read_status' => 0, 'time' => strtotime($nowin_server));

									$doctor_insert_id = $this->Webservice_model->insert_notification_doctor($notification);

									$fcm_doctor = $this->Webservice_model->get_doctor_fcm($booking_details['doctor_id']);

									if (!empty($fcm_doctor['fcm_token'])) {
										$doc_push_obj['id'] = $doctor_insert_id;
										$doc_push_obj['type'] = "New Consultation";
										$doc_push_obj['message'] = $text;
										$doc_push_obj['read_status'] = false;
										$doc_push_obj['to'] = $fcm_doctor['fcm_token'];
										$user_type = '2';
										$push_status = $this->Webservice_model->push_sent($doc_push_obj, $user_type);

									}

									/*------------------------------------------------*/

									/*CODE FOR SENTING NOTIFICATION  - PATIENT NOTIFICATION*/

									/*------------------------------------------------*/

									$text_pat = 'Your appointment was scheduled in the system, on '.date('d.m.Y', $booking_details['time_start']).' at '.date('H:i a', $booking_details['time_start']).', doctor '.$doctor_data['dr_name'];

									$notification_pat = array('patient_id' => $booking_details['patient_id'], 'type' => 0, 'message' => $text_pat, 'read_status' => 0, 'time' => strtotime($nowin_server), 'booking_id' => $data['booking_id']);

									$patient_insert_id = $this->Webservice_model->insert_notification_patient($notification_pat);

									$fcm_user = $this->Webservice_model->get_patient_fcm($booking_details['patient_id']);
									if (!empty($fcm_user['fcm_token'])) {
										$pat_push_obj['id'] = $patient_insert_id;
										$pat_push_obj['type'] = "Consultation Confirmation";
										$pat_push_obj['booking_id'] = $data['booking_id'];
										$pat_push_obj['booking_date'] = $booking_details['date'];
										$pat_push_obj['doctor_id'] = $booking_details['doctor_id'];
										$pat_push_obj['doctor_name'] = $doctor_data['dr_name'];
										$pat_push_obj['doctor_specialization'] = $doctor_data['dr_specialization'];
										$pat_push_obj['message'] = $text_pat;
										$pat_push_obj['time'] = strtotime($nowin_server);
										$pat_push_obj['to'] = $fcm_user['fcm_token'];
										$user_type = '1'; //patient push
										$push_status = $this->Webservice_model->push_sent($pat_push_obj, $user_type);

									}
									/*------------------------------------------------*/
								}
								$result = $this->Webservice_model->set_payment_status($data['booking_id']);

								//$res = array('status' => 'success', 'payment_status' => '1', 'message' => 'payment success', 'booking_date' => date('d/m/Y', $check_markbooking['booking_date']), 'booking_slot' => $check_markbooking['booking_slot']);
								$res = array('status' => 'success', 'data' =>array('booking_id'=>$data['booking_id']));

							} else {
								// $res = array('status' => 'fail', 'payment_status'=>'0','message'=>$charge->message);
								$res = array('status' => 'error', 'payment_status' => '0', 'message' => $charge->errors);
							}
						}
					}else{
						$res = array(
							"status" => "error",
							"error" => "error",
							"message" => $payed_data['message']);
					}
				} else {
					$res = array(
							"status" => "error",
							"error" => "required",
							"message" => "All Field is required");
				}
			} else {
				$res = array(
						"status" => "error",
						"error" => "Not Valid",
						"message" => "Authtoken is not valid");
			}
		} else {
			$res = array(
					"status" => "error",
					"error" => "required",
					"message" => "Authtoken is required");
		}
		print json_encode($res);
	}

























}