<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ManageCustomer extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		
		}
		//print_r($this->session->userdata('logged_in'));die();
		$this->load->model('Customer_model');
		//print_r("hhhh");die();
 	}
	
	public function index() {	
		$cus_add = $this->Customer_model->get_customer_data();
		$template['data'] = $cus_add;
		$template['page'] = "ManageCustomers/view_customer";
		$template['page_title'] = "Manage Patient Page";
		$this->load->view('template', $template);
	}

	public function status(){
		$id = $this->uri->segment(3);
		$s=$this->Customer_model->update_status($id);
		$this->session->set_flashdata('message', array('message' => 'Patient Status enable Successfully','class' => 'success'));
		redirect(base_url().'ManageCustomer');
	}

     public function status_active(){
	  $id = $this->uri->segment(3);
 	  $s=$this->Customer_model->enable_status($id);
	  $this->session->set_flashdata('message', array('message' => 'Patient Status disable Successfully','class' => 'success'));
	  redirect(base_url().'ManageCustomer');
	 }	
}
