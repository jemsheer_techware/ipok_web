<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
//		date_default_timezone_set("Asia/Kolkata");
		$this->load->helper(array( 'url')); 
		$this->load->model('Profile_model');
		$this->load->library('form_validation');
		$this->login = 	$this->session->userdata('logged_in');;
 	}
	
	function ProfilePage() {
		
		

			$template['page'] = "profile";
			$template['page_title'] = "Profile Page";
			$template['data'] =  $this->login;
			$this->load->view('template', $template);
	
	}
	function EditProfile() {
		$editdata=array('username'=>$_POST['username'],'display_name'=>$_POST['name']);
		if(isset($_FILES['image'])){
			//print_r($_FILES['image']);die();
			$config = set_upload_options('../assets/uploads'); 
			$config['file_name'] = $_FILES['image']['name'];
			 // $config['upload_path']   = '../assets/uploads/'; 
	   //       $config['allowed_types'] = 'gif|jpg|png'; 
	   //       $config['max_size']      = 100; 
	   //       $config['max_width']     = 1024; 
	   //       $config['max_height']    = 768;  
	         $this->load->library('upload', $config);
				
	         if ( ! $this->upload->do_upload('image')) {
	            $error = array('error' => $this->upload->display_errors()); 
	            //$this->load->view('upload_form', $error); 
	            print_r($error);
	         }
	         else{
	         	//print_r("expression");die();
	         	$imagedata = $this->upload->data(); 
            	$fullfilepath='assets/uploads/'.$imagedata['file_name'];
	         	$editdata['profile_picture'] = $fullfilepath;
	         }
		}                 
      	$uid=$_POST['id'];
      	$result=$this->Profile_model->editProfile($editdata,$uid);
      	if($result){
      		$updateData = $this->login;
      		if($updateData['table'] == 'superadmin' || $updateData['table'] == 'users'){
      			if($updateData['table'] == 'users'){
      				$updateData['display_name'] = decrypt_data($result['display_name']);
      			}else{
      				$updateData['display_name'] = $result['display_name'];
      			}
      			$updateData['profile_picture'] = $result['profile_picture'];
      			$updateData['username'] = $result['username'];
      		}
      		if($updateData['table'] == 'tbl_clinic'){
      			$updateData['display_name'] = $result['name'];
      			$updateData['profile_picture'] = $result['profile_photo'];
      			$updateData['username'] = $result['username'];
      		}
       		$this->session->set_userdata('logged_in',$updateData);
      		redirect(base_url().'profile/ProfilePage');
      	}

         
	}
	function ChangePassword() {
		 $login=$this->session->userdata('logged_in');
		if(isset($_POST)){		
			$this->form_validation->set_rules('password_c', 'Current Password', 'trim|required|callback_currentPassword');
			$this->form_validation->set_rules('password_n', 'New Password', 'trim|required');
			$this->form_validation->set_rules('password_cn', 'Confirm Password', 'trim|required|callback_checkequal');			                  
			if($this->form_validation->run() == TRUE) {
				
				$dataPword=array('password'=>md5($_POST['password_n']));
				$result=$this->Profile_model->update_profileusers($dataPword, $login['id']);
				$this->session->set_flashdata('message', array('message' => 'Password Changed successfully', 'title' => 'Success !', 'class' => 'success'));
				if($result){
					
		          	redirect(base_url().'profile/ProfilePage');
		        }
			}
		}
		$template['page'] = "profile";
		$template['page_title'] = "Profile Page";
		$template['data'] =  $login;
		$this->load->view('template', $template);
	}
	function checkequal(){
		$newPassword=$this->input->post('password_n');
		$confrmPassword=$this->input->post('password_cn');
		if($newPassword == $confrmPassword){
			return true;
		}
		else{
			$this->form_validation->set_message('checkequal', 'Password Doesnot match');

			return false;

		}
	}
	function currentPassword(){
		$currentPword=$this->input->post('password_c');
		$uid=$this->input->post('id');
		$result = $this->Profile_model->checkPassword($currentPword,$uid);
		if($result){
			return true;
		}
		else{
			$this->form_validation->set_message('currentPassword', 'Invalid Current Password');
			return false;
		}
	}
}
