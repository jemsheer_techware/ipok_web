<?php 
class Error extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct(); 
        
        if(!$this->session->userdata('logged_in')) { 
			redirect(base_url());
		}
    } 

    public function index() 
    { 
        $template['page'] = "error_404";
        $template['page_title'] = "You can't access";

        
        $this->load->view('error_404');
    } 
    /* public function my404() 
    { 

        $this->load->view('error_500');
    } */
} 
?> 