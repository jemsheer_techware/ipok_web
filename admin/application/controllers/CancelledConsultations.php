<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CancelledConsultations extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		
		}
		$this->load->model('Cancelled_model');
		
 	}
	
	public function index() {	
		$cus_add = $this->Cancelled_model->get_cancelled_consultation();
		$template['data'] = $cus_add;
		$template['page'] = "CancelledConsultation/view_consultations";
		$template['page_title'] = "Manage Cancelled Consultation Page";
		$this->load->view('template', $template);
	}
}
