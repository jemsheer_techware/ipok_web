<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(!can_access_page()) {
			redirect(base_url()."error");
		}
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
		$this->load->model('Settings_model');		
//		date_default_timezone_set("Asia/Kolkata");
 	}
 	public function index(){
 		$result = $this->Settings_model->get_settings();
 		$template['page'] = "settings";
		$template['page_title'] = "Settings";
		$template['data'] = $result;
		$this->load->view('template', $template);
 	}
 	public function change_sitetitle(){
 		
 		if(isset($_POST) && !empty($_POST)){
 			$updateArray = $_POST;
 			$updateArray['site_title'] = encrypt_data($updateArray['site_title']);
 			$updateArray['mini_title'] = encrypt_data($updateArray['mini_title']);
 			$updateArray['site_title'] = encrypt_data($updateArray['smtp_host']);
 			$updateArray['smtp_username'] = encrypt_data($updateArray['smtp_username']);
 			$updateArray['smtp_password'] = encrypt_data($updateArray['smtp_password']);
 			$result = $this->Settings_model->update_sitetitle($updateArray);
 			if($result){
 				$this->session->set_flashdata('message', array('message' => 'Successfully Changed', 'title' => 'Success !', 'class' => 'success'));
 				// redirect(base_url().'Settings');
 			}
 		}
 		if(isset($_FILES['fav_icon']) || isset($_FILES['site_logo'])){
 			if(isset($_FILES['fav_icon'])){
	 			$fileName = 'fav_icon'.'_'.$_FILES['fav_icon']['name'];
	 		}
	 		else if(isset($_FILES['site_logo'])){
	 			$fileName = 'site_logo'.'_'.$_FILES['site_logo']['name'];
	 		}
				$config = set_upload_options('../assets/uploads/siteImages'); 
	         	$config['file_name'] = $fileName;
	         	$config['overwrite']     = TRUE;
	         	$this->load->library('upload', $config);
		         if ( $this->upload->do_upload('fav_icon')) {
		         	$imagedata = $this->upload->data(); 
					$fullfilepath='assets/uploads/siteImages/'.$imagedata['file_name'];
					$updateArray = array('fav_icon' => $fullfilepath);
					$result = $this->Settings_model->update_sitetitle($updateArray);
		         }
		         if($this->upload->do_upload('site_logo')){
		         	$imagedata = $this->upload->data(); 
					$fullfilepath='assets/uploads/siteImages/'.$imagedata['file_name'];
					$updateArray = array('site_logo' => $fullfilepath);
					$result = $this->Settings_model->update_sitetitle($updateArray);
		         }
		 			if($result){
		 				$this->session->set_flashdata('message', array('message' => 'Successfully Changed', 'title' => 'Success !', 'class' => 'success'));
		 				
		 			}	 		
 		}
 		redirect(base_url().'Settings');
 	}
 	public function imageChange(){
 		
 		if(isset($_FILES['fav_icon'])){
 			$fileName = 'fav_icon'.'_'.$_FILES['fav_icon']['name'];
 		}
 		else if(isset($_FILES['site_logo'])){
 			$fileName = 'site_logo'.'_'.$_FILES['site_logo']['name'];
 		}
			$config = set_upload_options('../assets/uploads/siteImages'); 
         	$config['file_name'] = $fileName;
         	$config['overwrite']     = TRUE;
         	$this->load->library('upload', $config);
	         if ( $this->upload->do_upload('fav_icon')) {
	         	$imagedata = $this->upload->data(); 
				$fullfilepath='assets/uploads/siteImages/'.$imagedata['file_name'];
				$updateArray = array('fav_icon' => $fullfilepath);
	         }
	         else if($this->upload->do_upload('site_logo')){
	         	$imagedata = $this->upload->data(); 
				$fullfilepath='assets/uploads/siteImages/'.$imagedata['file_name'];
				$updateArray = array('site_logo' => $fullfilepath);
	         }
				$result = $this->Settings_model->update_sitetitle($updateArray);
	 			if($result){
	 				$this->session->set_flashdata('message', array('message' => 'Successfully Changed', 'title' => 'Success !', 'class' => 'success'));
	 				redirect(base_url().'Settings');
	 			} 		
 	}
}