<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManagePolicy extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		
		}
		$this->load->model('Policy_model');
		
 	}
	
	public function index() {

		if(isset($_POST) && !empty($_POST)){

			$policy_data = $_POST;
			$policy_add = $this->Policy_model->addPolicy($policy_data);
			if($policy_add == true){
				$this->session->set_flashdata('message', array('message' => 'Successfully Added', 'title' => 'Success !', 'class' => 'success'));
			}
			else{
				$this->session->set_flashdata('message', array('message' => 'Error Occured Policy Not Added', 'title' => 'Error !', 'class' => 'error'));
			}
		}
		$all_policy = $this->Policy_model->get_all_policy();	

		$template['page'] = "ManagePolicy/addPolicy";
		$template['page_title'] = "Manage Policy Page";
		$template['data'] = $all_policy;
		$this->load->view('template', $template);
	}

	function exams_delete($id){
		$delete_data = $this->Exam_model->delete_exams($id);
		if($delete_data){
			$this->session->set_flashdata('message', array('message' => 'Successfully Deleted', 'title' => 'Success !', 'class' => 'success'));
			redirect(base_url().'ManageExams');
		}
	}
	function exams_edit(){
        $id = $this->uri->segment(3);
        if($id == ''){
        	redirect(base_url().'ManageExams/index');
        } 
        else{

			$exam_data = $this->Exam_model->get_single_exam($id);
			if($exam_data != ''){
				$template['page'] = "ManageExams/editExams";
				$template['page_title'] = "Manage Exam Page";
				$template['data'] = $exam_data;
				if(isset($_POST) && !empty($_POST)){
					$data=$_POST;
					$success_update = $this->Exam_model->update_exam($data, $id);
					if($success_update == true){
						$this->session->set_flashdata('message', array('message' => 'Successfully Updated', 'title' => 'Success !', 'class' => 'success'));
					redirect(base_url().'ManageExams');
					}else{
						$this->session->set_flashdata('message', array('message' => 'Sorry Updation Failed. Exams Already Exist', 'title' => 'Error', 'class' => 'error'));
					redirect(base_url().'ManageExams');
					}
				}
	   		}else{
	   			redirect(base_url().'ManageExams/index');
	   		}
		}
		$this->load->view('template', $template);
	}
}