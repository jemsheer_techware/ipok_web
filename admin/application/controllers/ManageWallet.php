<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ManageWallet extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		
		}
		$this->load->model('Wallet_model');
		
 	}
	
	public function index() {	
		$withdraw_data = $this->Wallet_model->get_withdrawal_history();
		//print_r($withdraw_data);exit();
		$template['data'] = $withdraw_data;
		$template['page'] = "ManageWallet/withdrawalHistory";
		$template['page_title'] = "Manage Withdrawals";
		$this->load->view('template', $template);
	}
}
