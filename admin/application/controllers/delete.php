<?php function assignDoctors(){
				if(isset($_POST) && !empty($_POST)){
                 
					$insert_array = array();
					$not_available_day = array();
					$flag = 0;
					$request = $_POST;	
					$this->Clinic_model->update_duration($request['doctors'],$request['duration']);
					if(!empty($request['day'])){
						$result = $this->Clinic_model->checkDoctorExist($request['doctors'],$request['clinicId']);
						//echo "<pre>";	print_r($result);  echo "</pre>";	
						//print_r($result);exit();
						if(!empty($result)){					

							foreach ($result as $key => $value) {
								
									foreach ($request['doctors'] as $key_docId => $value_docId) {
										
										if($value['doctor_id'] == $value_docId){
											$insert_array_exist = array();
											foreach ($request['day'] as $key_day => $value_day) {
												if(isset($value['date'])){
												  
													$decode_time = json_decode($value['date'],true);
													$startTime = $value_day.'_startTime';
													$endTime = $value_day.'_endTime';
													//$interval = $value_day.'_intervalTime';	
													$breakfrom = $value_day.'_Breakfrom';
													$breakto = $value_day.'_Breakto';
													if($request[$breakfrom] != ''){
														$break_from = $request[$breakfrom];
													}
													else{
														$break_from = '';
													}

													if($request[$breakto] != ''){
														$break_to = $request[$breakto];
													}
													else{
														$break_to = '';
													}

																					if(!empty($decode_time)){
													    				foreach ($decode_time as $k => $v) {
														if($value_day == $v['day']){
															if((strtotime($this->string->default_date.$v['time']['start']) <= strtotime($this->string->default_date.$request[$startTime ]) && strtotime($this->string->default_date.$request[$startTime ]) <= strtotime($this->string->default_date.$v['time']['end'])) || (strtotime($this->string->default_date.$v['time']['start']) <= strtotime($this->string->default_date.$request[$endTime]) && strtotime($this->string->default_date.$request[$endTime ]) <= strtotime($this->string->default_date.$v['time']['end'])) || (strtotime($this->string->default_date.$request[$startTime ]) <= strtotime($this->string->default_date.$v['time']['start']) && strtotime($this->string->default_date.$v['time']['start']) <=strtotime($this->string->default_date.$request[$endTime])) || (strtotime($this->string->default_date.$request[$startTime ]) <= strtotime($this->string->default_date.$v['time']['end']) && strtotime($this->string->default_date.$v['time']['end']) <= strtotime($this->string->default_date.$request[$endTime]))){
															
																array_push($not_available_day , array('day'=>$value_day,'id'=>$value_docId));

																$flag = 1;

															}
															else{
																
																$res = array('day'=>$value_day,
																			'time'=>array('start'=>$request[$startTime ],
																					'end'=>$request[$endTime],
																					'interval'=>$request['duration']),
																					'break_from'=>$break_from,
																					'break_to'=>$break_to
																		);
																array_push($insert_array_exist,$res);

																
															}
														}
														else if(!in_array($value_day, $v)){
																$res = array('day'=>$value_day,
																			'time'=>array('start'=>$request[$startTime ],
																					'end'=>$request[$endTime],
																					'interval'=>$request['duration'],
																					'break_from'=>$break_from,
																					'break_to'=>$break_to));
																array_push($insert_array_exist,$res);
														}
													}
													}		
																					
												}										
											}
										}
									}
							}
							if($flag ==0 && !empty($insert_array_exist))
							{
											
								$this->Clinic_model->assignDoctors($request['doctors'],$request['clinicId']);
								$this->Clinic_model->set_new_consultation($insert_array_exist,$request['clinicId'],$request['doctors']);
								$this->session->set_flashdata('message', array('message' => 'Successfully assigned', 'title' => 'Success !', 'class' => 'success'));
							redirect(base_url()."ManageClinic/view");
							}
							else
							{	
							  
								$this->session->set_flashdata('message', array('message' => 'Selected Doctors are not available at this time schedule', 'title' => 'Error !', 'class' => 'danger'));
								redirect(base_url()."ManageClinic/addDoctor?id=".$request['clinicId']);
							}											
												
						}
						else{
							foreach ($request['day'] as $key_elseDay => $value_elseDay) {
								
								$start = $value_elseDay.'_startTime';
								$end = $value_elseDay.'_endTime';
								//$interval = $value_elseDay.'_intervalTime';
								$breakfrom = $value_elseDay.'_Breakfrom';
								$breakto = $value_elseDay.'_Breakto';
								if($request[$breakfrom] != ''){
									$break_from = $request[$breakfrom];
								}
								else{
									$break_from = '';
								}

								if($request[$breakto] != ''){
									$break_to = $request[$breakto];
								}
								else{
									$break_to = '';
								}

								$res = array('day'=>$value_elseDay,
											'time'=>array('start'=>$request[$start],
												'end'=>$request[$end],
												'interval'=>$request['duration'],
												'break_from'=>$break_from,
												'break_to'=>$$break_to));
								array_push($insert_array, $res);
							}
							$this->Clinic_model->assignDoctors($request['doctors'],$request['clinicId']);
							$this->Clinic_model->set_new_consultation($insert_array,$request['clinicId'],$request['doctors']);
							$this->session->set_flashdata('message', array('message' => 'Successfully assigned', 'title' => 'Success !', 'class' => 'success'));
							if($this->session->userdata('logged_in')['uType'] != 'clinic'){

							redirect(base_url()."ManageClinic/view");
							}
							else{
								redirect(base_url()."ManageDoctors/view");
							}
						}
					}
					else{

						$this->Clinic_model->assignDoctors($request['doctors'],$request['clinicId']);
						$this->Clinic_model->set_new_consultation('',$request['clinicId'],$request['doctors']);
						$this->session->set_flashdata('message', array('message' => 'Successfully assigned', 'title' => 'Success !', 'class' => 'success'));
						if($this->session->userdata('logged_in')['uType'] != 'clinic'){

							redirect(base_url()."ManageClinic/view");
						}
						else
						{
							redirect(base_url()."ManageDoctors/view");
						}
					}				
				}
				else{
					redirect(base_url()."error");
				}
			}
			?>