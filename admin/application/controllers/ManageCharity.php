<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManageCharity extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		
		}
		$this->load->model('Charity_model');
		
 	}
	
	public function index() {

		if(isset($_POST) && !empty($_POST)){

			$data = $_POST;
            $result = $this->Charity_model->addCharity($data);
            if($result['status'] == 'success'){

				$fileName =$_FILES['image']['name'];
				$fileName = str_replace('%','a',$fileName);
				$config = set_upload_options('../assets/uploads/profilepic/charity'); 
	         	$config['file_name'] = $fileName;
	         	$this->load->library('upload', $config);
		        if ( ! $this->upload->do_upload('image')) {
		            $error = array('error' => $this->upload->display_errors('', '')); 
		            $res = array(
							"status"=> "error",
							"error"=> "Upload Error",
							"message"=> "Sorry! Charity Image not uploaded".$error['error']
						);	
		            
		            $this->Charity_model->delete_register($result['data']);
		            $ress =$res;
		        }	
		        else { 				         	
		            $imagedata = $this->upload->data(); 
		            $data['image']='assets/uploads/profilepic/charity/'.$imagedata['file_name'];
		            $ress = $this->Charity_model->add_immage($result['data'],$data);
				}
				if($ress['status'] == 'success'){
					$this->session->set_flashdata('message', array('message' => 'Successfully Added', 'title' => 'Success !', 'class' => 'success'));
				}else{
					$this->session->set_flashdata('message', array('message' => $ress['message'], 'title' => 'Error !', 'class' => 'error'));
				}

			}elseif($result == false){
				$this->session->set_flashdata('message', array('message' => 'Sorry Not Inserted. Charity Already Exist', 'title' => 'Error !', 'class' => 'error'));
			}
			else{
				$this->session->set_flashdata('message', array('message' => $result['message'], 'title' => 'Error !', 'class' => 'error'));
			}
		}
		$all_charity = $this->Charity_model->get_all_charity();	

		$template['page'] = "ManageCharity/addCharity";
		$template['page_title'] = "Manage Charity Page";
		$template['data'] = $all_charity;
		$this->load->view('template', $template);
	}

	function charity_delete($id){
		$delete_data = $this->Charity_model->charity_delete($id);
		if($delete_data){
			$this->session->set_flashdata('message', array('message' => 'Successfully Deleted', 'title' => 'Success !', 'class' => 'success'));
			redirect(base_url().'ManageCharity');
		}
	}
	function charity_edit(){
        $id = $this->uri->segment(3);
        if($id == ''){
        	redirect(base_url().'ManageCharity/index');
        } 
        else{

			$charity_data = $this->Charity_model->get_single_charity($id);
			if($charity_data != ''){
				$template['page'] = "ManageCharity/editCharity";
				$template['page_title'] = "Manage Charity Page";
				$template['data'] = $charity_data;
				if(isset($_POST) && !empty($_POST)){
					$data=$_POST;
					$success_update = $this->Charity_model->update_charity($data, $id);
					//print_r($success_update);exit();
					if($success_update == true){
						if($_FILES['image']['name'] != ''){
							$fileName =$_FILES['image']['name'];
							$fileName = str_replace('%','a',$fileName);
							$config = set_upload_options('../assets/uploads/profilepic/charity'); 
				         	$config['file_name'] = $fileName;
				         	$this->load->library('upload', $config);
					        if ( ! $this->upload->do_upload('image')) {
					            $error = array('error' => $this->upload->display_errors('', '')); 
					            $res = array(
										"status"=> "error",
										"error"=> "Upload Error",
										"message"=> "Sorry! Charity Image not uploaded".$error['error']
									);	
					            
					            $this->Charity_model->delete_register($id);
					            $ress =$res;
					        }	
					        else { 				         	
					            $imagedata = $this->upload->data(); 
					            $data['image']='assets/uploads/profilepic/charity/'.$imagedata['file_name'];
					           
							}
						}else{
							$data['image'] = $charity_data['image'];
						}
						$ress = $this->Charity_model->add_immage($id,$data);
						if($ress['status']=='success'){
						 	$this->session->set_flashdata('message', array('message' => 'Successfully Updated', 'title' => 'Success !', 'class' => 'success'));
							redirect(base_url().'ManageCharity');
						}else{
						 	$this->session->set_flashdata('message', array('message' => $result['message'], 'title' => 'Success !', 'class' => 'success'));
							redirect(base_url().'ManageCharity');
						}	
					}else{
						$this->session->set_flashdata('message', array('message' => 'Sorry Updation Failed. charity Already Exist', 'title' => 'Error', 'class' => 'error'));
					redirect(base_url().'ManageCharity');
					}
				}
	   		}else{
	   			redirect(base_url().'ManageCharity/index');
	   		}
		}
		$this->load->view('template', $template);
	}

	public function add_services(){

		if(isset($_POST) && !empty($_POST)){

			$service_data = $_POST;
			$service_add = $this->Charity_model->addServices($service_data);
			if($service_add['status'] == 'success'){
				$this->session->set_flashdata('message', array('message' => $service_add['message'], 'title' => 'Success !', 'class' => 'success'));
			}
			else{
				$this->session->set_flashdata('message', array('message' => $service_add['message'], 'title' => 'Error !', 'class' => 'error'));
			}
		}
		$all_clinic = $this->Charity_model->get_all_clinic();	
		$all_charity = $this->Charity_model->get_all_charity();
		$all_services = $this->Charity_model->get_all_services();
		$template['page'] = "ManageCharity/addServices";
		$template['page_title'] = "Manage Service Page";
		$template['clinic'] = $all_clinic;
		$template['data'] = $all_charity;
		$template['services'] = $all_services;
		$this->load->view('template', $template);
	}

	public function service_delete($id){
		$delete_data = $this->Charity_model->service_delete($id);
		if($delete_data){
			$this->session->set_flashdata('message', array('message' => 'Successfully Deleted', 'title' => 'Success !', 'class' => 'success'));
			redirect(base_url().'ManageCharity/add_services');
		}
	}

	public function service_edit($id){
		  $id = $this->uri->segment(3);
        if($id == ''){
        	redirect(base_url().'ManageCharity/add_services');
        } 
        else{

			$service_data = $this->Charity_model->get_single_service($id);
			if($service_data != ''){
				$all_clinic = $this->Charity_model->get_all_clinic();	
				$all_charity = $this->Charity_model->get_all_charity();
				$template['clinic'] = $all_clinic;
				$template['data'] = $all_charity;
				$template['page'] = "ManageCharity/editServices";
				$template['page_title'] = "Manage Service Page";
				$template['datas'] = $service_data;
				if(isset($_POST) && !empty($_POST)){
					$data=$_POST;
					$success_update = $this->Charity_model->update_service($data, $id);
					if($success_update['status'] == 'success'){
						$this->session->set_flashdata('message', array('message' => $success_update['message'], 'title' => 'Success !', 'class' => 'success'));
					redirect(base_url().'ManageCharity/add_services');
					}else{
						$this->session->set_flashdata('message', array('message' => $success_update['message'], 'title' => 'Error', 'class' => 'error'));
					redirect(base_url().'ManageCharity/add_services');
					}
				}
	   		}else{
	   			redirect(base_url().'ManageCharity/add_services');
	   		}
		}
		$this->load->view('template', $template);
	}
}