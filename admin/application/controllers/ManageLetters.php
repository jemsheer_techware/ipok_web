<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManageLetters extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		
		}
		$this->load->model('Letter_model');
		
 	}
	
	public function index() {

		if(isset($_POST) && !empty($_POST)){

			$letter_data = $_POST;
			$letter_add = $this->Letter_model->addLetter($letter_data);
			if($letter_add == 4){
				$this->session->set_flashdata('message', array('message' => 'Successfully Added', 'title' => 'Success !', 'class' => 'success'));
			}else if($letter_add == 3){
				$this->session->set_flashdata('message', array('message' => 'Error Occured! This Code Already Exist', 'title' => 'Error !', 'class' => 'error'));

			}else if($letter_add == 2){
				$this->session->set_flashdata('message', array('message' => 'Error Occured! Disease Already Exist', 'title' => 'Error !', 'class' => 'error'));

			}
			else{
				$this->session->set_flashdata('message', array('message' => 'Error Occured Disease with this Code Already Exist', 'title' => 'Error !', 'class' => 'error'));
			}
		}
		$all_letter = $this->Letter_model->get_all_letters();	
 		$all_certificate = $this->Letter_model->get_all_certificates();
		$template['page'] = "ManageLetters/addLetter";
		$template['page_title'] = "Manage Budget Page";
		$template['data'] = $all_letter;
		$template['certificate'] = $all_certificate;
		$this->load->view('template', $template);
	}

	function letter_delete($id){
		$delete_data = $this->Letter_model->delete_letter($id);
		if($delete_data){
			$this->session->set_flashdata('message', array('message' => 'Successfully Deleted', 'title' => 'Success !', 'class' => 'success'));
			redirect(base_url().'ManageLetters');
		}
	}
	function letter_edit(){
        $id = $this->uri->segment(3);
        //print_r($id);exit();
        if($id == ''){
        	redirect(base_url().'ManageLetters/index');
        } 
        else{

			$letter_data = $this->Letter_model->get_single_letter($id);
			if($letter_data != ''){
				$template['page'] = "ManageLetters/editLetter";
				$template['page_title'] = "Manage letter Page";
				$template['data'] = $letter_data;
				if(isset($_POST) && !empty($_POST)){
					$data=$_POST;
					$success_update = $this->Letter_model->update_letter($data, $id);
					if($success_update == 4){
						$this->session->set_flashdata('message', array('message' => 'Successfully Updated', 'title' => 'Success !', 'class' => 'success'));
						redirect(base_url().'ManageLetters');
					}else if($success_update == 3){
						$this->session->set_flashdata('message', array('message' => 'Error Occured! This Code Already Exist', 'title' => 'Error !', 'class' => 'error'));
						redirect(base_url().'ManageLetters');

					}else if($success_update == 2){
						$this->session->set_flashdata('message', array('message' => 'Error Occured! Disease Already Exist', 'title' => 'Error !', 'class' => 'error'));
						redirect(base_url().'ManageLetters');
					}
					else{
						$this->session->set_flashdata('message', array('message' => 'Sorry Updation Failed. Disease with this Code Already Exist', 'title' => 'Error', 'class' => 'error'));
						redirect(base_url().'ManageLetters');
					}
				}
	   		}else{
	   			redirect(base_url().'ManageLetters/index');
	   		}
		}
		$this->load->view('template', $template);
	}

	public function add_certificates(){
		$letter_data = $_POST;
		$letter_add = $this->Letter_model->addCertificate($letter_data);
		if($letter_add){
			$this->session->set_flashdata('message', array('message' => 'Successfully Added', 'title' => 'Success !', 'class' => 'success'));
			redirect(base_url().'ManageLetters');
		}else{
			$this->session->set_flashdata('message', array('message' => 'Sorry Not Added', 'title' => 'Error !', 'class' => 'error'));
			redirect(base_url().'ManageLetters');
		}
	}
}