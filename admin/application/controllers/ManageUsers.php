<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ManageUsers extends CI_Controller {

	public function __construct() {
		parent::__construct();				
		if(!can_access_page()) {
			redirect(base_url()."error");
		}
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}		
		$this->load->model("User_model");
		$this->load->model("Role_model");		
 	}
	public function index()
	{		
			if(isset($_POST) and !empty($_POST)) {			
				$userdata = $_POST;				
				$config = array();
				$config['upload_path'] = '../assets/uploads/profilepic/users';
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['max_size']      = '0';
				$config['overwrite']     = FALSE;				
				$new_name = time()."_".$_FILES["profile_pic"]['name'];
				$config['file_name'] = $new_name;
				$this->load->library('upload', $config);				
				$this->upload->initialize($config);				
				if ( $this->upload->do_upload('profile_pic')) {
					$upload_data = $this->upload->data();					
					$userdata['profile_picture'] = 'assets/uploads/profilepic/users'."/".$upload_data['file_name'];			
					$userdata['password']=md5($userdata['password']);
					$result=$this->User_model->addusers($userdata);
					if($result==true){						
						$this->session->set_flashdata('message', array('message' => 'User saved successfully', 'title' => 'Success !', 'class' => 'success'));
						redirect(base_url()."ManageUsers");
					}
					else{						
						$this->session->set_flashdata('message', array('message' => 'Username already exist', 'title' => 'Warning !', 'class' => 'warning'));
					}
				}
				else{
					$errors = $this->upload->display_errors();
	                $this->session->set_flashdata('message', array('message' => 'Sorry,'.$errors, 'title' => 'Error !', 'class' => 'danger'));
				}				
			}
			else{
				$template['page'] = "ManageUser/addUser";
				$template['page_title'] = "Add New User";
				$template['roles'] = $this->Role_model->get_roles();
				$template['data']=$this->User_model->get_users();
				$this->load->view('template', $template);
			}
	}
	public function user_edit($id) {	
		if($id == ''){
			redirect(base_url()."ManageUsers/index");
		}else{	
			$template['page'] = "ManageUser/editUser";
			$template['page_title'] = "User Edit";
			$getsingleuser = $this->User_model->get_singleuser($id);
			if($getsingleuser[0] == ''){
				redirect(base_url()."ManageUsers/index");
			}else{
				$template['data'] = $getsingleuser;
				$template['roles'] = $this->Role_model->get_roles();
				if(isset($_POST) && !empty($_POST)){
					$data=$_POST;			
					if(isset($_FILES['profile_pic'])){
						$config = array();
						$config['upload_path'] = '../assets/uploads/profilepic/users';
						$config['allowed_types'] = 'gif|jpg|jpeg|png';
						$config['max_size']      = '0';
						$config['overwrite']     = FALSE;				
						$new_name = time()."_".$_FILES["profile_pic"]['name'];
						$config['file_name'] = $new_name;
						$this->load->library('upload',$config);				
						$this->upload->initialize($config);				
						if ( $this->upload->do_upload('profile_pic')) {
						$upload_data = $this->upload->data();
						$data['profile_picture'] = 'assets/uploads/profilepic/users'."/".$upload_data['file_name'];
						}
						else{
							$errors = $this->upload->display_errors();					
						}
					}		

		//			date_default_timezone_set("Asia/Kolkata");			
					$returnData=$this->User_model->update_users($data, $id);
					if($returnData==false){
											$this->session->set_flashdata('message', array('message' => 'Username already exist', 'title' => 'Warning !', 'class' => 'warning'));
					}
				}
			}
		}
		$this->load->view('template', $template);
	}
	public function user_delete($id){
		$this->User_model->delete_users($id);
		$this->session->set_flashdata('message', array('message' => 'Successfully Deleted', 'title' => 'Success !', 'class' => 'Success'));
		redirect(base_url()."ManageUser");
	}
	
}
