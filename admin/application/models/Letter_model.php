<?php 

class Letter_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}
 	function addLetter($letterData){

 		$check_letter_name = $this->db->get_where('tbl_disease_code',array('disease_name'=>$letterData['disease_name'],'code'=>$letterData['code']))->row();
 		if(!$check_letter_name){
 			$check_disease_name = $this->db->get_where('tbl_disease_code',array('disease_name'=>$letterData['disease_name']))->row();
 			if(!$check_disease_name){
 				$check_code_name = $this->db->get_where('tbl_disease_code',array('code'=>$letterData['code']))->row();
 				if(!$check_code_name){
 					if($this->db->insert('tbl_disease_code',array('disease_name'=>ucfirst($letterData['disease_name']),'code'=>$letterData['code']))){
 						return 4;
 					}

 				}else{
 					return 3;
 				}

 			}else{
 				return 2;
 			}
 		}else{
 			return 1;
 		}
 	}

 	function get_all_letters(){
 		$all_letter = $this->db->get('tbl_disease_code');
 		if($all_letter->num_rows() > 0){
 			$result = $all_letter->result_array();
 			return $result;
 		}
 	}
 	function delete_letter($id){
 		if($this->db->delete('tbl_disease_code',array('id'=>$id))){
 			return true;
 		}
 	}
 	function get_single_letter($id){
 		$single_letter = $this->db->get_where('tbl_disease_code',array('id' => $id));
 		if($single_letter->num_rows() > 0){
 			return $single_letter->row_array();
 		}
 	}
 	function update_letter($data,$id){
 		$this->db->where('id !=',$id);
 		$this->db->where('disease_name',$data['disease_name']);
 		$this->db->where('code',$data['code']);
 		$check_letter_name = $this->db->get('tbl_disease_code')->row();
 		if(!$check_letter_name){
 			$this->db->where('id !=',$id);
	 		$this->db->where('disease_name',$data['disease_name']);
	 		$check_disease_name = $this->db->get('tbl_disease_code')->row();
	 		if(!$check_disease_name){
	 			$this->db->where('id !=',$id);
		 		$this->db->where('code',$data['code']);
		 		$check_code_name = $this->db->get('tbl_disease_code')->row();
		 		if(!$check_code_name){
		 			if($this->db->update('tbl_disease_code',array('disease_name'=>ucfirst($data['disease_name']),'code'=>$data['code']),array('id'=>$id))){
		 				return 4;
		 			}

		 		}else{
		 			return 3;
		 		}
	 		}else{
	 			return 2;
	 		}
 		}else{
 			return 1;
 		}
 		
 	}

 	public function addCertificate($data){
 		$this->db->update('tbl_policy',array('cid_letter'=>$data['cid_letter'],'letter'=>$data['letter']),array('id'=>'1'));
 		
 		return true;
 	}

 	public function get_all_certificates(){
 		return $this->db->get_where('tbl_policy')->row();
 	}
 }