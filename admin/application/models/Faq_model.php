<?php 

class Faq_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}

 	function addFaq($data){
 		if($this->db->insert('tbl_faq',array("faq_title"=>ucfirst($data['faq_title']),"faq_description"=>ucfirst($data['faq_description']),'faq_for'=>'0'))){
 			return true;
 		}
 	}

 	function addFaqDoctor($data){
 		if($this->db->insert('tbl_faq',array("faq_title"=>ucfirst($data['faq_title']),"faq_description"=>ucfirst($data['faq_description']),'faq_for'=>'1'))){
 			return true;
 		}
 	}

 	function get_all_faqs(){
 		$this->db->where('faq_for','0');
 		$all_faqs = $this->db->get('tbl_faq');
 		if($all_faqs->num_rows() > 0){
 			$result = $all_faqs->result_array();
 			return $result;
 		}
 	}

 	function get_all_faqs_doctor(){
 		$this->db->where('faq_for','1');
 		$all_faqs = $this->db->get('tbl_faq');
 		if($all_faqs->num_rows() > 0){
 			$result = $all_faqs->result_array();
 			return $result;
 		}
 	}

 	function delete_faq($id){
 		if($this->db->delete('tbl_faq',array('id'=>$id))){
 			return true;
 		}
 	}
 	function get_single_faq($id){
 		$single_faq = $this->db->get_where('tbl_faq',array('id' => $id));
 		if($single_faq->num_rows() > 0){
 			return $single_faq->row();
 		}
 	}

 	function update_faq($data,$id){
	 		if($this->db->update('tbl_faq',array('faq_title'=>ucfirst($data['faq_title']),'faq_description'=>ucfirst($data['faq_description'])),array('id'=>$id))){
	 			return true;
	 		}
 		
 	}
 }