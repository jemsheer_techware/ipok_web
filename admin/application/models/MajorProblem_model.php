<?php 

class MajorProblem_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}
 	function addProblems($data){
 		$this->db->where('problem_name',$data['problem_name']);
 		if($this->db->get('tbl_major_problems')->row()){
 			return false;
 		}else{
 			$datas = array('problem_name'=>ucfirst(strtolower($data['problem_name'])));
 			if($this->db->insert('tbl_major_problems',$datas)) {
 				return true;
 			}
 		}
 	}
 	function get_all_Problems(){
 		return $this->db->get('tbl_major_problems')->result();
 		
 	}
 	function delete_problem($id){
 		if($this->db->delete('tbl_major_problems',array('id'=>$id))){
 			return true;
 		}
 	}
 	function get_single_problem($id){
 		$this->db->where('id',$id);
 		return $this->db->get('tbl_major_problems')->row();
 	}
 	function update_problem($data,$id){
 		$this->db->where('id !=',$id);
 		$this->db->where('problem_name',$data['problem_name']);
 		if($this->db->get('tbl_major_problems')->row()){
 			return false;
 		}else{
	 		$datas = array('problem_name'=>ucfirst(strtolower($data['problem_name'])));
	 		$this->db->where('id',$id);
	 		if($this->db->update('tbl_major_problems',$datas)){
	 			return true;
	 		}
 		}
 	}

 	function get_all_subProblems(){
 		$this->db->select('tbl_major_subproblems.subproblem_name,tbl_major_subproblems.id,tbl_major_problems.problem_name');
 		$this->db->join('tbl_major_problems','tbl_major_problems.id = tbl_major_subproblems.problem_category_id');
 		return $this->db->get('tbl_major_subproblems')->result();
 	}

 	function addSubProblems($data){
 		$this->db->where('problem_category_id',$data['problem_category_id']);
 		$this->db->where('subproblem_name',$data['subproblem_name']);
 		if($this->db->get('tbl_major_subproblems')->row()){
 			return false;
 		}else{
 			$datas = array('problem_category_id'=>$data['problem_category_id'],'subproblem_name'=>ucwords($data['subproblem_name']));
 			if($this->db->insert('tbl_major_subproblems',$datas)){
 				return true;
 			}
 		}
 	}

 	function get_single_subProblem($id){
 		return $this->db->get_where('tbl_major_subproblems',array('id'=>$id))->row();
 	}

 	function update_subProblem($data,$id){
 		$this->db->where('id !=',$id);
 		$this->db->where('problem_category_id',$data['problem_category_id']);
 		$this->db->where('subproblem_name',$data['subproblem_name']);
 		if($this->db->get('tbl_major_subproblems')->row()){
 			return false;
 		}else{
            if($this->db->update('tbl_major_subproblems',array("problem_category_id"=>$data['problem_category_id'],"subproblem_name"=>ucwords($data['subproblem_name'])),array('id'=>$id))){
            	return true;
            }
 		}
 	}

 	function delete_subproblem($id){
 		if($this->db->delete('tbl_major_subproblems',array('id'=>$id))){
 			return true;
 		}
 	}
 }