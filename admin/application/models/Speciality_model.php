<?php 

class Speciality_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}
 	public function addSpeciality($data){
 		if(isset($data['speciality_name']) && strlen($data['speciality_name'])){
 			$get_count = $this->db->get_where('tbl_specialization',array('specialization_name'=>$data['speciality_name']));
 			if($get_count->num_rows() > 0){
 				$rseult = array('status'=>'error','msg'=>'already exist');
 			}
 			else if($this->db->insert('tbl_specialization',array('specialization_name'=>ucfirst($data['speciality_name']),'sub_name'=>$data['speciality_name']))){
 				$result = array('status'=>'success');

 			}
 			else{
 				$rseult = array('status'=>'error','msg'=>'insertion failed');
 			}
 			return $result;
 		}
 	}
 	public function get_speciality(){
 		$result = $this->db->get('tbl_specialization');
 		return $result->result_array();
 	}
 	public function get_single_speciality($id){
 		$result = $this->db->get_where('tbl_specialization',array('id'=>$id));
 		return $result->row_array();
 	}
 	public function delete_speciality($id){
 		
 		$get_count_speciality = $this->db->get_where('tbl_doctors',array('specialization'=>$id))->result();
 		//print_r(count($get_count_speciality));die();
 		if(count($get_count_speciality) > 0){
 			$result = array('status'=>'error','msg'=>'already exist');
 		}
 		else if($this->db->delete('tbl_specialization',array('id'=>$id))){
 			$result = array('status'=>'success');
 		}
 		return $result;
 	}
 	public function edit_speciality($data,$id){
 		$data['specialization_name'] = ucfirst($data['specialization_name']);
 		$this->db->where('specialization_name',$data['specialization_name']);
 		$this->db->where('id !=',$id);
 		$get_count = $this->db->get('tbl_specialization')->result();
 		
		if(count($get_count) <= 0){
	 		if($this->db->update('tbl_specialization',$data,array('id'=>$id))){
	 			return true;
	 		}
 		}
 		else{
 			return false;
 		}
 	}
}