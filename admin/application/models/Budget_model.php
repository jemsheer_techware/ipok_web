<?php 

class Budget_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}
 	function addBudget($budgetData){

 		/*$check_budget_name = $this->db->get_where('tbl_budget',array('budget_procedure'=>$budgetData['budget_procedure']));
 		if($check_budget_name->num_rows() > 0){
 			return false;
 		}else{*/
 			if($this->db->insert('tbl_budget',array('budget_procedure'=>ucfirst($budgetData['budget_procedure']),'amount'=>ucfirst($budgetData['amount']),'quantity'=>$budgetData['quantity']))) {
 				return true;
 			}
 		//}
 	}
 	function get_all_budget(){
 		$all_budget = $this->db->get('tbl_budget');
 		if($all_budget->num_rows() > 0){
 			$result = $all_budget->result_array();
 			return $result;
 		}
 	}
 	function delete_budget($id){
 		if($this->db->delete('tbl_budget',array('id'=>$id))){
 			return true;
 		}
 	}
 	function get_single_budget($id){
 		$single_exams = $this->db->get_where('tbl_budget',array('id' => $id));
 		if($single_exams->num_rows() > 0){
 			return $single_exams->row_array();
 		}
 	}
 	function update_budget($data,$id){
 	/*	$this->db->where('budget_procedure',$data['budget_procedure']);
 		$this->db->where('id !=',$id);
 		$res = $this->db->get('tbl_budget')->row();
 		if($res){
          return false;
 		}else{*/
	 		if($this->db->update('tbl_budget',array('budget_procedure'=>ucfirst($data['budget_procedure']),'amount'=>ucfirst($data['amount']),'quantity'=>$data['quantity']),array('id'=>$id))){
	 			return true;
	 		}
 		//}
 	}
 }