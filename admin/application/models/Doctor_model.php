<?php 
class Doctor_model extends CI_Model {	
	public function _consruct(){
		parent::_construct();
 	}
 	function get_all_doctor(){
 		$doctor_data = $this->db->get("tbl_doctors");
 		return $doctor_data->result_array();
 	}
 	function get_bookinglist($doctorid){
 		$this->db->select("tbl_booking.*,tbl_doctors.name,tbl_doctors.profile_pic,tbl_clinic.name as clinicName");
 		$this->db->from('tbl_booking');
 		$this->db->join('tbl_doctors','tbl_doctors.id = tbl_booking.doctor_id','INNER');
 		$this->db->join('tbl_clinic','tbl_clinic.id = tbl_booking.clinic_id','left');
 		if($doctorid != 0){

 			$this->db->where('tbl_booking.doctor_id',$doctorid);
 		}
 		$result = $this->db->get();
 		return $result->result_array();
 	}

 	function get_specializations(){
 		return $this->db->get('tbl_specialization')->result_array();
 	}


  	 public function update_status($id){
 	 	$data = array('account_status'=>'1');
 	 	$this->db->where('id',$id);
 	 	$this->db->update('tbl_doctors',$data);
 	 	return "success";
 	 }

 	 public function enable_status($id){
 	 	$data = array('account_status'=>'0');
 	 $this->db->where('id',$id);
 	 	$result = $this->db->update('tbl_doctors',$data);
 	 	//echo $this->db->last_query();
 	 	return "success";
	}

}