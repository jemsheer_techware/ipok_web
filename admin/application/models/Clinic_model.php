<?php 

class Clinic_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}
 	function dataExist($data){
		if(isset($data['username']) && strlen($data['username'])){
			$query_uname = $this->db->get_where("tbl_clinic",array("username"=>$data['username'])); 
		}
		if(isset($data['email']) && strlen($data['email'])){
			$query_email = $this->db->get_where("tbl_clinic",array("email"=>$data['email']));
		}
		if(isset($query_uname))
		{
			if($query_uname->num_rows() > 0){
				if(isset($query_email)){
					if($query_email->num_rows() > 0){
						$return_array = array('message'=>'username and email are already exist');
					}
					else{
						$return_array = array('message'=>'username already exist');
					}
				}
				else{
					$return_array = array('message'=>'username already exist');
				}
			}
			else{
				$return_array = array('message'=>'success');
			}
		}
		else if(isset($query_email)){
			if($query_email->num_rows() > 0){
				$return_array = array('message'=>'email already exist');
			}
			else{
				$return_array = array('message'=>'success');
			}
		}
		else{
			$return_array = array('message'=>'fail');
		}
		return $return_array;
	}
	function register($data){
		if($this->db->insert('tbl_clinic', $data)){
				$insertid = $this->db->insert_id();
				
				$query = $this->db->get_where("tbl_clinic",array("id"=>$insertid)); 
				$return_array = array('status'=>'success','userdata'=>$query->row_array());
		}
		else{
			$return_array = array('status'=>'fail');
		}
		return $return_array;
	}
	function authtoken_registration($authtoken,$userid){
		$data = array('authtoken'=>$authtoken,'clinic_id'=>$userid);
		if($this->db->insert('tbl_authtoken_clinic', $data)){
			return true;
		}
		else{
			return false;
		}
	}
	function delete_registration($uid){
		 if($this->db->where_in('id', $uid)->delete('tbl_clinic')){
		 }
	}
	function updatePic($data,$id){
		$this->db->update('tbl_clinic', $data, array('id' => $id));
	}
	function get_all_clinic(){
 		$doctor_data = $this->db->get("tbl_clinic");
 		return $doctor_data->result_array();
 	}
 	function update_duration($id,$duration){
 		if($this->db->update('tbl_doctors',array('consultation_duration'=>$duration),array('id'=>$id))){
 			return true;
 		}
 	}
 	function assignDoctor($data){
 		
 		foreach ($data as $key => $value) {
 			if($key != 'clinicId'){
 				$this->db->insert("tbl_clinic_doctors",array('clinic_id'=>$data['clinicId'],'doctor_id'=>$key));
 			}
 		}
 	}
 	function get_exist_doctor($id){
 		
 		$all_doctor = $this->db->get_where("tbl_clinic_doctors",array('clinic_id'=>$id));
 		return $all_doctor->result_array();
 	}
 	function checkDoctorExist($data,$clinicId){
 		$this->db->select("*");
 		$this->db->from("tbl_consultation");
 		$this->db->where_in("tbl_consultation.doctor_id",$data);
 		//$this->db->where("tbl_consultation.clinic_id",$clinicId);
 		$query = $this->db->get();
 		return $query->result_array();
 		
 	}

 	public function get_doctor_duration($id){
 		$this->db->select('consultation_duration');
 		return $this->db->get_where('tbl_doctors',array('id'=>$id))->row();
 		//echo $this->db->last_query();
 	}
 	/**********Reeba*********************/
 	/*function set_new_consultation($data,$clinicId,$doctors){ 		
 		$newData = json_encode($data);
 		foreach ($doctors as $key => $value) { 		
 			$this->db->insert('tbl_consultation',array('doctor_id'=>$value,'clinic_id'=>$clinicId,'date'=>$newData));
 		}
 	}*/
 	/**********Reeba*********************/

 	function set_new_consultation($data,$clinicId,$doctors){ 
 	//print_r($data);		
 		$newData = json_encode($data);
         //print_r($newData);exit();
 		    $this->db->insert('tbl_consultation',array('doctor_id'=>$doctors,'clinic_id'=>$clinicId,'date'=>$newData,'date_secondary'=>$newData));		
 	}
 	function assignDoctors($doctors,$clinicId){
	
 			$this->db->insert('tbl_clinic_doctors',array('doctor_id'=>$doctors,'clinic_id'=>$clinicId));
 	}
 	function getDoctor($doctorId){
 		$this->db->select('tbl_doctors.name');
 		$query = $this->db->get_where('tbl_doctors',array('id'=>$doctorId));
 		return $query->row_array();
 	}
 	function deleteClinic($clinicid){ 		
 		if($this->db->where_in('id', $clinicid)->delete('tbl_clinic')){
 			if($this->db->where_in('clinic_id', $clinicid)->delete('tbl_consultation')){
		 			if($this->db->where_in('clinic_id', $clinicid)->delete('tbl_clinic_doctors')){
		 			return true;
		 		}
 			}
 		}
 		else{
 			return false;
 		}
 	}
 	function get_single_clinic($clinicid){
 		$query = $this->db->get_where('tbl_clinic',array('id'=>$clinicid));
 		if($query->num_rows() > 0){ 			 
 			$result_array = array('status'=>'success','data'=>$query->row_array());
 		}
 		else{
 			$result_array = array('status'=>'fail');
 		}
 		return $result_array;
 	}
 	function updateClinic($data,$clinicid){
 		$this->db->from("tbl_clinic");
 		$this->db->where_in('tbl_clinic.email',$data['email']);
 		$this->db->where_not_in('tbl_clinic.id',$clinicid);
 		$query = $this->db->get();  		
 		if($query ->num_rows() <= 0){
 			if($this->db->update('tbl_clinic', $data, array('id' => $clinicid))){
 				$result_array = array('message'=>'success');
 			}
 			else{
 				$result_array = array('message'=>'fail');
 			}
 		}
 		else{
 			$result_array = array('message'=>'fail');
 		}
 		return $result_array;
 	}
 	function get_bookinglist($clinicid){
 		$this->db->select("tbl_booking.*,tbl_doctors.name,tbl_doctors.profile_pic,tbl_clinic.name as clinicName");
 		$this->db->from('tbl_booking');
 		$this->db->join('tbl_doctors','tbl_doctors.id = tbl_booking.doctor_id','INNER');
 		$this->db->join('tbl_clinic','tbl_clinic.id = tbl_booking.clinic_id','left');
 		if($clinicid != 0){
 			//$this->db->join('tbl_clinic','tbl_doctors.id = tbl_booking.doctor_id','INNER');
 			$this->db->where('tbl_booking.clinic_id',$clinicid);
 		}
 		$result = $this->db->get();
 		return $result->result_array();
 	}

 	function get_doctor_specialization($id,$clinic_id){
 		$this->db->select('tbl_doctors.specialization');
 		$this->db->where('tbl_doctors.id',$id);
 		$query = $this->db->get('tbl_doctors')->row();

 		if($query){
 			$this->db->select('specialization_id');
 			$this->db->where('clinic_id',$clinic_id);
 			$ro = $this->db->get('tbl_clinic_specialization')->result_array();
 			//echo"<pre>";print_r($ro);echo"</pre>";
 			$ros = array();
 			foreach ($ro as $key => $value) {
 				$ros[$key] = $value['specialization_id'];
 			}
 			if(count($ro) > 0){
 				if(in_array($query->specialization, $ros)){
 					
 				}else{
 					$this->db->insert('tbl_clinic_specialization',array('clinic_id'=>$clinic_id,'specialization_id'=>$query->specialization));
 					return true;
 				}
 			
 			}else{
 				$this->db->insert('tbl_clinic_specialization',array('clinic_id'=>$clinic_id,'specialization_id'=>$query->specialization));
 					return true;
 			}
 		}
 	}

 	public function set_notification($id,$clinic_id){
	//echo "hhh";exit();
 		$date = strtotime(date('Y-m-d h:i:s'));
 		$this->db->select('tbl_clinic.name');
 		$this->db->where('tbl_clinic.id',$clinic_id);
 		$res = $this->db->get('tbl_clinic')->row();
 		$msg = "You are added to a new clinic ".$res->name;
 		$data = array('doctor_id'=>$id,'message'=>$msg,'type'=>'3','read_status'=>'0','time'=>$date);
 		//print_r($data);exit();
 		if($this->db->insert('tbl_doctor_notifications',$data)){
		//echo "yes";exit();
			$doctor_insert_id = $this->db->insert_id();
			$fcm_doctor = $this->db->get_where('tbl_authtoken_doctors',array('doctor_id'=>$id))->row();
			$news['id'] = $doctor_insert_id;
			$news['type'] = "Added to New Clinic";
			$news['message'] =$msg;
			$news['read_status'] = false;
			$news['to'] = $fcm_doctor->fcm_token;
			//print_r($news);exit();
			$doctor_push = $this->push_sent($news);
 			return true;
 		}
 	}
	
	function push_sent($fcm_data) { 
	//print_r($fcm_data);exit();
		$data1 = "SELECT * FROM settings WHERE id = '0' ";

		$query1 = $this->db->query($data1);

		$rs = $query1->row();
		$key = $rs->api_key;
		//free booking,confirmed,for user
		 $data = "{ \"notification\": { \"title\": \"".$fcm_data['type']."\", \"text\": \"".$fcm_data['message']."\" , \"sound\": \"default\" }, \"time_to_live\": 60, \"data\" : {\"response\" : {\"status\" : \"success\", \"data\" : {\"id\" : \"".$fcm_data['id']."\",\"type\" : \"".$fcm_data['type']."\",\"message\" : \"".$fcm_data['message']."\",\"read_status\" : \"".$fcm_data['read_status']."\"}}}, \"collapse_key\" : \"trip\", \"priority\":\"high\", \"to\" : \"".$fcm_data['to']."\"}";
		
		//print_r($data);exit();
		$ch = curl_init("https://fcm.googleapis.com/fcm/send"); 
		$header = array('Content-Type: application/json', 'Authorization: key='.$key);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		$out = curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);

		curl_close($ch);

    }
}