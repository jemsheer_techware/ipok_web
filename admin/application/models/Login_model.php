<?php 
class Login_model extends CI_Model {	
	public function _consruct(){
		parent::_construct();
 	}
	function login($username, $password) {
	    $query_admin = $this->db->get_where("superadmin",array("username"=>$username,'password'=>$password)); 
	 	$query_user = $this->db->get_where("users",array("username"=>$username,'password'=>$password));
	 	$query_clinic = $this->db->get_where("tbl_clinic",array("username"=>$username,'password'=>$password));
	    if ( $query_admin->num_rows() > 0 )
	    {
	        $row = $query_admin->row_array();
	        $row['uType'] = 'superadmin';
	        $row['table'] = 'superadmin';
	       	return $row;			
	    }
	    else if($query_user->num_rows() > 0 )
	    {
	    	$rows = $query_user->row_array();
	    	$rows['email_id'] = decrypt_data($rows['email_id']);
	    	$rows['phone_no'] = decrypt_data($rows['phone_no']);
	    	$rows['display_name'] = decrypt_data($rows['display_name']);
	    	$row = $rows;
	    	$row['uType'] = 'admin_user';
	    	$row['table'] = 'users';
	       	return $row;
	    } 
	    else if($query_clinic->num_rows() > 0 )
	    {
	    	$roleId_clinic = $this->db->get_where('tbl_roles',array("role_name"=>'clinic'));
	    	if($roleId_clinic->num_rows() > 0){
	    		
	    		$Role_clinic =$roleId_clinic->row_array();
	    		//print_r($Role_clinic['roleID']);die();
		    	$row = $query_clinic->row_array();
		    	$row['uType'] = 'clinic';
		    	$row['display_name'] = $row['name'];
		    	$row['profile_picture'] = $row['profile_photo'];
		    	$row['role_id'] = $Role_clinic['roleID'];
		    	$row['table'] = 'tbl_clinic';
		    	//print_r($row);die();
	       		return $row;
	    	}
	    }
	}
}