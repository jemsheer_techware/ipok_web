<?php 

class User_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}
	function get_users() {
			$this->db->select("users.*, tbl_roles.role_name");
			$this->db->from("users");			
			$this->db->join("tbl_roles","tbl_roles.roleID=users.role_id","left");
			return $this->db->get()->result();
	}
	function addusers($userdata) {
		 $this->db->select("count(*) as count");
		 $this->db->where('username',$userdata['username']);
		 $this->db->from('users');
		 $count=$this->db->get()->row();
		 if($count->count <=0){			   
			// $userdata['status']='1';
			//$userdata['last_login_ip']=null;
			date_default_timezone_set("Asia/Kolkata");
			//$userdata['created_date']=date("Y-m-d h:i:sa");
			//$userdata['modified_date']=date("Y-m-d h:i:sa");
			//$userdata['username'] = encrypt_data($userdata['username']);
			$userdata['email_id'] = encrypt_data($userdata['email_id']);
			$userdata['phone_no'] = encrypt_data($userdata['phone_no']);
			$userdata['display_name'] = encrypt_data($userdata['display_name']);
			//print_r($userdata);exit();
			$this->db->insert("users", $userdata);
			return true;
		}
	}
	function update_users($data,$id){
	
		 $this->db->select("count(*) as count,users.id as uid");
		 $this->db->where('username',$data['username']);
		 $this->db->from('users');
		 $count=$this->db->get()->row();
		if($count->count <=0  || $count->uid==$id){
		 	//$data['username'] = encrypt_data($data['username']);
			$data['email_id'] = encrypt_data($data['email_id']);
			$data['phone_no'] = encrypt_data($data['phone_no']);
			$data['display_name'] = encrypt_data($data['display_name']);
			//print_r($data);exit();
			$this->db->where('id', $id);
	   		$result = $this->db->update('users', $data); 
	   		redirect(base_url().'ManageUsers');
		}
		else{
			return false;
		}
	}
	function delete_users($id){
		$this->db->where('id', $id);
   		$this->db->delete('users');
   		redirect(base_url().'ManageUsers');
	}
	function get_singleuser($id){		
		$query = $this->db->get_where("users", array('id'=> $id )); 
		return $query->result();
	}
}