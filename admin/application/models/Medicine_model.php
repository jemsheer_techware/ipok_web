<?php 

class Medicine_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}
 	function addMedicine($medicineData){

 		$check_medicine_name = $this->db->get_where('tbl_medicine',array('medicine_name'=>$medicineData['medicine_name']));
 		if($check_medicine_name->num_rows() > 0){
 			$result_array = $check_medicine_name->result_array();
 			$newDosages = explode(',',$medicineData['medicine_dosage']);
 			$new = array();
            foreach ($result_array as $key => $value) {
                $new[] = $value['medicine_dosage'];
            }    
 			foreach ($newDosages as $key_newDosages => $value_newDosages) {
 				if(in_array($value_newDosages,  $new))
 				{
 					return false;
 				}else{
 					foreach ($newDosages as $key => $value) {
 						$this->db->insert('tbl_medicine',array('medicine_dosage'=>$value,'medicine_name'=>$medicineData['medicine_name'],'medicine_procedure'=>$medicineData['medicine_procedure']));
 					}
 				}
 			}
 			
 			return true;
 		}
 		else{
 			$newDosages = explode(',',$medicineData['medicine_dosage']);
 			foreach ($newDosages as $key => $value) {
 				$this->db->insert('tbl_medicine',array('medicine_dosage'=>$value,'medicine_name'=>$medicineData['medicine_name'],'medicine_procedure'=>$medicineData['medicine_procedure']));
 			}
 			
 			return true;
 		}
 	}
 	function get_all_medicine(){
 		$all_medicine = $this->db->get('tbl_medicine');
 		if($all_medicine->num_rows() > 0){
 			$result = $all_medicine->result_array();
 			return $result;
 		}
 	}
 	function delete_medicine($id){
 		if($this->db->delete('tbl_medicine',array('id'=>$id))){
 			return true;
 		}
 	}
 	function get_single_medicine($id){
 		$single_medicine = $this->db->get_where('tbl_medicine',array('id' => $id));
 		if($single_medicine->num_rows() > 0){
 			return $single_medicine->row_array();
 		}
 	}
 	function update_medicine($data,$id){
 		$this->db->where('medicine_name',$data['medicine_name']);
 		$this->db->where('medicine_dosage',$data['medicine_dosage']);
 		$this->db->where('id !=',$id);
 		$res = $this->db->get('tbl_medicine')->row();
 		if($res){
          return false;
 		}else{
	 		if($this->db->update('tbl_medicine',$data,array('id'=>$id))){
	 			return true;
	 		}
 		}
 	}
 }