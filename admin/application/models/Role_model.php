<?php 

class Role_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}

 	function get_roles(){
		$query = $this->db->get("tbl_roles"); 
		return $query->result(); 
	}
	function rolemng_users($roledata){		
		$roledata['capability']=implode(",", $roledata['capability']);
		$checkRole = $this->db->get_where('tbl_roles',array('role_name'=>$roledata['role_name']));
		if($checkRole->num_rows() <= 0){

			$this->db->insert("tbl_roles",$roledata);
			return true;
		}
		else{
			return false;
		}
	}
	function get_singleroles($roleid){
		$query = $this->db->get_where("tbl_roles", array('roleID'=> $roleid )); 		
		return $query->row_array();
	}
	function update_role($data,$roleid){
		$data['capability']=implode(",", $data['capability']);
		$this->db->where('roleID', $roleid);
   		$result = $this->db->update('tbl_roles', $data); 
   		redirect(base_url().'ManageRole');
	}
	function delete_role($roleid){
		$this->db->where('roleID', $roleid);
   		$this->db->delete('tbl_roles');
   		$this->db->where('role_id', $roleid);
   		$this->db->delete('users');
   		redirect(base_url().'ManageRole');
	}
}
?>