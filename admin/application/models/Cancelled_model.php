<?php 

class Cancelled_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}
 	function get_cancelled_consultation(){
 		$this->db->select('tbl_doctors.name,tbl_registration.name as patient_name,tbl_booking.date,tbl_booking.total_sum,tbl_booking.time,tbl_booking.id,tbl_booking.is_refund');
 		$this->db->join('tbl_registration','tbl_registration.id = tbl_booking.patient_id');
 		$this->db->join('tbl_doctors','tbl_doctors.id = tbl_booking.doctor_id');
 		return $this->db->get_where('tbl_booking',array('booking_status'=>'4'))->result_array();
 	}
 	
 }