<?php 

class Wallet_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}
 	function get_withdrawal_history(){
 		$this->db->select("tbl_doctors.name,tbl_withdrawal_history.id,tbl_withdrawal_history.doctor_id,tbl_withdrawal_history.bank_id,tbl_withdrawal_history.amount,tbl_withdrawal_history.date,tbl_withdrawal_history.previous_reedem_earn as reedem_earn,tbl_bank_accounts.bank_name,CASE 
 			WHEN tbl_withdrawal_history.status = '0' THEN 'PENDING'
 			WHEN tbl_withdrawal_history.status = '1' THEN 'INPROGRESS'
 			WHEN tbl_withdrawal_history.status = '2' THEN 'COMPLETED'
 			ELSE 'REJECTED' END as status");
 		$this->db->join('tbl_doctors','tbl_doctors.id = tbl_withdrawal_history.doctor_id');
 		$this->db->join('tbl_bank_accounts','tbl_bank_accounts.type_id = tbl_withdrawal_history.doctor_id');
 		return $this->db->get_where('tbl_withdrawal_history')->result_array();
 	}
 	
 }