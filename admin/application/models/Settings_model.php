<?php 

class Settings_model extends CI_Model {
	public function _consruct(){
		parent::_construct();
 	}
	function get_settings(){
		$query = $this->db->get("settings");
		return $query->row_array();
	}
	function update_sitetitle($siteTitle){
		if($this->db->update('settings',$siteTitle)){
			return true;
		}
		else{
			return false;
		}
	}
}
