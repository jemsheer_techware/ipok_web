<?php 

class Complaint_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}
 	function addComplaint($data){
 		$this->db->where('complaint_name',$data['complaint_name']);
 		if($this->db->get('tbl_main_complaints')->row()){
 			return false;
 		}else{
 			$datas = array('complaint_name'=>ucfirst(strtolower($data['complaint_name'])));
 			if($this->db->insert('tbl_main_complaints',$datas)) {
 				return true;
 			}
 		}
 	}
 	function get_all_complaints(){
 		return $this->db->get('tbl_main_complaints')->result();
 		
 	}
 	function delete_complaint($id){
 		if($this->db->delete('tbl_main_complaints',array('id'=>$id))){
 			return true;
 		}
 	}
 	function get_single_complaint($id){
 		$this->db->where('id',$id);
 		return $this->db->get('tbl_main_complaints')->row();
 	}
 	function update_complaint($data,$id){
 		$this->db->where('id !=',$id);
 		$this->db->where('complaint_name',$data['complaint_name']);
 		if($this->db->get('tbl_main_complaints')->row()){
 			return false;
 		}
 		else{
 		$datas = array('complaint_name'=>ucfirst(strtolower($data['complaint_name'])));
	 		$this->db->where('id',$id);
	 		if($this->db->update('tbl_main_complaints',$datas)){
	 			return true;
	 		}
 		}
 	}
 }