<?php 

class Policy_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}
 	function addPolicy($policyData){
 		$this->db->where('id','1');
    	if($this->db->update('tbl_policy',$policyData)){
    		return true;
    	}
 
 	}
 	function get_all_policy(){
 		$this->db->where('id','1');
 		$all_policy = $this->db->get('tbl_policy');
 		if($all_policy->num_rows() > 0){
 			$result = $all_policy->row_array();
 			return $result;
 		}
 	}
 	function delete_exams($id){
 		if($this->db->delete('tbl_exams',array('id'=>$id))){
 			return true;
 		}
 	}
 	function get_single_exam($id){
 		$single_exams = $this->db->get_where('tbl_exams',array('id' => $id));
 		if($single_exams->num_rows() > 0){
 			return $single_exams->row_array();
 		}
 	}
 	function update_exam($data,$id){
 		$this->db->where('exam_procedure',$data['exam_procedure']);
 		$this->db->where('id !=',$id);
 		$res = $this->db->get('tbl_exams')->row();
 		if($res){
          return false;
 		}else{
	 		if($this->db->update('tbl_exams',array('exam_procedure'=>ucfirst($data['exam_procedure']),'observation'=>ucfirst($data['observation'])),array('id'=>$id))){
	 			return true;
	 		}
 		}
 	}
 }