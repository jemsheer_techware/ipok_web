<?php 

class Profile_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}
	public function editProfile($data,$id){
		$login = $this->session->userdata('logged_in');
		if($login['table'] == 'tbl_clinic'){
			$newdata = array('name'=>$data['display_name'],'username'=>$data['username']);
			if(isset($data['profile_picture'])){
				$newdata['profile_photo'] = $data['profile_picture'];
			}
		}
		elseif($login['table'] == 'users'){
			$newdata = array('display_name'=>encrypt_data($data['display_name']),'username'=>$data['username']);
			if(isset($data['profile_picture'])){
				$newdata['profile_picture'] = $data['profile_picture'];
			}
		}else{
			$newdata = $data;
		}
		
			$this->db->where('id',$id);
			if($this->db->update($login['table'],$newdata)){
				$this->db->select('*');
			 	$this->db->from($login['table']);
	    		$this->db->where('id',$id);
	    		$query = $this->db->get();
			    if ( $query->num_rows() > 0 )
			    {
			        
			        $row = $query->row_array();

			       
			       	return $row;					
			    }
			}
			else{
				return false;
			}
		
	}
	public function checkPassword($pword,$id) {
		$login = $this->session->userdata('logged_in');
		
		$query = $this->db->get_where($login['table'],array("password"=>md5($pword),"id"=>$id)); 
		//$query_user = $this->db->get_where("users",array("password"=>md5($pword),"id"=>$id)); 
		if ( $query->num_rows() > 0 )
		    {
		    	return true;
		    }
		    
		else{
			return false;
		}
	}
	function update_profileusers($data,$id){
		$login = $this->session->userdata('logged_in');
		
			$this->db->where('id', $id);
	   		if($this->db->update($login['table'], $data)){
	   			$this->db->select('*');
				$this->db->from($login['table']);
		    	$this->db->where('id',$id);
				$query = $this->db->get();

			    if ( $query->num_rows() > 0 )
			    {
			        $row = $query->row_array();
			       	return $row;
			    }
	   		}   		
			
	}
}