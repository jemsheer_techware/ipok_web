<?php 

class Charity_model extends CI_Model {
	
	public function _consruct(){
		parent::_construct();
 	}
 	function addCharity($charityData){
 		$check_exam_name = $this->db->get_where('tbl_charity',array('title'=>$charityData['title']));
 		if($check_exam_name->num_rows() > 0){
 			return false;
 		}else{
 			if($this->db->insert('tbl_charity',array('title'=>ucfirst($charityData['title']),'description'=>ucfirst($charityData['description']),'date'=>strtotime(date('Y-m-d'))))) {
 				$insert_id = $this->db->insert_id();
 				$res = array('status'=>'success','data'=>$insert_id);
 			
 			}else{
 				$res = array('status'=>'Failed','message'=>"Insertion Failed. Something went wrong");
 			}
 			return $res;
 		}
 	}

 	function delete_register($id){
 		$this->db->delete('tbl_charity',array('id'=>$id));

 	}

 	function add_immage($id,$data){
 		if($this->db->update('tbl_charity',array('image'=>$data['image']),array('id'=>$id))){
 			return $res = array('status'=>'success');
 		}
 	}

 	function get_all_charity(){
 		$all_charity = $this->db->get('tbl_charity');
 		if($all_charity->num_rows() > 0){
 			$result = $all_charity->result_array();
 			return $result;
 		}
 	}

 	function get_all_clinic(){
 		$all_clinic = $this->db->get('tbl_clinic');
 		if($all_clinic->num_rows() > 0){
 			$result = $all_clinic->result_array();
 			return $result;
 		}
 	}
 	function charity_delete($id){
 		if($this->db->delete('tbl_charity',array('id'=>$id))){
 			return true;
 		}
 	}
 	function get_single_charity($id){
 		$single_charity= $this->db->get_where('tbl_charity',array('id' => $id));
 		if($single_charity->num_rows() > 0){
 			return $single_charity->row_array();
 		}
 	}
 	function update_charity($data,$id){
 		$this->db->where('title',$data['title']);
 		$this->db->where('id !=',$id);
 		$res = $this->db->get('tbl_charity')->row();
 		if($res){
          return false;
 		}else{
	 		if($this->db->update('tbl_charity',array('title'=>ucfirst($data['title']),'description'=>ucfirst($data['description'])),array('id'=>$id))){
	 			return true;
	 		}
 		}
 	}

 	function addServices($service_data){
 		$query = $this->db->get_where('tbl_charity_service',array('charity_id'=>$service_data['charity_id'],'clinic_id'=>$service_data['clinic_id']))->row_array();
 		if($query > 0){
 			$amount = explode(',',$service_data['amount']);
 			$db_amount = explode(',', $query['amount']);
 			foreach ($amount as $value) {
 				if(in_array($value, $db_amount)){
 					$return = 1;
 				}else{
 					$return = 0;
 				}
 			}
 			if($return == 1){
 				$res = array('status'=>'failed','message'=>'Sorry Not Inserted. This amount for Clinic Id already Exist in this Charity');
 				return $res;
 			}else{
 				$new_amount = array_merge($amount,$db_amount);
 				$new = implode(',',$new_amount);
 				unset($service_data['amount']);
 				$service_data['amount'] = $new;
 				$result = $this->insert_section($service_data,$query['id']);
 			}
 		}else{
 			$result = $this->insert_section($service_data,'');
 		}
 		return $result;
 	}

 	public function insert_section($service_data,$id){
        if($id != ''){
        	if($this->db->update('tbl_charity_service',array('clinic_id'=>$service_data['clinic_id'],'amount'=>$service_data['amount'],'charity_id'=>$service_data['charity_id']),array('id'=>$id))){
				$res = array('status'=>'success','message'=>'Inserted Successfully');
			}else{
				$res = array('status'=>'failed','message'=>'Sorry Not Inserted. Something Went Wrong. Try Again Later');
			}
        }
        else{
			if($this->db->insert('tbl_charity_service',array('clinic_id'=>$service_data['clinic_id'],'amount'=>$service_data['amount'],'charity_id'=>$service_data['charity_id']))){
				$res = array('status'=>'success','message'=>'Inserted Successfully');
			}else{
				$res = array('status'=>'failed','message'=>'Sorry Not Inserted. Something Went Wrong. Try Again Later');
			}
		}
		return $res;
 	}

 	public function get_all_services(){
 		$this->db->select('tbl_clinic.name,tbl_charity.title,tbl_charity_service.amount,tbl_charity_service.id');
 		$this->db->join('tbl_charity','tbl_charity.id = tbl_charity_service.charity_id');
 		$this->db->join('tbl_clinic','tbl_clinic.id = tbl_charity_service.clinic_id');
 		return $this->db->get('tbl_charity_service')->result_array();
 	}

 	public function service_delete($id){
 		if($this->db->delete('tbl_charity_service',array('id'=>$id))){
 			return true;
 		}
 	}

 	public function get_single_service($id){
 		$single_service= $this->db->get_where('tbl_charity_service',array('id' => $id));
 		if($single_service->num_rows() > 0){
 			return $single_service->row_array();
 		}
 	}

 	public function update_service($data,$id){
 		$this->db->where('id',$id);
 		$res = $this->db->get('tbl_charity_service')->row_array();
 		if($res){
 			$amount = explode(',',$data['amount']);
 			$db_amount = explode(',', $res['amount']);
 			foreach ($amount as $value) {
 				//print_r($value);echo"<br>";
 				$new = array();
 				if(in_array($value, $db_amount)){
 					$return = 1;
 				}else{
 					$new[] = $value;
 					$return = 0;
 				}
 			}
             $data['clinic_id'] = $res['clinic_id'];
             $data['charity_id'] = $res['charity_id'];
 			if($return == 1 && (count($new) == 0) && (count($amount) == count($db_amount))){
 				$res = array('status'=>'failed','message'=>'Sorry Not Inserted. This amount for Clinic Id already Exist in this Charity');
				return $res;
 			}elseif((count($amount) != count($db_amount)) && (count($new) == 0)) {

 				$res = $this->insert_section($data,$id);
 			}elseif((count($new) > 0)){
 				$new_amount = array_merge($new,$db_amount);
				$new = implode(',',$new_amount);
				unset($data['amount']);
				$data['amount'] = $new;
				$res = $this->insert_section($data,$id);
 			}
 			return $res;
 		}
 		
 	}
 }