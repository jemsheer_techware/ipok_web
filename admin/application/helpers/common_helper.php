<?php

function set_upload_options($path) {
	//upload an image options
	$config = array();
	$config['upload_path']   = $path; 
	$config['allowed_types'] = 'jpg|jpeg|png|gif|mp4|mpg|mpeg|m4v|wmv|mov|avi|mkv|flv'; 
	$config['max_size']      = 0; 
	$config['max_width']     = 0; 
	$config['max_height']    = 0; 
	return $config;
}

function load_curl($url,$data)
{
	$CI = & get_instance();
	$CI->config->load('iugu');  

	$c_handle = curl_init();
	$headers = array('Authorization: '.$CI->config->item('auth'));
	curl_setopt($c_handle, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($c_handle, CURLOPT_URL, $url);
	curl_setopt($c_handle, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($c_handle, CURLOPT_POST, 1);
	curl_setopt($c_handle, CURLOPT_POSTFIELDS, http_build_query($data));
	$buffer = curl_exec($c_handle);
	if($buffer === false)
	{
	    return 'Curl error: ' . curl_error($c_handle);
	}
	else
	{
		return $buffer;
	}
	curl_close($c_handle);
}


function check_cep_viacep($cep)
{
	$path = 'https://viacep.com.br/ws/'.$cep.'/json/'; 	$buffer = file_get_contents($path);
	return $buffer;
}

function encrypt_data($data){
	$CI = & get_instance();
	return $CI->encrypt->encode($data); 
}

function decrypt_data($data){
	$CI = & get_instance();
	return $CI->encrypt->decode($data); 
}

function filteredData(){
		$dd = array();
		$dd[0] = array(
					"others_id" => 1,
					"others_name" =>"DOMICILIARY ATTENDANCE"
					);
		$dd[1] = array(	
					"others_id" => 2,
					"others_name" =>"MOST WELL ASSESSED"
					
					);
		$dd[2] = array(	
					"others_id" => 3,
					"others_name" =>"LOWER VALUE"
					
					);
		$dd[3] = array(	
					"others_id" => 4,
					"others_name" =>"MEN"
					
					);
		$dd[4] = array(	
					"others_id" => 5,
					"others_name" =>"WOMEN"
					
					);
		return $dd;
}


/* User menu */
function user_menu() {

	$mainmenu = array(
					
					array(
						"slug" 			=> "Dashboard",
						"name" 			=> "Dashboard",
						"url"  			=> "Dashboard",
						"icon" 			=> "fa-dashboard",
						"submenu" 		=> false,
						"capabilities" 	=> array("manage_dashboard")
						),
					
					
					array(
						"slug" => "Users",
						"name" => "Users",
						"url" => "#",
						"icon" => "fa-user",
						"submenu" => true,
						"capabilities" => array("manage_roles","manage_users"),
						"submenu_items" => '[
											
											
											{"name":"Manage Users","cap":"manage_users","url":"ManageUsers","subcap":"manage_users"},
											{"name":"Manage Roles","cap":"manage_roles","url":"ManageRole","subcap":"manage_roles"}
											]'
						),
					array(
						"slug" => "Manage Doctors",
						"name" => "Manage Doctors",
						"url" => "#",
						"icon" => "fa-stethoscope",
						"submenu" => true,
						"capabilities" => array("manage_doctors","manage_doctors/add","manage_doctors/view","manage_doctors/bookinglist"),
						"submenu_items" => '[
											{"name":"Add","cap":"manage_doctors","url":"ManageDoctors","subcap":"manage_doctors/add"},
											{"name":"View","cap":"manage_doctors","url":"ManageDoctors/view","subcap":"manage_doctors/view"},
											{"name":"Booking List","cap":"manage_doctors","url":"ManageDoctors/bookingList","subcap":"manage_doctors/bookinglist"}
											
											]'
						),
					array(
						"slug" => "Manage Patient",//fa-wheelchair-alt,fa-blind
						"name" => "Manage Patient",
						"url" => "ManageCustomer",
						"icon" => "fa-medkit",
						"submenu" => false,
						"capabilities" => array("manage_customer"),
						
						),
					array(
						"slug" => "Manage Clinic",
						"name" => "Manage Clinic",
						"url" => "#",
						"icon" => "fa-hospital-o",
						"submenu" => true,
						"capabilities" => array("manage_clinic","manage_clinic/view","manage_clinic/add","manage_clinic/bookinglist"),
						"submenu_items" => '[
											{"name":"Add","cap":"manage_clinic","url":"ManageClinic","subcap":"manage_clinic/add"},
											{"name":"View","cap":"manage_clinic","url":"ManageClinic/view","subcap":"manage_clinic/view"},
											{"name":"Booking List","cap":"manage_clinic","url":"ManageClinic/bookingList","subcap":"manage_clinic/bookinglist"}
											
											]'
						),

					array(
						"slug" => "Manage Promocode",
						"name" => "Manage Promocode",
						"url" => "#",
						"icon" => "fa-pinterest-p",
						"submenu"=>true,
						"capabilities" => array("manage_promocode"),
						"submenu_items" => '[
											{"name":"Add","cap":"manage_promocode","url":"Promocode","subcap":"manage_promocode/add"},
											{"name":"View","cap":"manage_promocode","url":"Promocode/promocode_view","subcap":"manage_promocode/view"}
											]'
					),


					array(
						"slug" => "Manage FAQs - Patient",
						"name" => "Manage FAQs - Patient",
						"url" => "#",
						"icon" => "fa-question-circle",
						"submenu"=>true,
						"capabilities" => array("manage_faqs"),
						"submenu_items" => '[
											{"name":"Add","cap":"manage_faqs","url":"ManageFaqs","subcap":"manage_faqs/add"},
											{"name":"View","cap":"manage_faqs","url":"ManageFaqs/faq_view","subcap":"manage_faqs/view"}
											]'
					),

					array(
						"slug" => "Manage FAQs - Doctor",
						"name" => "Manage FAQs - Doctor",
						"url" => "#",
						"icon" => "fa-question",
						"submenu"=>true,
						"capabilities" => array("manage_faqs"),
						"submenu_items" => '[
											{"name":"Add","cap":"manage_faqs","url":"ManageFaqs/faq_doctor_add","subcap":"manage_faqs/add_doctor_faqs"},
											{"name":"View","cap":"manage_faqs","url":"ManageFaqs/faq_view_doctor","subcap":"manage_faqs/view_doctor_faqs"}
											]'
					),

					array(
						"slug" => "Manage Charity",
						"name" => "Manage Charity",
						"url" => "#",
						"icon" => "fa-copyright",
						"submenu"=>true,
						"capabilities" => array("manage_charity"),
						"submenu_items" => '[
											{"name":"Add Charity","cap":"manage_charity","url":"ManageCharity","subcap":"manage_charity/add"},
											{"name":"Add Charity to Clinic","cap":"manage_charity","url":"ManageCharity/add_services","subcap":"manage_charity/add_services"}
											]'
					),
					
					array(
						"slug" => "Manage Wallet",
						"name" => "Manage Wallet",
						"url" => "#",
						"icon" => "fa-google-wallet",
						"submenu"=>true,
						"capabilities" => array("manage_wallet"),
						"submenu_items" => '[
											{"name":"Withdrawal History","cap":"manage_wallet","url":"ManageWallet","subcap":"manage_wallet/Withdrawal"}
											]'
					),
					
					array(
						"slug" => "Cancelled Consultations",//fa-wheelchair-alt,fa-blind
						"name" => "Cancelled Consultations",
						"url" => "CancelledConsultations",
						"icon" => "fa-ban",
						"submenu" => false,
						"capabilities" => array("cancelled_consultations"),
						
						),


					array(
						"slug" => "Principal Issues",//fa-wheelchair-alt,fa-blind
						"name" => "Principal Issues",
						"url" => "MainComplaints",
						"icon" => "fa-info-circle",
						"submenu" => false,
						"capabilities" => array("main_complaints"),
						
						),

					array(
						"slug" => "Major Problems",//fa-wheelchair-alt,fa-blind
						"name" => "Major Problems",
						"url" => "MajorProblems",
						"icon" => "fa-medkit",
						"submenu" => false,
						"capabilities" => array("major_problems","major_problems/problem_edit","major_problems/problem_delete"),
						
						),

					array(
						"slug" => "Major SubProblems",//fa-wheelchair-alt,fa-blind
						"name" => "Major SubProblems",
						"url" => "MajorProblems/subproblem_index",
						"icon" => "fa-info",
						"submenu" => false,
						"capabilities" => array("major_problems/subproblem_index","major_problems/subproblem_edit","major_problems/subproblem_delete"),
						
						),

					array(
						"slug" => "Manage Medicine",
						"name" => "Manage Medicine",
						"url" => "ManageMedicine",
						"icon" => "fa-medium",
						"submenu" => false,
						"capabilities" => array("manage_medicine"),
						
						),

					array(
						"slug" => "Medical Exams",
						"name" => "Medical Exams",
						"url" => "ManageExams",
						"icon" => "fa-medkit",
						"submenu" => false,
						"capabilities" => array("manage_exams"),
						
						),

					array(
						"slug" => "Medical Bill",
						"name" => "Medical Bill",
						"url" => "ManageBudget",
						"icon" => "fa-credit-card",
						"submenu" => false,
						"capabilities" => array("manage_budget"),
						
						),

					array(
						"slug" => "Medical Certificate",
						"name" => "Medical Certificate",
						"url" => "ManageLetters",
						"icon" => "fa-certificate",
						"submenu" => false,
						"capabilities" => array("manage_letter"),
						
						),

					array(
						"slug" => "Manage Policy",
						"name" => "Manage Policy",
						"url" => "ManagePolicy",
						"icon" => "fa-pinterest",
						"submenu" => false,
						"capabilities" => array("manage_policy"),
						
						),

					array(
						"slug" => "Manage Speciality",
						"name" => "Manage Speciality",
						"url" => "ManageSpeciality",
						"icon" => "fa-medkit",
						"submenu" => false,
						"capabilities" => array("manage_speciality"),
						
						),
					
					
					array(
						"slug" => "Settings",
						"name" => "Settings",
						"url" => "#",
						"icon" => "fa-cog",
						"submenu" => true,
						"capabilities" => array("manage_settings"),
						"submenu_items" => '[
											
											
											
											{"name":"Manage Settings","cap":"manage_settings","url":"Settings"}
											
											
											
											
											]'
						),
					);

	return $mainmenu;

}

/* User Capabilities */
function user_capabilities() {

	$capabilities = array(
						
						
						"manage_roles"                   => "Manage Roles",
			     		"manage_users"		 			 =>	"Manage Users",
						"manage_dashboard"				 => "Manage Dashboard",
						//"manage_clinic"					 =>	"Manage Clinic",
						//"manage_doctors"				 => "Manage Doctors"





						"manage_clinic/add"					 =>	"Add New Clinic",
						"manage_clinic/view"				 =>	"View All Clinic",
						"manage_clinic/bookinglist"			 =>	"Booking List of Clinic",
						"manage_promocode/add"				 => "Add Promocode",
						"manage_promocode/view"				 => "View Promocode",
						"manage_faqs/add"					 => "Add Faqs",
						"manage_faqs/view"					 => "View Faqs",
						"manage_faqs/add_doctor_faqs"		 => "Add Doctor Faqs",
						"manage_faqs/view_doctor_faqs"		 => "View Doctor Faqs",
						"manage_charity/add"				 => "Add Charity",
						"manage_charity/add_services"		 => "Add Charity to Clinic",
						"manage_wallet/withdrawal"		 	 => "Withdrawal History",
						"cancelled_consultations"			 => "Cancelled Consultations",
						"manage_doctors/add"				 => "Add New Doctors",
						"manage_doctors/view"				 => "View All Doctors",
						"manage_doctors/bookinglist"		 => "Booking List of Doctors",
						"main_complaints"					 => "Principal Issues",
						"manage_customer"					 => "Manage Patient",
						"major_problems"					 => "Major Problems",
						"major_problems/subproblem_index"	 => "Major SubProblems",
						"manage_medicine"					 => "Add Medicine",
						"manage_exams"						 => "Add Medical Exams",
						"manage_budget"						 => "Add Medical Bill",
						"manage_letter"						 => "Add Medical Certificate",
						"manage_policy"						 => "Add Policy",
						"manage_speciality"		 			 => "Add Speciality",
						"manage_settings"		 			 => "Manage Settings",
						

						);

	return $capabilities;

}
function user_page_capabilities() {

	$capability_pages= array(

							

							"ManageRole-index"  => "manage_roles",
							"ManageRole-role_edit"  => "manage_roles",
							"ManageRole-role_delete"  => "manage_roles",
							
							"ManageUsers-index"  => "manage_users",
							"ManageUsers-user_edit"  => "manage_users",
							"ManageUsers-user_delete"  => "manage_users",

							"Dashboard-index"  => "manage_dashboard",

							"ManageClinic-index"  => "manage_clinic/add",
							"ManageClinic-main_Registration"  => "manage_clinic/add",
							"ManageClinic-addDoctor"  => "manage_clinic/view",
							"ManageClinic-assignDoctors"  => "manage_clinic/view",
							"ManageClinic-view"  => "manage_clinic/view",
							"ManageClinic-bookingList"  => "manage_clinic/bookinglist",

							"Promocode-index" => "manage_promocode/add",
							"Promocode-promocode_view" => "manage_promocode/view",
							"ManageFaqs-index" => "manage_faqs/add",
							"ManageFaqs-faq_view" => "manage_faqs/view",
							"ManageFaqs-faq_doctor_add" => "manage_faqs/addDoctorFaqs",
							"ManageFaqs-faq_view_doctor" => "manage_faqs/viewDoctorFaqs",
							"ManageCharity-index" => "manage_charity/add",
							"ManageCharity-add_services" => "manage_charity/add_services",
							"ManageWallet-index" => "manage_wallet/withdrawal",
							"CancelledConsultations-index" => "cancelled_consultations",

							"ManageDoctors-index"  => "manage_doctors/add",
							"ManageDoctors-main_Registration"  => "manage_doctors/add",
							"ManageDoctors-view"  => "manage_doctors/view",
							"ManageDoctors-assign_clinic_doctor"  => "manage_doctors/view",
							"ManageClinic-assignDoctors"  => "manage_doctors/view",
							"ManageDoctors-bookingList"  => "manage_doctors/bookinglist",
							"MajorProblems-index" => "major_problems",
							"MajorProblems-subproblem_index" => "major_problems/add",
							"MainComplaints-index" => "main_complaints",
							"ManageCustomer-index" => "manage_customer",
							"ManageMedicine-index" => "manage_medicine",
							"ManageBudget-index" => "manage_budget",
							"ManageExams-index" => "manage_exams",
							"ManageLetters-index" => "manage_letter",
							"ManagePolicy-index" => "manage_policy",
							"ManageSpeciality-index"  => "manage_speciality",
							"Settings-index"  => "manage_settings",
							);

	return $capability_pages;
}

/* Get Role Capabilities */
function get_capabilities($role_id) {
	if($role_id != 0) {
	$CI = & get_instance();
	$CI->load->model("Role_model");
	$roles = $CI->Role_model->get_singleroles($role_id);
	$user_roles = explode(",", $roles['capability']);
	
	return $user_roles;
	}
}

/* Check the page is accessible */
function can_access_page() {
	$CI = & get_instance();
	//print_r($CI->session->userdata('logged_in'));die();
	if($CI->session->userdata('logged_in')!=null){
		if($CI->session->userdata('logged_in')['id']==0) {
			return true;
		}
		else {

		$exclude_pages = array("dashboard-index");
		$user_caps = array();
		$all_caps = user_page_capabilities();


		$controller_name = $CI->uri->segment(1);

		$method_name = $CI->uri->segment(2);

		if(!$method_name) {
			$method_name = "index";
		}
		$page = $controller_name."-".$method_name;

		if(in_array($page, $exclude_pages)) {
			return true;
		}
		else {
			//
			$current_page_cap = $all_caps[$page];
			//print_r($current_page_cap);die();
			$role = $CI->session->userdata('logged_in')['role_id'];
			//print_r($role);die();
			$user_caps = get_capabilities($role);
			if($user_caps) {
				if(in_array($current_page_cap, $user_caps)) {
					return true;
				}
				else {
					return false;
				}
			}
		}
		//exit;
		}
	}
}

