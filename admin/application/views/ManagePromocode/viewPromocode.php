<div class="content-wrapper">
        
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>View Promocode</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <!-- Add -->
          <div class="col-md-12">
            <?php
             if($this->session->flashdata('message')) {
                $message = $this->session->flashdata('message');
             ?>
             <div class="alert alert-<?php echo $message['class']; ?>">
                <button class="close" data-dismiss="alert" type="button">×</button>
                <?php echo $message['message']; ?>
             </div>
             <?php
             }
           ?>
          </div>
          <div class="col-xs-12">

              <div class="box box-solid box-info">
                <div class="box-header">
                  <h3 class="box-title">Promocode List</h3>
                 
                </div>
                <div class="box-body">
                  <?php if(count($data) > 0){?>
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th>Promocode Name</th>
                        
                        <th>Valid From</th>
                        <th>Valid To</th>
                        <th>Amount</th>
                        <th>Assigned Doctors</th>
                        <th>Image</th>
                        
                  
                      </tr>
                    </thead>
                    <tbody>
                    
                      <?php
                      foreach($data as $promocode) {?>
                            
                     
                              <tr>
                              <td class="hidden"><?php echo $promocode->id; ?></td>
                              <td><?php echo $promocode->promo_name; ?></td>
                              
                              <td><?php echo $promocode->valid_from; ?></td>
                              <td><?php echo $promocode->valid_to; ?></td>
                              <td><?php echo $promocode->amount; ?></td>
                              <td><?php echo $promocode->doctors; ?></td>
                              <td><img src="<?php echo base_pic_url().$promocode->image; ?>" alt="" width="50px" height="50px"  /></td>
                              <td>
                              <a class='btn btn-sm btn-primary' href='<?php echo base_url(); ?>Promocode/promocode_edit?id=<?php echo $promocode->id; ?>'> <i class='fa fa-fw fa-edit'></i> Edit </a> 
                              <a class='btn btn-sm btn-danger' href='<?php echo base_url(); ?>Promocode/promocode_delete?id=<?php echo $promocode->id; ?>'> <i class='fa fa-fw fa-trash'></i> Delete </a>
                              </td>
                              </tr>
                      <?php
                    }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                         <th class="hidden">ID</th>
                        <th>Promocode Name</th>
                        
                        <th>Valid From</th>
                        <th>Valid To</th>
                        <th>Amount</th>
                        <th>Assigned Doctors</th>
                        <th>Image</th>
                      </tr>
                    </tfoot>
                  </table>
                  <?php } else{ ?>
                   <div>No Result Found</div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <!-- /.row -->
        </section><!-- /.content -->
      </div>