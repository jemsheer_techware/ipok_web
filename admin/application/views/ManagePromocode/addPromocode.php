      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Promocode
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
               if($this->session->flashdata('message')) {
                  $message = $this->session->flashdata('message');
               ?>
               <div class="alert alert-<?php echo $message['class']; ?>">
                  <button class="close" data-dismiss="alert" type="button">×</button>
                  <?php echo $message['message']; ?>
               </div>
               <?php
               }
            ?>
            </div>
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Manage Promocode</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" id="add_promocode_form"   enctype="multipart/form-data">
                  <div class="box-body">
                    
                    <div class="form-group col-md-10">
                      <label>Promo Name</label>
                      <input type="text" name="promo_name" data-parsley-length="[8,8]" class="form-control required" placeholder="Enter Complaint Name" data-parsley-required>
                    </div>
                    <div class="form-group  col-md-10">
                      <label>Description</label>
                        <textarea name="description"  data-parsley-length="[15,500]"  type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Description" data-parsley-required></textarea>
                    </div> 
                    <div class="form-group  col-md-10">
                      <label>Image</label>
                        <input type="file" name="image" class="form-control"  value="" accept="image/*" data-parsley-required>
                        <!-- <img src" alt=""  style="width:100px; height:100px"/> -->
                    </div>
                    <div class="form-group  col-md-10">
                      <label>Date range</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control" id="reservation" name="valid_from" data-parsley-required="true">
                        </div>
                    </div>
                    <div class="form-group col-md-10">
                      <label>Amount(in %)</label>
                      <input type="text" name="amount" class="form-control required" placeholder="Enter Amount" data-parsley-required>
                    </div>
                    <div class="form-group col-md-10">
                      <label>Choose Doctors</label>
                      <select class="form-control select2" multiple="multiple" placeholder="Select a State" style="width: 100%;" name="doctor_id[]" data-parsley-required="true">
                                
                                <?php 
                                foreach ($data as $key => $value) 
                                {
                                ?>
                                      <option value="<?php echo $value->id?>"><?php echo $value->name?></option>
                                <?php
                                  }
                                ?>
                      </select>                
                    </div>
                    <div class="form-group col-md-10">
                      <label>Status</label>
                       <select class="form-control select2" data-placeholder="Select Status" style="width: 100%;" name="status">
                        <!--   <option value="-1">Select Status</option> -->
                          <option value="Active" selected>Active</option>
                          <option value="Inactive">Inactive</option>
                      </select> 
                    </div>

                    
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

             

            </div>
            <!--/.col (Add) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
