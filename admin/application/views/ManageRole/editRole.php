<?php
if(isset($data)) {
  
	?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Role Management
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
        if($this->session->flashdata('message')) {
          $message = $this->session->flashdata('message');
        ?>
        <div class="callout callout-<?php echo $message['class']; ?>">
                <h4><?php echo $message['title']; ?></h4>
                <p><?php echo $message['message']; ?></p>
        </div>
        <?php
        }
        ?>
              <!-- general form elements -->
              <div class="box box-solid box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Role</h3>
                  <!-- <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div> -->
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form">
                  <div class="box-body">
                    
                    <div class="form-group">
                      <label>Role Name</label>
                      <input type="text" name="role_name" value="<?php echo $data['role_name'];?>" class="form-control required" placeholder="Enter Role Name">
                    </div>
                    
                    <div class="form-group">
                      <label>Select Capabilities</label>
                      <div class="checkbox">
                        <?php
                        
                        $user_cap = explode(",", $data['capability']);

                        $capabilities = user_capabilities();
                        foreach($capabilities as $key => $capability) {
                          $c = (in_array($key, $user_cap)) ? "checked" : "";
                        ?>
                        <label class="col-md-4">
                          <input value="<?php echo $key; ?>" name="capability[]" <?php echo $c; ?> type="checkbox">
                          <?php echo $capability; ?>
                        </label>
                        <?php
                        }
                        ?>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

             

            </div>
            <!--/.col (Add) -->
            
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
<?php
}
else {
	$this->load->view("error_500");
}
?>