      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Role Management
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
        if($this->session->flashdata('message')) {
          $message = $this->session->flashdata('message');
        ?>
        <div class="callout callout-<?php echo $message['class']; ?>" id="successMessage">
                <h4><?php echo $message['title']; ?></h4>
                <p><?php echo $message['message']; ?></p>
        </div>
        <?php
        }
        ?>
              <!-- general form elements -->
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Add New Role</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form" data-parsley-validate>
                  <div class="box-body">
                    
                    <div class="form-group">
                      <label>Role Name</label>
                      <input type="text" name="role_name" class="form-control required" placeholder="Enter Role Name" data-parsley-required="true">
                    </div>
                    
                    <div class="form-group">
                      <label>Select Capabilities</label>
                      <div class="checkbox">
                        <?php
                        $capabilities = user_capabilities();
                        foreach($capabilities as $key => $capability) {
                        ?>
                        <label class="col-md-4">
                          <input value="<?php echo $key; ?>" name="capability[]" type="checkbox" data-parsley-required="true">
                          <?php echo $capability; ?>
                        </label>
                        <?php
                        }
                        ?>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

             

            </div>
            <!--/.col (Add) -->
           <div class="col-xs-12">

              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Saved Roles</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Role Name </th>
                        <th> Pages</th>
                        
                        <th width="150px">Options</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                    if($roles) {
                      foreach($roles as $role) {
                      
                      $user_capabilities = array();
                      $user_pages = explode(",", $role->capability);
                      foreach($user_pages as $page) {
                       $user_capabilities[] = $capabilities[$page];
                      }
                     $pages = implode(", ", $user_capabilities)
                      ?>
                              <tr>
                              <td class="hidden"><?php echo $role->roleID; ?></td>
                              <td><?php echo $role->role_name; ?></td>
                              <td><i><?php echo $pages; ?></i></td>
                              <td>
                              <a class='btn btn-sm btn-primary' href='<?php echo base_url(); ?>ManageRole/role_edit/<?php echo $role->roleID; ?>'> <i class='fa fa-fw fa-edit'></i> Edit </a> 
                              <a class='btn btn-sm btn-danger' href='<?php echo base_url(); ?>ManageRole/role_delete/<?php echo $role->roleID; ?>'> <i class='fa fa-fw fa-trash'></i> Delete </a>
                              </td>
                              </tr>
                      <?php
                    }
                  }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Role Name </th>
                        <th> Pages</th>
                        
                        <th>Options</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div> 
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
