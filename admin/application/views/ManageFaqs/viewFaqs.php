<div class="content-wrapper">
        
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>View FAQ's</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <!-- Add -->
          <div class="col-md-12">
            <?php
             if($this->session->flashdata('message')) {
                $message = $this->session->flashdata('message');
             ?>
             <div class="alert alert-<?php echo $message['class']; ?>">
                <button class="close" data-dismiss="alert" type="button">×</button>
                <?php echo $message['message']; ?>
             </div>
             <?php
             }
           ?>
          </div>
          <div class="col-xs-12">

              <div class="box box-solid box-info">
                <div class="box-header">
                  <h3 class="box-title">FAQ List</h3>
                 
                </div>
                <div class="box-body">
                  <?php if(count($data) > 0){?>
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th>Question</th>
                        
                        <th>Answer</th>
                        <th>Action</th>
                  
                      </tr>
                    </thead>
                    <tbody>
                    
                      <?php
                      
                      foreach($data as $faq) { ?>
                            
                     
                              <tr>
                              <td class="hidden"><?php echo $faq['id']; ?></td>
                              <td><?php echo $faq['faq_title']; ?></td>
                              
                              <td><?php echo $faq['faq_description']; ?></td>
                              <td>
                              <a class='btn btn-sm btn-primary' href='<?php echo base_url(); ?>ManageFaqs/faq_edit?id=<?php echo $faq['id']; ?>'> <i class='fa fa-fw fa-edit'></i> Edit </a> 
                              <a class='btn btn-sm btn-danger' href='<?php echo base_url(); ?>ManageFaqs/faq_delete?id=<?php echo $faq['id']; ?>'> <i class='fa fa-fw fa-trash'></i> Delete </a>
                              </td>
                              </tr>
                      <?php
                    }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                         <th class="hidden">ID</th>
                        <th>Question</th>
                        
                        <th>Answer</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>
                  <?php } else{ ?>
                   <div>No Result Found</div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <!-- /.row -->
        </section><!-- /.content -->
      </div>