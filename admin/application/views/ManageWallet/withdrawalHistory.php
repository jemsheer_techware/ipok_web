<div class="content-wrapper">
        
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Withdrawal History</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <!-- Add -->
          <div class="col-md-12">
            <?php
             if($this->session->flashdata('message')) {
                $message = $this->session->flashdata('message');
             ?>
             <div class="alert alert-<?php echo $message['class']; ?>">
                <button class="close" data-dismiss="alert" type="button">×</button>
                <?php echo $message['message']; ?>
             </div>
             <?php
             }
           ?>
          </div>
          <div class="col-xs-12">
              <div class="box box-solid box-info">
                <div class="box-header">
                  <h3 class="box-title">Withdrawal History</h3>
                </div>
                <div class="box-body">
                  <?php if(count($data) > 0){?>
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th>Doctor Name</th>
                        <th>Bank</th>
                        <th>Reedem Amount</th>
                        <th>Withdrawal Amount</th>
                        <th>Date</th>
                        <th>Status</th>
                  
                      </tr>
                    </thead>
                    <tbody>
                    
                      <?php
                      
                      foreach($data as $list) { ?>
                            
                     
                              <tr>
                              <td class="hidden"><?php echo $list['id']; ?></td>
                              <td><?php echo $list['name']; ?></td>
                              
                              <td><?php echo $list['bank_name']; ?></td>
                              <td><?php echo $list['reedem_earn']; ?></td>
                              <td><?php echo $list['amount']; ?></td>
                              <td><?php echo date('Y-m-d',$list['date']); ?></td>
                              <td><b><?php echo $list['status']; ?></b></td>
                              </tr>
                      <?php
                    }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                         <th class="hidden">ID</th>
                        <th>Doctor Name</th>
                        <th>Bank</th>
                        <th>Reedem Amount</th>
                        <th>Withdrawal Amount</th>
                        <th>Date</th>
                        <th>Status</th>
                      </tr>
                    </tfoot>
                  </table>
                  <?php } else{ ?>
                   <div>No Result Found</div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <!-- /.row -->
        </section><!-- /.content -->
      </div>