      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Add Complaints
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
               if($this->session->flashdata('message')) {
                  $message = $this->session->flashdata('message');
               ?>
               <div class="alert alert-<?php echo $message['class']; ?>">
                  <button class="close" data-dismiss="alert" type="button">×</button>
                  <?php echo $message['message']; ?>
               </div>
               <?php
               }
            ?>
            </div>
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Add New Complaints</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form" data-parsley-validate>
                  <div class="box-body">
                    
                    <div class="form-group">
                      <label>Complaint</label>
                      <input type="text" name="complaint_name" class="form-control required" placeholder="Enter Complaint Name" data-parsley-required="true">
                    </div>
                   <!--  <div class="form-group">
                      <label>Medicine Dosage</label>
                      <input type="text" name="medicine_dosage" class="form-control required" placeholder="Enter Dosage" data-parsley-required="true" data-role="tagsinput">
                    </div> -->
                    
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

             

            </div>
            <!--/.col (Add) -->
            <div class="col-xs-12">

              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">List</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div>
                <div class="box-body">
                  <?php  if($data) {?>
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Complaint Name </th>
                      <!--   <th> Dosage</th> -->
                        
                        <th width="200px">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php
                   
                      foreach($data as $complaint) {                     
                      
                      ?> 
                              <tr>
                              <td class="hidden"><?php echo $complaint->id;?></td>
                              <td><?php echo $complaint->complaint_name;?></td>
                             <!--  <td><i><?php echo $medicine['medicine_dosage'];?></i></td> -->
                              <td>
                              <a class='btn btn-sm btn-primary' href='<?php echo base_url(); ?>MainComplaints/complaint_edit/<?php echo $complaint->id; ?>'> <i class='fa fa-fw fa-edit'></i> Edit </a> 
                              <a class='btn btn-sm btn-danger' href='<?php echo base_url(); ?>MainComplaints/complaint_delete/<?php echo $complaint->id; ?>'> <i class='fa fa-fw fa-trash'></i> Delete </a>
                              </td>
                              </tr>
                     <?php
                    }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Complaint Name </th>
                       <!--  <th> Dosage</th> -->
                        
                        <th>Actions</th>
                      </tr>
                    </tfoot>
                  </table>
                  <?php } else{?>
                    <div>No Data Found</div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
