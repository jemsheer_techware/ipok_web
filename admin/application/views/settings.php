<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Settings
        <!-- <small>Preview</small> -->
      </h1>
      <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol> -->
    </section>
      <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="box-body">
    	<?php
        if($this->session->flashdata('message')) {
          $message = $this->session->flashdata('message');
        ?>
        <div class="callout callout-<?php echo $message['class']; ?>" id="successMessage">
                <h4><?php echo $message['title']; ?></h4>
                <p><?php echo $message['message']; ?></p>
        </div>
        <?php
        }
        ?></div>
        </div>
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-solid box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Update Data</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form method="post" class="validate" role="form" action="Settings/change_sitetitle" enctype="multipart/form-data">
              <div class="box-body">
              	<div class="row">
              	
	                <div class="form-group col-xs-4">
	                  <label>Site Title</label>
	                  <?php $site_title =decrypt_data($data['site_title']); ?>
	                  <input type="text" name="site_title" class="form-control required" placeholder="Enter Site Title" value="<?php echo $site_title;?>">
	                </div>
	                <div class="form-group col-xs-4">
	                  <label>Site Logo</label>
	                   <input type="file" name="site_logo" class="form-control"  value="" accept="image/*">
	                  <img src="<?php echo base_pic_url().$data['site_logo']?>" alt=""  style="width:100px; height:100px"/>
	                </div>
	                <div class="form-group col-xs-4">
	                  <label>Fav Icon</label>
	                  <input type="file" name="fav_icon" class="form-control"  value="" accept="image/*">
	                  <img src="<?php echo base_pic_url().$data['fav_icon']?>" alt=""  style="width:100px; height:100px"/>
	                </div>
              	</div>
                <div class="row">
                	<div class="form-group col-xs-4">
                      <label>SMTP Username</label>
                      <?php $smtp_username =decrypt_data($data['smtp_username']); ?>
                      <input type="text" name="smtp_username" class="form-control required" placeholder="Enter SMTP Username" value="<?php echo $smtp_username;?>">
                    </div>
                    <div class="form-group col-xs-4">
                      <label>SMTP Password</label>
                      <?php $smtp_password =decrypt_data($data['smtp_password']); ?>
                      <input type="text" name="smtp_password" class="form-control required" placeholder="Enter SMTP Password" value="<?php echo $smtp_password;?>">
                    </div>
                    <div class="form-group col-xs-4">
                      <label>SMTP Host</label>
                      <?php $smtp_host =decrypt_data($data['smtp_host']); ?>
                      <input type="text" name="smtp_host" class="form-control required" placeholder="Enter Host Name" value="<?php echo $smtp_host;?>">
                    </div>
                </div>
                <div class="row">
                	<div class="form-group col-xs-4">
                      <br><label>IPOK Fee(in %)</label>
                      <?php $ipok_fee =$data['ipok_fee']; ?>
                      <input type="text" name="ipok_fee" class="form-control required" placeholder="Enter IPOK Fee" value="<?php echo $ipok_fee;?>">
                    </div>

                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-info">Update</button>
              </div>
            </form>
          </div>
          </div>
          </div>
          </section>
          </div>



 <!-- <div class="content-wrapper">
	<section class="content-header">
      <h1>
        Settings
      </h1>
    </section>
    <section class="content">
      <div class="row">
      	<div class="col-md-10">
        <?php
        if($this->session->flashdata('message')) {
          $message = $this->session->flashdata('message');
        ?>
        <div class="callout callout-<?php echo $message['class']; ?>" id="successMessage">
                <h4><?php echo $message['title']; ?></h4>
                <p><?php echo $message['message']; ?></p>
        </div>
        <?php
        }
        ?>
      </div>
      </div>
      <form method="post" class="validate" role="form" action="Settings/change_sitetitle" enctype="multipart/form-data">
      <div class="row">
      	<div class="col-md-4">
      		
	     <div class="box box-primary">
	            <div class="box-header with-border">
	                <h3 class="box-title">Update Site Title</h3>
	                	<div class="pull-right box-tools">
	            			<button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
	            				<i class="fa fa-minus"></i>
	                    	</button>
	          			</div>
	            </div> --><!-- /.box-header -->
	            <!-- <form method="post" class="validate" role="form" action="Settings/change_sitetitle"> 
	                <div class="box-body">
	                    
	                    <div class="form-group">
	                      <label>Site Title</label>
	                      <input type="text" name="site_title" class="form-control required" placeholder="Enter Site Title" value="<?php echo $data['site_title']?>">
	                    </div>-->
	                    <!-- <input type="submit" value="Update"> 
	                </div>-->
	            <!-- </form> -->
	        <!-- </div> 
      	</div>
      	<div class="col-md-4">-->
      		<!-- <div class="box box-primary">
	            <div class="box-header with-border">
	                <h3 class="box-title">Update Site Logo</h3>
	                	<div class="pull-right box-tools">
	            			<button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
	            				<i class="fa fa-minus"></i>
	                    	</button>
	          			</div>
	            </div> --><!-- /.box-header -->
	            <!-- <form method="post" class="validate" role="form" enctype="multipart/form-data" action="Settings/imageChange">
	                <div class="box-body">
	                    
	                    <div class="form-group">
	                      <label>Site Logo</label>
	                       <input type="file" name="site_logo" class="form-control"  value="" accept="image/*">
	                      <img src="<?php echo base_url().$data['site_logo']?>" alt=""  style="width:100px; height:100px"/>
	                    </div> -->
	                    <!-- <input type="submit" name="SiteLogo" value="Update"> 
	                </div>-->
	            <!-- </form> -->
	        <!-- </div> 
      	</div>
      	<div class="col-md-4">-->
      		<!-- <div class="box box-primary">
	            <div class="box-header with-border">
	                <h3 class="box-title">Update Fav Icon</h3>
	                	<div class="pull-right box-tools">
	            			<button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
	            				<i class="fa fa-minus"></i>
	                    	</button>
	          			</div>
	            </div> --><!-- /.box-header -->
	            <!-- <form method="post" class="validate" role="form" enctype="multipart/form-data" action="Settings/imageChange"> 
	                <div class="box-body">
	                    
	                    <div class="form-group">
	                      <label>Fav Icon</label>
	                      <input type="file" name="fav_icon" class="form-control"  value="" accept="image/*">
	                      <img src="<?php echo base_url().$data['fav_icon']?>" alt=""  style="width:100px; height:100px"/>
	                    </div>-->
	                	<!-- <input type="submit" name="FavIcon" value="Update"> 
	                </div>-->
	            <!-- </form> -->
	        <!-- </div> 
      	</div>
      </div>
      <div class="row">
      	<div class="col-md-4">
      		
	      <div class="box box-primary">
	            <div class="box-header with-border">
	                <h3 class="box-title">Update SMTP Username</h3>
	                	<div class="pull-right box-tools">
	            			<button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
	            				<i class="fa fa-minus"></i>
	                    	</button>
	          			</div>
	            </div> -->
	            <!-- /.box-header
	            <!-- <form method="post" class="validate" role="form" action="Settings/change_sitetitle"> 
	                <div class="box-body">
	                    
	                    <div class="form-group">
	                      <label>SMTP Username</label>
	                      <input type="text" name="smtp_username" class="form-control required" placeholder="Enter SMTP Username" value="<?php echo $data['smtp_username']?>">
	                    </div>-->
	                    <!-- <input type="submit"  value="Update"> 
	                </div>-->
	            <!-- </form> -->
	       <!--  </div> 
      	</div>
      	<div class="col-md-4">-->
      		<!-- <div class="box box-primary">
	            <div class="box-header with-border">
	                <h3 class="box-title">Update SMTP Password</h3>
	                	<div class="pull-right box-tools">
	            			<button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
	            				<i class="fa fa-minus"></i>
	                    	</button>
	          			</div>
	            </div> --><!-- /.box-header -->
	            <!-- <form method="post" class="validate" role="form" action="Settings/change_sitetitle"> 
	                <div class="box-body">
	                    
	                    <div class="form-group">
	                      <label>SMTP Password</label>
	                      <input type="text" name="smtp_password" class="form-control required" placeholder="Enter SMTP Password" value="<?php echo $data['smtp_password']?>">
	                    </div>-->
	                    <!-- <input type="submit"  value="Update"> 
	                </div>-->
	            <!-- </form> -->
	       <!--  </div>
      	</div>
      	<div class="col-md-4"> -->
      		<!-- <div class="box box-primary"> -->
	            <!-- <div class="box-header with-border">
	                <h3 class="box-title">Update SMTP Host</h3>
	                	<div class="pull-right box-tools">
	            			<button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
	            				<i class="fa fa-minus"></i>
	                    	</button>
	          			</div>
	            </div> --><!-- /.box-header -->
	            <!-- <form method="post" class="validate" role="form" action="Settings/change_sitetitle"> 
	                <div class="box-body">
	                    
	                    <div class="form-group">
	                      <label>SMTP Host</label>
	                      <input type="text" name="smtp_host" class="form-control required" placeholder="Enter Host Name" value="<?php echo $data['smtp_host']?>">
	                    </div>-->
	                	<!-- <input type="submit"  value="Update"> 
	                </div>-->
	            <!-- </form> -->
	        <!-- </div> 
      	</div>
      	<div class="row">
      		
	      	<div class="col-md-4">
	      		<div class="box-body">
		                    
		                    <div class="form-group">
		                      <label>Admin Email</label>
		                      <input type="email" name="admin_email" class="form-control required" placeholder="Enter Admin Email" value="<?php echo $data['admin_email']?>">
		                    </div>-->
		                	<!-- <input type="submit"  value="Update"> 
		                </div>
	      	</div>-->
	      	<!-- <div class="col-md-4">
	      		<div class="box-body">
		                    
		                    <div class="form-group">
		                      <label></label>
		                      <input type="text" name="admin_email" class="form-control required" placeholder="Enter Admin Email" value="<?php echo $data['admin_email']?>" data-parsley-email="">
		                    </div>
		                	<!-- <input type="submit"  value="Update"> -->
		                <!-- </div>
	      	</div> 
      	</div>
      	<div class="btn-cntr-setng">
      		
      		<input type="submit"  value="Update" class="btn btn-primary" style="width: 20%">
      	</div>
      </div>
      </form>
    </section>
</div>-->