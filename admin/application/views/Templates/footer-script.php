	<script>
	
	base_url = "<?php echo base_url(); ?>";
	
	</script>
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
 
    <!-- Bootstrap 3.3.5 -->
     <script src="<?php echo base_url(); ?>assets/js/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
     <script src="<?php echo base_url(); ?>assets/js/custom-script.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/pace.js"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/select2.full.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>assets/js/select2.js"></script> -->
    
    <!-- DataTables -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap.min.js"></script>
    
    <!-- FastClick 
    <script src="../../plugins/fastclick/fastclick.min.js"></script>-->
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyDmjOrvg2IiJjJonTnHZ4aiEcGE3h_RHys"></script>
    <script src="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>
   
    <script src="<?php echo base_url(); ?>assets/js/daterangepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-tagsinput.js"></script>

       <!--parsley.min.js -->

     <script src="<?php echo base_url(); ?>assets/js/parsley.min.js"></script>
     <script src="<?php echo base_url(); ?>assets/js/parsley.js"></script>
   
    
    <!-- CK Editor -->
    <script>

      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        //$(".select3").select2();
        $('#myAssign_Doctor_form').parsley();
        $('#add_promocode_form').parsley();
        $('#add_service_form').parsley();
       // $('#add_doctors_form').parsley();
		$('.datatable').DataTable({
			"ordering" : $(this).data("ordering"),
			"order": [[ 0, "desc" ]]
        });

		
	  });
       $(function() {
            $('#datetimepicker3').datetimepicker({
              pickDate: false
            });
        });

       $(function() {
         $('#reservation').daterangepicker();
          //$('input[name="valid_from"]').daterangepicker();

           var date1 = $('#reservation_dub').val();

           $('#reservation').val(date1);
       })

         $(function () {

    if($('#editor1').length == 1) { CKEDITOR.replace('editor1'); }
    if($('#editor2').length == 1) { CKEDITOR.replace('editor2'); }
    if($('#editor3').length == 1) { CKEDITOR.replace('editor3'); }
    if($('#editor4').length == 1) { CKEDITOR.replace('editor4'); }

 //   $(".textarea").wysihtml5();

  });

	</script>
