<?php if($this->session->userdata('logged_in')){

 $login=$this->session->userdata('logged_in');
 //print_r($login);die();

 }?>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
             <img src="<?php echo base_pic_url().$login['profile_picture'] ?>" class="img-circle" alt="User Image" style="width: 45px;height: 45px;">
            </div>
            <div class="pull-left info">
              <p><?php echo $login['display_name']?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <!--<form method="post" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>-->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            
            

            <?php
              $menus = user_menu();
             
              $loginID = $this->session->userdata('logged_in')['id'];

              if($loginID == 0) {
                foreach($menus as $menu) {
                  //echo $menu['name']."<br>";
                  $menu_class = ($menu['submenu']) ? "treeview" : "rootmenu";
                  $treeview = '<i class="fa fa-angle-left pull-right"></i>';
                  $rootmenu = '';
                  ?>
                  <li class="<?php echo $menu_class; ?>">
                    <a href="<?php echo base_url().$menu['url']; ?>">
                      <i class="fa <?php echo $menu['icon']; ?>"></i> <span><?php echo $menu['name']; ?></span>
                      <?php echo $$menu_class; ?>
                    </a>
                    <?php
                    if($menu['submenu']) {
                    ?>
                      <ul class="treeview-menu">
                        <?php
                          $submenu = json_decode($menu['submenu_items']);
                         // print_r($menu['submenu_items']);
                          foreach($submenu as $sub_menu) {
                            
                            ?>
                            <li>
                              <a href="<?php echo base_url().$sub_menu->url; ?>"><i class="fa fa-circle-o text-aqua"></i>
                                <?php echo $sub_menu->name; ?>
                              </a>
                            </li>
                            <?php
                          }
                        ?>
                      </ul>
                    <?php
                    }
                    ?>


                  </li>
                  <?php
                }
              }
              else {
                $role = $this->session->userdata('logged_in')['role_id'];
               // print_r($role);die();
                $user_caps = get_capabilities($role);
               // print_r($user_caps);die();
                foreach($menus as $menu) {
                  $menu_class = ($menu['submenu']) ? "treeview" : "rootmenu";
                  $treeview = '<i class="fa fa-angle-left pull-right"></i>';
                  $rootmenu = '';
                  if(array_intersect($user_caps, $menu['capabilities']) or in_array("basic_cap", $menu['capabilities'] )) {
                  ?>
                  <li class="<?php echo $menu_class; ?>">
                    <a href="<?php echo base_url().$menu['url']; ?>">
                      <i class="fa <?php echo $menu['icon']; ?>"></i> <span><?php echo $menu['name']; ?></span>
                      <?php echo $$menu_class; ?>
                    </a>
                    <?php
                    if($menu['submenu']) {
                    ?>
                      <ul class="treeview-menu">
                        <?php
                          $submenu = json_decode($menu['submenu_items']);
                          
                          foreach($submenu as $sub_menu) {
                           
                            if(in_array($sub_menu->cap, $user_caps) || in_array($sub_menu->subcap, $user_caps)) {
                            ?>
                            <li>
                              <a href="<?php echo base_url().$sub_menu->url; ?>"><i class="fa fa-circle-o text-aqua"></i>
                                <?php echo $sub_menu->name; ?>
                              </a>
                            </li>
                            <?php
                          }
                          }
                        ?>
                      </ul>
                    <?php
                    }
                    ?>
                  </li>
                  <?php
                  }
                }
              }
            ?>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
