<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Users
            <!-- <small>Preview</small> -->
          </h1>
          <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Elements</li>
          </ol> -->
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                     <?php
              if($this->session->flashdata('message')) {
                $message = $this->session->flashdata('message');
              ?>
              <div class="callout callout-<?php echo $message['class']; ?>">
                      <h4><?php echo $message['title']; ?></h4>
                      <p><?php echo $message['message']; ?></p>
              </div>
              <?php
              }
              ?>
              <!-- general form elements -->
              <div class="box box-solid box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Add New User</h3>
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
                    <i class="fa fa-minus"></i>
                            </button>
                  </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form" enctype="multipart/form-data">
                  <div class="box-body">
                    
                    <div class="form-group col-md-12">
                      <label>Full Name</label>
                      <input type="text" name="display_name" class="form-control required" placeholder="Enter Name">
                    </div>

                    <div class="form-group col-md-6">
                      <label>Username</label>
                      <input type="text" name="username" value="" class="form-control required" placeholder="Enter Username">
                    </div>

                    <div class="form-group col-md-6">
                      <label>Password</label>
                      <div class="input-group">
                        <input type="password" name="password" value="" id="myPassword" class="form-control required" placeholder="Enter Password">
                        <span class="input-group-btn">
                          <button class="btn btn-flat show-pwd-btn" type="button">
                            <i class="fa fa-eye"></i>
                          </button>
                        </span>
                      </div>
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label>Email ID</label>
                      <input type="email" name="email_id" value="" class="form-control required" placeholder="Enter Email ID">
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label>Phone</label>
                      <input type="text" name="phone_no" value="" class="form-control required" placeholder="Enter Phone No" pattern="[0-9]{10}">
                    </div>
                     <div class="form-group col-md-6">
                      <label>Role</label>
                      <select class="form-control select2 required" name="role_id">
                        <option></option>
                        <?php
                        if($roles) {
                          foreach($roles as $role) {
                        ?>
                        <option value="<?php echo $role->roleID; ?>"><?php echo $role->role_name; ?></option>
                        <?php
                          }
                        }
            
                        ?>
                      </select>
                    </div>

                    <!-- <div class="form-group col-md-6">
                      <label>Status</label>
                      <select class="form-control select2 required" name="status">
                        <option value="1">Enable</option>
                        <option value="0">Disable</option>
                      </select>
                    </div> -->
                    
                    <div class="form-group col-md-12">
                      <label>Profile Picture</label>
                      <input name="profile_pic" id="profile_pic" type="file" class="required" accept="image/*" style="display:none" onchange="readURL_usr(this)">
                      <img src="" alt=""  style=" width:100px; height:100px" id="preview_profile_pic_usr"/>
                    </div>
                    <!-- /.box-body -->

                  <div class="box-footer"  style="clear:left">
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div>
                </form>

              </div><!-- /.box -->
            </div>
            
          </div>   <!-- /.row -->

          <div class="col-xs-12">

              <div class="box  box-info">
                <div class="box-header">
                  <h3 class="box-title">Saved Users</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Name </th>
                        <th> Username</th>
                        <th> Profile Pic</th>
                        <th> Role</th>
                        <th>Options</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                    if($data) {
                      foreach($data as $dt) {
                        
                      ?>
                              <tr>
                              <td class="hidden"><?php echo $dt->id; ?></td>
                              <td><?php echo decrypt_data($dt->display_name); ?></td>
                              <td><?php echo $dt->username; ?></td>
                              <td><img src="<?php echo base_pic_url().$dt->profile_picture; ?>" alt="" width="50px" height="50px"  /></td> 
                               <td><?php echo $dt->role_name; ?></td>
                              
                              <td>
                              <a class='btn btn-sm btn-primary' href='<?php echo base_url(); ?>ManageUsers/user_edit/<?php echo $dt->id; ?>'> <i class='fa fa-fw fa-edit'></i> Edit </a>
                              <a class='btn btn-sm btn-danger' href='<?php echo base_url(); ?>ManageUsers/user_delete/<?php echo $dt->id; ?>'> <i class='fa fa-fw fa-trash'></i> Delete </a>
                              </td>
                              </tr>
                      <?php
                                        }
                  }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Name </th>
                        <th> Username</th>
                        <th> Profile Pic</th>
                        <th> Role</th>
                        
                        <th>Options</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>