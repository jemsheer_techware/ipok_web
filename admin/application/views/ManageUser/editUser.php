<?php
if(isset($data)) {
  
	?>
     <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage User
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
        if($this->session->flashdata('message')) {
          $message = $this->session->flashdata('message');
        ?>
        <div class="callout callout-<?php echo $message['class']; ?>">
                <h4><?php echo $message['title']; ?></h4>
                <p><?php echo $message['message']; ?></p>
        </div>
        <?php
        }
        ?>
        </div>
        	<div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-solid box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Profile</h3>
                  	
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form" enctype="multipart/form-data">  
                  <div class="box-body">
                  
                    <div class="form-group col-md-12">
                      <label>Full Name</label>
                      <input type="text" name="display_name" value="<?php echo decrypt_data($data[0]->display_name); ?>" class="form-control required" placeholder="Enter Name">
                    </div>

                    <div class="form-group col-md-12">
                      <label>Username</label>
                      <input type="text" name="username" value="<?php echo $data[0]->username; ?>" class="form-control required" placeholder="Enter Username">
                    </div>
					
                    <div class="form-group col-md-6">
                      <label>Email ID</label>
                      <input type="email" name="email_id" value="<?php echo decrypt_data($data[0]->email_id); ?>" class="form-control required" placeholder="Enter Email ID">
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label>Phone</label>
                      <input type="text" name="phone_no" value="<?php echo decrypt_data($data[0]->phone_no); ?>" class="form-control required" placeholder="Enter Phone No" pattern="[0-9]{10}">
                    </div>
                    
                    <div class="form-group col-md-6">
                      <label>Role</label>
                      <select class="form-control select2 required" name="role_id">
                        
                        <?php
                        if($roles) {
                          foreach($roles as $role) {
                            $s = ($role->roleID == $data[0]->role_id) ? "selected" : "";
                        ?>
                        <option <?php echo $s; ?> value="<?php echo $role->roleID; ?>"><?php echo $role->role_name; ?></option>
                        <?php
                          }
                        }
            
                        ?>
                      </select>
                    </div>

                    <!-- <div class="form-group col-md-6">
                      <label>Status</label>
                      <select class="form-control select2 required" name="status">
                        <option <?php echo ($data->status == 1) ? "selected" : ""; ?> value="1">Enable</option>
                        <option <?php echo ($data->status == 0) ? "selected" : ""; ?> value="0">Disable</option>
                      </select>
                    </div>-->
                    
                    <div class="form-group col-md-12">
                      <label>Profile Picture</label>
                      <input name="profile_pic" id="profile_pic" type="file" class="" accept="image/*">
                      <img src="<?php echo base_pic_url().$data[0]->profile_picture;?>" alt=""  style="width:100px; height:100px"/>
                    </div> 
                    <!-- /.box-body -->

                  <div class="box-footer"  style="clear:left">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                
              </div><!-- /.box -->

             </form>

            </div>
            <!--/.col (Add) -->
            
          </div>   <!-- /.row -->
          
          <!-- <div class="col-md-5">
              
              
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Reset Password</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div>
                <form method="post" class="validate" role="form">
                  <div class="box-body">
                    
                    <div class="form-group col-md-12">
                      <label>Old Password</label>
                      <div class="input-group">
                      <input type="password" name="o_password" class="form-control required" placeholder="Enter Old Password">
                      <span class="input-group-btn">
							<button class="btn btn-flat show-pwd-btn" type="button">
								<i class="fa fa-eye"></i>
							</button>
						</span>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                      <label>New Password</label>
                      <div class="input-group">
                      <input type="password" name="n_password" class="form-control required" placeholder="Enter New Password">
                      <span class="input-group-btn">
							<button class="btn btn-flat show-pwd-btn" type="button">
								<i class="fa fa-eye"></i>
							</button>
						</span>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                      <label>Confirm Password</label>
                      <div class="input-group">
                      <input type="password" name="c_password" class="form-control required" placeholder="Enter Password Again">
                      <span class="input-group-btn">
							<button class="btn btn-flat show-pwd-btn" type="button">
								<i class="fa fa-eye"></i>
							</button>
						</span>
                        </div>
                    </div>
                    
                    

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" name="reset_pwd">Update</button>
                  </div>
                </div>
                </form>
              

             

            </div>
            
          </div>
          </div> -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
<?php
}
else {
	$this->load->view("error_500");
}
?>