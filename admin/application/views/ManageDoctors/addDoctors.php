<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Add New Doctor</h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php
            if($this->session->flashdata('message')) {
              $message = $this->session->flashdata('message');        
          ?>
          <div class="callout callout-<?php echo $message['class']; ?>" id="successMessage">
            <h4><?php echo $message['title']; ?></h4>
            <p><?php echo $message['message']; ?></p>
          </div>
          <?php
          }
          ?>
        <!-- general form elements -->
          <form method="post" data-parsley-validate="" class="validate" role="form" enctype="multipart/form-data">
           
            <div class="box box-solid box-info">
              <div class="box-header">
                <h3 class="box-title">PERSONAL DATA</h3>
              </div><!-- /.box-header -->
              <!-- form start -->
              <div class="box-body">
                <div class="row">
                  
                  <div class="form-group col-md-6">
                    <label>Email</label>
                      <input  name="reg_pat_email" data-parsley-maxlength="100" type="text" data-parsley-required="true" data-parsley-email="" data-parsley-emailalreadyexist="" class="ip_reg_form_input form-control reset-form-custom" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"   placeholder="E-mail" id="reg-form-email" >
                  </div>
                  <div class="form-group col-md-6">
                    <label>RG</label>
                    <input name="reg_pat_rg" data-parsley-maxlength="25" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="RG">
                  </div> 
                </div>
                <div class="row">
                  
                  <div class="form-group col-md-6">
                    <label>CPF</label>
                    <input name="reg_pat_cpf" data-parsley-required="true"  data-parsley-length="[11,11]" type="number" class="ip_reg_form_input form-control reset-form-custom" placeholder="CPF" data-parsley-cpf="">
                  </div> 
                  <div class="form-group col-md-6">
                    <label>CRM</label>
                    <input name="reg_pat_crm"  data-parsley-maxlength="25" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="CRM">
                  </div> 
                </div>
                <div class="row">
                  
                   <div class="form-group col-md-6">
                    <label>Specialization</label>
                     <select class="form-control" data-placeholder="Select a State"
                      style="width: 100%;" name="specialization" data-parsley-required>
                        <option value="-1">Select Your Specialization</option>
                        <?php  
                        foreach ($data as $key => $value) 
                        {
                        ?>
                          <option value="<?php echo $value['id'] ?>"><?php echo $value["specialization_name"] ?></option>
                        <?php
                          }
                        ?>
                      </select> 
                  </div>
                   <div class="form-group col-md-6">
                    <label>Price</label>
                    <input name="price"  data-parsley-length="[2,5]" type="number" class="ip_reg_form_input form-control reset-form-custom" data-parsley-required="true" placeholder="PRICE">
                  </div>
                  <div class="form-group  col-md-6" >
                    <label>Date of Birth</label>
                    <div class="input-group date" id="sandbox-container">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input name="reg_pat_dob" class="ip_reg_form_input form-control reset-form-custom" data-parsley-required="true"/>
                    </div>
                  </div>
                  <div class="form-group col-md-6">
                    <div class="ip_day_time_schedule_details_data p0 floatLeft ip_gender_check">
                      <label for="reg-form-patient-male" class="ip_custom_checkbox_label1 ip_gender_check_label">Male</label>
                      <input id="reg-form-patient-male" class="ip_custom_checkbox1 ip_gender_check_checkbox " name="reg_pat_gender" type="radio"   value="MALE">
                    </div>
                    <div class="ip_day_time_schedule_details_data p0 floatLeft ip_gender_check">
                       <label for="reg-form-patient-female" class="ip_custom_checkbox_label1 ip_gender_check_label">Female</label>
                       <input id="reg-form-patient-female" class="ip_custom_checkbox1 ip_gender_check_checkbox "  name="reg_pat_gender" type="radio" value="FEMALE">
                    </div>
                    <div class="ip_day_time_schedule_details_data p0  floatLeft ip_gender_check">
                      <label for="reg-form-patient-others" class="ip_custom_checkbox_label1 ip_gender_check_label">Others</label>
                      <input id="reg-form-patient-others" class="ip_custom_checkbox1 ip_gender_check_checkbox "  name="reg_pat_gender" type="radio" value="OTHERS"  data-parsley-required="true">
                    </div>
                  </div>
                </div>
              </div>
                    
                    
                   
                   
                    
                    
                  
                   
                    
                
                </div>
                  <div class="box box-solid box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title">ADDRESS</h3>
                        
                    </div>
                    <div class="box-body">
                      <div class="form-group col-md-6">
                        <label>CEP</label>
                        <input data-parsley-required name="reg_pat_cep" data-parsley-length="[8,8]" type="number" class="ip_reg_form_input reset-form-custom form-control" placeholder="CEP" id="doctor_cep" data-parsley-cep="">
                      </div>
                      <div class="form-group col-md-6">
                        <label>RUA </label>
                        <input data-parsley-required name="reg_pat_streetadd" data-parsley-maxlength="50" type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="Rua" id="doctor_rua">
                      </div>
                      <div class="form-group col-md-6">
                        <label>Neighborhood </label>
                        <input data-parsley-required data-parsley-maxlength="50" name="reg_pat_locality" type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="Neighborhood">
                      </div>
                      <div class="form-group col-md-6">
                        <label>Number </label>
                        <input data-parsley-required data-parsley-maxlength="30" name="reg_pat_number" type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="Number">
                      </div>
                      <div class="form-group col-md-12">
                        <label>Complement </label>
                        <input data-parsley-maxlength="50" name="reg_pat_complement" type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="Complement">
                      </div>
                    </div>
                  </div>
                  <div class="box box-solid box-info">
                    <div class="box-header">
                      <h3 class="box-title">LOGIN AND PASSWORD</h3>
                        
                    </div>
                    <div class="box-body">
                      <div class="form-group">
                        <label>Name </label>
                        <input data-parsley-required id="reg-form-patient-name" data-parsley-pattern="[A-Za-z]+" name="reg_pat_name" data-parsley-length="[5,40]" type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="Name">
                      </div>
                      <div class="form-group">
                        <label>User Name </label>
                        <input data-parsley-required  data-parsley-username="" data-parsley-unamealreadyexist="" name="reg_pat_username" data-parsley-length="[5,25]" data-parsley-pattern="[A-Za-z0-9]+" type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="User Name">
                      </div>
                      <div class="form-group">
                        <label>Password</label>
                        <div class="input-group">
                        <input id="reg-form-pass" data-parsley-required name="reg_pat_password" data-parsley-length="[8,25]" type="password" class="ip_reg_form_input reset-form-custom form-control" placeholder="Password">
                        <span class="input-group-btn">
                          <button class="btn btn-flat show-pwd-btn" type="button">
                            <i class="fa fa-eye"></i>
                          </button>
                        </span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label>Confirm Password</label>
                        <div class="input-group">
                        <input data-parsley-equalto="#reg-form-pass" data-parsley-required name="reg_pat_confirmpassword" data-parsley-length="[8,25]" type="password"  class="ip_reg_form_input reset-form-custom form-control" placeholder="Confirm Password">
                        <span class="input-group-btn">
                          <button class="btn btn-flat show-pwd-btn" type="button">
                            <i class="fa fa-eye"></i>
                          </button>
                        </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box box-solid box-info">
                      <div class="box-header">
                        <h3 class="box-title">ADDITIONAL DATA</h3>
                      </div>
                      <div class="box-body">
                        
                        <div class="form-group">
                          <label>Profile Picture</label>
                          <input name="profile_pic" id="profile_pic" type="file" data-parsley-required class="required" accept="image/*" onchange="readURL(this)" style="display: none;">
                          <div>
                            <img alt=""  style="width:100px; height:100px" id="preview_profile_pic" />
                          </div>
                        </div>
                        <div class="form-group">
                          <label>Biography</label>
                          <textarea name="reg_pat_biography"  data-parsley-length="[15,500]" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Biography" data-parsley-required></textarea>
                        </div> 

                       <!--  <div class="form-group">
                          <label>Photos</label>
                           <input name="multiple_pic[]" id="multiple_pic" type="file"  multiple accept="image/*" style="display: none;" onchange="readURL_multiple(this)">
                            <input name="filearray[]" id="filearray" style="display:none" value=""/>
                            <div id="preview_multiple_pic">
                           
                            </div>
                            <div id="preview_multiple_cpypic">
                            
                           
                            </div>
                        </div> -->
                      </div>
                      <div class="box-footer">
                          <button type="submit" class="btn btn-info btn-lg">Submit</button>
                      </div>
                  </div> 
         </form>
                      
                  
              

            
            <!--/.col (Add) -->
           
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
