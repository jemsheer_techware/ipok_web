<?php $userData = $this->session->userdata('logged_in');
?>
<div class="content-wrapper">    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1></h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php
            if($this->session->flashdata('message')) {
              $message = $this->session->flashdata('message');        
          ?>
          <div class="alert alert-<?php echo $message['class']; ?>" id="successMessage">
            <button class="close" data-dismiss="alert" type="button">×</button>
        <!--     <h4><?php echo $message['title']; ?></h4> -->
            <p><?php echo $message['message']; ?></p>
          </div>
          <?php
          }
          ?>
        </div>
       <!--/.col (Add) -->
            <div class="col-xs-12">

              <div class="box box-solid box-info">
                <div class="box-header">
                  <h3 class="box-title">Registered Doctors</h3>

                </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Name </th>
                        <th> Gender </th>
                        <th> Email ID </th>
                        <th> Address </th>
                        <th>Status</th>
                        <th> Pic </th>
                        <?php 
                        if(isset($userData['uType']))
                        {
                        if($userData['uType'] == 'clinic')
                          {
                        ?>
                        <th> Options </th>
                        
                        <?php
                          }
                          }
                          ?>

                      </tr>
                    </thead>
                    <tbody>
                    
                      <?php
                    if($data) {
                      foreach($data as $doctor) 
                      {
                        if(isset($doctor['id']))
                          {?>
                              <tr>
                              <td class="hidden"><?php echo $doctor['id']; ?></td>
                              <td><?php echo $doctor['name']; ?></td>
                              <td><?php echo $doctor['gender']; ?></td>
                              <td><?php echo $doctor['email']; ?></td>
                              <td><?php echo $doctor['street_address'].', '.$doctor['locality'].', '.$doctor['cep']; ?></td>
                              <td>  <?php
                                if( $doctor['account_status'] =='0'){ ?>
                                    <a class="btn btn-sm label-success" href="<?php echo base_url();?>ManageDoctors/status/<?php echo $doctor['id']; ?>" > 
                                    <i class="fa fa-folder-open"></i> Enable </a>           
                                    <?php
                                }                                
                                else
                                {
                                ?>
                                  <a class="btn btn-sm label-danger" href="<?php echo base_url();?>ManageDoctors/status_active/<?php echo $doctor['id']; ?>"> 
                                  <i class="fa fa-folder-o"></i> Disable </a>
                                  <?php
                                } ?>
                              </td>
                              <td><img src="" alt="" width="50px" height="50px"/></td>
                              <?php 
                              if(isset($userData['uType']))
                              {
                                if($userData['uType'] == 'clinic' && isset($data_existDoctor))
                                {
                                ?>
                                  
                                  <?php
                                   $show_flag = 0;
                                  foreach ($data_existDoctor as $key_data_existDoctor => $value_data_existDoctor) {
                                   if($value_data_existDoctor['doctor_id'] == $doctor['id']){
                                    $show_flag = 1;
                                    
                                   }

                                   
                                  }
                                  ?>
                                  <td>
                                  <?php if($show_flag == 0){
                                  ?>
                                    <a class='btn btn-sm btn-primary' href='<?php echo base_url(); ?>ManageDoctors/assign_clinic_doctor/<?php echo $doctor['id']; ?>'><i class='fa fa-fw fa-edit'></i>Assign to Clinic </a> 
                                    <?php
                                  }
                                    ?>
                                  </td>
                                <?php
                                }
                              }
                              ?>
                              </tr>
                          <?php
                          }

                        }
                      }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Name </th>
                        <th> Gender </th>
                        <th> Email ID </th>
                        <th> Address </th>
                        <th>Status</th>
                        <th> Pic </th>
                         <?php 
                          if(isset($userData['uType'])){
                          if($userData['uType'] == 'clinic')
                            {
                          ?>
                        <th>Options</th>
                        <?php
                          }
                          }
                          ?>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->