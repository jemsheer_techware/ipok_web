<div class="content-wrapper">
        
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Booking List</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <!-- Add -->
        <div class="col-md-12">
        <?php
          if($this->session->flashdata('message')) {
            $message = $this->session->flashdata('message');
        ?>
        <div class="callout callout-<?php echo $message['class']; ?>" id="successMessage">
          <h4><?php echo $message['title']; ?></h4>
          <p><?php echo $message['message']; ?></p>
        </div>
        <?php
        }
        ?>
        </div> 
          <div class="col-xs-12">

              <div class="box box-solid box-info">
                
                <div class="box-body">
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Clinic Name </th>
                        <th> Booking Date </th>
                        <th> Booking Time </th>
                        <?php 
                        if($this->session->userdata('logged_in')['id'] == 0){
                        ?>
                        <th> Doctor Name </th>
                        <?php 
                        }
                        ?>
                        
                        <th> Pic </th>
                        
                        
                  
                      </tr>
                    </thead>
                    <tbody>
                    
                      <?php
                    if($data) {
                      foreach($data as $bookinglist) {
                        if(isset($bookinglist['id'])){?>
                            
                     
                              <tr>
                              <td class="hidden"><?php echo $bookinglist['id']; ?></td>
                              <td><?php echo $bookinglist['clinicName']; ?></td>
                              
                             <td><?php echo date('d-m-Y',$bookinglist['date']); ?></td>
                              <td><?php echo $bookinglist['time']; ?></td> 
                              <?php 
                              if($this->session->userdata('logged_in')['id'] == 0){
                              ?>
                              <td> <?php echo $bookinglist['name']; ?> </td>
                              <?php 
                              }
                              ?>
                              <td><img src="<?php echo base_pic_url().$bookinglist['profile_pic']; ?>" alt="" width="50px" height="50px"  /></td>
                             
                              </tr>
                      <?php
                    }
                    }
                  }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Name </th>
                        <th> Booking Date </th>
                        <th> Booking Time </th>
                        <?php 
                        if($this->session->userdata('logged_in')['id'] == 0){
                        ?>
                        <th> Clinic </th>
                        <?php 
                        }
                        ?>
                        <th> Pic </th>
                        
                       
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
            <!-- /.row -->
        </section><!-- /.content -->
      </div>