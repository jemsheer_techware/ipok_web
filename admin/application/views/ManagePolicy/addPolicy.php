      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Policy
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
        if($this->session->flashdata('message')) {
          $message = $this->session->flashdata('message');
        ?>
        <div class="alert alert-<?php echo $message['class']; ?>" id="successMessage">
           <button class="close" data-dismiss="alert" type="button">×</button>
            <!--     <h4><?php echo $message['title']; ?></h4> -->

                <p><?php echo $message['message']; ?></p>

        </div>
        <?php
        }
        ?>
              <!-- general form elements -->
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Policy</h3>
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
                      <i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form" >
                  <div class="box-body">
                    
                    <div class="form-group">
                      <div class="col-md-6">

                        <label>Minimun Time for Cancel Booking( Hrs)</label>
                        <div>
                            <select name="duration" id="duration" class="form-control select2" data-parsley-required>       
                              <option value="-1">Select Duration</option>
                                <?php                                
                                    for ($i=0; $i <= 24; $i++) {       
                                ?>                                 
                                    <option <?php if($data['duration'] == $i){ echo "SELECTED"; }?> value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php 
                                  }
                                ?>
                            </select>
                        </div>
                      </div>
                    </div><br><br><br>
                    <div class="form-group">
                      <div class="col-md-12">
                        <label>Cancelation Policy</label>
                         <textarea id="editor1"  type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Cancelation Policy" name="cancelation_policy"  data-parsley-required rows="10" cols="80" ><?php echo $data['cancelation_policy'];?> </textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12"><br>
                        <label>Waiting List Policy</label>
                         <textarea id="editor2"  type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Waiting List Policy" name="waiting_policy"  data-parsley-required rows="10" cols="80" ><?php echo $data['waiting_policy'];?> </textarea>
                      </div>
                    </div>
                    <!--  <div class="form-group">
                      <div class="col-md-12">
                   <label for="inputPassword3" class="col-sm-2 control-label">Description</label>
                   <div class="col-sm-12">
                      <div class="col-sm-12">
                     <div class="box box-info">
                       <div class="box-body pad">
                          <textarea id="editor1"  class="description" name="description" rows="10" cols="150" required=""> </textarea>
                       </div>
                     </div>
                   </div>
                 </div>
               </div>
              </div> -->

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

             

            </div>
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
