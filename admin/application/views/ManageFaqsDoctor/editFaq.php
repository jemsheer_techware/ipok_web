      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage FAQ's for Doctors
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
               if($this->session->flashdata('message')) {
                  $message = $this->session->flashdata('message');
               ?>
               <div class="alert alert-<?php echo $message['class']; ?>">
                  <button class="close" data-dismiss="alert" type="button">×</button>
                  <?php echo $message['message']; ?>
               </div>
               <?php
               }
            ?>
            </div>
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Add FAQ for Doctor</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post"  enctype="multipart/form-data" class="validate" data-parsley-validate>
                  <div class="box-body">
                    
                    <div class="form-group col-md-11">
                      <label>Question</label>
                      <textarea name="faq_title"  data-parsley-minlength="8"   type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Question" data-parsley-required><?php echo $data->faq_title;?></textarea>
                    </div>
                    <div class="form-group  col-md-11">
                      <label>Answer</label>
                        <textarea name="faq_description"  data-parsley-minlength="8"  type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Answer" data-parsley-required><?php echo $data->faq_description;?></textarea>
                    </div> 
                    
                    
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

             

            </div>
            <!--/.col (Add) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
