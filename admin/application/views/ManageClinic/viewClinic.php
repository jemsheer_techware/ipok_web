<div class="content-wrapper">
        
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Manage Clinic</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <!-- Add -->
        <div class="col-md-12">
        <?php
          if($this->session->flashdata('message')) {
            $message = $this->session->flashdata('message');
        ?>
        <div class="callout callout-<?php echo $message['class']; ?>" id="successMessage">
          <h4><?php echo $message['title']; ?></h4>
          <p><?php echo $message['message']; ?></p>
        </div>
        <?php
        }
        ?>
        </div> 
          <div class="col-xs-12">

              <div class="box box-solid box-info">
                <div class="box-header">
                  <h3 class="box-title">Registered Clinics</h3>
                 
                </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Name </th>
                        
                        <th> Email ID </th>
                        <th> Address </th>
                        <th> Pic </th>
                        <th> Options </th>
                        
                  
                      </tr>
                    </thead>
                    <tbody>
                    
                      <?php
                    if($data) {
                      foreach($data as $role) {
                        if(isset($role['id'])){?>
                            
                     
                              <tr>
                              <td class="hidden"><?php echo $role['id']; ?></td>
                              <td><?php echo $role['name']; ?></td>
                              
                              <td><?php echo $role['email']; ?></td>
                              <td><?php echo $role['street_address'].', '.$role['locality'].', '.$role['cep']; ?></td>
                              <td><img src="<?php echo base_pic_url().$role['profile_photo']; ?>" alt="" width="50px" height="50px"  /></td>
                              <td>
                              <a class='btn btn-sm btn-warning'  href='<?php echo base_url(); ?>ManageClinic/addDoctor?id=<?php echo $role['id']; ?>'>  Assign Doctors </a> 
                              <a class='btn btn-sm btn-primary' href='<?php echo base_url(); ?>ManageClinic/edit?id=<?php echo $role['id']; ?>'> <i class='fa fa-fw fa-edit'></i> Edit </a> 
                              <a class='btn btn-sm btn-danger' href='<?php echo base_url(); ?>ManageClinic/delete?id=<?php echo $role['id']; ?>'> <i class='fa fa-fw fa-trash'></i> Delete </a>
                              </td>
                              </tr>
                      <?php
                    }
                    }
                  }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Name </th>
                        
                        <th> Email ID </th>
                        <th> Address </th>
                        <th> Pic </th>
                        
                        <th>Options</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
            <!-- /.row -->
        </section><!-- /.content -->
      </div>