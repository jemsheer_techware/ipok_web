<div class="content-wrapper">

  <section class="content-header">
    <h1>Add New Clinic</h1>
  </section>
  
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <?php
          if($this->session->flashdata('message')) {
            $message = $this->session->flashdata('message');        
        ?>
        <div class="callout callout-<?php echo $message['class']; ?>" id="successMessage">
          <h4><?php echo $message['title']; ?></h4>
          <p><?php echo $message['message']; ?></p>
        </div>
        <?php
        }
        ?>      
        <form method="post" class="validate" role="form" enctype="multipart/form-data" data-parsley-validate>        
          <div class="box box-solid box-info">
            <div class="box-header">
              <h3 class="box-title">PERSONAL DATA</h3>
            </div>            
            <div class="box-body">
              <div class="row">                
                <div class="form-group col-md-6">                                   
                  <label>Email</label>
                    <input  name="reg_clnc_email" data-parsley-maxlength="100" type="text" data-parsley-required="true" data-parsley-email="" data-parsley-clncemailalreadyexist="" class="ip_reg_form_input form-control reset-form-custom" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"   placeholder="E-mail" id="reg-form-email" >                
                </div>
                <div class="form-group col-md-6">
                  <label>About</label>
                  <textarea name="reg_clnc_about" data-parsley-required  data-parsley-length="[15,500]" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="About"></textarea>
                </div> 
              </div>
            </div>          
          </div>
          <div class="box box-solid box-info">
            <div class="box-header with-border">
              <h3 class="box-title">ADDRESS</h3>
            </div>
            <div class="box-body">
              <div class="form-group col-md-6">
                <label>CEP</label>
                <input data-parsley-required name="reg_clnc_cep" data-parsley-length="[8,8]"  type="number" class="ip_reg_form_input reset-form-custom form-control" data-parsley-cep="" placeholder="CEP" id="reg_clnc_cep">
              </div>
              <div class="form-group col-md-6">
                <label>RUA </label>
                <input data-parsley-required name="reg_clnc_streetadd" data-parsley-maxlength="50"  type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="Rua" id="reg_clnc_streetadd">
              </div>
              <div class="form-group col-md-6">
                <label>Neighborhood </label>
                <input data-parsley-required data-parsley-maxlength="50" name="reg_clnc_locality" type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="Neighborhood">
              </div>
              <div class="form-group col-md-6">
                <label>Number </label>
                <input data-parsley-required data-parsley-maxlength="30" name="reg_clnc_number" type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="Number">
              </div>
              <div class="form-group col-md-12">
                <label>Complement </label>
                <input data-parsley-maxlength="50" name="reg_clnc_complement" type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="Complement">
              </div>
              <input type="hidden" name="clnc_latitude" id="clnc_latitude">
              <input type="hidden" name="clnc_longitude" id="clnc_longitude">
            </div>
          </div>
          <div class="box box-solid box-info">
            <div class="box-header">
              <h3 class="box-title">LOGIN AND PASSWORD</h3>                      
            </div>
            <div class="box-body">
              <div class="form-group">
                <label>Name </label>
                <input data-parsley-required id="reg-form-clinic-name" name="reg_clnc_name" maxlength="100" type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="Name">
              </div>
              <div class="form-group">
                <label>User Name </label>
                <input data-parsley-required   data-parsley-clncunamealreadyexist="" name="reg_clnc_username" data-parsley-length="[5,25]" data-parsley-pattern="[A-Za-z0-9]+" type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="User Name">
              </div>
              <div class="form-group">
                <label>Password</label>
                <div class="input-group">
                  <input id="reg-form-pass" data-parsley-required name="reg_clnc_password" data-parsley-length="[8,25]" type="password" class="ip_reg_form_input reset-form-custom form-control" placeholder="Password">
                  <span class="input-group-btn">
                    <button class="btn btn-flat show-pwd-btn" type="button">
                      <i class="fa fa-eye"></i>
                    </button>
                  </span>
                </div>
              </div>
              <div class="form-group">
                <label>Confirm Password</label>
                <div class="input-group">
                  <input data-parsley-equalto="#reg-form-pass" data-parsley-required name="reg_clnc_confirmpassword" data-parsley-length="[8,25]" type="password"  class="ip_reg_form_input reset-form-custom form-control" placeholder="Confirm Password">
                  <span class="input-group-btn">
                    <button class="btn btn-flat show-pwd-btn" type="button">
                      <i class="fa fa-eye"></i>
                    </button>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="box box-solid box-info">
            <div class="box-header">
              <h3 class="box-title">ADDITIONAL DATA</h3>
            </div>
            <div class="box-body">                
              <div class="form-group">
                <label>Picture</label>
                <input name="profile_pic" id="clnc_profile_pic" type="file" data-parsley-required class="required" accept="image/*" onchange="readURL_clnc(this)" style="display: none;">
                <div>              
                  <img alt=""  style="width:100px; height:100px" id="clnc_preview_profile_pic" />
                </div>
              </div>                
            </div>
            <div class="box-footer">
              <button type="submit" class="btn btn-info btn-lg">Submit</button>
            </div>
          </div> 
        </form>
      </div>          
    </div>
  </section>
</div>


<script type="text/javascript"> 
  var  myVarSet =setInterval(getaddress, 500);
  document.getElementById("reg_clnc_streetadd").addEventListener("change", getaddress);
  document.getElementById("reg_clnc_cep").addEventListener("change", getaddress);
  document.getElementById("reg-form-clinic-name").addEventListener("change", getaddress);
  function getaddress() {
    if(document.getElementById('reg_clnc_streetadd').value != '' && document.getElementById('reg_clnc_cep').value != '' && document.getElementById('reg-form-clinic-name').value !='')
    {
      var address =  document.getElementById('reg-form-clinic-name').value +','+ document.getElementById('reg_clnc_streetadd').value +','+ document.getElementById('reg_clnc_cep').value;                  
      getLatitudeLongitude(showResult, address)
      clearInterval(myVarSet);
    }
  }
</script>