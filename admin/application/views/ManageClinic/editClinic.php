<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Edit Clinic Data</h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php
            if($this->session->flashdata('message')) {
              $message = $this->session->flashdata('message');        
          ?>
          <div class="callout callout-<?php echo $message['class']; ?>" id="successMessage">
            <h4><?php echo $message['title']; ?></h4>
            <p><?php echo $message['message']; ?></p>
          </div>
          <?php
          }
          ?>
        <!-- general form elements -->
          <form method="post" class="validate" role="form" enctype="multipart/form-data" data-parsley-validate>
           
            <div class="box box-solid box-info">
              <div class="box-header">
                <h3 class="box-title">PERSONAL DATA</h3>
              </div><!-- /.box-header -->
              <!-- form start -->
              <div class="box-body">
                <div class="row">
                  
                  <div class="form-group col-md-6">
                                     
                    <label>Email</label>
                      <input  name="reg_clnc_email" maxlength="100" type="text" data-parsley-required="true" data-parsley-email=""  class="ip_reg_form_input form-control reset-form-custom" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"   placeholder="E-mail" id="reg-form-email" value="<?php echo $data['email'];?>">
                  
                  </div>
                  <div class="form-group col-md-6">
                    <label>Name </label>
                    <input data-parsley-required id="reg-form-clinic-name" name="reg_clnc_name" maxlength="100" type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="Name" value="<?php echo $data['name'];?>">
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-12">
                    <label>About</label>
                    <textarea name="reg_clnc_about" data-parsley-required  type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="About" ><?php echo $data['about'];?></textarea>
                  </div> 
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                          <label>Picture</label>
                          <input name="profile_pic" id="clnc_editprofile_pic" type="file"  accept="image/*" onchange="readURL_edit(this)" style="display: none;">
                          <div>
                      
                            <img alt="" src="<?php echo base_pic_url().$data['profile_photo'];?>" style="width:100px; height:100px"  id="preview_profile_edit"/>
                          </div>
                        </div>
                </div>
                <!-- <div class="row">
                  
                  <div class="form-group col-md-6">
                    <label>CPF</label>
                    <input name="reg_pat_cpf"  maxlength="100" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="CPF">
                  </div> 
                  <div class="form-group col-md-6">
                    <label>CRM</label>
                    <input name="reg_pat_crm"  maxlength="100" type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="CRM">
                  </div> 
                </div> -->

              </div>
                    
                    
                   
                   
                    
                    
                  
                   
                    
                
                </div>
                  <div class="box box-solid box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title">ADDRESS</h3>
                        
                    </div>
                    <div class="box-body">
                      <div class="form-group col-md-6">
                        <label>CEP</label>
                        <input data-parsley-required name="reg_clnc_cep"  type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="CEP" id="reg_clnc_cep" value="<?php echo $data['cep'];?>">
                      </div>
                      <div class="form-group col-md-6">
                        <label>RUA </label>
                        <input data-parsley-required name="reg_clnc_streetadd"  type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="Rua" id="reg_clnc_streetadd" value="<?php echo $data['street_address'];?>">
                      </div>
                      <div class="form-group col-md-6">
                      <label>Neighborhood </label>
                      <input data-parsley-required maxlength="100" name="reg_clnc_locality" type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="Neighborhood" value="<?php echo $data['locality'];?>">
                      </div>
                      <div class="form-group col-md-6">
                        <label>Number </label>
                        <input data-parsley-required maxlength="100" name="reg_clnc_number" type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="Number" value="<?php echo $data['number'];?>">
                      </div>
                      <div class="form-group col-md-12">
                        <label>Complement </label>
                        <input maxlength="100" name="reg_clnc_complement" type="text" class="ip_reg_form_input reset-form-custom form-control" placeholder="Complement" value="<?php echo $data['complement'];?>">
                      </div>
                      <input type="hidden" name="clnc_latitude" id="clnc_latitude" value="<?php echo $data['location_lattitude'];?>">
                      <input type="hidden" name="clnc_longitude" id="clnc_longitude" value="<?php echo $data['location_longitude'];?>">
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info btn-lg">Submit</button>
                    </div>
                  </div>
                  
         </form>
                      
                  
              

            
            <!--/.col (Add) -->
           
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->






























      

          <script type="text/javascript">
            
 
           var  myVarSet =setInterval(getaddress, 500);
           document.getElementById("reg_clnc_streetadd").addEventListener("change", getaddress);
          document.getElementById("reg_clnc_cep").addEventListener("change", getaddress);
          document.getElementById("reg-form-clinic-name").addEventListener("change", getaddress);
            function getaddress() {

              if(document.getElementById('reg_clnc_streetadd').value != '' && document.getElementById('reg_clnc_cep').value != '' && document.getElementById('reg-form-clinic-name').value !=''){
                  var address =  document.getElementById('reg-form-clinic-name').value +','+ document.getElementById('reg_clnc_streetadd').value +','+ document.getElementById('reg_clnc_cep').value;
                  //alert(address)
                  getLatitudeLongitude(showResult, address)
                  clearInterval(myVarSet);
              }
            }
          </script>