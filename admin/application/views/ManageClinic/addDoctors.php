<div class="content-wrapper">
  <section class="content-header">
    <h1>Assign Doctors</h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
            <?php
            if($this->session->flashdata('message')) {
              $message = $this->session->flashdata('message');        
          ?>
          <div class="alert alert-<?php echo $message['class']; ?>" id="successMessage">
             <button class="close" data-dismiss="alert" type="button">×</button>
            <h4><?php echo $message['title']; ?></h4>
            <p><?php echo $message['message']; ?></p>
          </div>
          <?php
          }
          ?>
        <div class="box box-solid box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Set Schedule</h3>
          </div><!-- /.box-header -->
          <form role="form" id="myAssign_Doctor_form" action="<?php echo base_url();?>ManageClinic/assignDoctors" method="post">            
            <div class="box-body">
            <input type="hidden" name="clinicId" class="form-control" value="<?php echo $data_id;?>">
              <?php
                if(!isset($data_doctorId) && isset($data)){
              ?>
                <div class="form-group">
                
                <label>Choose Doctors:</label>
                <select class="form-control select2" data-placeholder="Select a State" 
                          style="width: 100%;" name="doctors" id="doctor_select" data-parsley-required>
                          <option value="0" selected disabled>Select</option>
                          <?php 
                          foreach ($data as $key => $value) 
                          {
                          ?>
                                <option value="<?php echo $value['id'] ?>"><?php echo $value["name"] ?></option>
                          <?php
                            }
                          ?>
                </select>                
              </div>
              <?php    
                }
                else if(isset($data_doctorId))
                {
              ?>

                 
              <?php
                }
              ?>

              <div class="form-group col-md-12">
                <div class="col-md-6">
                 <label>Duration(in Min)</label>
                      <div>   
                       <!--   <input type="hidden" name="doctors" id="doctorid" value=""> -->                         
                        <select name="duration" id="intervalTime" class="form-control select2" data-parsley-required>       
                          <option value="0" selected disabled>Select Duration</option>
                            <?php                                
                                for ($i=1; $i <= 60; $i++) {                                
                            ?>                                 
                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                            <?php 
                              }
                            ?>
                      </select>
                      </div>
                    </div>
        <!--       
               <div class="col-md-6">

                   <button type="submit" class="btn  btn-info" style="margin-top: 20px">Save</button>

            </div> -->
            </div>
  <!--             <div class="form-group col-md-12">
                <div class="ip_check_primary col-md-2">
                  <input id="reg-form-patient-male" name="schedule" type="radio"   value="Primary">
                   <label for="reg-form-Primary" class="ip_custom_checkbox_label1 ip_gender_check_label">Primary</label>
                </div>
                <div class="ip_check_primary col-md-2">
                   <input id="reg-form-patient-female" name="schedule" type="radio" value="Secondary">
                    <label for="reg-form-Secondary" class="ip_custom_checkbox_label1 ip_gender_check_label">Secondary</label>
                </div>
              </div> -->
  <form role="form" id="myAssign_Doctorsubmit_form" action="<?php echo base_url();?>ManageClinic/assignDoctors" method="post"> 
              <div class="chse-days-div">
                <label>Choose Days:</label>
                <div class="container">                  
                  <label class="container-cstm">Sunday
                    <input type="checkbox" id="Sunday" name="day[]" value="sun">
                    <span class="checkmark"></span>
                  </label>
                  <label class="container-cstm">Monday
                    <input type="checkbox" id="Monday" name="day[]" value="mon">
                    <span class="checkmark"></span>
                  </label>
                  <label class="container-cstm">Tuesday
                    <input type="checkbox" id="Tuesday" name="day[]" value="tue">
                    <span class="checkmark"></span>
                  </label>
                  <label class="container-cstm">Wednesday
                    <input type="checkbox" id="Wednesday" name="day[]" value="wed">
                    <span class="checkmark"></span>
                  </label>
                  <label class="container-cstm">Thursday
                    <input type="checkbox" id="Thursday" name="day[]" value="thu">
                    <span class="checkmark"></span>
                  </label>
                  <label class="container-cstm">Friday
                    <input type="checkbox" id="Friday" name="day[]" value="fri">
                    <span class="checkmark"></span>
                  </label>
                  <label class="container-cstm">Saturday
                    <input type="checkbox" id="Saturday" name="day[]" value="sat">
                    <span class="checkmark"></span>
                  </label>
                </div>              
              </div>
              <div class="form-group">
                <button type="button"  class="areasun btn-block  cmn-cls-fulbtn " data-toggle="collapse" data-target="#areasun" aria-controls="areasun">
                      Sunday Time
                </button>
                <div class="area1sun  collapse" id="areasun" disabled="disabled">
                  <div class="row">
                    <div class="col-md-3">
                      <label>Start Time</label>
                      <div class="input-group bootstrap-timepicker">
                        <input type="text" class="form-control input-small timepicker_clnc1" id="sun1" name="sun_startTime" disabled="disabled" readonly="true">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <label>End Time</label>
                      <div class="input-group  bootstrap-timepicker">
                        <input type="text" class="form-control input-small  timepicker_clnc1 sun_endTimes" id="timepicker_clnc2" name="sun_endTime" disabled="disabled" readonly="true">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                      </div>
                    </div>
         <!--            <div class="col-md-2">
                      <label>Duration</label>
                      <div>                            
                        <select name="sun_intervalTime" id="sun_intervalTime" class="form-control select2" disabled="disabled">       
                            <?php                                
                                for ($i=0; $i < 60; $i++) {                                
                            ?>                                 
                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                            <?php 
                              }
                            ?>
                        </select>
                        <label>Min</label>
                      </div>
                    </div> -->
                    <div class="col-md-2 intervalcheck">
                       <label>interval </label>
                    <input type="checkbox" id="sun" name="check" class="check" value="check_sun" style="margin-top: 30px">
                    </div>
                    <div class="inter_sun" disabled="disabled"  style="display: none">
                    <div class="col-md-2">
                      <label>Break From</label>
                      <div class="input-group bootstrap-timepicker">
                        <input type="text" class="form-control input-small timepicker_clnc1 sun_breaktostart" id="break1" name="sun_Breakfrom" disabled="disabled" readonly="true">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <label>Break to</label>
                      <div class="input-group  bootstrap-timepicker">
                        <input type="text" class="form-control input-small  timepicker_clnc1 breaktoend" id="sun_Breakto" name="sun_Breakto" disabled="disabled"  readonly="true">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                      </div>
                    </div>
                  </div>
                  </div>                     
                </div>
              </div>
              <div class="form-group">                  
                <button type="button"  class="areamon btn-block  cmn-cls-fulbtn " data-toggle="collapse" data-target="#areamon" aria-controls="areamon" disabled>
                      Monday Time
                </button>
                <div class="area1mon collapse" id="areamon">
                  <div class="row">
                    <div class="col-md-3">
                      <label>Start Time</label>
                      <div class="input-group bootstrap-timepicker">
                        <input type="text" class="form-control input-small timepicker_clnc1" id="mon1" name="mon_startTime" disabled="disabled" readonly="true">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <label>End Time</label>
                      <div class="input-group bootstrap-timepicker ">
                        <input type="text" class="form-control input-small timepicker_clnc1 mon_endTimes" id="timepicker_clnc5" name="mon_endTime" disabled="disabled" readonly="true">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                      </div>
                    </div>
          <!--           <div class="col-md-2">
                      <label>Interval</label>
                      <div>
                        <select name="mon_intervalTime" id="mon_intervalTime"  class="form-control select2" disabled="disabled" >          
                          <?php                              
                              for ($i=0; $i < 60; $i++) {                              
                          ?>                              
                              <option value="<?php echo $i;?>"><?php echo $i;?></option>
                          <?php 
                            }
                          ?>
                        </select>
                        <label>Min</label>
                      </div>
                    </div> -->
                        <div class="col-md-2 intervalcheck">
                       <label>interval </label>
                    <input type="checkbox" id="mon" name="check"  class="check" value="check_mon" style="margin-top: 30px">
                    </div>
                    <div class="inter_mon"  disabled="disabled"  style="display: none">
                      <div class="col-md-2">
                      <label>Break From</label>
                      <div class="input-group bootstrap-timepicker">
                        <input type="text" class="form-control input-small timepicker_clnc1 mon_breaktostart" id="break2" name="mon_Breakfrom" disabled="disabled" readonly="true">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <label>Break To</label>
                      <div class="input-group bootstrap-timepicker ">
                        <input type="text" class="form-control input-small timepicker_clnc1 breaktoend" id="mon_Breakto"  name="mon_Breakto" disabled="disabled" readonly="true">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                      </div>
                    </div>
                  </div>
                  </div>                    
                </div>
              </div>
              <div  class="form-group">                  
                <button type="button"  class="areatue btn-block  cmn-cls-fulbtn " data-toggle="collapse" data-target="#areatue" aria-controls="areatue" disabled>
                        Tuesday Time
                </button>
                <div class="area1tue collapse" id="areatue">
                  <div class="row">
                    <div class="col-md-3">
                      <label>Start Time</label>
                      <div class="input-group bootstrap-timepicker">
                        <input type="text" class="form-control input-small timepicker_clnc1" id="tue1" name="tue_startTime" disabled="disabled" readonly="true">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <label>End Time</label>
                      <div class="input-group bootstrap-timepicker">
                        <input type="text" class="form-control input-small timepicker_clnc1 tue_endTimes" id="timepicker_clnc8" name="tue_endTime" disabled="disabled" readonly="true">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                      </div>
                    </div>
           <!--          <div class="col-md-2">
                      <label>Interval</label>
                      <div>
                        <select name="tue_intervalTime" id="tue_intervalTime" class="form-control select2" disabled="disabled"  >          
                              <?php                                
                                  for ($i=0; $i < 60; $i++) {                                
                              ?>                                 
                                  <option value="<?php echo $i;?>"><?php echo $i;?></option>
                              <?php 
                                }
                              ?>
                        </select>
                        <label>Min</label>
                      </div>
                    </div> -->
                    <div class="col-md-2 intervalcheck">
                    <label>interval </label>
                    <input type="checkbox" id="tue" name="check"  class="check" value="check_tue" style="margin-top: 30px">
                    </div>
                    <div class="inter_tue"  disabled="disabled"  style="display: none">
                    <div class="col-md-2">
                      <label>Break From</label>
                      <div class="input-group bootstrap-timepicker">
                        <input type="text" class="form-control input-small timepicker_clnc1 tue_breaktostart" id="break3" name="tue_Breakfrom" disabled="disabled" readonly="true">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <label>Break To</label>
                      <div class="input-group bootstrap-timepicker">
                        <input type="text" class="form-control input-small timepicker_clnc1 breaktoend" id="tue_Breakto" name="tue_Breakto" disabled="disabled" readonly="true">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                      </div>
                    </div>
                  </div>
                  </div>                      
                </div>
              </div>
                  <div  class="form-group">
                  
                    <button type="button" class="areawed btn-block  cmn-cls-fulbtn" data-toggle="collapse" data-target="#areawed" aria-controls="areawed" disabled>

                        Wednesday Time
                    </button>
                    <div class="area1wed collapse" id="areawed">
                      <div class="row">
                        <div class="col-md-3">
                          <label>Start Time</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1" id="wed1" name="wed_startTime" disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <label>End Time</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1 wed_endTimes" id="timepicker_clnc11" name="wed_endTime" disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>

                        </div>
              <!--           <div class="col-md-2">
                        
                          <label>Interval</label>
                          <div>
                           <select name="wed_intervalTime" id="wed_intervalTime" class="form-control select2" disabled="disabled" >
                          
                            <?php
                              
                                for ($i=0; $i < 60; $i++) { 
                             
                                ?>
                               
                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                               <?php 
                              }
                            ?>
                          </select>
                          <label>Min</label>
                          </div>
                        </div> -->
                            <div class="col-md-2 intervalcheck">
                       <label>interval </label>
                    <input type="checkbox" id="wed" name="check"  class="check" value="check_wed" style="margin-top: 30px">
                    </div>
                    <div class="inter_wed" disabled="disabled"  style="display: none">
                        <div class="col-md-2">
                          <label>Break From</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1 wed_breaktostart" id="break4" name="wed_Breakfrom" disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <label>Break To</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1 breaktoend" id="wed_Breakto"  name="wed_Breakto" disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div  class="form-group">
                  
                    <button type="button"  class="areathu btn-block  cmn-cls-fulbtn " data-toggle="collapse" data-target="#areathu" disabled>

                        Thursday Time
                    </button>
                    <div class="area1thu collapse" id="areathu">
                      <div class="row">
                        <div class="col-md-3">
                          <label>Start Time</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1" id="thu1" name="thu_startTime" disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <label>End Time</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1 thu_endTimes" id="timepicker_clnc14" name="thu_endTime" disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>

                        </div>
           <!--              <div class="col-md-2">
                          <label>Interval</label>
                          <div>
                           <select name="thu_intervalTime" id="thu_intervalTime" class="form-control select2" disabled="disabled" >
                          
                            <?php
                              
                                for ($i=0; $i < 60; $i++) { 
                             
                                ?>
                               
                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                               <?php 
                              }
                            ?>
                          </select>
                          <label>Min</label>
                          </div>
                        </div> -->
                            <div class="col-md-2 intervalcheck">
                       <label>interval </label>
                    <input type="checkbox" id="thu" name="check"  class="check" value="check_thu" style="margin-top: 30px">
                    </div>
                    <div class="inter_thu" disabled="disabled"  style="display: none">
                        <div class="col-md-2">
                          <label>Break From</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1 thu_breaktostart" id="break5" name="thu_Breakfrom" disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <label>Break To</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1 breaktoend" id="thu_Breakto"  name="thu_Breakto" disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div  class="form-group">
                  
                    <button type="button" class="areafri btn-block  cmn-cls-fulbtn" data-toggle="collapse" data-target="#areafri" disabled>

                        Friday Time
                    </button>
                    <div class="area1fri collapse" id="areafri">
                      <div class="row">
                        <div class="col-md-3">
                          <label>Start Time</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1" id="fri1" name="fri_startTime" disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <label>End Time</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1 fri_endTimes" id="timepicker_clnc17" name="fri_endTime" disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>
                        </div>
          <!--               <div class="col-md-2">
                          <label>Interval</label>
                          <div>
                            
                             <select name="fri_intervalTime" id="fri_intervalTime" class="form-control select2"  disabled="disabled">
                            
                              <?php
                                
                                  for ($i=0; $i < 60; $i++) { 
                               
                                  ?>
                                 
                                  <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                 <?php 
                                }
                              ?>
                            </select>
                            <label>Min</label>
                          </div>
                        </div> -->
                            <div class="col-md-2 intervalcheck">
                       <label>interval </label>
                    <input type="checkbox" id="fri" class="check"  name="check" value="check_fri" style="margin-top: 30px">
                    </div>
                    <div class="inter_fri" disabled="disabled" style="display: none">
                        <div class="col-md-2">
                          <label>Break From</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1 fri_breaktostart" id="break6" name="fri_Breakfrom" disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <label>Break To</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1 breaktoend" id="fri_Breakto" name="fri_Breakto"  disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>
                        </div>
                      </div>
                      
                     
                    </div>
                  </div>
                  </div>
                  <div  class="form-group">
                  
                    <button type="button" class="areasat btn-block  cmn-cls-fulbtn"  data-toggle="collapse" data-target="#areasat" disabled>

                        Saturday Time
                    </button>
                    <div class="area1sat collapse" id="areasat">
                      <div class="row">
                        <div class="col-md-3">
                          <label>Start Time</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1" id="sat1" name="sat_startTime" disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <label>End Time</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1 sat_endTimes" id="timepicker_clnc20" name="sat_endTime" disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>
                        </div>
           <!--              <div class="col-md-2">
                          <label>Interval</label>
                          <div>
                           <select name="sat_intervalTime" id="sat_intervalTime" class="form-control select2" disabled="disabled" >
                             
                            <?php
                                for ($i=0; $i < 60; $i++) { 
                             
                                ?>
                               
                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                               <?php 
                              }
                            ?>
                          </select>
                          <label>Min</label>
                          </div>
                        </div> -->
                            <div class="col-md-2 intervalcheck">
                       <label>interval </label>
                    <input type="checkbox" id="sat" class="check"  name="check" value="check_sat" style="margin-top: 30px">
                    </div>
                    <div class="inter_sat" disabled="disabled"  style="display: none">
                        <div class="col-md-2">
                          <label>Break From</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1 sat_breaktostart" id="break7" name="sat_Breakfrom" disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <label>Break To</label>
                          <div class="input-group bootstrap-timepicker">
                            <input type="text" class="form-control input-small timepicker_clnc1 breaktoend" id="sat_Breakto"  name="sat_Breakto" disabled="disabled" readonly="true">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                          </div>
                        </div>
                      </div>
                      </div>
                      
                      
                       
                    </div>
                    
                  </div> 
            </div>
            <div class="box-footer">
                  <button type="submit" class="btn  btn-info">Submit</button>
                  <button type="button" class="btn  btn-danger" onclick="history.go(0)">CANCEL</button>
                  <button  type="button" class="btn  " onclick="location.href='<?php echo base_url();?>ManageDoctors/index'">REGISTER DOCTOR</button>
              
              
             
              
            </div>
          </form>      
        </div>
      </div>
    </div>
  </section>
  
</div>
 