<div class="content-wrapper">
        
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Patient List</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <!-- Add -->
          <div class="col-md-12">
            <?php
             if($this->session->flashdata('message')) {
                $message = $this->session->flashdata('message');
             ?>
             <div class="alert alert-<?php echo $message['class']; ?>">
                <button class="close" data-dismiss="alert" type="button">×</button>
                <?php echo $message['message']; ?>
             </div>
             <?php
             }
           ?>
          </div>
          <div class="col-xs-12">

              <div class="box box-solid box-info">
                <div class="box-header">
                  <h3 class="box-title">Patient List</h3>
                 
                </div>
                <div class="box-body">
                  <?php if(count($data) > 0){?>
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th>Name</th>
                        
                        <th>Email</th>
                        <th>Gender</th>
                        <th>Image</th>
                        <th>Status</th>
                        
                  
                      </tr>
                    </thead>
                    <tbody>
                    
                      <?php
                      foreach($data as $customer) {?>
                            
                     
                              <tr>
                              <td class="hidden"><?php echo $customer->id; ?></td>
                              <td><?php echo $customer->name; ?></td>
                              
                              <td><?php echo $customer->email; ?></td>
                              <td><?php echo $customer->gender; ?></td>
                              <td><img src="<?php echo base_pic_url().$customer->profile_photo; ?>" alt="" width="50px" height="50px"  /></td>
                               <td>  <?php
                                if( $customer->account_status =='0'){ ?>
                                    <a class="btn btn-sm label-success" href="<?php echo base_url();?>ManageCustomer/status/<?php echo $customer->id; ?>" > 
                                    <i class="fa fa-folder-open"></i> Enable </a>           
                                    <?php
                                }                                
                                else
                                {
                                ?>
                                  <a class="btn btn-sm label-danger" href="<?php echo base_url();?>ManageCustomer/status_active/<?php echo $customer->id; ?>"> 
                                  <i class="fa fa-folder-o"></i> Disable </a>
                                  <?php
                                } ?>
                              </td>
                              </tr>
                      <?php
                    }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                         <th class="hidden">ID</th>
                        <th>Name</th>
                        
                        <th>Email</th>
                        <th>Gender</th>
                        <th>Image</th>
                        <th>Status</th>
                      </tr>
                    </tfoot>
                  </table>
                  <?php } else{ ?>
                   <div>No Result Found</div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <!-- /.row -->
        </section><!-- /.content -->
      </div>