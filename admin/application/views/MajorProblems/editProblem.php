<?php
if(isset($data)) {
  
  ?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Problem
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <?php
               if($this->session->flashdata('message')) {
                  $message = $this->session->flashdata('message');
               ?>
               <div class="alert alert-<?php echo $message['class']; ?>">
                  <button class="close" data-dismiss="alert" type="button">×</button>
                  <?php echo $message['message']; ?>
               </div>
               <?php
               }
            ?>
            </div>
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-solid box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Problem</h3>
                  <!-- <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div> -->
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form" data-parsley-validate>
                  <div class="box-body">
                    
                    <div class="form-group">
                       <label>Name</label>
                      <input type="text" name="problem_name" class="form-control required" placeholder="Enter Problem Name" value="<?php echo $data->problem_name;?>" data-parsley-required="true">
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>

            </div>
            
          </div> 
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
<?php
}
else {
  $this->load->view("error_500");
}
?>