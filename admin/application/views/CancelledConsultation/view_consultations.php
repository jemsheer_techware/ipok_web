<div class="content-wrapper">
        
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>View Cancelled Consultations</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <!-- Add -->
          <div class="col-md-12">
            <?php
             if($this->session->flashdata('message')) {
                $message = $this->session->flashdata('message');
             ?>
             <div class="alert alert-<?php echo $message['class']; ?>">
                <button class="close" data-dismiss="alert" type="button">×</button>
                <?php echo $message['message']; ?>
             </div>
             <?php
             }
           ?>
          </div>
          <div class="col-xs-12">

              <div class="box box-solid box-info">
                <div class="box-header">
                  <h3 class="box-title">Cancelled Consultations</h3>
                 
                </div>
                <div class="box-body">
                  <?php if(count($data) > 0){?>
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th>Patient Name</th> 
                        <th>Doctor Name</th>
                        <th>Booking Date</th>
                        <th>Booking Time</th>
                        <th>Booking Amount</th>
                        <th>Status</th>
                  
                      </tr>
                    </thead>
                    <tbody>
                    
                      <?php
                      
                      foreach($data as $list) { ?>
                            
                     
                              <tr>
                              <td class="hidden"><?php echo $list['id']; ?></td>
                              <td><?php echo $list['patient_name']; ?></td>
                              
                              <td><?php echo $list['name']; ?></td>
                              <td><?php echo date('Y-m-d',$list['date']); ?></td>
                              <td><?php echo $list['time']; ?></td>
                              <td><?php echo $list['total_sum']; ?></td>
                              <td><?php if($list['is_refund'] == '0'){ ?>
                                  <a class='btn btn-sm btn-primary' href='<?php echo base_url(); ?>CancelledConsultations/refund_amount?id=<?php echo $list['id']; ?>'> <i class='fa fa-money'></i>  Not Refunded</a> 
                                <?php } else{ echo "Refunded"; }?>
                              </td>
                              </tr>
                      <?php
                    }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                         <th class="hidden">ID</th>
                        <th>Patient Name</th> 
                        <th>Doctor Name</th>
                        <th>Booking Date</th>
                        <th>Booking Time</th>
                        <th>Booking Amount</th>
                        <th>Status</th>
                      </tr>
                    </tfoot>
                  </table>
                  <?php } else{ ?>
                   <div>No Result Found</div>
                  <?php } ?>
                </div>
              </div>
            </div>
            <!-- /.row -->
        </section><!-- /.content -->
      </div>