<?php
if(isset($data)) {
  
	?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Edit Services
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
        if($this->session->flashdata('medicine_message')) {
          $message = $this->session->flashdata('medicine_message');
        ?>
        <div class="alert alert-<?php echo $message['class']; ?>">
                <h4><?php echo $message['title']; ?></h4>
                <p><?php echo $message['message']; ?></p>
        </div>
        <?php
        }
        ?>
              <!-- general form elements -->
              <div class="box box-solid box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Exams</h3>
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
                       <i class="fa fa-minus"></i>
                    </button>
                  </div> 
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form" data-parsley-validate>
                  <div class="box-body">
                   
                    <div class="form-group col-md-12">
                      <label>Charity</label>
                      <select class="form-control select2" placeholder="Select a State" style="width: 100%;" name="charity_id" disabled data-parsley-required="true">
                         <option value="0">Choose charity</option>
                                <?php 
                                foreach ($data as $key => $value) 
                                {
                                ?>
                                      <option <?php if($datas['charity_id'] == $value['id']) { echo "SELECTED" ;}?> value="<?php echo $value['id'];?>"><?php echo $value['title'];?></option>
                                <?php
                                  }
                                ?>
                      </select>                
                    </div>                
                    <div class="form-group col-md-12">
                      <label>Choose Clinic</label>
                      <select class="form-control select2" placeholder="Select a State" style="width: 100%;" name="clinic_id" disabled data-parsley-required="true">
                                <option value="0">Choose a clinic</option>
                                <?php 
                                foreach ($clinic as $key => $value) 
                                {
                                ?>
                                      <option <?php if($datas['clinic_id'] == $value['id']) { echo "SELECTED" ;}?> value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
                                <?php
                                  }
                                ?>
                      </select>                
                    </div>
                    <div class="form-group">
                      <div class="form-group col-md-12">
                      <label>Donation Amount</label></div>
                      <div class="form-group col-md-12">
                      <input type="text" name="amount" class="form-control required" placeholder="Enter Donation Amount" value="<?php echo $datas['amount'];?>" data-parsley-required="true" data-role="tagsinput"></div>
                    </div>

                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>

            </div>
            
          </div> 
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
<?php
}
else {
	$this->load->view("error_500");
}
?>