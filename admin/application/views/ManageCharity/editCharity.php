      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Promocode
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
               if($this->session->flashdata('message')) {
                  $message = $this->session->flashdata('message');
               ?>
               <div class="alert alert-<?php echo $message['class']; ?>">
                  <button class="close" data-dismiss="alert" type="button">×</button>
                  <?php echo $message['message']; ?>
               </div>
               <?php
               }
            ?>
            </div>
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Manage Promocode</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" data-parsley-validate enctype="multipart/form-data">
                  <div class="box-body">
                    
                     <div class="form-group col-md-6">
                      <label>title</label>
                      <input type="text" name="title" class="form-control required" placeholder="Enter title" data-parsely-minlength="10" value="<?php echo $data['title'];?>" data-parsley-required="true">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Image</label>
                        <input type="file" name="image" class="form-control"  value="" accept="image/*">
                         <img src="<?php echo base_pic_url().$data['image'];?>" alt=""  style="width:100px; height:100px"/>
                    </div>
                    <div class="form-group col-md-12">
                      <label>Description</label>
                      <textarea name="description"  data-parsley-minlength="8"   type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Description" data-parsley-required><?php echo $data['description'];?></textarea>
                    </div>

                    
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

             

            </div>
            <!--/.col (Add) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
