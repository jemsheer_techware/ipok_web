 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Add Charity
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
        if($this->session->flashdata('message')) {
          $message = $this->session->flashdata('message');
        ?>
        <div class="alert alert-<?php echo $message['class']; ?>" id="successMessage">
           <button class="close" data-dismiss="alert" type="button">×</button>
            <!--     <h4><?php echo $message['title']; ?></h4> -->

                <p><?php echo $message['message']; ?></p>

        </div>
        <?php
        }
        ?>
              <!-- general form elements -->
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Charity</h3>
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form" id="add_service_form" enctype="multipart/form-data">
                  <div class="box-body">
                    
                      <div class="form-group col-md-12">
                      <label>Charity</label>
                      <select class="form-control select2" placeholder="Select a State" style="width: 100%;" name="charity_id" data-parsley-required="true">
                         <option value="0">Choose charity</option>
                                <?php 
                                foreach ($data as $key => $value) 
                                {
                                ?>
                                      <option value="<?php echo $value['id'];?>"><?php echo $value['title'];?></option>
                                <?php
                                  }
                                ?>
                      </select>                
                    </div>                
                    <div class="form-group col-md-12">
                      <label>Choose Clinic</label>
                      <select class="form-control select2" placeholder="Select a State" style="width: 100%;" name="clinic_id" data-parsley-required="true">
                                <option value="0">Choose a clinic</option>
                                <?php 
                                foreach ($clinic as $key => $value) 
                                {
                                ?>
                                      <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
                                <?php
                                  }
                                ?>
                      </select>                
                    </div>
                    <div class="form-group">
                      <div class="form-group col-md-12">
                      <label>Donation Amount</label></div>
                      <div class="form-group col-md-12">
                      <input type="text" name="amount" class="form-control required" placeholder="Enter Donation Amount" data-parsley-required="true" data-role="tagsinput"></div>
                    </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

             

            </div>
            <!--/.col (Add) -->
           <div class="col-xs-12">

              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Saved Services</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Charity Name </th>
                        <th> Clinic Name</th>
                        <th> Amount</th>
                        <th width="150px">Options</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php
                    if($data) {
                      foreach($services as $data) {                     
                      
                      ?> 
                              <tr>
                              <td class="hidden"><?php echo $data['id'];?></td>
                              <td><?php echo $data['title'];?></td>
                              <td><?php echo $data['name'];?></td>
                               <td><?php echo $data['amount'];?></td>
                              <td>
                              <a class='btn btn-sm btn-primary' href='<?php echo base_url(); ?>ManageCharity/service_edit/<?php echo $data['id']; ?>'> <i class='fa fa-fw fa-edit'></i> Edit </a> 
                              <a class='btn btn-sm btn-danger' href='<?php echo base_url(); ?>ManageCharity/service_delete/<?php echo $data['id']; ?>'> <i class='fa fa-fw fa-trash'></i> Delete </a>
                              </td>
                              </tr>
                     <?php
                    }
                  }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Charity Name </th>
                        <th> Clinic Name</th>
                        <th> Amount</th>
                        <th>Options</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div> 
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->





















































