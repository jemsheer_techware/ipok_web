 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Add Charity
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
        if($this->session->flashdata('message')) {
          $message = $this->session->flashdata('message');
        ?>
        <div class="alert alert-<?php echo $message['class']; ?>" id="successMessage">
           <button class="close" data-dismiss="alert" type="button">×</button>
            <!--     <h4><?php echo $message['title']; ?></h4> -->

                <p><?php echo $message['message']; ?></p>

        </div>
        <?php
        }
        ?>
              <!-- general form elements -->
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Charity</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form" data-parsley-validate enctype="multipart/form-data">
                  <div class="box-body">
                    
                      <div class="form-group col-md-6">
                      <label>title</label>
                      <input type="text" name="title" class="form-control required" placeholder="Enter title" data-parsely-minlength="10" data-parsley-required="true">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Image</label>
                        <input type="file" name="image" class="form-control"  value="" accept="image/*" data-parsley-required>
                        <!-- <img src" alt=""  style="width:100px; height:100px"/> -->
                    </div>
                    <div class="form-group col-md-12">
                      <label>Description</label>
                      <textarea name="description"  data-parsley-minlength="8"   type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Description" data-parsley-required></textarea>
                    </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

             

            </div>
            <!--/.col (Add) -->
           <div class="col-xs-12">

              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Saved Charity</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Charity </th>
                        <th> Description</th>
                        <th> Image</th>
                        <th width="150px">Options</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php
                    if($data) {
                      foreach($data as $charity) {                     
                      
                      ?> 
                              <tr>
                              <td class="hidden"><?php echo $charity['id'];?></td>
                              <td><?php echo $charity['title'];?></td>
                              <td><?php echo $charity['description'];?></td>
                               <td><img src="<?php echo base_pic_url().$charity['image']; ?>" alt="" width="50px" height="50px"  /></td>
                              <td>
                              <a class='btn btn-sm btn-primary' href='<?php echo base_url(); ?>ManageCharity/charity_edit/<?php echo $charity['id']; ?>'> <i class='fa fa-fw fa-edit'></i> Edit </a> 
                              <a class='btn btn-sm btn-danger' href='<?php echo base_url(); ?>ManageCharity/charity_delete/<?php echo $charity['id']; ?>'> <i class='fa fa-fw fa-trash'></i> Delete </a>
                              </td>
                              </tr>
                     <?php
                    }
                  }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Charity </th>
                        <th> Description</th>
                        <th> Image</th>
                        <th>Options</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div> 
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->





















































