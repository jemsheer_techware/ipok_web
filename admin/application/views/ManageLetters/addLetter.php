      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Letters
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
        if($this->session->flashdata('message')) {
          $message = $this->session->flashdata('message');
        ?>
        <div class="alert alert-<?php echo $message['class']; ?>" id="successMessage">
           <button class="close" data-dismiss="alert" type="button">×</button>
            <!--     <h4><?php echo $message['title']; ?></h4> -->

                <p><?php echo $message['message']; ?></p>

        </div>
        <?php
        }
        ?>
              <!-- general form elements -->
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Letters</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form">
                  <div class="box-body">
                    
                    <div class="form-group col-md-6">
                      <label>Diseases</label>
                      <input type="text" name="disease_name" class="form-control required" placeholder="Enter Diseases" data-parsely-minlength="10" data-parsley-required="true">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Code</label>
                      <input type="text" name="code" class="form-control required" placeholder="Enter Code" data-parsley-required="true">
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

             

            </div>

            
            <!--/.col (Add) -->
           <div class="col-xs-12">

              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Saved Budget</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Disease Name </th>
                        <th> Code</th>
                        <th width="150px">Options</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php
                    if($data) {
                      foreach($data as $letter) {                     
                      
                      ?> 
                              <tr>
                              <td class="hidden"><?php echo $letter['id'];?></td>
                              <td><?php echo $letter['disease_name'];?></td>
                              <td><i><?php echo $letter['code'];?></i></td>
                              <td>
                              <a class='btn btn-sm btn-primary' href='<?php echo base_url(); ?>ManageLetters/letter_edit/<?php echo $letter['id']; ?>'> <i class='fa fa-fw fa-edit'></i> Edit </a> 
                              <a class='btn btn-sm btn-danger' href='<?php echo base_url(); ?>ManageLetters/letter_delete/<?php echo $letter['id']; ?>'> <i class='fa fa-fw fa-trash'></i> Delete </a>
                              </td>
                              </tr>
                     <?php
                    }
                  }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th class="hidden">ID</th>
                         <th> Disease Name </th>
                        <th> Code</th>
                        <th>Options</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div> 

            <div class="col-xs-12">
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Certificates</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form" action="<?php echo base_url();?>ManageLetters/add_certificates">
                  <div class="box-body">
                    <div class="form-group">
                      <div class="col-md-12">
                        <label>Letter with CID</label>
                         <textarea id="editor3"  type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Certificate" name="cid_letter"  data-parsley-required rows="10" cols="80" ><?php echo $certificate->cid_letter;?> </textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12"><br>
                        <label>Letter without CID</label>
                         <textarea id="editor4"  type="text" class="ip_reg_form_input form-control reset-form-custom" placeholder="Certificate" name="letter"  data-parsley-required rows="10" cols="80" ><?php echo $certificate->letter;?></textarea>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
