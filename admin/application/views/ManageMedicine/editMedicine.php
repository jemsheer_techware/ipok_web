<?php
if(isset($data)) {
  
	?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Medicines
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
        if($this->session->flashdata('medicine_message')) {
          $message = $this->session->flashdata('medicine_message');
        ?>
        <div class="callout callout-<?php echo $message['class']; ?>">
                <h4><?php echo $message['title']; ?></h4>
                <p><?php echo $message['message']; ?></p>
        </div>
        <?php
        }
        ?>
              <!-- general form elements -->
              <div class="box box-solid box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Medicine</h3>
                  <!-- <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div> -->
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form" data-parsley-validate>
                  <div class="box-body">
                    
                    <div class="form-group">
                      <label>Medicine Name</label>
                      <input type="text" name="medicine_name" value="<?php echo $data['medicine_name'];?>" class="form-control" placeholder="Enter Medicine Name" data-parsley-required="true">
                    </div>
                    <div class="form-group">
                      <label>Procedure & administration</label>
                      <input type="text" name="medicine_procedure" class="form-control required"  value="<?php echo $data['medicine_procedure'];?>" placeholder="Enter Procedure" data-parsley-required="true">
                    </div>
                    
                    <div class="form-group">
                      <label>Medicine Dosage</label>
                      <input type="text" name="medicine_dosage" value="<?php echo $data['medicine_dosage'];?>" class="form-control" placeholder="Enter Dosage" data-parsley-required="true">
                    </div>
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>

            </div>
            
          </div> 
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
<?php
}
else {
	$this->load->view("error_500");
}
?>