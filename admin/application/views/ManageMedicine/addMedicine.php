      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Medicine
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
        if($this->session->flashdata('message')) {
          $message = $this->session->flashdata('message');
        ?>
        <div class="alert alert-<?php echo $message['class']; ?>" id="successMessage">
           <button class="close" data-dismiss="alert" type="button">×</button>
            <!--     <h4><?php echo $message['title']; ?></h4> -->

                <p><?php echo $message['message']; ?></p>

        </div>
        <?php
        }
        ?>
              <!-- general form elements -->
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Add New Medicine</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form" data-parsley-validate>
                  <div class="box-body">
                    
                    <div class="form-group">
                      <label>Medicine Name</label>
                      <input type="text" name="medicine_name" class="form-control required" placeholder="Enter Medicine Name" data-parsely-minlength="10" data-parsley-required="true">
                    </div>
                    <div class="form-group">
                      <label>Procedure & administration</label>
                      <input type="text" name="medicine_procedure" class="form-control required" placeholder="Enter Procedure" data-parsley-required="true">
                    </div>
                    <div class="form-group">
                      <label>Medicine Dosage</label>
                      <input type="text" name="medicine_dosage" class="form-control required" placeholder="Enter Dosage" data-parsley-required="true" data-role="tagsinput">
                    </div>
                    
                    
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

             

            </div>
            <!--/.col (Add) -->
           <div class="col-xs-12">

              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Saved Medicine</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Medicine Name </th>
                        <th> Dosage</th>
                        <th> Procedure</th>
                        
                        <th width="150px">Options</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php
                    if($data) {
                      foreach($data as $medicine) {                     
                      
                      ?> 
                              <tr>
                              <td class="hidden"><?php echo $medicine['id'];?></td>
                              <td><?php echo $medicine['medicine_name'];?></td>
                              <td><i><?php echo $medicine['medicine_dosage'];?></i></td>
                              <td><i><?php echo $medicine['medicine_procedure'];?></i></td>
                              <td>
                              <a class='btn btn-sm btn-primary' href='<?php echo base_url(); ?>ManageMedicine/medicine_edit/<?php echo $medicine['id']; ?>'> <i class='fa fa-fw fa-edit'></i> Edit </a> 
                              <a class='btn btn-sm btn-danger' href='<?php echo base_url(); ?>ManageMedicine/medicine_delete/<?php echo $medicine['id']; ?>'> <i class='fa fa-fw fa-trash'></i> Delete </a>
                              </td>
                              </tr>
                     <?php
                    }
                  }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Medicine Name </th>
                        <th> Dosage</th>
                        <th> Procedure</th>
                        <th>Options</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div> 
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
