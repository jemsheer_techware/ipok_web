      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Sub Problems
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
               if($this->session->flashdata('message')) {
                  $message = $this->session->flashdata('message');
               ?>
               <div class="alert alert-<?php echo $message['class']; ?>">
                  <button class="close" data-dismiss="alert" type="button">×</button>
                  <?php echo $message['message']; ?>
               </div>
               <?php
               }
            ?>
            </div>
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Add New</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form" data-parsley-validate>
                  <div class="box-body">
                    <div class="form-group col-md-8">
                      <label>Problem Category</label>
                        <select class="form-control" data-placeholder="Select category"
                        style="width: 100%;" name="problem_category_id" data-parsley-required>
                          <option value="-1">Select Category</option>
                          <?php  
                          foreach ($data as $key => $value) 
                          {
                          ?>
                            <option value="<?php echo $value->id; ?>"><?php echo $value->problem_name; ?></option>
                          <?php
                            }
                          ?>
                        </select> 
                    </div>

                    <div class="form-group col-md-8">
                      <label>Name</label>
                      <input type="text" name="subproblem_name" class="form-control required" placeholder="Enter SubProblem Name" data-parsley-required="true">
                    </div>
                   <!--  <div class="form-group">
                      <label>Medicine Dosage</label>
                      <input type="text" name="medicine_dosage" class="form-control required" placeholder="Enter Dosage" data-parsley-required="true" data-role="tagsinput">
                    </div> -->
                    
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

             

            </div>
            <!--/.col (Add) -->
            <div class="col-xs-12">

              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">List of SubProblems</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div>
                <div class="box-body">
                  <?php  if($datas) {?>
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th>Problem Category</th>
                        <th>SubProblem Name </th>
                      <!--   <th> Dosage</th> -->
                        
                        <th width="200px">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php
                   
                      foreach($datas as $subproblem) {                     
                      
                      ?> 
                        <tr>
                          <td class="hidden"><?php echo $subproblem->id;?></td>
                          <td><?php echo $subproblem->problem_name;?></td>
                          <td><?php echo $subproblem->subproblem_name;?></td>
                          <td>
                          <a class='btn btn-sm btn-primary' href='<?php echo base_url(); ?>MajorProblems/subproblem_edit/<?php echo $subproblem->id; ?>'> <i class='fa fa-fw fa-edit'></i> Edit </a> 
                          <a class='btn btn-sm btn-danger' href='<?php echo base_url(); ?>MajorProblems/subproblem_delete/<?php echo $subproblem->id; ?>'> <i class='fa fa-fw fa-trash'></i> Delete </a>
                          </td>
                        </tr>
                     <?php
                    }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Problem Category </th>
                        <th> SubProblem Name </th>
                       <!--  <th> Dosage</th> -->
                        
                        <th>Actions</th>
                      </tr>
                    </tfoot>
                  </table>
                  <?php } else{?>
                    <div>No Data Found</div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
