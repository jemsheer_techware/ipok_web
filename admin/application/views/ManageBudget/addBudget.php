      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Budget
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
        if($this->session->flashdata('message')) {
          $message = $this->session->flashdata('message');
        ?>
        <div class="alert alert-<?php echo $message['class']; ?>" id="successMessage">
           <button class="close" data-dismiss="alert" type="button">×</button>
            <!--     <h4><?php echo $message['title']; ?></h4> -->

                <p><?php echo $message['message']; ?></p>

        </div>
        <?php
        }
        ?>
              <!-- general form elements -->
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Budget</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form" data-parsley-validate>
                  <div class="box-body">
                    
                    <div class="form-group">
                      <label>Item or Procedure</label>
                      <input type="text" name="budget_procedure" class="form-control required" placeholder="Enter Exams or Procedure" data-parsely-minlength="10" data-parsley-required="true">
                    </div>
                    <div class="form-group">
                      <label>Amount</label>
                      <input type="number" name="amount" class="form-control required" placeholder="Enter Amount" data-parsley-required="true">
                    </div>
                    <div class="form-group">
                      <label>Quantity</label>
                      <input type="number" name="quantity" class="form-control required" placeholder="Enter Quantity" data-parsley-required="true">
                    </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

             

            </div>
            <!--/.col (Add) -->
           <div class="col-xs-12">

              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Saved Budget</h3>
                  <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div>
                </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped datatable" data-ordering="true">
                    <thead>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Items </th>
                        <th> Amount</th>
                        <th> Quantity</th>
                        <th width="150px">Options</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php
                    if($data) {
                      foreach($data as $budget) {                     
                      
                      ?> 
                              <tr>
                              <td class="hidden"><?php echo $budget['id'];?></td>
                              <td><?php echo $budget['budget_procedure'];?></td>
                              <td><i><?php echo $budget['amount'];?></i></td>
                              <td><i><?php echo $budget['quantity'];?></i></td>
                              <td>
                              <a class='btn btn-sm btn-primary' href='<?php echo base_url(); ?>ManageBudget/budget_edit/<?php echo $budget['id']; ?>'> <i class='fa fa-fw fa-edit'></i> Edit </a> 
                              <a class='btn btn-sm btn-danger' href='<?php echo base_url(); ?>ManageBudget/budget_delete/<?php echo $budget['id']; ?>'> <i class='fa fa-fw fa-trash'></i> Delete </a>
                              </td>
                              </tr>
                     <?php
                    }
                  }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th class="hidden">ID</th>
                        <th> Items </th>
                        <th> Amount</th>
                        <th> Quantity</th>
                        <th>Options</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div> 
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
