<?php
if(isset($data)) {
  
	?>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Budget
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- Add -->
            <div class="col-md-12">
              <?php
        if($this->session->flashdata('medicine_message')) {
          $message = $this->session->flashdata('medicine_message');
        ?>
        <div class="callout callout-<?php echo $message['class']; ?>">
                <h4><?php echo $message['title']; ?></h4>
                <p><?php echo $message['message']; ?></p>
        </div>
        <?php
        }
        ?>
              <!-- general form elements -->
              <div class="box box-solid box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Budget</h3>
                  <!-- <div class="pull-right box-tools">
            <button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse">
            <i class="fa fa-minus"></i>
                    </button>
          </div> -->
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" class="validate" role="form" data-parsley-validate>
                  <div class="box-body">
                   
                   <div class="form-group">
                      <label>Item or Procedure</label>
                      <input type="text" name="budget_procedure" value="<?php echo $data['budget_procedure'];?>" class="form-control required" placeholder="Enter Exams or Procedure" data-parsely-minlength="10" data-parsley-required="true">
                    </div>
                    <div class="form-group">
                      <label>Amount</label>
                      <input type="number" name="amount" value="<?php echo $data['amount'];?>" class="form-control required" placeholder="Enter Amount" data-parsley-required="true">
                    </div>
                    <div class="form-group">
                      <label>Quantity</label>
                      <input type="number" name="quantity" value="<?php echo $data['quantity'];?>" class="form-control required" placeholder="Enter Quantity" data-parsley-required="true">
                    </div>
                    
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>

            </div>
            
          </div> 
        </section><!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
<?php
}
else {
	$this->load->view("error_500");
}
?>