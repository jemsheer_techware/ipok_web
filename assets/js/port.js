// This is included with the Parsley library itself,
// thus there is no use in adding it to your project.
//import Parsley from '../parsley/main';

window.Parsley.addMessages('es', {
  defaultMessage: "Este valor parece ser inválido.",
  type: {
    email:        "Este valor deve ser um email válido.",
    url:          "Este valor deve ser um URL válido.",
    number:       "Este valor deve ser um número válido.",
    integer:      "Este valor deve ser um inteiro válido.",
    digits:       "Este valor deve ser dígitos.",
    alphanum:     "Este valor deve ser alfanumérico."
  },
   datebirthdoc:   "Este campo não deve estar vazio.",
  emailcolabor:   "E-mail não disponível!",
  usernamedocedit: "Nome de usuário não disponível!",
  usernamepatedit: "Nome de usuário não disponível!",
  email:            "Esse endereço de email já existe!",
  bloodgroup:       "Digite Bloodgroup válido!",
  username:         "Nome de usuário não disponível!",
  cpf:              "CPF inválido",
  cpfunique:        "CPF não é exclusivo",
  cep:              "CEP inválido",
  usernamedoc:      "Nome de usuário não disponível!",
  emaildoc:         "E-mail não disponível!",
  consultduration:  "Adicione a duração da consulta primeiro!",
  mintime:          "O horário deve ser maior que o horário de início",
  maxtime:          "O tempo deve ser menor que o tempo final",
  mindate:          "Data de término inválida",
  notblank:       "Este valor não deve ficar em branco.",
  required:       "Este valor é obrigatório.",
  pattern:        "Este valor parece ser inválido.",
  min:            "Este valor deve ser maior ou igual a %s.",
  max:            "Este valor deve ser menor ou igual a %s.",
  range:          "Este valor deve estar entre% se %s.",
  minlength:      "Este valor é muito curto. Deve ter %s caracteres ou mais.",
  maxlength:      "Este valor é muito longo. Deve ter %s caracteres ou menos.",
  length:         "Este comprimento de valor é inválido. Deve estar entre% s e %s caracteres.",
  mincheck:       "Você deve selecionar pelo menos %s escolhas.",
  maxcheck:       "Você deve selecionar %s escolhas ou menos.",
  check:          "Você deve selecionar entre %s e %s escolhas.",
  equalto:        "Este valor deve ser o mesmo.",
  uppercase:      "Sua senha deve conter pelo menos (%s) letra maiúscula.",
  lowercase:      "Sua senha deve conter pelo menos (%s) letra minúscula.",
  special:        "Sua senha deve conter pelo menos (%s) caracteres especiais.",
  number:         "Sua senha deve conter pelo menos (%s) o número."



});

window.Parsley.setLocale('es');