

<div class="container ip_custom_container">
  <div class="row">
	
		<div class="col-md-12">
			<div class="ip_result_div">
			
				
				
				<div class="ip_result_listing">
					<div class="ip_profile_complete_detail">
            <div class="ip_profile_complete_banner">
              <div class="ip_profile_complete_pic">
                <img src="<?php echo base_url();echo $doctor_data['dr_pic']; ?>">
              </div>
              <div class="ip_profile_detail">
               
                <h2>Dr.<?php echo $doctor_data['dr_name'];?></h2>
               <!--  <fieldset class="ip_rating">
  								<input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
  								<input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
  								<input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
  								<input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
  								<input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
  								<input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
  								<input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
  								<input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
  								<input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
  								<input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
  							</fieldset> -->
              </div>
              <div class="clear"></div>
            </div>
            <div class="ip_profile_details_listing">
                <li><?php load_language('email');?>:<strong><?php echo $doctor_data['dr_email'];?></strong></li>
                <!-- <li>Birthday:<strong><?php echo date('d F Y',$doctor_data["dr_dob"]);?></strong></li> -->
                <li><?php load_language('clinic');?>:<strong><?php echo $doctor_data['clinic_name'];?>,<?php echo $doctor_data['clinic_street_address'];?> <?php echo $doctor_data['clinic_locality'];?></strong></li>
                <div class="clear"></div>
            </div>
            <hr>
            <div class="ip_profile_bio">

              <h6 class="uppercase"><?php load_language('biography');?></h6>
              <p><?php echo $doctor_data['dr_bio'];?></p>
                
            </div>
            <div class="ip_profile_others">
            <div class="row">
              <div class="col-md-7">
                <div class="ip_detailed_map">
                  <div class="ip_location_map_head">
                    <?php load_language('specialization');?>
                  </div>
                  <div class="ip_location_map_area bordernone">
                    <li>- <?php echo $doctor_data['dr_specialization'];?></li>
                  </div>
                </div>
              </div>
              <div class="col-md-5">
                <div class="ip_detailed_map">
                  <div class="ip_location_map_head">
                    <?php load_language('location');?>
                  </div>
                  <div class="ip_location_map_area" data-lat="<?php echo $doctor_data['clinic_lat']?>" data-lng="<?php echo $doctor_data['clinic_lng']?>" id="doctor_location">
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
				</div>
			</div>
		</div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-12">
        <div class="ip_full_calender_div">
          <div class="ip_full_calender_head">
            <div class="ip_full_calender_nav">
              <div class="btn-group">
                <button type="button" id="complete_profile_appointment_prevbtn" class="btn"><img src="<?php echo base_url();?>assets/images/ip_arw_left.png"></button>
                <button type="button" id="complete_profile_appointment_nextbtn" class="btn"><img src="<?php echo base_url();?>assets/images/ip_arw_right.png"></button>
              </div>
            </div>
            <h3><?php load_language('appointment');?></h3>
          </div>
          <div class="ip_full_calender_content" id="complete_profile_appointment">
            <?php $template['doctorid'] = $doctor_data['doctorid'];
            $this->load->view('search_doctor_complete_profile_appointments_week',$template); ?>
          </div>

        </div>
      </div>
    </div>
    <div class="ip_agenda_btn_bay">
      <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-8">
          <div class="ip_knw_more_btn_bay">
              <a href="<?php echo base_url()?>Searchdoctor/confirmbooking/<?php echo $doctor_data["doctorid"]?>/<?php echo $doctor_data["clinic_id"]?>">
                <button class="ip_knwmore_detail_btn floatRight ip_knw_more_btn_2" type="button"><?php load_language('mark_consultation');?></button>
              </a>
              

            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
</div>


<script >
  setTimeout(function(){

 initialize_map('doctor_location'); 
  },1000)
</script>

