
              <?php 
              $userdata = $this->session->userdata('UserData');
              if(!empty($day_appointment))
              {
               // print_r($day_appointment);die();


               foreach ($day_appointment as $key => $value) 
                    { 
                      if($value['booking_status']==1)
                      {

                      
              ?>
                         <li>
                          <div class="ip_day_time_slot">
                            <p><?php echo $value['time_start'];?></p>
                          </div>
                          <div class="ip_day_time_schedule_details">
                            <div class="ip_avialable p0">
                              <div class="row m0 height100">

                                <div class="col-md-5 p0 height100">
                                  <div class="ip_day_time_schedule_details_data height100">
                                    <span><img src="<?php echo base_url();echo $value['pat_pic'];?>"></span>

                                    <span><?php echo $this->encrypt->decode($value['pat_name']);?></span>

                                  </div>
                                </div>

                                <?php 
                                if($userdata['type']=="DOCTOR")
                                {
                                ?>
                                   <div class="col-md-4 p0 height100">
                                    <div class="ip_day_time_schedule_details_data height100">
                                     <a href="<?php echo base_url();?>Doctor/service/<?php echo $value['booking_id']?>">
                                       <button id="doc_service_startbtn" type="button"    class="ip_start_service_btn"><?php load_language('start_service');?></button>
                                     </a>
                                    </div>
                                  </div>
                                <?php 
                                }
                                ?>
                                
                                <div class="col-md-3 p0 height100 textCenter">
                                  <div class="ip_day_time_schedule_details_data height100">
                                    <div id="doc_dash_appoint_cancel" bookid="<?php echo $value['booking_id'];?>">
                                      <span class="ip_canceler"><img src="<?php echo base_url();?>assets/images/ip_cancel.png"></span>
                                      <span class="ip_canceler capitalize"><?php load_language('cancel');?></span>
                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>
                          </div>
                          <div class="clear"></div>
                        </li>

                     
                    <?php 

                      }
                      elseif($value['booking_status']==4)
                      {
                    ?>
                      <!-- CODE FOR CANCELED APPOIONMENTS -->
                      <li>
                          <div class="ip_day_time_slot">
                            <p><?php echo $value['time_start'];?></p>
                          </div>
                          <div class="ip_day_time_schedule_details ip_not_avialable">
                            <div class="row m0 height100">
                              <div class="col-md-5 p0 height100">
                                <div class="ip_day_time_schedule_details_data height100">
                                  <span><img src="<?php echo base_url();echo $value['pat_pic'];?>"></span>

                                  <span><?php echo $this->encrypt->decode($value['pat_name']);?></span>

                                </div>
                              </div>
                              <div class="col-md-4 p0 height100">
                                <div class="ip_day_time_schedule_details_data height100">
                                  <span class="ip_cancel_consultation ip_session"><?php load_language('canceled_consultation');?></span>
                                </div>
                              </div>
                              <div class="col-md-1 p0 height100">
                                <div class="ip_day_time_schedule_details_data height100">

                                </div>
                              </div>
                              <div class="col-md-2 p0 height100">
                                <div class="ip_day_time_schedule_details_data height100">

                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="clear"></div>
                        </li>

                    <?php

                      }
                      } 
                    } 
                   else
                    {
                    ?>

                        <li>
                          <div class="ip_day_time_slot">
                          <p></p>
                          </div>
                          <div class="ip_day_time_schedule_details ip_avialable">
                          <div class="row m0 height100">
                          <div class="col-md-4 p0 height100">
                          <div class="ip_day_time_schedule_details_data height100">

                          </div>
                          </div>
                          <div class="col-md-8 p0 height100">
                          <div class="ip_day_time_schedule_details_data height100">
                         <?php load_language('no_appointments');?> 
                          </div>
                          </div>

                          </div>
                          </div>
                          <div class="clear"></div>
                        </li>
                    <?php 
                    }
                    ?>

                        