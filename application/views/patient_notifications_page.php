
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">
  

    <div class="ip_grid_cols">
      <div class="row">
        <div class="col-md-8">
          <div class="ip_bio_tab_div">
            <div class="ip_bio_head">
              <?php load_language('notification_center');?>
            </div>
            <div class="ip_bio_detail"  id="pat-noti-view">
            <?php
                $this->load->view('patient_notifications_list')
            ?>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>

  </div>
</div>

