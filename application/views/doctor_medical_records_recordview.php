  <div class="ip_records_tab">
    <?php 
      if(!empty($record_data))
      {
    ?>
      <div class="row m0">
        <div class="col-md-2 p0">
          <div class="ip_record_section_header">
            <!-- 08 Sept, 13.00 - 14:00 -->
            <?php echo date('d M',$booking_details['book_date'])?><br><?php echo $booking_details['book_time'];?>
          </div>
        </div>
        <div class="col-md-10 p0">
          <div class="ip_record_header1">
            <div class="ip_record_back">
              <div class="ip_pres_image">
                <img src="<?php echo base_url();echo $patient_data['pt_pic']?>">
              </div>
            </div>
            <div class="ip_record_search_box backgroundnone">
              <h4><?php echo $patient_data['pt_name']?></h4>
            </div>
            <div class="ip_record_settings">
              <span class="settings"><img src="<?php echo base_url();?>assets/images/ip_settings.png"></span>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
      <div class="ip_bio_tab_div m0">
        <div class="row m0">
          <div class="col-md-2 p0 height100">
            <div class="ip_bio_tab_bay height100">
              <ul>
                <li class="active" data-toggle="tab" href="#record"><?php load_language('anamnesis');?></li>
                <li data-toggle="tab" href="#prescription"><?php load_language('prescription');?></li>
                <li data-toggle="tab" href="#exam"><?php load_language('exams');?></li>
                <li data-toggle="tab" href="#other"><?php load_language('letters_and_certificate');?></li>
                <li data-toggle="tab" href="#budjet"><?php load_language('budget');?></li>
                <li data-toggle="tab" href="#ratting"><?php load_language('evaluation');?></li>
              </ul>
            </div>
          </div>
          <div class="col-md-10 p0">
            <div class="ip_bio_tab_content">
              <div class="tab-content">
                <div class="ip_profile_tab_top p0">
                  <div class="ip_profile_tab_name">
                    <h3><?php if(!empty($record_data['main_complaint'])) echo $record_data['main_complaint'];?></h3>
                  </div>

                  <div class="clear"></div>
                </div>

                <div id="record" class="tab-pane fade in active">
                  <div class="ip_profile_tab_detail">

                    <?php if(!empty($record_data['description']))echo $record_data['description'];?>
                       <br>
                       <div class="ip_prescription_div">
                         <ul>

                            <?php 
                            if(!empty($record_data['diseases']->anamnese))
                            {
                            foreach ($record_data['diseases']->anamnese as $key => $value) 
                            {
                              if($key!='others' and $key!='Medications')
                              {
                              $key = str_replace('_', ' ', $key);
                              ?>
                               <li class="p12">
                                 <h5><?php echo $key;?></h5>
                                 <p><?php echo $value;?></p>
                               </li>
                              <?php
                                }

                                if($key=='Medications')
                                {
                              ?>

                                <li class="p12">
                                 <h5><?php echo $key;?></h5>
                                 <p><?php echo implode(",",$value);?></p>
                               </li>

                              <?php

                                }
                              }
                            }
                            ?>

                            
                             <!-- <li class="p0">
                               <h5>Renal Problems</h5>
                               <p>been the industry's standard </p>
                             </li> -->

                             <!-- <li class="p0">
                               <h5>Alergies</h5>
                               <p>been the industry's standard </p>
                             </li> -->

                             <div class="clear"></div>
                             <br>

                      <?php 
                      if(!empty($record_data['diseases']->anamnese->others))
                      {
                      foreach ($record_data['diseases']->anamnese->others as $key => $value){
                        $value = str_replace('_', ' ', $value);
                      ?>
                      <div class="ip_day_time_schedule_details_data p0 floatLeft">
                          <input id="checkbox-2" class="ip_custom_checkbox1" name="checkbox-2" type="checkbox" checked="" disabled>
                          <label for="checkbox-2" class="ip_custom_checkbox_label1"><?php echo $value;?></label>
                     </div>
                      <?php
                       }
                     }
                      ?>
                          <div class="clear"></div>
                           </ul>

                         </div>
                  </div>
                </div>


                <div id="prescription" class="tab-pane fade">
                  <div class="ip_profile_tab_detail">
                    <div class="ip_prescription_div">
                      <ul>
                        <?php if(!empty($record_data['prescribtions']))
                        {
                          foreach ($record_data['prescribtions'] as $key => $value) 
                          {
                        ?>
                          <li>
                            <h5><?php echo $value->name;?> <?php echo $value->quantity;?></h5>
                           <!--  <p>been the industry's standard </p> -->
                            <h6><?php echo $value->procedure;?></h6>
                          </li>
                        <?php
                          }
                        }

                        ?>
                          <!-- <li>
                            <h5>Broncoflux 30mg</h5>
                            <p>been the industry's standard </p>
                            <h6>since the 1500s, when an unknown printer took a</h6>
                          </li> -->
                          
                          <div class="clear"></div>
                      </ul>
                    </div>
                  </div>
                </div>


                <div id="exam" class="tab-pane fade">
                  <div class="ip_profile_tab_detail">
                    <div class="ip_prescription_div">
                      <ul>

                        <?php if(!empty($record_data['exams'][0]))
                        {
                        ?>
                          <li class="p12">
                            <h5><?php echo $record_data['exams'][0]->procedure;?></h5>
                            <h6><?php echo $record_data['exams'][0]->observation;?></h6>
                          </li>
                        <?php 
                        }
                        ?>
                          <!-- <li>
                            <h5>USG Obstetrica / doppler</h5>
                            <h6>since the 1500s, when an unknown printer took a</h6>
                          </li> -->
                          
                        </ul>
                      </div>
                  </div>
                </div>


                <div id="other" class="tab-pane fade">
                  <div class="ip_profile_tab_detail">
                    <?php if(!empty($record_data['letters']))
                        {
                        ?>
                          <p><?php echo $record_data['letters']->certificate;?></p>
                        <?php 
                        }
                        ?>
                  </div>
                </div>


                <div id="budjet" class="tab-pane fade">
                  <div class="ip_profile_tab_detail">
                      <div class="ip_edit_row">
                        <div class="ip_budject_list">
                          <?php
                          if(!empty($record_data['budget']))
                          {
                            foreach ($record_data['budget']as $key => $value) 
                            {
                          ?>
                            <li>
                              <div class="child1"><?php echo $value->quantity;?>-<?php echo $value->procedure;?></div>
                              <div class="child2">R$<?php echo $value->amount;?></div>
                              <div class="clear"></div>
                            </li>
                          <?php
                            $total_budget[$key] =  $value->amount * $value->quantity;
                            }
                          }

                          ?>
                         <!--  <li>
                            <div class="child1">2-Leisure peeling sessions</div>
                            <div class="child2">R$150,00</div>
                              <div class="clear"></div>
                          </li> -->

                          <li class="select">
                            <div class="child1"><?php load_language('total');?></div>
                            <div class="child2 select">R$<?php if(!empty($total_budget)) {echo array_sum($total_budget);}?></div>
                            <div class="clear"></div>
                          </li>

                        </div>
                      </div>
                  </div>
                </div>


                <div id="ratting" class="tab-pane fade">
                  <div class="ip_profile_tab_detail p0">
                    <div class="ip_ratting_tab_content">
                      <div class="row m0 p15 ip_grey">
                          <div class="ip_profile_tab_circle">
                            <img src="<?php echo base_url();echo $doctor_data['dr_pic']?>">
                          </div>
                          <div class="ip_profile_tab_name">
                            <h3>Dr. <?php echo $doctor_data['dr_name']?></h3>
                           <!--  <form id="ip_user_rating_form">
                              <div id="ip_selected_rating" class="ip_selected_rating floatLeft">5.0</div>
                              <span class="ip_user_rating floatLeft">
                                <input type="radio" name="rating" value="5.0"><span class="star"></span>
                                <input type="radio" name="rating" value="4.0"><span class="star"></span>
                                <input type="radio" name="rating" value="3.0"><span class="star"></span>
                                <input type="radio" name="rating" value="2.0"><span class="star"></span>
                                <input type="radio" name="rating" value="1.0"><span class="star"></span>
                              </span>
                              <div class="clear"></div>
                            </form> -->
                          </div>
                          <div class="ip_profile_tab_name_detail">
                            <p><strong>
                              <?php if(!empty($record_data['patient_review'])) {echo $record_data['patient_review']; } ?>
                            </strong></p> 
                          </div>
                          <div class="clear"></div>
                          
                          </form>
                          <br>
                          
                      </div>

                    </div>
                  </div>
                </div>


                <div class="ip_prescription_bottom_btn_bay">
                  <!-- <a target="_blank" href="<?php echo base_url();?>Sentmail/record/<?php echo $record_data['id']?>">
                  <button class="ip_prescription_btn2 floatRight">SEND BY EMAIL</button>
                  </a> -->
                  <a href="javascript:void(0)">
                  <button type="button" record="<?php echo $record_data['id']?>" class="ip_prescription_btn2 floatRight record-sent-mail"><?php load_language('send_by_email');?></button>
                  </a>

                  <a target="_blank" href="<?php echo base_url();?>Printer/record/<?php echo $record_data['id']?>">
                  <button class="ip_prescription_btn1 floatRight"><?php load_language('print_out');?></button>
                  </a>
                  <div class="clear"></div>
                </div>


              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php 
    }
    else
    {
    ?>
      <div class="ip_record_main_head_data p15 textCenter capitalize"><?php load_language('no_records_found');?>!</div>
    <?php
    }
    ?>
    

  </div>