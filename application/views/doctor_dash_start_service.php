
<div id="main-content"  class="main ip_set_two_wrapper">
  <div class="container ip_custom_container">

    <div class="ip_main_path_stream">
      <ul>
        <li><?php load_language('dashboard');?><span><img src="<?php echo base_url();?>assets/images/ip_tab_list_arw.png"></span></li>
        <li><?php load_language('medical_records');?><span><img src="<?php echo base_url();?>assets/images/ip_tab_list_arw.png"></span></li>
      </ul>
    </div>

        <div class="alert alert-danger alert-dismissible flash-msg hidden" id="final-save-error">
        </div>

    <div class="ip_edit_record_wrapper">
      <div class="row">
        <div class="col-md-8 content"  id="content" >
          <div class="ip_edit_record_name" style="padding-top: 40px;">
            <h5 id="patdetail" patid="<?php echo $booking_details['pat_id'];?>"><?php echo $this->encrypt->decode($booking_details['pat_name']);?></h5>
            <p><?php echo date('d M',$booking_details['book_date']);?>, <?php echo $booking_details['book_time'];?><img src="<?php echo base_url();?>assets/images/ip_menu5.png"></p>
          </div>
          <br><br>

          <div class="ip_edit_record_cover">
            <div class="ip_edit_record_head" data-toggle="collapse" data-target="#records">
              <?php load_language('anamnesis');?>
            </div>
            <div id="records" class="collapse in">
              <form data-parsley-validate="" role="form" id="doc-service-record"> 
            <div class="ip_edit_record_detail">
              <div class="ip_edit_row">
                <div class="ip_bank_detail_frame">
                  <input type="hidden" name="booking_id" value="<?php echo $booking_details['book_id'];?>">
                  <input type="hidden" name="section" value="anamnesis">
                    <select class="ip_bank_input" name="main_complaint" data-parsley-required="" data-parsley-error-message="Choose Complaint" >
                        <option selected disabled><?php load_language('main_complaint');?></option>
                        <?php if(!empty($main_complaints))
                        { 
                          foreach ($main_complaints as $key => $complaint) 
                          {
                        ?>
                        <option value="<?php echo $complaint['complaint_name']?>"><?php echo $complaint['complaint_name']?></option>
                        <?php
                          }
                        } 
                        ?>
                    </select>
                </div>
              </div>
      
           <textarea id="editor1"  type="text"  rows="10" cols="80" ></textarea>

              <!-- <div class="ip_edit_bottom_btn_bay">
                <button type="button" class="ip_edit_save_btn floatRight" id="record-sec-1-btn">SAVE</button>
                <button class="ip_edit_cancel_btn floatRight">CANCEL</button>
                <div class="clear"></div>
              </div> -->
              <br><br>
              <div class="ip_edit_row">
                <p class="ip_row_p"><?php load_language('kidney_problems');?> :</p>
                <div class="ip_bank_detail_frame">
                    <select class="ip_bank_input make-autocomplete" name="Kidney_Problem">
                        <option selected disabled><?php load_language('select_any');?></option>
                        <?php if(!empty($major_problems['1'])&&!empty($major_problems['1']['sub_problem']))
                        {
                          foreach ($major_problems['1']['sub_problem'] as $key => $elem) 
                          {
                        ?>
                        <option value="<?php echo $elem['subproblem_name']?>"><?php echo $elem['subproblem_name']?></option>
                        <?php                            
                          }
                        }

                        ?>
                    </select>
                </div>
              </div>
              <div class="ip_edit_row">
                <p class="ip_row_p"><?php load_language('joint_problems_or_rheumatism');?> :</p>
                <div class="ip_bank_detail_frame">
                    <select class="ip_bank_input make-autocomplete" name="Joint_Problem">
                        <option selected disabled><?php load_language('select_any');?></option>
                        <?php if(!empty($major_problems['2'])&&!empty($major_problems['2']['sub_problem']))
                        {
                          foreach ($major_problems['2']['sub_problem'] as $key => $elem) 
                          {
                        ?>
                        <option value="<?php echo $elem['subproblem_name']?>"><?php echo $elem['subproblem_name']?></option>
                        <?php                            
                          }
                        }

                        ?>
                    </select>
                </div>
              </div>
              <div class="ip_edit_row">
                <p class="ip_row_p"><?php load_language('heart_problems');?> :</p>
                <div class="ip_bank_detail_frame">
                    <select class="ip_bank_input make-autocomplete" name="Heart_Problem">
                        <option selected disabled><?php load_language('select_any');?></option>
                        <?php if(!empty($major_problems['0'])&&!empty($major_problems['0']['sub_problem']))
                        {
                          foreach ($major_problems['0']['sub_problem'] as $key => $elem) 
                          {
                        ?>
                        <option value="<?php echo $elem['subproblem_name']?>"><?php echo $elem['subproblem_name']?></option>
                        <?php                            
                          }
                        }

                        ?>
                    </select>
                </div>
              </div>
              <div class="ip_edit_row">
                <p class="ip_row_p"><?php load_language('breathing_problems');?> :</p>
                <div class="ip_bank_detail_frame">
                    <select class="ip_bank_input make-autocomplete" name="Breathing_Problem">
                        <option selected disabled><?php load_language('select_any');?></option>
                        <?php if(!empty($major_problems['3'])&&!empty($major_problems['3']['sub_problem']))
                        {
                          foreach ($major_problems['3']['sub_problem'] as $key => $elem) 
                          {
                        ?>
                        <option value="<?php echo $elem['subproblem_name']?>"><?php echo $elem['subproblem_name']?></option>
                        <?php                            
                          }
                        }

                        ?>
                    </select>
                </div>
              </div>
              <div class="ip_edit_row">
                <p class="ip_row_p"><?php load_language('gastric_problems');?> :</p>
                <div class="ip_bank_detail_frame">
                    <select class="ip_bank_input make-autocomplete" name="Gastric_Problem">
                        <option selected disabled><?php load_language('select_any');?></option>
                        <?php if(!empty($major_problems['4'])&&!empty($major_problems['4']['sub_problem']))
                        {
                          foreach ($major_problems['4']['sub_problem'] as $key => $elem) 
                          {
                        ?>
                        <option value="<?php echo $elem['subproblem_name']?>"><?php echo $elem['subproblem_name']?></option>
                        <?php                            
                          }
                        }

                        ?>
                    </select>
                </div>
              </div>
              <div class="ip_edit_row">
                <p class="ip_row_p"><?php load_language('allergies');?> :</p>
                <div class="ip_bank_detail_frame">
                    <select class="ip_bank_input make-autocomplete" name="Allergies">
                        <option selected disabled><?php load_language('select_any');?></option>
                        <?php if(!empty($major_problems['5'])&&!empty($major_problems['5']['sub_problem']))
                        {
                          foreach ($major_problems['5']['sub_problem'] as $key => $elem) 
                          {
                        ?>
                        <option value="<?php echo $elem['subproblem_name']?>"><?php echo $elem['subproblem_name']?></option>
                        <?php                            
                          }
                        }

                        ?>
                    </select>
                </div>
              </div>
              <div class="ip_edit_row">
                <p class="ip_row_p"><?php load_language('use_of_medicines');?> :</p>
                <div class="ip_bank_detail_frame">
                    <select class="ip_bank_input chosen-select"  placeholder="Select Any" name="Medications[]" multiple="multiple" tabindex="4">
                      
                         <?php if(!empty($medicines))
                        {
                          foreach ($medicines as $key => $elem) 
                          {
                        ?>
                        <option value="<?php echo $elem['medicine_name']?>"><?php echo $elem['medicine_name']?></option>
                        <?php                            
                          }
                        }

                        ?>
                    </select>
                </div>
              </div>
              <div class="ip_edit_row">
                <div class="ip_day_time_schedule_details_data p0 floatLeft">
                     <input id="checkbox-21" class="ip_custom_checkbox1" name="others[]" type="checkbox"  value="hepatitis">
                     <label for="checkbox-21" class="ip_custom_checkbox_label1"><?php load_language('hepatitis');?></label>
                 </div>
                 <div class="ip_day_time_schedule_details_data p0 floatLeft">
                      <input id="checkbox-22" class="ip_custom_checkbox1" name="others[]" value="pregnancy" type="checkbox" >
                      <label for="checkbox-22" class="ip_custom_checkbox_label1"><?php load_language('pregnancy');?></label>
                  </div>
                  <div class="ip_day_time_schedule_details_data p0 floatLeft">
                      <input id="checkbox-23" class="ip_custom_checkbox1" name="others[]" value="diabetis" type="checkbox" >
                      <label for="checkbox-23" class="ip_custom_checkbox_label1"><?php load_language('diabetis');?></label>
                  </div>
                  <div class="ip_day_time_schedule_details_data p0 floatLeft">
                      <input id="checkbox-24" class="ip_custom_checkbox1" name="others[]" value="healing_problems" type="checkbox" >
                      <label for="checkbox-24" class="ip_custom_checkbox_label1"><?php load_language('healing_problems');?></label>
                  </div>
                  <div class="clear"></div>
              </div>
              <div class="ip_edit_bottom_btn_bay">
                <a href="javascript:void(0)">
                <button type="button" class="ip_edit_save_btn floatRight uppercase" id="record-sec-1-btn"><?php load_language('save');?></button></a>
                <div class="clear"></div>
              </div>
              <br>
              <div class="alert hidden alert-success textCenter" id="anamnesis-success"></div>
              <div class="alert hidden alert-danger textCenter" id="anamnesis-error"></div>
            </div>
          </form>
          </div>
          </div>



          <div class="ip_edit_record_cover">
            <div class="ip_edit_record_head" data-toggle="collapse" data-target="#prescription">
              <?php load_language('prescriptions');?>
            </div>
            <div id="prescription" class="collapse in">
               
               <div class="ip_edit_record_detail">
                <div class="ip_medical_prescription" >
                  <form role="form" data-parsley-validate="" id="selected-medicine-form">
                    <input type="hidden" name="booking_id" value="<?php echo $booking_details['book_id'];?>">
                    <input type="hidden" name="section" value="medicine">
                    
                  
                  <div id="show-medicine-main">
                    <!-- DIV SHOWING ADDED MEDICINE -->
                  </div>
                  </form>
                <!--  <li>
                    <h6>Broncoflux 30mg</h6>
                    <p>Lorem Ipsum</p>
                    <p>Lorem Ipsum is simply dummy</p>
                    <div class="ip_medical_pres_btn_bay">
                      <button class="ip_medical_pres_btn">Edit</button>
                      <button class="ip_medical_pres_btn2">Delete</button>
                    </div>
                  </li>
                   -->
                  <div class="clear"></div>
                </div>
              </div>
              <hr>

              <div class="ip_edit_record_detail" id="add-medicine-main">
                <form role="form" data-parsley-validate="" id="add-medicine-form">
                  <input type="hidden" value="<?php echo $this->session->userdata('language');?>" id="language">
                <div class="ip_edit_row">
                  <div class="ip_bank_detail_frame">
                      
                       <select class="ip_bank_input select-in-medi-rec" type="medicine" placeholder="Select Any" name="name" id="medicine-select" data-parsley-required="" >
                        <option disabled selected value="0"><?php load_language('select_any');?></option>
                         <?php if(!empty($medicines))
                        {
                          foreach ($medicines as $key => $elem) 
                          {
                        ?>
                        <option value="<?php echo $elem['medicine_name']?>"><?php echo $elem['medicine_name']?></option>
                        <?php                            
                          }
                        }

                        ?>
                    </select>
                  </div>
                </div>
                <div class="ip_edit_row">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="ip_bank_detail_frame">
                          <select class="ip_bank_input" name="quantity" id="medicine-quantity" data-parsley-required="">
                              <option disabled selected ><?php load_language('select_quantity');?></option>
                          </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="ip_bank_detail_frame">
                          <select class="ip_bank_input" id="medicine-dosage" name="procedure" data-parsley-required="">
                              <option disabled selected><?php load_language('select_dosage_and_administration');?></option>
                          </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="ip_edit_row">
                  <button type="button" id="add-medicine-btn" class="ip_edit_set_btn"><?php load_language('add_medicine');?></button>
                </div>
              </form>
              </div>

              <div class="ip_edit_record_detail hidden" id="edit-medicine-main">
                <form role="form" data-parsley-validate="" id="edit-medicine-form">
                <div class="ip_edit_row">
                  <div class="ip_bank_detail_frame">
                     <input type="hidden" id="edit-medcine-id"> 
                       <select class="ip_bank_input select-in-medi-rec" type="medicine-edit" placeholder="Select Any" name="name" id="edit-medicine-select" data-parsley-required="" >
                        <option disabled selected value="0"><?php load_language('select_any');?></option>
                         <?php if(!empty($medicines))
                        {
                          foreach ($medicines as $key => $elem) 
                          {
                        ?>
                        <option value="<?php echo $elem['medicine_name']?>"><?php echo $elem['medicine_name']?></option>
                        <?php                            
                          }
                        }

                        ?>
                    </select>
                  </div>
                </div>
                <div class="ip_edit_row">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="ip_bank_detail_frame">
                          <select class="ip_bank_input" name="quantity" id="edit-medicine-quantity" data-parsley-required="">
                              <option disabled selected ><?php load_language('select_quantity');?></option>
                          </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="ip_bank_detail_frame">
                          <select class="ip_bank_input" id="edit-medicine-dosage" name="procedure" data-parsley-required="">
                              <option disabled selected><?php load_language('select_dosage_and_administration');?></option>
                          </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="ip_edit_row">
                  <button type="button" id="edit-medicine-btn" class="ip_edit_set_btn"><?php load_language('edit_medicine');?></button>
                </div>
              </form>
              </div>

              <hr>
              <div class="ip_edit_record_detail">
                  <div class="row m0">
                    <div class="col-md-2 p5">
                      <a target="_blank" href="<?php echo base_url();?>Printer/medicine/<?php echo $record_id?>">
                      <button class="ip_edit_set_btn1 hidden" type="button" id="med-rec-med-print"><?php load_language('print');?></button>
                      </a>
                    </div>
                    <div class="col-md-3 p5">
                    <a href="javascript:void(0)">
                      <button type="button" id="med-rec-med-mail" func="medicine" class="ip_edit_set_btn2 hidden record-section-mail-btn" record="<?php echo $record_id?>"><?php load_language('send_by_email');?></button>
                    </a>
                    </div>
                    <div class="col-md-5 p5"><!-- <button class="ip_edit_set_btn2">SAVE AS A MODEL</button> --></div>
                    <div class="col-md-2 p5"><a href="javascript:void(0)"><button class="ip_edit_set_btn uppercase" id="selected-medicine-final-save" type="button"><?php load_language('save');?></button></a></div>
                  </div>
                  <div class="alert hidden alert-success textCenter" id="medicine-success"></div>
                  <div class="alert hidden alert-danger textCenter" id="medicine-error"></div>
              </div>
            </div>
          </div>


          <div class="ip_edit_record_cover">
            <div class="ip_edit_record_head" data-toggle="collapse" data-target="#exam">
              <?php load_language('exams');?>
            </div>
            <div id="exam" class="collapse in">
              <div class="ip_edit_record_detail">
                <form role="form" data-parsley-validate="" id="add-exam-form">
                  <input type="hidden" name="booking_id" value="<?php echo $booking_details['book_id'];?>">
                  <input type="hidden" name="section" value="exams">
                <div class="ip_edit_row">
                  <div class="ip_bank_detail_frame">
                      <select class="ip_bank_input select-in-medi-rec" data-parsley-required="" name="exam_procedure" type="exams" id="exam-select">
                        <option disabled selected value="0"><?php load_language('exams_desc');?></option>
                         <?php if(!empty($exams))
                        {
                          foreach ($exams as $key => $elem) 
                          {
                        ?>
                        <option value="<?php echo $elem['exam_name'];?>"><?php echo $elem['exam_name'];?></option>
                        <?php                            
                          }
                        }

                        ?>
                      </select>
                  </div>
                </div>
                <div class="ip_edit_row">
                  <div class="row m0">
                    <div class="col-md-8 p0">
                      <div class="ip_bank_detail_frame">
                          <select class="ip_bank_input" data-parsley-required=""  name="observation" id="exam-observation">
                              <option selected disabled><?php load_language('exams_observation_note');?></option>
                          </select>
                      </div>
                    </div>
                    <div class="col-md-4 p0"></div>
                  </div>
                </div>
                <div class="ip_edit_row">
                  <div class="row m0">
                    <div class="col-md-2 p5">
                      <a target="_blank" href="<?php echo base_url();?>Printer/exam/<?php echo $record_id?>">
                      <button class="ip_edit_set_btn1 hidden" type="button" id="med-rec-exam-print"><?php load_language('print');?></button>
                      </a>
                    </div>
                    <div class="col-md-3 p5">
                      <a href="javascript:void(0)">
                      <button type="button" id="med-rec-exam-mail" func="exam" class="ip_edit_set_btn2 hidden record-section-mail-btn" record="<?php echo $record_id?>"><?php load_language('send_by_email');?></button>
                    </a>
                    </div> 
                    <div class="col-md-5 p5">
                      
                    </div>
                    <div class="col-md-2 p5">
                      <button class="ip_edit_set_btn uppercase" type="button" id="add-exam-btn"><?php load_language('save');?></button>
                    </div>
                    <div class="col-md-4 p5"></div>
                  </div>
                  <div class="alert hidden alert-success textCenter" id="exam-success"></div>
                  <div class="alert hidden alert-danger textCenter" id="exam-error"></div>
                </div>
              </form>
              </div>
            </div>
          </div>

          <div class="ip_edit_record_cover">
            <div class="ip_edit_record_head" data-toggle="collapse" data-target="#budjet">
              <?php load_language('budget');?>
            </div>
            <div id="budjet" class="collapse in">
              <div class="ip_edit_record_detail">
                <div class="ip_edit_row">
                  <div class="ip_budject_list">
                    <form role="form" data-parsley-validate="" id="selected-budget-form">
                      <input type="hidden" name="booking_id" value="<?php echo $booking_details['book_id'];?>">
                      <input type="hidden" name="section" value="budget">
                    
                      <div id="show-budget-main">
                        <!-- DIV SHOWING ADDED BUDGET -->
                      </div>
                    </form>
                    <!-- 
                      <li>
                      <div class="child1">1-Varicose vein removal surgery</div>
                      <div class="child2">R$240,00</div>
                      <div class="clear"></div>
                      <hr>
                      <button class="ip_exclude" type="button">Delete</button>
                      </li>-->

                    <li class="select">
                      <div class="child1"><?php load_language('total');?></div>
                      <div class="child2 select">R$<span id="budget-total-price">0</span></div>
                      <div class="clear"></div>
                    </li>
                  </div>
                </div>
              </div>
              <hr>
                <div class="ip_edit_record_detail">
                  <form role="form" data-parsley-validate="" id="add-procedure-form">
                    <input type="hidden" value="<?php echo $this->session->userdata('language');?>" id="language">
                <div class="ip_edit_row">
                  <p class="ip_row_p"><?php load_language('budget_heading');?></p>
                  <div class="ip_bank_detail_frame">
                    <select class="ip_bank_input select-in-medi-rec" data-parsley-required=""  name="procedure_name" type="budget" id="budget-select">
                            <option selected disabled value="0"><?php load_language('procedure_desc');?></option>
                             <?php if(!empty($procedure))
                            {
                              foreach ($procedure as $key => $elem) 
                              {
                            ?>
                            <option value="<?php echo $elem['procedure_name']?>"><?php echo $elem['procedure_name']?></option>
                            <?php                            
                              }
                            }

                            ?>
                    </select>
                  </div>
                </div>
                <div class="ip_edit_row">
                  <div class="row">
                    <div class="col-md-8">
                      <div class="ip_bank_detail_frame">
                          <select class="ip_bank_input" data-parsley-required=""  name="procedure-quantity" id="budget-quantity">
                              <option selected disabled><?php load_language('procedure_value');?></option>
                          </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="ip_bank_detail_frame">
                          <select class="ip_bank_input" data-parsley-required="" name="procedure-amount" id="budget-amount">
                              <option selected disabled><?php load_language('procedure_amount');?></option>
                          </select>
                      </div>
                    </div>
                  </div>
                </div>
                <a href="javascript:void(0)">
                  <button class="ip_edit_set_btn" type="button" id="add-procedure-btn" ><?php load_language('procedure_add_button');?></button>
                </a>
                </form>
              </div>
              <hr>
              <div class="ip_edit_record_detail">
                  <div class="row m0">
                    <!-- <div class="col-md-3 p5"><button class="ip_edit_set_btn1">FINAL BUDJECT</button></div> -->
                    <div class="col-md-2 p5">
                      <a target="_blank" href="<?php echo base_url();?>Printer/budget/<?php echo $record_id?>">
                      <button class="ip_edit_set_btn1 hidden" type="button" id="med-rec-budget-print"><?php load_language('print');?></button>
                      </a>
                    </div>
                    <div class="col-md-3 p5">
                     <a href="javascript:void(0)">
                      <button type="button" id="med-rec-budget-mail" func="budget" class="ip_edit_set_btn2 hidden record-section-mail-btn" record="<?php echo $record_id?>"><?php load_language('send_by_email');?></button>
                    </a>
                    </div>
                    <div class="col-md-5 p5"></div>
                    <div class="col-md-2 p5">
                      <a href="javascript:void(0)">
                        <button class="ip_edit_set_btn" type="button" id="selected-budget-final-save"><?php load_language('save');?></button>
                      </a>
                    </div>
                  </div>
                    <div class="alert hidden alert-success textCenter" id="budget-success"></div>
                    <div class="alert hidden alert-danger textCenter" id="budget-error"></div>
              </div>
            </div>
          </div>


          <div class="ip_edit_record_cover">
            <form id="add-letter-form" data-parsley-validate="" role="form">
            <div class="ip_edit_record_head" data-toggle="collapse" data-target="#letters">
              <?php load_language('attached_letters');?>
            </div>
            <div id="letters" class="collapse in">
            <div class="ip_edit_record_detail">
              <div class="ip_edit_row">
                <div class="row">
                  <div class="col-md-7">
                    <div class="ip_edit_row">
                      <div class="ip_day_time_schedule_details_data p0 floatLeft">
                           <input id="checkbox-31" class="ip_custom_checkbox1 certificate-type" name="choose-certificate-type" type="radio" value="standard">
                           <label for="checkbox-31" class="ip_custom_checkbox_label1"><?php load_language('standard_certificate');?></label>
                       </div>
                       <div class="ip_day_time_schedule_details_data p0 floatLeft">
                            <input id="checkbox-32" class="ip_custom_checkbox1 certificate-type" name="choose-certificate-type" type="radio" value="standardcid" data-parsley-required="">
                            <label for="checkbox-32" class="ip_custom_checkbox_label1"><?php load_language('standard_certificate_with_cid');?></label>
                        </div>
                        <div class="clear"></div>
                      </div>
                  </div>
                  <div class="col-md-5">
                    <div class="ip_edit_row">
                      <p class="ip_row_p"><?php load_language('days_of_removal');?></p>
                      <div class="ip_bank_detail_frame">
                          <select class="ip_bank_input certificate-type" id="certificate-days" data-parsley-required="">
                              <option value="0" disabled selected><?php load_language('select_day');?></option>
                              <?php
                              for($i=1;$i<=30;$i++)
                              {
                              ?>
                                <option class="capitalize" value="<?php echo$i?>"><?php echo$i?> <?php load_language('days');?></option>
                              <?php
                              }
                              ?>
                          </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="ip_edit_row">
                <p class="ip_row_p"><?php load_language('diagnostic_cid');?></p>
                <div class="ip_bank_detail_frame">
                    <select class="ip_bank_input certificate-type" id="certificate-cid" data-parsley-required="">
                        <option value="0" selected disabled><?php load_language('select_any');?></option>
                        
                        <?php if(!empty($cidnumbers))
                            {
                              foreach ($cidnumbers as $key => $elem) 
                              {
                            ?>
                            <option value="<?php echo $elem['code']?>"><?php echo $elem['disease_name']?>-<?php echo $elem['code']?></option>
                            <?php                            
                              }
                            }

                            ?>
                    </select>
                </div>
              </div>

              <textarea id="certificate"  type="text"  rows="10" cols="80"></textarea>

            </div>
            <hr>
            <div class="ip_edit_record_detail">
                <div class="row m0">
                  <div class="col-md-2 p5">
                    <a target="_blank" href="<?php echo base_url();?>Printer/letter/<?php echo $record_id?>">
                      <button class="ip_edit_set_btn1 hidden" type="button" id="med-rec-letter-print"><?php load_language('print');?></button>
                      </a>
                  </div>
                  <div class="col-md-3 p5">
                    <a href="javascript:void(0)">
                      <button type="button" id="med-rec-letter-mail" func="letter" class="ip_edit_set_btn2 hidden record-section-mail-btn" record="<?php echo $record_id?>"> <?php load_language('send_by_email');?> </button>
                    </a>
                  </div>
                  <div class="col-md-5 p5"><!-- <button class="ip_edit_set_btn2">SAVE AS A MODEL</button> --></div>
                  <div class="col-md-2 p5">
                    <a href="javascript:void(0)">                      
                    <button class="ip_edit_set_btn" section="certificate" bookid="<?php echo $booking_details['book_id'];?>" id="certificate-save-btn" type="button"><?php load_language('save');?></button>
                    </a>
                  </div>
                </div>
            </div>
            <div class="alert hidden alert-success textCenter" id="certificate-success"></div>
            <div class="alert hidden alert-danger textCenter" id="certificate-error"><?php load_language('certificate_minimum_length_error');?></div>
          </div>
        </form>
          </div>

        </div>


        <div class="col-md-4 sidebar" id="sidebar" >
          <form role="form" id="record-final-save-form" method="POST" action="<?php echo base_url();?>Doctor/endservice/<?php echo $booking_details['book_id'];?>" enctype="multipart/form-data">
          <input type="hidden" name="booking_id" value="<?php echo $booking_details['book_id'];?>">
          <input type="hidden" name="section" value="finalsave">
    
          <div class="ip_coutdown_timmer_wrap">
            <a href="javascript:void(0)">
            <button class="ip_circle_btn bal_btn" id="record-final-save-btn" type="button"><?php load_language('end_of_service');?></button>
            </a>
            <div class="ip_coutdown_timmer_body">
              <div class="ip_count_timmer_inner">
                <div class="ip_count_timmer">
                  <span id="doc_service_timer" >00:00:00</span>
                  <p><?php load_language('consultation_duration');?></p>
                </div>
                <div class="ip_count_clock">
                  <img src="<?php echo base_url();?>assets/images/ip_clock.png">
                </div>
                <div class="clear"></div>
              </div>
            </div>
          </div>
          <div class="ip_edit_record_cover">
            <div class="ip_edit_record_head" data-toggle="collapse" data-target="#other">
              <?php load_language('others_heading');?>
            </div>
            <div id="other" class="collapse in">
              <div class="ip_edit_record_detail">
                <div class="ip_edit_row">
                  <div class="ip_bank_detail_frame">
                      <div class="ip_bank_input p12">
                         <?php load_language('other_observation');?> 
                      </div>
                      <!-- <p class="ip_bank_input">Other observation</p> -->
                  </div>
                </div>

                <textarea class="ip_edit_record_content_textarea" name="other-observation-desc" id="otherobservation" rows="3"></textarea>

              </div>
            </div>
            <hr>
            <div class="ip_edit_attachement">
              <div class="ip_edit_record_detail pt0">
              <h6><img src="<?php echo base_url();?>assets/images/ip_attachment.png"><?php load_language('attach_images');?> </h6>
              <ul id="certificate-show-details">

                <!-- <li>
                  <div class="ip_attach_file_name">67asd4hbhb.psd</div>
                  <div class="ip_attach_file_size">657 KB</div>
                  <div class="ip_attach_close"><img src="<?php echo base_url();?>assets/images/ip_attach_close.png"></div>
                  <div class="clear"></div>
                </li> -->
                
              </ul>
            </div>
            </div>
            <div class="ip_image_attach">
              <ul id="certificate-show-img">
                  
              <!-- <li><img src="<?php echo base_url();?>assets/images/ip_image1.jpg"></li>-->
                
              </ul>
              <div class="ip_upload_img ">
                <input type="file" name="images[]" id="obsr-images" onchange="certificate_images_loadthumbnail(this)" multiple="multiple">
              </div>
              <div class="clear"></div>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>

<input type="hidden" id="certificatedata" standard="<?php echo $certificate_letter['letter']?>" standardcid="<?php echo $certificate_letter['cid_letter']?>">

<script>
  var seconds = 0, minutes = 0, hours = 0,t;

  function add() 
  {
    seconds++;
    if (seconds >= 60) 
    {
      seconds = 0;
      minutes++;
      if (minutes >= 60) 
      {
        minutes = 0;
        hours++;
      }
    }

    var elm = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);
    appendTimer_Service(elm);
    timer();
  }

  function timer() 
  {
    t = setTimeout(add, 1000);
  }
  timer();
</script>


<div id="sentmail-dialog-success" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="sentmail-dialog-head"><?php load_language('medical_record');?></h4>
      </div>
      <div class="modal-body">
        <p id="sentmail-dialog-content"><?php load_language('medical_record_sent_success');?>.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php load_language('close');?></button>
      </div>
    </div>

  </div>
</div>

<div id="sentmail-dialog-error" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="sentmail-dialog-head"><?php load_language('medical_record');?></h4>
      </div>
      <div class="modal-body">
        <p id="sentmail-dialog-content"><?php load_language('medical_record_sent_error');?>.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php load_language('close');?></button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
  function setMaximumSelected(amount,element) {
  console.log("22")
    var itemsSelected = [];
    for (var i=0;i<element.options.length;i++) {
      if (element.options[i].selected) itemsSelected[itemsSelected.length]=i;
    }
    if (itemsSelected.length>3) {
      itemsSelected = element.getAttribute("itemsSelected").split(",");
      for (i=0;i<element.options.length;i++) {
        element.options[i].selected = false;
      }
      for (i=0;i<itemsSelected.length;i++) {
        element.options[itemsSelected[i]].selected = true;
      }     
    } else {
      element.setAttribute("itemsSelected",itemsSelected.toString()); 
    }
  }
</script>
