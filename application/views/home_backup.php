<?php if($this->session->userdata('UserData'))
    {$userdata = $this->session->userdata('UserData');}

     if(auto_logout("user_time"))
    {
        $this->session->set_userdata('user_time', time());
        if($this->session->userdata('UserData'))
        {
          	$this->session->set_userdata('logout', 'autologoff');
			redirect(base_url().'Home/logout');
		}
    }   
?>

 
<style>
.ip_main_wrapper{
  margin-top:80px !important;
}
</style>
  <div class="ip_main_wrapper">

    <nav class="navbar  navbar-fixed-top">

		<!-- SECONDARY-HEADER-LOGEDOUT-->

	 <div class="ip_header_secondary ip_secondary_head_white">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
				<div class="ip_logo"><img src="<?php echo base_url();?>assets/images/ip_logo1.png"></div>
				</div>
				<div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
					<ul class="nav navbar-nav ip_navbar_nav">
						<?php if(!empty($userdata))
						{
						?>
						<li class="active"><a  class="uppercase" href="<?php echo base_url()?>Home/Dashboard"><?php load_language('home');?></a></li>
						<?php }else{
						?>
						<li class="active"><a class="uppercase"><?php load_language('home');?></a></li>
						<?php
						}
						?>
						
						<li><a  class="uppercase" href="<?php echo base_url()?>Home/about"><?php load_language('about');?></a></li>
						<?php 
				          	if(!$this->session->userdata('UserData'))
				     		{ 
			     		?>
						<li data-toggle="modal" data-target="#choose"><a><?php load_language('register_consulting');?></a></li>
						<?php
			     			}
			     		?>
						<li><a class="uppercase" href="<?php echo base_url()?>Home/contact"><?php load_language('contact_us');?></a></li>
					</ul>

					<ul class="nav navbar-right ip_nav_bar_right">
          <div class="ip_right_nav_home">
          	<?php 
	          	if($this->session->userdata('UserData'))
	     		{ 
     		?>
     			<li class="logout-btn"><a href="<?php echo base_url()?>Home/logout"><?php $UserData = $this->session->userdata('UserData'); echo $UserData['name'] ?> , <?php load_language('log_out');?></a></li> 
     		<?php
     			}
     			else
     			{
     		?>
     			<li class="open-loginmodel"><a href="javascript:void(0)"><?php load_language('log_in');?></a></li>
     		<?php
     			}
          	?>
            
            <li class="ip_nav_download_btn">
              <div class="ip_nav_download_btn_inner">
                <a><?php load_language('download_app');?></a>
              </div>
            </li>

            <?php 
				$langVal=$this->session->userdata('language');
			?> 
           <li class="">
            	<select class="nav_select" onchange="langChange(this)">
            		<option value="en" <?php echo ($langVal == 'en') ? "selected" : "";?>>EN</option>
            		<option value="pr" <?php echo ($langVal == 'pr') ? "selected" : "";?>>PR</option>
            	</select>
            </li>

<div class="clear"></div>

            
          </div>
					</ul>
				</div>
			</div>
		</div>
    </nav>


    <!--LOGIN MODEL BEGINS-->
    <div id="login" class="modal fade" role="dialog">
		<div class="modal-dialog ip_login_modal">
			<div class="modal-content ip_login_modal_content">
				<div class="ip_login_form">
					<div class="ip_login_logo">
						<img src="<?php echo base_url();?>assets/images/ip_logo1.png">
					</div>
					<hr>
					<form role="form" data-parsley-validate="" id="login-form">
					<div id="loading" style="display: none;">
					  <img id="loading-image" src="<?php echo base_url();?>assets/images/ipok-loading.gif" alt="Loading..." />
					</div>
					<div class="ip_login_input_form">
						<div class="ip_login_input_row">
							<div>
							 <input id="a" class="ip_custom_checkbox1 ip_gender_check_checkbox " type="radio" data-parsley-required data-parsley-error-message="Choose Login Type" class="" name="login_type" value="DOCTOR">
							 <label for="a" class="ip_custom_checkbox_label1 ip_doc_paitent ip_gender_check_label"><?php load_language('doctor');?></label>

							 <input id="b" class="ip_custom_checkbox1 ip_gender_check_checkbox " type="radio" name="login_type" value="PATIENT">
							 <label for="b" class="ip_custom_checkbox_label1 ip_doc_paitent ip_gender_check_label"><?php load_language('patient');?></label>

							 <input id="c" class="ip_custom_checkbox1 ip_gender_check_checkbox " type="radio" name="login_type" value="COLLABORATOR">
							 <label for="c" class="ip_custom_checkbox_label1 ip_doc_paitent ip_gender_check_label"><?php load_language('collaborator');?></label>
							<div class="clear"></div>
						</div>
						</div>
						<div class="ip_login_input_row">
							<input name="login-form-username" data-parsley-required class="ip_login_input ip_login_user clear-login-data" onKeyPress="if(this.value.length > 25) return false;"  placeholder="<?php load_language('login');?>">

						</div>
						<div class="ip_login_input_row">
							<input name="login-form-password" data-parsley-required class="ip_login_input ip_login_pass clear-login-data" onKeyPress="if(this.value.length > 25) return false;" placeholder="<?php load_language('password');?>" type="password">
						</div>
						<div class="">
							<a href="javascript:void(0)">
							<button type="button" class="ip_login_modal_signin floatLeft uppercase" id="login_submit"><?php load_language('login');?></button>	
							</a>

							<p class="floatLeft" id="forgot_password_btn"><?php load_language('forgot_password');?></p>
							<div class="clear"></div>
						</div>
					</div>
				</form>
				<div id="err-login" class="alert alert-danger hidden textCenter"> </div>
				<div class="alert alert-success hidden" id="pat-reg-success">
				  <strong><?php load_language('alert_success');?></strong><?php load_language('patient_account_register_desc');?>
				</div>
					<hr>
					<div class="ip_login_input_form">
						<div class="textCenter">
							<p id="home_registernowbtn"><?php load_language('not_registered');?><a href="javascript:void(0)"><?php load_language('register_now');?></a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="forgot" class="modal fade" role="dialog">
		<div class="modal-dialog ip_login_modal">
			<div class="modal-content ip_login_modal_content">
				<div class="ip_login_form">
					<div class="ip_login_logo">
						<img src="<?php echo base_url();?>assets/images/ip_logo1.png">
					</div>
					<hr>
					<div class="ip_login_input_form">
					<form role="form" data-parsley-validate="" id="forgot-pass-form">
						<p class="pl0"><strong><?php load_language('forgot_password');?></strong></p>
						<div class="ip_login_input_row">
							<div>
								 <input id="forgot-doc" class="ip_custom_checkbox1 ip_gender_check_checkbox " type="radio" data-parsley-required data-parsley-error-message="<?php load_language('choose_type');?>" class="" name="type" value="DOCTOR">
								 <label for="forgot-doc" class="ip_custom_checkbox_label1 ip_doc_paitent ip_gender_check_label"><?php load_language('doctor');?></label>

								 <input id="forgot-pat" class="ip_custom_checkbox1 ip_gender_check_checkbox " type="radio" name="type" value="PATIENT">
								 <label for="forgot-pat" class="ip_custom_checkbox_label1 ip_doc_paitent ip_gender_check_label"><?php load_language('patient');?></label>
								<div class="clear"></div>
							</div>
						</div>
						<div class="ip_login_input_row">
							<input class="ip_login_input ip_login_msg" onKeyPress="if(this.value.length > 75) return false;" name="email"  data-parsley-required="true" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="<?php load_language('enter_mail_id');?>">
						</div>
						<div class="textCenter">
							<button type="button" class="ip_login_modal_signin" id="forgot_password_sent_btn"><?php load_language('sent_mail');?></button>
						</div>
					</form>
					</div>
					<div id="forgot-pass-error" class="hidden alert alert-danger textCenter"></div>
					<hr>
					<div class="ip_login_input_form">
						<div class="textCenter">
							<p><?php load_language('forgot_password_desc');?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<div id="verify" class="modal fade" role="dialog">
		<div class="modal-dialog ip_login_modal">
			<div class="modal-content ip_login_modal_content">
				<div class="ip_login_form">
					<div class="ip_login_logo">
						<img src="<?php echo base_url();?>assets/images/ip_logo1.png">
					</div>
					<hr>
					<div class="ip_login_input_form">
						<div class="">
							<p class="textCenter">
							<img src="<?php echo base_url();?>assets/images/ip_verify_mail.png">
							<a><strong><?php load_language('check_your_email');?></strong></a><br>
<?php load_language('check_your_email');?><?php load_language('check_your_email_desc');?><br><?php load_language('password_recovery');?></p>
							<div class="clear"></div>
						</div>
					</div>
					<hr>
				</div>
			</div>
		</div>
	</div>

    <!--LOGIN MODEL ENDS-->
	
	

	<!--REGISTRATION CHOOSE-MODAL BEGINS-->
	
	<div id="choose" class="modal fade" role="dialog">
	  <div class="modal-dialog ip_reg_modal">
		<div class="modal-content">
			<div class="ip_reg_modal_head">			
				<?php load_language('register_as');?>
			</div>
			<br><br>
			<div class="ip_reg_modal_footer">
				<button class="ip_sign_footer_btn" id="reg_choose_dct" type="button" onclick="location.href='<?php echo base_url();?>Home/RegisterDoctor'"  data-toggle="modal"><?php load_language('register_as_doctor');?></button>
				<button class="ip_sign_footer_btn" id="reg_choose_pat"  data-toggle="modal"><?php load_language('register_as_patient');?></button>
			</div>
		</div>
	  </div>
	</div>
	<!--REGISTRATION CHOOSE-MODAL ENDS-->


	<!--PATIENT REGISTRATION MODEL BEGINS-->
	<div id="regpaitent" class="modal fade" role="dialog">
	  <div class="modal-dialog ip_reg_modal">
		<div class="modal-content">
			<div class="ip_reg_modal_head">			
				<?php load_language('create_patient_account');?>
			</div>
			<div class="stepwizard hide">
				<div class="stepwizard-row setup-panel">
					<div class="stepwizard-step col-xs-3"> 
						<a href="#step-1" type="button" class="btn btn-success btn-circle">1</a>
						<p><small>Shipper</small></p>
					</div>
					<div class="stepwizard-step col-xs-3"> 
						<a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
						<p><small>Destination</small></p>
					</div>
					<div class="stepwizard-step col-xs-3"> 
						<a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
						<p><small>Schedule</small></p>
					</div>
					<div class="stepwizard-step col-xs-3"> 
						<a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
						<p><small>Cargo</small></p>
					</div>
				</div>
			</div>
			<!-- <form role="form" id="reg-form-patient-main"> -->
    
			<form role="form" data-parsley-validate=""  id="reg-form-patient-1">
			
				<!--STEP-1 -->
				
				<div class="setup-content" id="step-1">
					<div class="ip_reg_modal_content">
						<p class="textCenter"><?php load_language('register_as_doctor');?><a href="<?php echo base_url();?>Home/RegisterDoctor"><?php load_language('click_here');?></a></p>
						<div class="ip_reg_with_fb">
							<div class="ip_logo_fb floatLeft">
								<img src="<?php echo base_url();?>assets/images/facebook.png">
							</div>

							<a href="<?php if(!empty($FBauthUrl)) echo $FBauthUrl ?>"><div class="ip_content_fb floatRight"><?php load_language('enter_with_facebook');?></div>
							</a>

							<div class="clear"></div>
						</div>
						<p class="textCenter"><?php load_language('account_creation_condition');?><a class="uppercase"><?php load_language('terms_and_conditions');?>.</a></p>
						<hr>
						<h6 class="textCenter"><?php load_language('personal_data');?></h6>
						<div class="ip_reg_modal_row">
							<div class="row">
								<div class="col-md-12">
									<div class="ip_bank_detail_frame">
										<input  name="reg_pat_email" maxlength="100" type="text" data-parsley-required="true" data-parsley-email="" class="ip_reg_form_input form-control reset-form-custom" onKeyPress="if(this.value.length > 100) return false;"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"   placeholder="<?php load_language('email');?>*"   id="reg-form-email" >
									</div>
									
									
								</div>
							</div>
						</div>

						<div class="ip_reg_modal_row">
							<div class="row">
								<div class="col-md-6">
									<p>RG</p>
									<div class="ip_bank_detail_frame">
										<input name="reg_pat_rg" maxlength="25" type="text" onKeyPress="if(this.value.length > 25) return false;" class="ip_reg_form_input form-control reset-form-custom" placeholder="">
									</div>
								</div>
								<div class="col-md-6">
									<p>CPF*</p>
									<div class="ip_bank_detail_frame">
										<input name="reg_pat_cpf" data-parsley-required="true" data-parsley-minlength="11" data-parsley-cpf="" data-parsley-cpfunique="" onKeyPress="if(this.value.length > 10) return false;"  type="number" class="ip_reg_form_input form-control reset-form-custom" placeholder="">
									</div>
								</div>
							</div>
						</div>
						<div class="ip_reg_modal_row">
							<div class="row">
								<div class="col-md-6">
									<p><?php load_language('date_of_birth');?>*</p>
									<div class="ip_bank_detail_frame" id="registration-container">
										<!-- <input class="ip_reg_form_input" type="text" form-control" placeholder=""> -->
										<input name="reg_pat_dob" readonly class="ip_reg_form_input form-control reset-form-custom background_transparent" data-parsley-required="true">
									</div>
								</div>
									<div class="col-md-6">
									<p><?php load_language('gender');?>*</p>
										<div class="ip_day_time_schedule_details_data p0  ip_gender_check">
											<div>
											 <input id="reg-form-patient-male" class="ip_custom_checkbox1 ip_gender_check_checkbox " name="reg_pat_gender" type="radio" required data-parsley-error-message="Choose Gender" value="MALE">
											 <label for="reg-form-patient-male" class="ip_custom_checkbox_label1 ip_gender_check_label"><?php load_language('male');?></label>
							
											 <input id="reg-form-patient-female" class="ip_custom_checkbox1 ip_gender_check_checkbox "  name="reg_pat_gender" type="radio" value="FEMALE">
											 <label for="reg-form-patient-female" class="ip_custom_checkbox_label1 ip_gender_check_label"><?php load_language('female');?></label>
										  <input id="reg-form-patient-others" class="ip_custom_checkbox1 ip_gender_check_checkbox "  name="reg_pat_gender" type="radio" value="OTHERS">
										  <label for="reg-form-patient-others" class="ip_custom_checkbox_label1 ip_gender_check_label"><?php load_language('others');?></label>
										  <div class="clear"></div>
										</div>
										</div>
										
								</div>
							</div>
						</div>
						<hr>
						<div class="ip_reg_modal_footer">
							<button class="ip_sign_footer_btn btn btn-primary nextBtn nextBtn-1" type="button"><?php load_language('next');?></button>
						</div>
					</div>
				</div>
				</form>
				
				<!--STEP-2 -->
				
				<form role="form" data-parsley-validate=""  id="reg-form-patient-2">
				<div class="setup-content" id="step-2">
					<div class="ip_reg_modal_content">
						<h6 class="textCenter"><?php load_language('basic_medical_data');?></h6>
							<hr>
							<div class="ip_reg_modal_row">
								<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-6">
										<p class="textCenter"><?php load_language('weight');?></p>
										<div class="ip_bank_detail_frame">
											<input  data-parsley-type="digits" name="reg_pat_weight"  type="number" onKeyPress="if(this.value.length > 2) return false;" data-parsley-maxlength="3" id="reg-form-weight" class="ip_reg_form_input reset-form-custom" placeholder="">
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>
							</div>
							<div class="ip_reg_modal_row">
								<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-6">
										<p class="textCenter"><?php load_language('height');?></p>
										<div class="ip_bank_detail_frame">
											<input data-parsley-type="digits" name="reg_pat_height"  maxlength="100" onKeyPress="if(this.value.length > 2) return false;" data-parsley-maxlength="3" type="number"  class="ip_reg_form_input reset-form-custom" placeholder="">
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>
							</div>
							<div class="ip_reg_modal_row">
								<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-6">
										<p class="textCenter"><?php load_language('blood_type');?></p>
										<div class="ip_bank_detail_frame">
											<input name="reg_pat_bloodgrp" style="text-transform:uppercase" onKeyPress="if(this.value.length > 10) return false;" maxlength="10" type="text" data-parsley-bloodgroup="" class="ip_reg_form_input reset-form-custom" placeholder="">
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>
							</div>
						<hr>
						<div class="ip_reg_modal_footer">
							<button class="ip_sign_footer_btn btn btn-primary nextBtn floatRight nextBtn-2" type="button"><?php load_language('next');?></button>
							<button class="ip_sign_footer_btn btn btn-primary prevBtn floatLeft prevBtn-2" type="button"><?php load_language('previous');?></button>
						</div>
					</div>
				</div>
				</form>
				<!--STEP-3 -->
				<form role="form" data-parsley-validate="" id="reg-form-patient-3">
				<div class="setup-content" id="step-3">
					<div class="ip_reg_modal_content">
						<h6 class="textCenter"><?php load_language('address');?></h6>
							<hr>
							<div class="ip_reg_modal_row">
								<div class="row">
									<div class="col-md-12">
										<div class="ip_bank_detail_frame">
											<input data-parsley-required name="reg_pat_cep"  class=" ip_reg_form_input reset-form-custom" onKeyPress="if(this.value.length > 7) return false;" type="number" data-parsley-minlength="8" data-parsley-maxlength="8" data-parsley-cep="patient" data-parsley-pattern="^[0-9]+$" onkeyup="loadaddress_cep(this,'patient')" placeholder="CEP*">
										</div>
									</div>
								</div>
							</div>
							<div class="ip_reg_modal_row">
								<div class="row">
									<div class="col-md-12">
										<div class="ip_bank_detail_frame">
											<input data-parsley-required name="reg_pat_streetadd"  type="text" data-parsley-maxlength="50" data-parsley-pattern="^[a-zA-Z ]+$" class="ip_reg_form_input reset-form-custom" onKeyPress="if(this.value.length > 50) return false;" data-parsley-minlength="5" id="pat-reg-rua" placeholder="Rua*">
										</div>
									</div>
								</div>
							</div>
							<div class="ip_reg_modal_row">
								<div class="row">
									<div class="col-md-6">
										<div class="ip_bank_detail_frame">
											<input data-parsley-required data-parsley-maxlength="50" onKeyPress="if(this.value.length > 50) return false;" name="reg_pat_locality" type="text" class="ip_reg_form_input reset-form-custom" id="pat-reg-locality" data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-minlength="5"  placeholder="<?php load_language('neighbourhood');?>*">
										</div>
									</div>
									<div class="col-md-6">
										<div class="ip_bank_detail_frame">
											<input data-parsley-required data-parsley-maxlength="30" name="reg_pat_number" type="number" data-parsley-pattern="^[0-9]+$" class="ip_reg_form_input reset-form-custom" onKeyPress="if(this.value.length > 30) return false;" id="pat-reg-number" data-parsley-minlength="5" placeholder="<?php load_language('number');?>*">
										</div>
									</div>
								</div>
							</div>
							<div class="ip_reg_modal_row">
								<div class="row">
									<div class="col-md-12">
										<div class="ip_bank_detail_frame">
											<input data-parsley-maxlength="50" name="reg_pat_complement" type="text" onKeyPress="if(this.value.length > 50) return false;"  class="ip_reg_form_input reset-form-custom" id="pat-reg-complement" placeholder="<?php load_language('complement');?>">
										</div>
									</div>
								</div>
							</div>
							<div class="ip_reg_modal_row">
								<div class="row">
									<div class="col-md-12">
										<div class="ip_bank_detail_frame">
											<input data-parsley-maxlength="50" name="reg_pat_occupation" type="text" onKeyPress="if(this.value.length > 50) return false;" data-parsley-pattern="^[a-zA-Z ]+$"  class="ip_reg_form_input reset-form-custom" placeholder="<?php load_language('occupation');?>">
										</div>
									</div>
								</div>
							</div>
						<hr>
						<div class="ip_reg_modal_footer">
							<button class="ip_sign_footer_btn btn btn-primary nextBtn floatRight nextBtn-3" type="button"><?php load_language('next');?></button>
							<button class="ip_sign_footer_btn btn btn-primary prevBtn floatLeft prevBtn-3" type="button"><?php load_language('previous');?></button>
						</div>
					</div>
				</div>
			</form>
        <!--STEP-4 -->
					<form  role="form" data-parsley-validate="" id="reg-form-patient-4">
				<div class="setup-content" id="step-4">
					<div class="ip_reg_modal_content">
						<h6 class="textCenter"><?php load_language('login_and_password');?></h6>
						<hr>
						<div class="ip_reg_modal_row">
							<div class="row">
								<div class="col-md-12">
									<p><?php load_language('name');?>*</p>
									<div class="ip_bank_detail_frame">
										<input data-parsley-required id="reg-form-patient-name" name="reg_pat_name" data-parsley-minlength="5" onKeyPress="if(this.value.length > 40) return false;" data-parsley-pattern="^[a-zA-Z ]+$" type="text" class="ip_reg_form_input reset-form-custom" >
									</div>
								</div>
							</div>
						</div>
						<div class="ip_reg_modal_row">
							<div class="row">
								<div class="col-md-6">
									<p><?php load_language('username');?>*</p>
									<div class="ip_bank_detail_frame">
										<input data-parsley-required  data-parsley-username="" name="reg_pat_username" data-parsley-minlength="5" onKeyPress="if(this.value.length > 25) return false;" maxlength="25" data-parsley-pattern="^[a-zA-Z0-9]+$" type="text" class="ip_reg_form_input reset-form-custom" placeholder="">
									</div>
								</div>
								<div class="col-md-6">
									<p><?php load_language('password');?>*</p>
									<div class="ip_bank_detail_frame">
										<input id="reg-form-pass" data-parsley-required name="reg_pat_password" data-parsley-minlength="8" onKeyPress="if(this.value.length > 25) return false;" type="password" class="ip_reg_form_input reset-form-custom" placeholder="" data-parsley-uppercase="1" data-parsley-lowercase="1" data-parsley-number="1" data-parsley-special="1">
									</div>
								</div>
							</div>
						</div>
						<div class="ip_reg_modal_row">
							<div class="row">
								<div class="col-md-6">
									<p><?php load_language('confirm_password');?>*</p>
									<div class="ip_bank_detail_frame">
										<input data-parsley-equalto="#reg-form-pass" data-parsley-required name="reg_pat_confirmpassword" maxlength="100" type="password" onKeyPress="if(this.value.length > 25) return false;"  class="ip_reg_form_input reset-form-custom" placeholder="">
									</div>
								</div>
								<div class="col-md-6">
									<p><?php load_language('add_photo_to_profile');?>*</p>

									<div class="ip_reg_add_phot_div">
									<button id="add_pat_pic_btn" class="ip_add_photo_doc"><?php load_language('add_photo');?>
										<input id="reg_pat_pic" data-parsley-required name="reg_pat_profilepic" type="file" accept="image/*"  class="ip_reg_form_input reset-form-custom "
										data-parsley-error-message="<?php load_language('profile_photo_error_text');?>" onchange="pat_loadthumbnail(this)" placeholder="" >
									</button>
									<div class="ip_reg_modal_addphoto">
										<img src="" id="reg-pat-temppic">
									</div>
								
									</div>




									
								</div>
							</div>
						</div>
						<hr>
						<div class="ip_reg_modal_footer">
							<button class="ip_sign_footer_btn btn btn-primary nextBtn nextBtn-4 floatRight" type="button"><?php load_language('finish');?></button>
							<button class="ip_sign_footer_btn btn btn-primary prevBtn floatLeft prevBtn-4" type="button"><?php load_language('previous');?></button>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</form>
		<!-- </form> -->
		</div>	
	 </div>
 </div>

<div class="alert alert-danger hidden" id="pat-reg-error">
  <strong><?php load_language('error');?>!</strong> <p><?php load_language('patient_registration_failed');?></p>
</div>


<?php
	if($this->session->flashdata('message')) {
	$message = $this->session->flashdata('message');
?>
	<div class="alert alert-<?php echo $message['class']; ?> alert-dismissible flash-msg">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><strong> <?php echo $message['title']; ?></strong></h4>
	<?php echo $message['message']; ?>
	</div>
<?php
	}
?><?php


	if($this->session->userdata('logout')) 
	{
	?>
	<div class="alert alert-danger alert-dismissible flash-msg">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><strong>Error</strong></h4>
		Session Expired, Kindly Login
	</div>
	<?php
	unset($_SESSION['logout']);
	}
	?>
	<!--PATIENT REGISTRATION MODEL ENDS-->

  

    <div class="ip_home_banner">
      <div class="ip_home_banner_inner">
        <div class="container">
          <h3><?php load_language('home_main_heading');?></h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
          <form role="form" id="doctor-search-form" action="<?php echo base_url()?>Searchdoctor" method="post" >
          <div class="ip_search_home_div">
            <div class="ip_speciality">
              <!-- <input class="ip_speciality_input" name="doctor-search-speciality"  placeholder="Speciality"> -->
               <select class="ip_speciality_input" placeholder="" name="doctor-search-speciality">
               	<option disabled selected><?php load_language('speciality');?></option>
			    <?php foreach ($speciality_list as $key => $value) {
			    ?>
			    	<option value="<?php echo $value['specialization_name']?>"><?php echo $value['specialization_name']?></option>
		        <?php	
		        }
		        ?>
			  </select>
            </div>
            <div class="ip_location_home_search">
              <input class="ip_speciality_input" name="doctor-search-location"  id="doctor_search_location" placeholder="<?php load_language('location');?>">
              <input type="hidden" id="locationLattitude" name="doctor-search-latitude">
              <input type="hidden" id="locationLongitude" name="doctor-search-longitude">
            </div>
            <a href="javascript:void(0)">
            	<div class="ip_search_home_search_btn"></div>
            </a>
            
            <div class="ip_home_search_menu">
              <div class="ip_home_search_menu_inner"></div>
            </div>
            <div class="ip_home_search_data" id="booking-container">
              <input class="ip_speciality_input" name="doctor-search-date" readonly="" placeholder="<?php load_language('date');?>">
            </div>
            <div class="clear"></div>
          </div>
      </form>
        </div>
      </div>
      <img src="<?php echo base_url();?>assets/images/ip_banner.jpg">
    </div>
    <div class="ip_home_main_function textCenter">
      <div class="container">
        <h3>Main Functions</h3>
        <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br>  when an unknownprinter took a galley of type and scrambledit <br> to make a type specimen book. It has survived not only five centuries,</p>
      </div>
    </div>
    <div class="ip_home_main_grid textCenter">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h4>FIRST ONE</h4>
            <p>Lorem Ipsum has been the<BR>
               text ever since the 1500s,<BR> when an unknown printer
               <BR>took a galley of type and <BR>scrambled it to make.</p>
          </div>
          <div class="col-md-6">
            <h4>SECOND ONE</h4>
            <p>Lorem Ipsum has been the<BR> industry's standard dummy<BR>
               text ever since the 1500s,
               <BR>took a galley of type and.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <h4>THIRD ONE</h4>
            <p>Lorem Ipsum has been the<BR> industry's standard dummy<BR>
              text ever since the 1500s,<BR> when an unknown printer
            .</p>
          </div>
          <div class="col-md-6">
            <h4>FOURTH ONE</h4>
            <p>Lorem Ipsum has been the<BR> industry's standard dummy<BR>
               text ever since the 1500s,<BR> when an unknown printer
               <BR>scrambled it to make.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="ip_home_video">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="ip_home_video_left">
              <h3>Video campaign</h3>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="ip_home_video_right">
              <div id='ip_player'>
                <video id='ip_video-element'>
                  <source src='<?php echo base_url();?>assets/videos/mov_bbb.mp4' type='video/mp4'>
                  
                </video>
                <div id='ip_player_controls'>
                  <!-- <progress id='progress-bar' min='0' max='100' value='0'>0% played</progress>
                  <button id='btnReplay' class='replay' title='replay' accesskey="R" onclick='replayVideo();'>Replay</button> -->
                  <button id='btnPlayPause' class='play' title='play' accesskey="P" onclick='playPauseVideo();'>Play</button>
                  <!-- <button id='btnStop' class='stop' title='stop' accesskey="X" onclick='stopVideo();'>Stop</button>
                  <input type="range" id="volume-bar" title="volume" min="0" max="1" step="0.1" value="1">
                  <button id='btnMute' class='mute' title='mute' onclick='muteVolume();'>Mute</button>
                  <button id='btnFullScreen' class='fullscreen' title='toggle full screen' accesskey="T" onclick='toggleFullScreen();'>[&nbsp;&nbsp;]</button> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="ip_home_main_function textCenter">
      <div class="container">
        <h3>Mark your Query</h3>
        <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown <BR>printer took a galley of type and scrambledit to make a type specimen book. It has survived not only five centuries, <BR>printer took a galley of type and scrambledit </p>
          <button class="ip_home_download_btn">Download App</button>
      </div>
    </div>
    <hr>
    <div class="ip_home_main_function textCenter">
      <div class="container">
        <h3>Our partners</h3>
        <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown <BR>and scrambledit to make a type specimen book. It has survived not only</p>
      </div>
    </div>
    <div class="ip_testimonial_slider">
      <div class="container">
      <div class="responsive" id="home_testimonials">
        <li>
          <div class="ip_testimonial_circle">
            <img src="<?php echo base_url();?>assets/images/ip_smiley.png">
          </div>
          <h4>JOHN DOE</h4>
          <h5>Professional</h5>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.dummy text ever since the 1500s,</p>
        </li>
        <li>
          <div class="ip_testimonial_circle">
            <img src="<?php echo base_url();?>assets/images/ip_smiley.png">
          </div>
          <h4>EDWARD MARCUS</h4>
          <h5>Professional</h5>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.dummy text ever since the 1500s,</p>
        </li>
        <li>
          <div class="ip_testimonial_circle">
            <img src="<?php echo base_url();?>assets/images/ip_smiley.png">
          </div>
          <h4>BRITENY LORENZ</h4>
          <h5>Professional</h5>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.dummy text ever since the 1500s,</p>
        </li>
        <li>
          <div class="ip_testimonial_circle">
            <img src="<?php echo base_url();?>assets/images/ip_smiley.png">
          </div>
          <h4>SARA ELIZEBETH</h4>
          <h5>Professional</h5>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.dummy text ever since the 1500s,</p>
        </li>
        <li>
          <div class="ip_testimonial_circle">
            <img src="<?php echo base_url();?>assets/images/ip_smiley.png">
          </div>
          <h4>CAMAROON MOROE</h4>
          <h5>Professional</h5>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.dummy text ever since the 1500s,</p>
        </li>
        <li>
          <div class="ip_testimonial_circle">
            <img src="<?php echo base_url();?>assets/images/ip_smiley.png">
          </div>
          <h4>HUGHAN PHILIPS</h4>
          <h5>Professional</h5>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.dummy text ever since the 1500s,</p>
        </li>
        <li>
          <div class="ip_testimonial_circle">
            <img src="<?php echo base_url();?>assets/images/ip_smiley.png">
          </div>
          <h4>MARIA PETERSON</h4>
          <h5>Professional</h5>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.dummy text ever since the 1500s,</p>
        </li>
        <li>
          <div class="ip_testimonial_circle">
            <img src="<?php echo base_url();?>assets/images/ip_smiley.png">
          </div>
          <h4>LARA PHELPS</h4>
          <h5>Professional</h5>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.dummy text ever since the 1500s,</p>
        </li>
      </div>
      </div>
    </div>
    <div class="ip_home_numbers textCenter">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <h5><strong>550</strong><br>
              CLIENTS
            </h5>
            <p>Lorem Ipsum is simply <br>dummy text of the <br>printing and </p>
          </div>
          <div class="col-md-3">
            <h5><strong>850</strong><br>
              PRODUCTS
            </h5>
            <p>Lorem Ipsum is simply <br>dummy text of the <br>printing and </p>
          </div>
          <div class="col-md-3">
            <h5><strong>90K</strong><br>
              SALES
            </h5>
            <p>Lorem Ipsum is simply <br>dummy text of the <br>printing and </p>
          </div>
          <div class="col-md-3">
            <h5><strong>20</strong><br>
              OFFICES
            </h5>
            <p>Lorem Ipsum is simply <br>dummy text of the <br>printing and </p>
          </div>
        </div>
      </div>
    </div>
</div>
<footer class="ip_home_footer">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <p>&copy ipok 2017</p>
      </div>
      <div class="col-md-6">
        <ul>
          <li>About</li>
          <li>Contact</li>
          <li>Stories</li>
          <li>Company</li>
          <div class="clear"></div>
        </ul>
      </div>
      <div class="col-md-3 textRight">
        <span><img src="<?php echo base_url();?>assets/images/ip_twit.png"></span>
        <span><img src="<?php echo base_url();?>assets/images/ip_fb.png"></span>
      </div>
    </div>
  </div>
</footer>

<script>
      // Get a handle to the player
	player       = document.getElementById('ip_video-element');
	btnPlayPause = document.getElementById('btnPlayPause');
	btnMute      = document.getElementById('btnMute');
	progressBar  = document.getElementById('progress-bar');
  volumeBar    = document.getElementById('volume-bar');

  // Update the video volume
 /* volumeBar.addEventListener("change", function(evt) {
		player.volume = evt.target.value;
	});*/
 // document.getElementById('btnFullScreen').disabled = true;
	// Add a listener for the timeupdate event so we can update the progress bar
	//player.addEventListener('timeupdate', updateProgressBar, false);

	// Add a listener for the play and pause events so the buttons state can be updated
	player.addEventListener('play', function() {
		// Change the button to be a pause button
		changeButtonType(btnPlayPause, 'pause');
	}, false);

	player.addEventListener('pause', function() {
		// Change the button to be a play button
		changeButtonType(btnPlayPause, 'play');
	}, false);

	player.addEventListener('volumechange', function(e) {
		// Update the button to be mute/unmute
		if (player.muted) changeButtonType(btnMute, 'unmute');
		else changeButtonType(btnMute, 'mute');
	}, false);

	player.addEventListener('ended', function() { this.pause(); }, false);

  //progressBar.addEventListener("click", seek);

 /* function seek(e) {
      var percent = e.offsetX / this.offsetWidth;
      player.currentTime = percent * player.duration;
      e.target.value = Math.floor(percent / 100);
      e.target.innerHTML = progressBar.value + '% played';
  }
*/
  function playPauseVideo() {
  	if (player.paused || player.ended) {
  		// Change the button to a pause button
  		changeButtonType(btnPlayPause, 'pause');
  		player.play();
  	}
  	else {
  		// Change the button to a play button
  		changeButtonType(btnPlayPause, 'play');
  		player.pause();
  	}
  }

  // Stop the current media from playing, and return it to the start position
  function stopVideo() {
  	player.pause();
  	if (player.currentTime) player.currentTime = 0;
  }

  // Toggles the media player's mute and unmute status
  function muteVolume() {
  	if (player.muted) {
  		// Change the button to a mute button
  		changeButtonType(btnMute, 'mute');
  		player.muted = false;
  	}
  	else {
  		// Change the button to an unmute button
  		changeButtonType(btnMute, 'unmute');
  		player.muted = true;
  	}
  }

  // Replays the media currently loaded in the player
  function replayVideo() {
  	resetPlayer();
  	player.play();
  }

  // Update the progress bar
  function updateProgressBar() {
  	// Work out how much of the media has played via the duration and currentTime parameters
  	var percentage = Math.floor((100 / player.duration) * player.currentTime);
  	// Update the progress bar's value
  	progressBar.value = percentage;
  	// Update the progress bar's text (for browsers that don't support the progress element)
  	progressBar.innerHTML = percentage + '% played';
  }

  // Updates a button's title, innerHTML and CSS class
  function changeButtonType(btn, value) {
  	btn.title     = value;
  	btn.innerHTML = value;
  	btn.className = value;
  }

  function resetPlayer() {
  	progressBar.value = 0;
  	// Move the media back to the start
  	player.currentTime = 0;
  	// Set the play/pause button to 'play'
  	changeButtonType(btnPlayPause, 'play');
  }

  function exitFullScreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    }
  }

  function toggleFullScreen() {
    //var player = document.getElementById("player");

    if (player.requestFullscreen)
        if (document.fullScreenElement) {
            document.cancelFullScreen();
        } else {
            player.requestFullscreen();
        }
        else if (player.msRequestFullscreen)
        if (document.msFullscreenElement) {
            document.msExitFullscreen();
        } else {
            player.msRequestFullscreen();
        }
        else if (player.mozRequestFullScreen)
        if (document.mozFullScreenElement) {
            document.mozCancelFullScreen();
        } else {
            player.mozRequestFullScreen();
        }
        else if (player.webkitRequestFullscreen)
        if (document.webkitFullscreenElement) {
            document.webkitCancelFullScreen();
        } else {
            player.webkitRequestFullscreen();
        }
    else {
        alert("Fullscreen API is not supported");

    }
  } 
      </script>

    
