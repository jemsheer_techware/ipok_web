
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">
    <div class="ip_bio_tab_div">
      <div class="row m0">
        <div class="col-md-2 p0 height100">
          <div class="ip_bio_tab_bay height100">
            <ul>
              <li class="active" data-toggle="tab" href="#profile"><?php load_language('profile');?></li>
              <li data-toggle="tab" href="#bio"><?php load_language('address');?></li>
              <!-- <li data-toggle="tab" href="#photo">Photos</li> -->
              <!-- <li class="arrow" data-toggle="tab" href="#special">Support</li> -->
              <!-- <li data-toggle="tab" href="#more" class="arrow">More</li> -->
            </ul>
          </div>
        </div>
        <div class="col-md-10 p0">
          <div class="ip_bio_tab_content">
            <div class="tab-content">

              <div id="profile" class="tab-pane fade in active">
                <div class="ip_profile_tab_top">
                  <div class="ip_profile_tab_circle">
                    <img src="<?php echo base_url();echo $patient_data['pt_pic']?>">
                  </div>
                  <div class="ip_profile_tab_name">
                    <h3><?php echo decrypt_data($patient_data['pt_name'])?></h3>
                  </div>
                  <div class="ip_profile_tab_button">
                    <a href="<?php echo base_url();?>Patient/editProfile">
                      <div class="ip_profile_tab_button_circle"><img src="<?php echo base_url();?>assets/images/ip_edit.png"></div>
                    </a>
                    <a href="javascript:void(0)" class="delete_profile">
                    <div class="ip_profile_tab_button_circle"><img src="<?php echo base_url();?>assets/images/ip_delete.png"></div>
                    </a>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                </div>
                <div class="ip_profile_tab_detail">
                  <div class="row">
                    <div class="col-md-6">
                      <ul>
                        <li>
                          <div class="child1"><?php load_language('email');?> :</div>
                          <div class="child2"><?php echo $patient_data['pt_email']?></div>
                          <div class="clear"></div>
                        </li>
                        <li>
                          <div class="child1"><?php load_language('phone');?> :</div>
                          <div class="child2"><?php echo decrypt_data($patient_data['pt_number']);?></div>
                          <div class="clear"></div>
                        </li>
                        <li>
                          <div class="child1"><?php load_language('bloodgroup');?> :</div>
                          <div class="child2"><?php echo decrypt_data($patient_data['pt_blood_group']);?></div>
                          <div class="clear"></div>
                        </li>
                       
                      </ul>
                    </div>
                    <div class="col-md-6">
                      <ul>
                        <li>
                          <div class="child1"><?php load_language('birthday');?> :</div>
                          <div class="child2"><?php echo date('d F Y',$patient_data['pt_dob']);?></div>
                          <div class="clear"></div>
                        </li>
                        <li>
                          <div class="child1"><?php load_language('weight');?> :</div>
                          <div class="child2"><?php echo decrypt_data($patient_data['pt_weight']);?>Kg</div>
                          <div class="clear"></div>
                        </li>
                        <li>
                          <div class="child1"><?php load_language('height');?> :</div>
                          <div class="child2"><?php echo decrypt_data($patient_data['pt_height']);?>cm</div>
                          <div class="clear"></div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>

              <div id="bio" class="tab-pane fade">

                 <div class="ip_profile_tab_top">
                  <div class="ip_profile_tab_circle">
                    <img src="<?php echo base_url();echo $patient_data['pt_pic']?>">
                  </div>
                  <div class="ip_profile_tab_name">
                    <h3><?php echo decrypt_data($patient_data['pt_name']);?></h3>
                  </div>
                  <div class="ip_profile_tab_button">
                    <a href="<?php echo base_url();?>Patient/editProfile">
                      <div class="ip_profile_tab_button_circle"><img src="<?php echo base_url();?>assets/images/ip_edit.png"></div>
                    </a>
                    <a href="javascript:void(0)"  class="delete_profile">
                    <div class="ip_profile_tab_button_circle"><img src="<?php echo base_url();?>assets/images/ip_delete.png"></div>
                    </a>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                </div>

                <!-- <div class="ip_profile_tab_detail">
                  <h3>Address</h3>
                  <p><?php echo $patient_data['pt_street_add']?></p>
                  <p><?php echo $patient_data['pt_locality']?></p>
                  <p><?php echo $patient_data['pt_zip_code']?></p>
                </div> -->

                 <div class="ip_profile_tab_detail">
                
                  <div class="row">
                    <div class="col-md-6">
                      <ul>
                        <li>
                          <div class="child1"><?php load_language('CEP');?> :</div>
                          <div class="child2"><?php echo decrypt_data($patient_data['pt_zip_code']);?></div>
                          <div class="clear"></div>
                        </li>
                        <li>
                          <div class="child1"><?php load_language('Rua');?> :</div>
                          <div class="child2"><?php echo decrypt_data($patient_data['pt_street_add']);?></div>
                          <div class="clear"></div>
                        </li>
                        <li>
                          <div class="child1"><?php load_language('number');?> :</div>
                          <div class="child2"><?php echo decrypt_data($patient_data['pt_number']);?></div>
                          <div class="clear"></div>
                        </li>
                       
                      </ul>
                    </div>
                    <div class="col-md-6">
                      <ul>
                        <li>
                          <div class="child1"><?php load_language('neighbourhood');?> :</div>
                          <div class="child2"><?php echo decrypt_data($patient_data['pt_locality']);?></div>
                          <div class="clear"></div>
                        </li>
                        <li>
                          <div class="child1"><?php load_language('complement');?> :</div>
                          <div class="child2"><?php echo decrypt_data($patient_data['pt_complement']);?></div>
                          <div class="clear"></div>
                        </li>
                       
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
<!-- 
              <div id="special" class="tab-pane fade">
                <div class="ip_profile_tab_detail p0">
                  <div class="ip_profile_tab_top p0">
                    <div class="ip_profile_contato">
                      <h5><strong>What is the reason for your contact?</strong></h5>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                      <div class="clear"></div>
                      <div class="row">
                        <div class="col-md-3">
                          <div class="ip_day_time_schedule_details_data p0">
                               <input id="checkbox-11" class="ip_custom_checkbox1" name="checkbox-11" type="checkbox" checked="">
                               <label for="checkbox-11" class="ip_custom_checkbox_label1">Problems Attendent</label>
                               <div class="clear"></div>
                           </div>
                           <div class="clear"></div>
                        </div>
                        <div class="col-md-3">
                          <div class="ip_day_time_schedule_details_data p0">
                               <input id="checkbox-12" class="ip_custom_checkbox1" name="checkbox-12" type="checkbox" checked="">
                               <label for="checkbox-12" class="ip_custom_checkbox_label1">Difficulty to schedule</label>
                               <div class="clear"></div>
                           </div>
                           <div class="clear"></div>
                        </div>
                        <div class="col-md-3">
                          <div class="ip_day_time_schedule_details_data p0">
                               <input id="checkbox-13" class="ip_custom_checkbox1" name="checkbox-13" type="checkbox" checked="">
                               <label for="checkbox-13" class="ip_custom_checkbox_label1">Problems with payment</label>
                               <div class="clear"></div>
                           </div>
                           <div class="clear"></div>
                        </div>
                        <div class="col-md-3">
                          <div class="ip_day_time_schedule_details_data p0">
                               <input id="checkbox-13" class="ip_custom_checkbox1" name="checkbox-13" type="checkbox" checked="">
                               <label for="checkbox-13" class="ip_custom_checkbox_label1">Others</label>
                               <div class="clear"></div>
                           </div>
                           <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                      </div>
                    </div>
                    </div>
                    <div>
                      <div class="ip_edit_record_detail">
                        <div class="ip_edit_text_bay">
                          <div class="ip_edit_record_text">
                            <ul>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit1.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit2.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit3.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit4.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit5.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit6.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit7.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit8.png"></li>
                              <div class="clear"></div>
                            </ul>
                          </div>
                          <textarea class="ip_edit_record_content_textarea" rows="4"></textarea>
                        </div>
                        <div class="ip_edit_bottom_btn_bay">
                          <button class="ip_edit_save_btn floatRight">SEND</button>
                          <div class="clear"></div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
 -->
             <!--  <div id="photo" class="tab-pane fade">
                <div class="ip_profile_tab_detail">
                  <h3>Photos</h3>
                </div>
              </div> -->

              <div id="more" class="tab-pane fade">
                <div class="ip_profile_tab_detail">
                  <h3 data-toggle="modal" data-target="#pop1">popup1</h3>
                  <h3 data-toggle="modal" data-target="#pop2">popup2</h3>
                  <h3 data-toggle="modal" data-target="#pop3">popup3</h3>
                  <h3 data-toggle="modal" data-target="#pop4">popup4</h3>
                  <h3 data-toggle="modal" data-target="#pop5">popup5</h3>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="ip_grid_cols">
      <div class="row">
        <div class="col-md-4">
          <div class="ip_bio_tab_div">
            <div class="ip_bio_head">
              <?php load_language('notification');?>
              <div class="ip_bio_more">
              </div>
            </div>
            <div class="ip_bio_detail">
              <div class="ip_bio_notification_list">
                <ul>
                  <?php 
                  if(!empty($notifications))
                  {
                    foreach ($notifications as $key => $value) 
                    {
                  ?>
                      <li>
                        <h5><?php echo $value['type_desc'];?>
                        <div class="ip_notification_time"><?php echo change_time_to_local($value['time'])?></div>
                        </h5>
                        <p><?php echo $value['message'];?></p>
                      </li>
                  <?php
                    }
                  }
                  else
                  {
                  ?>
                    <li>
                        <p><?php load_language('no_notification');?>!</p>
                    </li>
                  <?php
                  }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="ip_bio_tab_div">
            <div class="ip_bio_head">
              <?php load_language('messages');?>
              <div class="ip_bio_more">
              </div>
            </div>
            <div class="ip_bio_detail">
              <div class="ip_bio_message_list">
                <ul>
                  <?php if(!empty($recent))
                  {
                    foreach ($recent as $key => $value) 
                    {
                  ?>

                  <li>
                  <a href="<?php echo base_url();?>Patient/chat">
                    <div class="ip_bio_message_pic">
                      <img src="<?php echo base_url(); echo $value['doc_pic']?>">
                    </div>
                    <div class="ip_bio_messages">
                      <h5><?php echo $value['doc_name'];?></h5><div class="ip_message_time"><?php echo change_time_to_local($value['time'])?></div>
                      <div class="clear"></div>
                      <p><?php echo $value['msg'];?></p>
                    </div>
                  <div class="clear"></div>
                  </a>
                  </li>

                  <?php
                    }
                  } ?>
                  
                  
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="ip_bio_tab_div">
            <div class="ip_bio_head">
              <?php load_language('promotions');?>
              <div class="ip_bio_more">
              </div>
            </div>
            <div class="ip_bio_detail textCenter">
              <div class="ip_bio_message_list">
                <ul>
                  <?php if(!empty($promocodes))
                  {
                    foreach ($promocodes as $key => $promo) 
                    {
                  ?>
                    <a href="<?php echo base_url();?>Patient/promocode/<?php echo $promo['promo_id'];?>">
                       <li>
                        <div class="ip_bio_messages width100">
                          <div class="ip_promo_image">
                            <img src="<?php echo base_url();echo $promo['image'];?>">
                          </div>
                          <h5><?php echo $promo['promo_name'];?></h5>
                          <div class="clear"></div>
                          <p><?php echo $promo['description'];?></p>
                        </div>
                        <div class="clear"></div>
                      </li>                      
                    </a>
                     

                  <?php
                      
                    }
                  }
                  else
                  {
                  ?>
                      <li>
                        <div class="ip_bio_messages width100">
                          <h5><?php load_language('no_promotions_available');?></h5>
                          <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                      </li>
                  <?php
                  }

                  ?>
                  

                  <!-- <li>
                    <div class="ip_bio_messages width100">
                      <div class="ip_promo_image">
                      </div>
                      <h5>Nyla Augusta</h5>
                      <div class="clear"></div>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li>
                    <div class="ip_bio_messages width100">
                      <div class="ip_promo_image">
                      </div>
                      <h5>Nyla Augusta</h5>
                      <div class="clear"></div>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li>
                    <div class="ip_bio_messages width100">
                      <div class="ip_promo_image">
                      </div>
                      <h5>Nyla Augusta</h5>
                      <div class="clear"></div>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                    </div>
                    <div class="clear"></div>
                  </li> -->
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="ip_record_main_div">
      <div class="ip_bio_head">
        <?php load_language('consultation');?>
        <div class="ip_record_btn_group floatRight">
          <div class="btn-group ip_custom_tabs_menu">
                <button type="button" class="btn ip_apppointment_btn_custom current "><a href="#tab-1"><?php load_language('consultation_tab');?></a></button>
                <button type="button" class="btn ip_apppointment_btn_custom "><a href="#tab-2"><?php load_language('scheduled_consultation_tab');?></a></button> 
                <button type="button" class="btn ip_apppointment_btn_custom patient_dash_canceled_consult"><a href="#tab-3"><?php load_language('canceled_consultation_tab');?></a></button>
                
          </div>
      </div>
      <div class="clear"></div>
      </div>
      <div class="ip_custom_tab">
      <div id="tab-1"  class="ip_custom_tab_content">
      <ul>
        
         <?php 
          if(!empty($completed_consultation))
          {
           foreach ($completed_consultation as $key => $element) 
           {
          ?>
          <li>
              <div class="row m0 height100">
                <div class="col-md-2 p0 height100"><div class="ip_record_main_head_data"><strong><?php load_language('consultation');?>: </strong><?php echo date('d M Y',$element['book_date']);?></div></div>
                <div class="col-md-2 p0 height100"><div class="ip_record_main_head_data"><?php echo $element['book_time']?></div></div>
                <div class="col-md-3 p0 height100"><div class="ip_record_main_head_data">Dr.<?php echo $element['doc_name']?></div></div>
                <div class="col-md-2 p0 height100">
                  <div class="ip_record_main_head_data">
                   <!--  <form id="ip_user_rating_form">
                      <div id="ip_selected_rating" class="ip_selected_rating floatLeft">5.0</div>
                      <span class="ip_user_rating floatLeft">
                        <input type="radio" name="rating" value="5.0"><span class="star"></span>
                        <input type="radio" name="rating" value="4.0"><span class="star"></span>
                        <input type="radio" name="rating" value="3.0"><span class="star"></span>
                        <input type="radio" name="rating" value="2.0"><span class="star"></span>
                        <input type="radio" name="rating" value="1.0"><span class="star"></span>
                      </span>
                      <div class="clear"></div>
                    </form> -->
                    <button class="ip_reader_btn" ><?php load_language('completed_tab');?></button>
                  </div>
                </div>
                <div class="col-md-3 p0 height100"><div class="ip_record_main_head_data">
                <a href="<?php echo base_url();?>Patient/record/<?php echo $element['book_id'];?>">  <button class="ip_reader_btn"><?php load_language('open_medical_records');?></button></a>
                </div></div>
              </div>
          </li>
          <?php 
            }
          }
          else
          {
          ?>
             <li>
              <div class="row m0 height100">
                <div class="col-md-12 p0 height100"><div class="ip_record_main_head_data"><?php load_language('no_consultations');?></div>
               
              </div>
          </li>
          <?php
          }
          ?>
     
      </ul>
    </div>
    <div id="tab-2"  class="ip_custom_tab_content">
      <ul id="confirmed-schedules-div">
        <?php $this->load->view('patient_dash_scheduled_booking'); ?>
      </ul>
    </div>
    <div id="tab-3"  class="ip_custom_tab_content">
      <ul id="canceled-schedules-div">
        <?php $this->load->view('patient_dash_canceled_booking'); ?>
      </ul>
    </div>
    </div>
    </div>
  </div>

  </div>
</div>

<!-- REMOVED RESCHEDULING PART -->
<!-- SET-FOUR-SCREEN-SEVEN -->
<!-- 
<div id="pop1" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="ip_custom_modal">
      <div class="ip_custom_modal_head">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        Rescheduling
      </div>
      <div class="ip_custom_modal_content">
        <div class="ip_main_tab_content">
              <div class="ip_main_tab_pic">
              </div>
              <h5><strong>Ann Alexander</strong></h5>
              <p>Cardiologist</p>
              <div class="ip_profile_ratting">
                <fieldset class="ip_rating">
                  <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                  <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                  <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                  <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                  <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                  <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                  <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                  <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                  <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                  <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                </fieldset>
                <div class="clear"></div>
              </div>
              <h4>14th december 2017</h4>
              <h6>16:00 hours</h6>
              <div class="ip_profile_datetime">
                <input class="ip_calender floatLeft" placeholder="" id="ip_datepicker">
                <input class="ip_time floatRight" placeholder="" id="ip_timepicker">
               
                <div class="clear"></div>
              </div>
          </div>
          <div class="ip_custom_modal_btn_bay">
              <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                  <button class="ip_custom_modal_btm_btn1">SCHE: APPOINTMENT</button>
                </div>
                <div class="col-md-2">
                </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
 -->

<!-- SET-FOUR-SCREEN-EIGHT -->

<div id="pop2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="ip_custom_modal">
      <div class="ip_custom_modal_head">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <?php load_language('cancelation');?>
      </div>
      <div class="ip_custom_modal_content">
        <div class="ip_main_tab_content">
              <div class="ip_main_tab_pic">
                <img id="cancel-consult-modal-pic" src="">
              </div>
              <h5 ><strong id="cancel-consult-modal-name"></strong></h5>
              <p id="cancel-consult-modal-spec"></p>

              <h4 id="cancel-consult-modal-date" ></h4>
              <h6 id="cancel-consult-modal-time"></h6>
          </div>
      </div>
      <hr>
      <div class="ip_custom_modal_btn_bay">
        <p>
          <?php echo $policy['cancelation_policy'];?>
        </p>
          <div class="row">
            <div class="col-md-8">
              <button id="cancel-consult-modal-btn"  class="ip_custom_modal_btm_btn2">CANCEL CONSULTATION</button>
            </div>
            <div class="col-md-4">
              <button class="ip_custom_modal_btm_btn3" type="button"  data-dismiss="modal">BACK</button>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>


<!-- SET-FOUR-SCREEN-NINE -->

<div id="pop3" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Warning</h4>
      </div>
      <div class="modal-body">
        <p>The Consulation cannot be cancelled.Check Cancellation Policy!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<!-- SET-FOUT-SCREEN-ELEVEN -->

<div id="pop4" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="ip_custom_modal_wrapper">
      <div class="ip_custom_modal_head1">
        Consultation Advisory
      </div>
    <div class="ip_custom_modal_outter">
    <div class="ip_custom_modal">

      <div class="ip_custom_modal_head">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        Rescheduling
      </div>
      <div class="ip_custom_modal_content">
        <div class="ip_main_tab_content">
              <div class="ip_main_tab_pic">
                <img id="reschedule-consult-pic">
              </div>
              <h5><strong id="reschedule-consult-name"></strong></h5>
              <p id="reschedule-consult-spec"></p>
              <div class="ip_profile_ratting">
                <fieldset class="ip_rating">
                  <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                  <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                  <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                  <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                  <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                  <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                  <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                  <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                  <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                  <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                </fieldset>
                <div class="clear"></div>
              </div>
              <h4 id="reschedule-consult-date"></h4>
              <h6 id="reschedule-consult-time"></h6>
              <div class="ip_profile_datetime">
                  <form role="form" id="reschedule_book_form" data-parsley-validate="">
                <input type="hidden" name="reschedule-book-id"  id="reschedule_book_id" >
                <input type="hidden" name="confirm-book-clinic"  id="reschedule_book_clinic" >
                <input type="hidden" name="confirm-book-doctor" id="reschedule_book_doctor" >
                <div id="sandbox-container"><input class="ip_calender floatLeft reschedule_book_date_cus" placeholder="" data-parsley-required="" name="confirm-book-date" id="reschedule_book_date">
                </div>
                
                <!-- <input class="ip_time floatRight" placeholder="" id="ip_timepicker"> -->
                 <select id="reschedule-consult-timeslot" data-parsley-required="" name="confirm-book-time" class="ip_time floatRight">
                  <option disabled selected>Time</option>
                </select>
                <div class="clear"></div>
              </div>
            </form>
          </div>
          <div class="ip_custom_modal_btn_bay">
              <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                  <button id="reschedule-consult-btn" type="button" class="ip_custom_modal_btm_btn1">SCHE:APPOINTMENT</button>

                </div>
                <div class="col-md-2">
                </div>
              </div>
          </div>
          <div id="err_reschedule_booking" class="alert alert-danger ip_profile_reschedule_error hidden">DOCCTOE SNOR ASIAIJNCSMSS</div>
      </div>
    </div>
    </div>
  </div>
</div>
</div>

<!-- SET-FIVE-SCREEN-FIVE -->

<div id="pop5" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="ip_custom_modal">
      <div class="ip_custom_modal_head">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        Delay
      </div>
      <div class="ip_custom_modal_content">
        <div class="ip_main_tab_content">
              <p>Notify me if delayed</p>
              <div class="ip_notify_time">
                <li>
                  <div class="ip_day_time_schedule_details_data p0">
                       <input id="checkbox-41" class="ip_custom_checkbox1" name="checkbox-41" type="checkbox" checked="">
                       <label for="checkbox-41" class="ip_custom_checkbox_label1">15 minutes</label>
                       <div class="clear"></div>
                   </div>
                </li>
                <li>
                  <div class="ip_day_time_schedule_details_data p0">
                       <input id="checkbox-42" class="ip_custom_checkbox1" name="checkbox-42" type="checkbox" checked="">
                       <label for="checkbox-42" class="ip_custom_checkbox_label1">30 minutes</label>
                       <div class="clear"></div>
                   </div>
                </li>
                <li>
                  <div class="ip_day_time_schedule_details_data p0">
                       <input id="checkbox-43" class="ip_custom_checkbox1" name="checkbox-43" type="checkbox" checked="">
                       <label for="checkbox-43" class="ip_custom_checkbox_label1">01 hour</label>
                       <div class="clear"></div>
                   </div>
                </li>
                <li>
                  <div class="ip_day_time_schedule_details_data p0">
                       <input id="checkbox-44" class="ip_custom_checkbox1" name="checkbox-44" type="checkbox" checked="">
                       <label for="checkbox-44" class="ip_custom_checkbox_label1">01 hr 30 min</label>
                       <div class="clear"></div>
                   </div>
                </li>
                <div class="clearfix"></div>
              </div>
          </div>
      </div>
      <hr>
      <div class="ip_custom_modal_btn_bay">
        <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's scimen book
        </p>
          <div class="row">
            <div class="col-md-8">
              <button class="ip_custom_modal_btm_btn2">NOTIFY</button>
            </div>
            <div class="col-md-4">
              <button class="ip_custom_modal_btm_btn3">BACK</button>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>

<!-- DELETE-POP-UP -->

<div id="delete" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
      <div class="ip_patient_delete_pop_wrapper">
        <div class="ip_paitent_delete_header">
          <?php load_language('delete_user_account');?>
        </div>
        <div class="ip_patient_delete_content">
          <div class="ip_delete_pic_circle">
            <img src="<?php echo base_url();?>/assets/images/ip_delete_user_pic.png">
          </div>
          <h5><?php load_language('delete_user_desc');?></h5>
          <hr>
          <p><?php load_language('delete_user_message');?></p>
          <form data-parsley-validate="" role="form" id="pat_profile_delete">
            <div class="ip_patient_delete_form">
              <div class="ip_patient_delete_row">
                <input class="ip_patient_input ip_paitent_delete_user reset-form" data-parsley-required="" name="username" placeholder="<?php load_language('login');?>" onKeyPress="if(this.value.length > 25) return false;">
              </div>
              <div class="ip_patient_delete_row">
                <input class="ip_patient_input ip_paitent_delete_pass reset-form" data-parsley-required="" name="password" placeholder="<?php load_language('password');?>" onKeyPress="if(this.value.length > 25) return false;" type="Password">
              </div>
              <div id="pat_profile_delete_error" class="alert alert-danger hidden">
            </div>
             
              <div class="ip_patient_delete_row">
                <button type="button" id="pat_del_check_login_sub" class="ip_paitent_dark_btn floatLeft"><?php load_language('accept');?></button>
                <button class="ip_paitent_delete_btn floatRight" data-dismiss="modal"><?php load_language('cancel');?></button>
                <div class="clear"></div>
              </div>
            </div>
          </form>
        </div>
      </div>
  </div>
</div>


<!-- DELETE-CONFIRMATION POP-UP -->

<div id="delete-con" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
      <div class="ip_patient_delete_pop_wrapper">
        <div class="ip_paitent_delete_header">
          <?php load_language('delete_user_account');?>
        </div>
        <div class="ip_patient_delete_content">
          <div class="ip_delete_pic_circle">
            <img src="<?php echo base_url();?>/assets/images/ip_delete_user_pic.png">
          </div>
          <h5><?php load_language('delete_user_desc');?></h5>
          <hr>
          <p><?php load_language('delete_user_otp_desc');?>.<br>
          <?php load_language('enter_confirm_code');?>.</p>
          <div class="ip_patient_delete_form">
            <form data-parsley-validate="" role="form" id="pat_profile_delete_confirmation">
           <!--  <div class="ip_patient_delete_row">
              <input class="ip_patient_input ip_paitent_delete_user" placeholder="Login">
            </div>
            <div class="ip_patient_delete_row">
              <input class="ip_patient_input ip_paitent_delete_pass" placeholder="Password">
            </div> -->
            <div class="ip_patient_delete_row">
              <input class="ip_patient_input ip_paitent_delete_pass uppercase reset-form" name="confirmation_code" data-parsley-minlength="8" data-parsley-required="" data-parsley-maxlength="8" onKeyPress="if(this.value.length > 7) return false;" placeholder="<?php load_language('confirmation_code');?>">
            </div>
            <div id="pat_profile_delete_code_error" class="alert alert-danger hidden"></div>
            <div class="ip_patient_delete_row">
              <button type="button" class="ip_paitent_dark_btn floatLeft" id="pat_del_check_code_sub"><?php load_language('accept');?></button>
              <button class="ip_paitent_delete_btn floatRight" data-dismiss="modal"><?php load_language('cancel');?></button>
              <div class="clear"></div>
            </div>
          </form>
          </div>
        </div>
      </div>
  </div>
</div>


<!-- DELETED POP-UP -->

<div id="delete-complete" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
      <div class="ip_patient_delete_pop_wrapper">
        <div class="ip_paitent_delete_header">
         <?php load_language('delete_user_account');?>
        </div>
        <div class="ip_patient_delete_content">
          <div class="ip_delete_pic_circle">
            <img src="<?php echo base_url();?>/assets/images/ip_delete_user_pic.png">
          </div>
          <h5><?php load_language('account_deleted');?></h5>
          <hr>
           <div class="ip_patient_delete_row textCenter">
             <a href="javascript:void(0)">
                <button type="button" class="ip_paitent_dark_btn" data-dismiss="modal"><?php load_language('log_out');?></button>
             </a>
              <div class="clear"></div>
            </div>
         
        </div>
      </div>
  </div>
</div>



