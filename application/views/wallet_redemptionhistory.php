

<div class="container">
  <div class="row">
    <div class="col-md-8">
      <div class="ip_edit_record_cover">
  <div class="ip_edit_record_head">
    <?php load_language('redemption_history');?>
  </div>
  <div>
    <hr>
    <div class="ip_edit_record_detail">
      <div class="ip_edit_row">
        <div class="ip_budject_list">
          <?php if(!empty($redemptiondata))
          {
            foreach ($redemptiondata as $key => $value) 
            {
          ?>
              <li>
                <h6 class="m0"><?php echo date('d-m-Y',$value['date']);?></h6>
                <div class="child1"><h5><strong><?php echo decrypt_data($value['account_holder']);?> - <?php echo $value['bank_name'];?> <?php echo decrypt_data($value['account_no']);?></strong></h5>
                  <p class="select"><?php echo $value['status'];?></p>
                </div>
                <div class="child2"><strong><b>R$ <?php echo $value['amount'];?></b></strong></div>
                <div class="clear"></div>
              </li>
              <hr>
          <?php
            }
          } 
          else
          {
          ?>
            <li>
              
                <div class="child1"><h5><?php load_language('no_redemption_request');?></h5>
                </div>
                <div class="clear"></div>
              </li>
              <hr>
          <?php
          }
          ?>
         

        </div>
      </div>
      </div>
    </div>
</div>
    </div>
    <div class="col-md-4">
      
    </div>
    
  </div>
  
</div>

