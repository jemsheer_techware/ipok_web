<div class="ip_bank_detail">
  <?php 
 // print_r($banks);
  if(!empty($banks))
  {
    foreach ($banks as $key => $value) 
    {
  ?>
    <li>
      <div class="ip_bank_left">
        <h6><?php echo $value['bank_name'];?> <?php echo decrypt_data($value['account_no']);?> <?php echo $value['agency'];?></h6>
      </div>
      <div class="ip_bank_close delete-bank-btn" bankid="<?php echo $value['id'];?>">
      </div>
      <div class="clear"></div>
    </li>
  <?php
    }
  }
  else
  {
  ?>
     <li>
      <div class="ip_bank_left">
        <h6><?php load_language('you_have_not_added_any_banks');?>!</h6>
      </div>
      <div class="clear"></div>
    </li>
  <?php
  }
  ?>
</div>