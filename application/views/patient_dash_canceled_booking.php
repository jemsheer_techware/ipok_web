      <?php
      if(!empty($canceled_consultation))
      {
        foreach ($canceled_consultation as $key => $element) 
        {
      ?>
           
        <li>
            <div class="row m0 height100">
              <div class="col-md-3 p0 height100"><div class="ip_record_main_head_data"><strong><?php load_language('consultation');?>: </strong><?php echo date('d M Y',$element['book_date']);?></div></div>
              <div class="col-md-3 p0 height100"><div class="ip_record_main_head_data"><?php echo $element['book_time']?></div></div>
              <div class="col-md-4 p0 height100"><div class="ip_record_main_head_data">Dr.<?php echo $element['doc_name']?></div></div>
              <!-- onclick="change_consult(<?php echo $element['book_id']?>)" -->
            </div>
        </li>
      <?php 
        }
      }
      else
      {
      ?>
        <li>
            <div class="row m0 height100">
              <div class="col-md-12 p0 height100"><div class="ip_record_main_head_data"><?php load_language('no_canceled_consultations');?></div></div>
            </div>
        </li>
      <?php
      }
      ?>