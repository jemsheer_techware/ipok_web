

<div class="ip_table_head">
              <ul>
                  <li class="time_slot"></li>
                  <?php 
                  if(!empty($start_day)) 
                  {
                    for ($i=0; $i < 7; $i++) 
                    { 
                      $day = date('D',strtotime('+'.$i.'day', strtotime($start_day)));
                      $dayno = date('d',strtotime('+'.$i.'day', strtotime($start_day)));
                      $date = date('Y-m-d',strtotime('+'.$i.'day', strtotime($start_day)))
                  ?>
                      <li id="appoint-week-view-day<?php echo$i;?>" data-date="<?php echo$date;?>" data-docid="<?php echo $doctorid?>"><?php echo $day.','. $dayno;?></li>
                  <?php
                    }
                  }
                  else
                  {

                    for ($i=0; $i < 7; $i++) 
                    { 
                      $day = date('D',strtotime('+'.$i.'day'));
                      $dayno = date('d',strtotime('+'.$i.'day'));
                      $date = date('Y-m-d',strtotime('+'.$i.'day'))
                  ?>
                      <li id="appoint-week-view-day<?php echo$i;?>" data-date="<?php echo$date;?>" data-docid="<?php echo $doctor_data['doctorid']?>"><?php echo $day.','. $dayno;?></li>
                  <?php
                    }
                        
                
                  }
                        
                  ?>
                  <div class="clear"></div>
              </ul>
            </div>
            <div class="ip_table_head_divide">
              <ul>
                  <li class="time_slot"></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li class="borderrightnone"></li>
                  <div class="clear"></div>
              </ul>
            </div>
            <div class="ip_table_days">
              <ul>
                  <li class="time_slot">
                    <div class="ip_time_interval">
                      <ul>
                         <?php for ($i=1; $i <=24 ; $i++) { 
                              ?>
                              <li><p><?php echo $i;?>:00</p></li>
                              <?php
                              }?>
                      </ul>
                    </div>
                  </li>

                   
                  <?php foreach ($week_appointments as $key => $value) 
                  { 

                  ?>
                  <li>
                    <div class="ip_time_avialability">
                        <ul>
                              <?php for ($i=0; $i <24 ; $i++) { ?>
                              <li>
                                <div class="ip_avialability ">
                                  <?php
                                  foreach ($value as $key_inner => $value_inner) 
                                  {
                                  if($value_inner['hour']==$i)
                                  {
                                  ?>
                                  <div class="ip_avialable capitalize">
                                  <?php echo $value_inner['count'];?> <?php load_language('appointments');?>
                                  </div>
                                  <?php
                                    }
                                  }
                                  ?>
                                </div>
                              </li>
                              <?php
                              }
                              ?>
                        </ul>
                    </div>
                  </li>

                  <?php
                  }
                  ?>
                 
                  <div class="clear"></div>
              </ul>
            </div>
