
	  <div class="modal-dialog" style=" box-shadow: 0px 0px 5px 0px #888888;border-top-right-radius: 8px;border-top-left-radius: 8px;" id="forgot-pass-div">
	      <div class="ip_patient_delete_pop_wrapper">
	        <div class="ip_paitent_delete_header uppercase">
	          <?php load_language('forgot_password');?>
	        </div>
	        <div class="ip_patient_delete_content">
	          <h5><?php load_language('want_to_change_password');?>?</h5>
	          <hr>
	          <p><?php load_language('please_fill_credentials');?></p>
	          <div class="ip_patient_delete_form">
	          	<form role="form" id="reset-password-form" data-parsley-validate="" >
		            <div class="ip_patient_delete_row">
		            	<input type="hidden" name="id" value="<?php echo $id;?>">
		            	<input type="hidden" name="type" value="<?php echo $type;?>">
		              <input class="ip_patient_input ip_paitent_delete_pass" placeholder="<?php load_language('new_password');?>" name="password" data-parsley-minlength="8" data-parsley-required="true" onKeyPress="if(this.value.length > 25) return false;" type="Password" id="reset-form-pass">
		            </div>
		            <div class="ip_patient_delete_row">
		              <input class="ip_patient_input ip_paitent_delete_pass" placeholder="<?php load_language('confirm_new_password');?>" data-parsley-equalto="#reset-form-pass" data-parsley-required type="Password" onKeyPress="if(this.value.length > 25) return false;">
		            </div>
		            <div class="ip_patient_delete_row">
		              <a href="javascript:void(0)">
		              	<button class="ip_paitent_dark_btn floatLeft uppercase" type="button" id="reset-password-save-btn"><?php load_language('save');?></button>
		              </a>
		              <a href="<?php echo base_url();?>">
		              	<button class="ip_paitent_delete_btn floatRight uppercase"><?php load_language('cancel');?></button>
		              </a>
		              
		              <div class="clear"></div>
		            </div>
	            </form>
	          </div>
	        </div>
	      </div>
	  </div>

	  <div class="ip_set_two_wrapper hidden" id="forgot-pass-success">

	<div  class="">
	  <div class="modal-dialog" style=" box-shadow: 0px 0px 5px 0px #888888;border-top-right-radius: 8px;border-top-left-radius: 8px;">
	      <div class="ip_patient_delete_pop_wrapper">
	        <div class="ip_paitent_delete_header uppercase">
	          <?php load_language('success');?>
	        </div>
	        <div class="ip_patient_delete_content">
	        	<div class="textCenter">
	        		<img style="width: 20%;" src="<?php echo base_url();?>assets/images/tick.png">
	        	</div>
	          <h5 class="uppercase"><?php load_language('success');?> !</h5>
	          <hr>
	          <p><?php load_language('password_change_success');?></p>
	          <div class="ip_patient_delete_form">
	            <div class="ip_patient_delete_row textCenter">
	              <a href="<?php echo base_url();?>">
	              	<button class="ip_paitent_dark_btn uppercase" type="button"><?php load_language('continue');?></button>
	              </a>
	              <div class="clear"></div>
	            </div>
	          </div>
	        </div>
	      </div>
	  </div>
	</div>

</div>

	