
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">

      <?php
        if($this->session->flashdata('message')) {
        $message = $this->session->flashdata('message');
      ?>
        <div class="alert alert-<?php echo $message['class']; ?> alert-dismissible flash-msg">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><strong> <?php echo $message['title']; ?></strong></h4>
        <?php echo $message['message']; ?>
        </div>

      <?php
        }
      ?>

    <div class="ip_edit_record_wrapper">
    
       <div class="row">
        <div class="col-md-12">
          <div class="ip_edit_record_cover">
            <form role="form" data-parsley-validate="" id="pat-edt-depend-form" method="POST" action="<?php echo base_url();?>Patient/updateDependent" enctype="multipart/form-data">
            <div class="ip_edit_record_head backgroundnone">
              <?php load_language('edit_dependent');?>
            </div>
              <div class="ip_edit_record_detail">
                <div class="ip_edit_row">
                  <div class="ip_bank_detail_frame">
                      <input name="dependent_name" class="ip_bank_input" data-parsley-required="true" data-parsley-minlength="5" onKeyPress="if(this.value.length > 40) return false;" data-parsley-maxlength="40" data-parsley-pattern="^[a-zA-Z ]+$" type="text" placeholder="<?php load_language('name');?>" value="<?php echo $dependent_detail['dependent_name']?>">
                      <input type="hidden" name="dependent_id" value="<?php echo $dependent_detail['id']?>">
                  </div>
                </div>
                <div class="ip_edit_row">
                  <div class="row">
                    <div class="col-md-3">
                      <p class="ip_row_p"><?php load_language('date_of_birth');?></p>
                      <div class="ip_bank_detail_frame" id="edit-patient-dependent"> 
                          <input name="dob" id="pat-edt-dependent-dob" readonly data-parsley-required="true" class="ip_bank_input" placeholder="">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <p class="ip_row_p"><?php load_language('family_bond');?></p>
                      <div class="ip_bank_detail_frame">
                          <input name="relation" data-parsley-required="true" data-parsley-minlength="5" data-parsley-maxlength="25" onKeyPress="if(this.value.length > 25) return false;" class="ip_bank_input" placeholder="" data-parsley-pattern="^[a-zA-Z ]+$" value="<?php echo $dependent_detail['relation']?>">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <p class="ip_row_p">CPF</p>
                      <div class="ip_bank_detail_frame">
                          <input class="ip_bank_input" placeholder="" name="cpf"
                            data-parsley-required="true" data-parsley-minlength="11" data-parsley-cpf="" onKeyPress="if(this.value.length > 10) return false;" value="<?php echo $dependent_detail['cpf']?>"  type="number">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <p class="ip_row_p"><?php load_language('add_photo');?>:</p>
                       <div class="ip_reg_modal_addphoto floatLeft">
                        <img  id="pat-edit-depend-pic" src="<?php echo base_url(); echo $dependent_detail['image']?>">
                      </div>

                      <div class="ip_add_photo floatRight">
                        <input name="image"  type="file" accept="image/*"   class="ip_reg_form_input"
                      data-parsley-error-message="<?php load_language('profile_photo_error_text');?>" onchange="pat_add_depend_loadthumbnail(this,'pat-edit-depend-pic')" >
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <div class="ip_coloborator_btn_bay">
                <button class="ip_colaborator_btn"><?php load_language('update_and_save');?></button>
              </div>
            </form>
          </div>
        </div>
      </div> 
    </div>
  </div>
</div>


