<?php if($this->session->userdata('UserData'))
    {$userdata = $this->session->userdata('UserData');}

     if(auto_logout("user_time"))
    {
        $this->session->set_userdata('user_time', time());
        if($this->session->userdata('UserData'))
        {
            $this->session->set_userdata('logout', 'autologoff');
      redirect(base_url().'Home/logout');
    }
    }   
?>
<style>
.ip_main_wrapper{
  margin-top:80px !important;
}
.ip_home_banner{
  height:400px;
}
.ip_home_banner_inner{
      background: rgba(82,222,238,0.8);
}
</style>
  <div class="ip_main_wrapper" >
    <nav class="navbar  navbar-fixed-top">



	 <div class="ip_header_secondary ip_secondary_head_white">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
				<a href="<?php echo base_url();?>"><div class="ip_logo" href="http://getbootstrap.com/examples/starter-template/#"><img src="../assets/images/ip_logo1.png"></div></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
					<ul class="nav navbar-nav ip_navbar_nav">
            <?php if(!empty($userdata))
            {
            ?>
            <li class="active"><a  class="uppercase" href="<?php echo base_url()?>Home/Dashboard"><?php load_language('home');?></a></li>
            <?php }else{
            ?>
            <li class="active"><a class="uppercase" href="<?php echo base_url()?>"><?php load_language('home');?></a></li>
            <?php
            }
            ?>
            
            <!-- <li><a  class="uppercase" href="<?php echo base_url()?>Home/about"><?php load_language('about');?></a>
            </li> -->
           <!--  <?php 
                    if(!$this->session->userdata('UserData'))
                { 
              ?>
            <li data-toggle="modal" data-target="#choose"><a><?php load_language('register_consulting');?></a></li>
            <?php
                }
              ?> -->
              <li data-toggle="modal" data-target="#"><a><?php load_language('support');?></a></li>
            <li><a class="uppercase" href="<?php echo base_url()?>Home/contact"><?php load_language('contact_us');?></a></li>
          </ul>

          <ul class="nav navbar-right ip_nav_bar_right">
          <div class="ip_right_nav_home">
            <?php 
              if($this->session->userdata('UserData'))
          { 
        ?>
          <!-- <li class="logout-btn"><a href="<?php echo base_url()?>Home/logout"><?php $UserData = $this->session->userdata('UserData'); echo $UserData['name'] ?> , <?php load_language('log_out');?></a></li>  -->

          <li class="ip_account_nav_div dropdown">
              <?php $UserData = $this->session->userdata('UserData'); 
              ?>
              <div class="dropdown-toggle" data-toggle="dropdown">
                <span class="floatLeft">
                <div class="ip_nav_circle">
                  <img src="<?php echo base_url();?><?php echo $UserData['profile_photo'];?>">
                </div>
              </span>
              <span class="floatLeft">
                <a><?php echo $UserData['name'];?></a>
              </span>
              </div>
              <ul class="dropdown-menu ip_nav_profile_listing">
                      <div class="ip_arrow_up"></div>
                      <li><a href="<?php echo base_url()?>Patient""> My Account</a></li>
                      <li class="bordernone"><a href="<?php echo base_url()?>Home/logout"><?php load_language('log_out');?></a></li>
                    </ul>
            </li>
        <?php
          }
          else
          {
        ?>
          <!-- <li class="open-loginmodel"><a href="javascript:void(0)"><?php load_language('log_in');?></a></li> -->
          <li class="open-loginmodel">
            <a href="javascript:void(0)"><?php load_language('log_in');?></a>
            <span><a>|</a></span>
            <a href="javascript:void(0)"><?php load_language('registers');?></a>
          </li>
        <?php
          }
            ?>
            
            <li class="ip_nav_download_btn">
              <div class="ip_nav_download_btn_inner">
                <a><?php load_language('download_app');?></a>
              </div>
            </li>

            <?php 
        $langVal=$this->session->userdata('language');
      ?> 
           <li class="">
              <select class="nav_select" onchange="langChange(this)">
                <option value="en" <?php echo ($langVal == 'en') ? "selected" : "";?>>EN</option>
                <option value="pr" <?php echo ($langVal == 'pr') ? "selected" : "";?>>PR</option>
              </select>
            </li>

<div class="clear"></div>

            
          </div>
          </ul>

				</div>
			</div>
		</div>
    </nav>
    <div class="ip_home_banner">
      <div class="ip_home_banner_inner">
        <div class="container">

        
        </div>
      </div>
      <img src="<?php echo base_url()?>assets/images/ip_banner.jpg">
    </div>
    <div class="ip_home_main_function textCenter">
      <div class="container">
        <h3>About Us</h3>
        <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
        <hr>
      </div>
    </div>
    <div class="ip_about_detail_grid">
      <div class="container">
        <div class="col-md-6">
          <h3>it is a long established fact that a reader will be distracted</h3>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
           Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
         when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
           Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
         when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
           Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
         when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
           Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
         when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
        <div class="col-md-6">
          <div class="ip_about_us_image_box floatRight">
            <img src="<?php echo base_url()?>assets/images/ip_banner.jpg">
          </div>
          <div class="clear"></div>
        </div>
      </div>
    </div>
    <div class="ip_about_detail_grid_dark">
      <div class="container">
        <div class="col-md-6">
          <h3>Lorem Ipsum is simply dummy </h3>
          <p> Lorem Ipsum has been the industry's standard dummy text ever since the<br> 1500s,</p>
          <h4>It is a long established</h4>
          <p> Lorem Ipsum has been the industry's standard dummy <br>
          text ever since the 1500s,</p>
          <h4>It is a long established</h4>
          <p> Lorem Ipsum has been the <br>industry's standard dummy <br>
          text ever since the 1500s,</p>
           <h4>It is a long established</h4>
          <p> Lorem Ipsum<br> has been the <br>industry's standard dummy <br>
          text ever since the 1500s,</p>
        </div>
        <div class="col-md-6">
          <h3>Lorem Ipsum is simply dummy </h3>
          <p> Lorem Ipsum has been the industry's standard dummy text ever since the<br> 1500s,</p>
          <h4>It is a long established</h4>
          <p> Lorem Ipsum has been the industry's standard dummy <br>
          text ever since the 1500s,</p>
          <h4>It is a long established</h4>
          <p> Lreadable content of a page when looking at its layout. 
          The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,
           as opposed to using 'Content here, content here', making it look like readable English.</p>
           <h4>It is a long established</h4>
          <p> Lreadable content of a page when looking at its layout. 
          The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,
           as opposed to using 'Content here, content here', making it look like readable English.</p>
        </div>
      </div>
    </div>

    <div class="ip_home_numbers textCenter">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <h5><strong>550</strong><br>
              CLIENTS
            </h5>
            <p>Lorem Ipsum is simply <br>dummy text of the <br>printing and </p>
          </div>
          <div class="col-md-3">
            <h5><strong>850</strong><br>
              PRODUCTS
            </h5>
            <p>Lorem Ipsum is simply <br>dummy text of the <br>printing and </p>
          </div>
          <div class="col-md-3">
            <h5><strong>90K</strong><br>
              SALES
            </h5>
            <p>Lorem Ipsum is simply <br>dummy text of the <br>printing and </p>
          </div>
          <div class="col-md-3">
            <h5><strong>20</strong><br>
              OFFICES
            </h5>
            <p>Lorem Ipsum is simply <br>dummy text of the <br>printing and </p>
          </div>
        </div>
      </div>
    </div>
</div>



     <!--LOGIN MODEL BEGINS-->
    <div id="login" class="modal fade" role="dialog">
    <div class="modal-dialog ip_login_modal">
      <div class="modal-content ip_login_modal_content">
        <div class="ip_login_form">
          <div class="ip_login_logo">
            <img src="<?php echo base_url();?>assets/images/ip_logo1.png">
          </div>
          <hr>
          <form role="form" data-parsley-validate="" id="login-form">
          <div class="ip_login_input_form">
            <div class="ip_login_input_row">
              <div>
               <input id="a" class="ip_custom_checkbox1 ip_gender_check_checkbox " type="radio" data-parsley-required data-parsley-error-message="Choose Login Type" class="" name="login_type" value="DOCTOR">
               <label for="a" class="ip_custom_checkbox_label1 ip_doc_paitent ip_gender_check_label"><?php load_language('doctor');?></label>

               <input id="b" class="ip_custom_checkbox1 ip_gender_check_checkbox " type="radio" name="login_type" value="PATIENT">
               <label for="b" class="ip_custom_checkbox_label1 ip_doc_paitent ip_gender_check_label"><?php load_language('patient');?></label>

               <input id="c" class="ip_custom_checkbox1 ip_gender_check_checkbox " type="radio" name="login_type" value="COLLABORATOR">
               <label for="c" class="ip_custom_checkbox_label1 ip_doc_paitent ip_gender_check_label"><?php load_language('collaborator');?></label>
              <div class="clear"></div>
            </div>
            </div>
            <div class="ip_login_input_row">
              <input name="login-form-username" data-parsley-required class="ip_login_input ip_login_user clear-login-data" onKeyPress="if(this.value.length > 25) return false;"  placeholder="<?php load_language('login');?>">

            </div>
            <div class="ip_login_input_row">
              <input name="login-form-password" data-parsley-required class="ip_login_input ip_login_pass clear-login-data" onKeyPress="if(this.value.length > 25) return false;" placeholder="<?php load_language('password');?>" type="password">
            </div>
            <div class="">
              <a href="javascript:void(0)">
              <button type="button" class="ip_login_modal_signin floatLeft uppercase" id="login_submit"><?php load_language('login');?></button>  
              </a>

              <p class="floatLeft" id="forgot_password_btn"><?php load_language('forgot_password');?></p>
              <div class="clear"></div>
            </div>
          </div>
        </form>
        <div id="err-login" class="alert alert-danger hidden textCenter"> </div>
        <div class="alert alert-success hidden" id="pat-reg-success">
          <strong><?php load_language('alert_success');?></strong><?php load_language('patient_account_register_desc');?>
        </div>
          <hr>
          <div class="ip_login_input_form">
            <div class="textCenter">
              <p><?php load_language('not_registered');?><a href="<?php echo base_url();?>"><?php load_language('register_now');?></a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div id="forgot" class="modal fade" role="dialog">
    <div class="modal-dialog ip_login_modal">
      <div class="modal-content ip_login_modal_content">
        <div class="ip_login_form">
          <div class="ip_login_logo">
            <img src="<?php echo base_url();?>assets/images/ip_logo1.png">
          </div>
          <hr>
          <div class="ip_login_input_form">
          <form role="form" data-parsley-validate="" id="forgot-pass-form">
            <p class="pl0"><strong><?php load_language('forgot_password');?></strong></p>
            <div class="ip_login_input_row">
              <div>
                 <input id="forgot-doc" class="ip_custom_checkbox1 ip_gender_check_checkbox " type="radio" data-parsley-required data-parsley-error-message="<?php load_language('choose_type');?>" class="" name="type" value="DOCTOR">
                 <label for="forgot-doc" class="ip_custom_checkbox_label1 ip_doc_paitent ip_gender_check_label"><?php load_language('doctor');?></label>

                 <input id="forgot-pat" class="ip_custom_checkbox1 ip_gender_check_checkbox " type="radio" name="type" value="PATIENT">
                 <label for="forgot-pat" class="ip_custom_checkbox_label1 ip_doc_paitent ip_gender_check_label"><?php load_language('patient');?></label>
                <div class="clear"></div>
              </div>
            </div>
            <div class="ip_login_input_row">
              <input class="ip_login_input ip_login_msg" onKeyPress="if(this.value.length > 75) return false;" name="email"  data-parsley-required="true" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="<?php load_language('enter_mail_id');?>">
            </div>
            <div class="textCenter">
              <button type="button" class="ip_login_modal_signin" id="forgot_password_sent_btn"><?php load_language('sent_mail');?></button>
            </div>
          </form>
          </div>
          <div id="forgot-pass-error" class="hidden alert alert-danger textCenter"></div>
          <hr>
          <div class="ip_login_input_form">
            <div class="textCenter">
              <p><?php load_language('forgot_password_desc');?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
  <div id="verify" class="modal fade" role="dialog">
    <div class="modal-dialog ip_login_modal">
      <div class="modal-content ip_login_modal_content">
        <div class="ip_login_form">
          <div class="ip_login_logo">
            <img src="<?php echo base_url();?>assets/images/ip_logo1.png">
          </div>
          <hr>
          <div class="ip_login_input_form">
            <div class="">
              <p class="textCenter">
              <img src="<?php echo base_url();?>assets/images/ip_verify_mail.png">
              <a><strong><?php load_language('check_your_email');?></strong></a><br>
<?php load_language('check_your_email');?><?php load_language('check_your_email_desc');?><br><?php load_language('password_recovery');?></p>
              <div class="clear"></div>
            </div>
          </div>
          <hr>
        </div>
      </div>
    </div>
  </div>

    <!--LOGIN MODEL ENDS-->
  