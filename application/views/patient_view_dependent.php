
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">
    <div class="ip_edit_record_wrapper">
      <div class="row">
        
        <?php
        if(!empty($dependent_data))
          {
            foreach ($dependent_data as $key => $element) 
            {
        ?>
               <div class="col-md-6">
                <div class="ip_edit_record_cover">
                  <a href="<?php echo base_url();?>Patient/editDependent/<?php echo$element['id']?>">
                  <div class="ip_edit_record_detail" >
                    <div class="ip_add_dependent_photo">
                      <img src="<?php echo base_url();echo$element['image'];?> ">
                    </div>
                    <div class="ip_add_dependent_detail">
                      <h5><?php echo$element['dependent_name'];?></h5>
                      <p><?php echo$element['age'];?> Years / <?php echo$element['relation'];?></p>
                    </div>
                    <div class="clear"></div>
                  </div>
                </a>
                  <hr>
                  <div class="ip_edit_record_detail">
                  </div>
                </div>
              </div>
        <?php
            }
          }
        ?>
       

      </div>
    </div>
  </div>
</div>

