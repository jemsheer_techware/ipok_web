   <div class="ip_record_section">
        <div class="ip_record_header1">
          <div class="ip_select_all">
            <div class="ip_schedule_check">
                <input id="select-all-scheduled" class="ip_custom_checkbox " name="checkbox-1" type="checkbox" >
                <label for="select-all-scheduled" class="ip_custom_checkbox_label"><img src="<?php echo base_url();?>assets/images/ip_drp_grey.png"></label>
            </div>
          </div>
          <div class="ip_record_search_box">
            <input id="med_red_scheduled_search" value="<?php if(!empty($search_data)){echo $search_data;} ?>" class="ip_record_search_box_input" type="text" placeholder="<?php load_language('search');?>">
          </div>
          <div class="ip_record_settings">
            <span><?php $start_sc = ($patient_scheduled_page-1)*1;
                        $end_sc =  (($patient_scheduled_page-1)*1)+sizeof($patient_scheduled);
                        $total_sc = $patient_scheduled_total['count'];
                        echo $start_sc; ?>- <?php echo $end_sc?> of <?php echo $total_sc?>   
            </span>
            <span class="direction">
              <!-- direction prev -->
              <?php if($start_sc<=0)
              {
              ?>
                <img class="ip_direction_opacity"  src="<?php echo base_url();?>assets/images/ip_arw_left.png">
              <?php  
              }
              else
              {
              ?>
              <img id="medical_rec_scheduled_prev" src="<?php echo base_url();?>assets/images/ip_arw_left.png">
              <?php
              }
              ?>
             
              <!--_______________ -->

              <!-- direction next -->
              <?php if($end_sc<$total_sc)
              {
              ?>
                <img id="medical_rec_scheduled_next" src="<?php echo base_url();?>assets/images/ip_arw_right.png">
              <?php
              }
              else
              {
              ?>
                <img class="ip_direction_opacity" src="<?php echo base_url();?>assets/images/ip_arw_right.png">
              <?php
              }
              ?>
              <!--_______________ -->
             
            </span>
            <span class="settings"><img src="<?php echo base_url();?>assets/images/ip_settings.png"></span>
          </div>
          <div class="clear"></div>
        </div>
      </div>

       <div id="med_rec_scheduled_listing" class="ip_record_listing">
            <ul>
               <input type="hidden" id="medical_rec_scheduled_page" page="<?php echo $patient_scheduled_page?>" maxval="<?php echo sizeof($patient_scheduled);?>">
            <?php if(!empty($patient_scheduled))
            {
              foreach ($patient_scheduled as $key => $value) {
               
            ?>
             
                
              
              <li>
                <div class="row m0">

                  <div class="col-md-1 p0">
                    <div class="ip_schedule_check">
                        <input id="medical_records_scheduled_chkbox<?php echo $key;?>" class="ip_custom_checkbox select-scheduled" name="sent-broad-sch" type="checkbox" value="<?php echo $value['pat_id']?>" patname="<?php echo decrypt_data($value['pat_name']);?>" patpic="<?php echo $value['pat_pic'];?>">
                        <label for="medical_records_scheduled_chkbox<?php echo $key;?>" class="ip_custom_checkbox_label"></label>
                    </div>
                  </div>

                  <div class="col-md-3 p0">
                    <a href="<?php echo base_url();?>Doctor/recordsview/<?php echo $value['pat_id']?>">
                    <div class="ip_record_pic">
                      <img src="<?php echo base_url();echo $value['pat_pic'];?>">
                    </div>
                    <div class="ip_record_name">

                       <?php echo $this->encrypt->decode($value['pat_name']);?>

                    </div>
                    <div class="clear"></div>
                     </a>
                  </div>

                  <div class="col-md-3 p0">
                    <div class="ip_record_document">
                      <span>
                        <img src="<?php echo base_url();?>assets/images/ip_doc.png">
                      </span>
                      <span><?php load_language('last_consultation');?> :
                        <?php if(!empty($value['last_consult']))
                        {
                        ?>
                        <strong><?php echo date('d-M-Y',$value['last_consult']) ;?></strong>
                        <?php
                        }
                        else
                        {
                        ?>
                        <strong><?php load_language('not_available');?></strong>
                        <?php
                        }
                        ?>
                      </span>
                    </div>
                  </div>

                  <div class="col-md-3 p0">
                    <div class="ip_record_document">
                      <span><img src="<?php echo base_url();?>assets/images/ip_doc.png"></span>
                      <span><?php load_language('next_consultation');?> :
                        <?php if(!empty($value['next_consult']))
                        {
                        ?>
                        <strong><?php echo date('d-M-Y',$value['next_consult']) ;?></strong>
                        <?php
                        }
                        else
                        {
                        ?>
                        <strong><?php load_language('not_available');?></strong>
                        <?php
                        }
                        ?>
                      </span>
                    </div>
                  </div>

                  <div class="col-md-2 p0">
                    <a href="javascript:void(0)">
                    <div class="ip_record_name sent-broadcast-btn-sch">
                      <span><?php load_language('message');?></span><span><img src="<?php echo base_url();?>assets/images/ip_menu4.png"></span>
                    </div>
                    </a>
                   </div>

                </div>
              </li>
              
               <?php
                  }
              }
              else
              {
              ?>
                <li>
                  <div class="row m0">
                    <div class="col-md-3 p0"></div>
                    <div class="col-md-9 p0">
                      <div class="ip_record_document">
                        <span><strong><?php load_language('no_medical_records_found');?></strong></span> 
                      </div>
                    </div>
                  </div>
                </li>
              <?php
              }
              ?>
            </ul>
            </div>
          