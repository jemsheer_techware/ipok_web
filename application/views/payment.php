<div class="ip_main_tab_content">
        <div class="tab-content">

          <div id="review" class="tab-pane fade">
            <div class="ip_main_tab_pic">
              <img src="https://techlabz.in/ipok/assets/uploads/profilepic/doctors/32_female_doctor.png">
            </div>
            <h5><strong>Reeba Mathew</strong></h5>
            <p>Cardiology</p>
            <p>Fortis,Pezhakkapilly,Muvattupuzha-686669</p>
            <div class="ip_profile_ratting">
             <!--  <fieldset class="ip_rating">
                <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
              </fieldset> -->
              <div class="clear"></div>
            </div>
           <!--  <h4>14th december 2017</h4>
            <h6>16:00 hours</h6> -->
            <form role="form" id="confirm_book_form" data-parsley-validate="" novalidate="">
            <input type="hidden" name="confirm-book-clinic" id="confirm_book_clinic" value="19">
            <input type="hidden" name="confirm-book-doctor" id="confirm_book_doctor" value="32">
            <input type="hidden" name="book-status" id="book_status" value="1" drprice="1200" book-id="123">
            <input type="hidden" name="promocode-status" id="promocode_status" value="0">
            <input type="hidden" name="promocode-name" id="promocode_name" value="0">

            <div class="ip_profile_datetime ip_booking_date">
              <div class="row">
              <div id="booking-container">
                <input type="text" class="ip_calender floatLeft parsley-success" data-parsley-required="" readonly="" id="confirm_book_date" name="confirm-book-date" data-parsley-id="5">
              </div>
            </div>
            <div class="row">
              
            
              <select data-parsley-required="" id="schedule-consult-timeslot" class="ip_time floatRight parsley-success" placeholder="" name="confirm-book-time" data-parsley-id="7"><option disabled="" selected="">Time Slot</option><option value="08:00 am - 08:10 am" post="1531641600">08:00 am - 08:10 am</option><option value="08:10 am - 08:20 am" post="1531642200">08:10 am - 08:20 am</option><option value="08:20 am - 08:30 am" post="1531642800">08:20 am - 08:30 am</option><option value="08:30 am - 08:40 am" post="1531643400">08:30 am - 08:40 am</option><option value="08:40 am - 08:50 am" post="1531644000">08:40 am - 08:50 am</option><option value="08:50 am - 09:00 am" post="1531644600">08:50 am - 09:00 am</option><option value="09:00 am - 09:10 am" post="1531645200">09:00 am - 09:10 am</option><option value="09:10 am - 09:20 am" post="1531645800">09:10 am - 09:20 am</option><option value="09:20 am - 09:30 am" post="1531646400">09:20 am - 09:30 am</option><option value="09:30 am - 09:40 am" post="1531647000">09:30 am - 09:40 am</option><option value="09:40 am - 09:50 am" post="1531647600">09:40 am - 09:50 am</option><option value="09:50 am - 10:00 am" post="1531648200">09:50 am - 10:00 am</option><option value="10:00 am - 10:10 am" post="1531648800">10:00 am - 10:10 am</option><option value="10:10 am - 10:20 am" post="1531649400">10:10 am - 10:20 am</option><option value="10:20 am - 10:30 am" post="1531650000">10:20 am - 10:30 am</option><option value="10:30 am - 10:40 am" post="1531650600">10:30 am - 10:40 am</option><option value="10:40 am - 10:50 am" post="1531651200">10:40 am - 10:50 am</option><option value="10:50 am - 11:00 am" post="1531651800">10:50 am - 11:00 am</option><option value="11:00 am - 11:10 am" post="1531652400">11:00 am - 11:10 am</option><option value="11:10 am - 11:20 am" post="1531653000">11:10 am - 11:20 am</option><option value="11:20 am - 11:30 am" post="1531653600">11:20 am - 11:30 am</option><option value="11:30 am - 11:40 am" post="1531654200">11:30 am - 11:40 am</option><option value="11:40 am - 11:50 am" post="1531654800">11:40 am - 11:50 am</option><option value="11:50 am - 12:00 pm" post="1531655400">11:50 am - 12:00 pm</option><option value="12:00 pm - 12:10 pm" post="1531656000">12:00 pm - 12:10 pm</option><option value="12:10 pm - 12:20 pm" post="1531656600">12:10 pm - 12:20 pm</option><option value="12:20 pm - 12:30 pm" post="1531657200">12:20 pm - 12:30 pm</option><option value="12:30 pm - 12:40 pm" post="1531657800">12:30 pm - 12:40 pm</option><option value="12:40 pm - 12:50 pm" post="1531658400">12:40 pm - 12:50 pm</option><option value="12:50 pm - 01:00 pm" post="1531659000">12:50 pm - 01:00 pm</option><option value="01:30 pm - 01:40 pm" post="1531661400">01:30 pm - 01:40 pm</option><option value="01:40 pm - 01:50 pm" post="1531662000">01:40 pm - 01:50 pm</option><option value="01:50 pm - 02:00 pm" post="1531662600">01:50 pm - 02:00 pm</option></select>
              </div>
              <div class="clear"></div>
            </div>
          </form>
			<div class="ip_profile_list_div hidden" id="waitinglist-div">
				<ul>
				<li class="floatLeft" id="enter_waiting_list_btn">ENTER THE WAITING LIST</li>
				<li class="floatRight uppercase" data-toggle="modal" data-target="#waitinglistmodal">Know More</li>
				<div class="clear"></div>
				</ul>       
			</div>
      <div id="err_confirm_booking" class="alert alert-danger ip_profile_book_error hidden"></div>
      <div id="info_confirm_booking" class="alert alert-info ip_profile_book_error hidden"></div>
     
            <hr>
            <div class="row ip_promo_div">
              <div class="col-md-6">
                <form role="form" id="confirm_book_form_promo" data-parsley-validate="" novalidate="">
                  <input type="hidden" name="doctorid" value="32">
                  <input class="ip_coupon mr15" style="text-transform:uppercase" name="code" placeholder="COUPON" data-parsley-required="" data-parsley-minlength="8" data-parsley-maxlength="8" id="promocode_inp" onkeypress="if(this.value.length > 7) return false;">
                   <button type="button" id="promocode_submit_btn" class="ip_tab_bottom_btn ip_tab_bottom_btn_back floatLeft mr15" style="width: 20%;">APPLY</button>
                   <button type="button" id="promocode_cancel_btn" class="ip_tab_bottom_btn ip_tab_bottom_btn_back floatLeft " style="width: 20%;">CLEAR</button>
                </form>
              <div id="promo_success_error" class="alert ip_profile_book_error floatLeft mt30 hidden"></div>
              <div class="clear"></div>
              </div>
              <div class="col-md-6">
                <div class="ip_total_price">
                  <div class="ip_price floatLeft uppercase">
                    Amount                  </div>
                  <div class="ip_amount floatRight">
                    R$ 1200                  </div>
                  <div class="clear"></div>
                </div>
                <div class="clear"></div>   

                <div class="ip_total_price hidden mt10" id="show_offer_div">
                  <div class="ip_price floatLeft">
                    OFFER PRICE                  </div>
                  <div class="ip_amount floatRight">
                    R$ <span></span>
                  </div>
                  <div class="clear"></div>
                </div>
                <div class="clear"></div>

                 <div class="ip_total_price mt10" id="show_total_div">
                  <div class="ip_price floatLeft">
                    TOTAL PRICE                  </div>
                  <div class="ip_amount floatRight">
                    R$ <span>1200</span>
                  </div>
                  <div class="clear"></div>
                </div>
                  <div class="clear"></div>
              </div>
            </div>
            <div class="ip_bottom_tab_btn_bay">
            <div class="row">
              <div class="col-md-6">
                <a href="https://techlabz.in/ipok/Searchdoctor">
                  <button type="button" class="ip_tab_bottom_btn ip_tab_bottom_btn_back floatLeft uppercase">Back</button>
                </a>
            
                <div class="clear"></div>
              </div>
              <div class="col-md-6">
                <a href="javascript:void(0)">
                  <button class="ip_tab_bottom_btn ip_tab_bottom_btn_continue floatRight uppercase" id="confirm_booking_continue_btn">Continue</button>
                </a>

                <div class="clear"></div>
              </div>
            </div>
          </div>
          </div>

          <div id="login" class="tab-pane fade">
              <h1 class="capitalize">Login</h1>
            <div class="ip_main_tab_content_inner">
                   <form role="form" data-parsley-validate="" id="confirm-book-login-form" novalidate="">
                <div class="ip_login_input_form">
                 <input type="hidden" name="login_type" value="PATIENT">
                  <div class="ip_login_input_row">
                    <input name="login-form-username" data-parsley-required="" class="ip_login_input ip_login_user clear-login-data" placeholder="Login">
                  </div>
                  <div class="ip_login_input_row">
                    <input name="login-form-password" data-parsley-required="" class="ip_login_input ip_login_pass clear-login-data" placeholder="Password" type="password">
                  </div>
                  <input type="hidden" name="latitude" id="">
                  <input type="hidden" name="longitude">
                  <input type="hidden" name="address">
                  <div class="">
                    <button type="button" class="ip_login_modal_signin floatLeft  capitalize" id="confirm-book-login_submit">Login</button>

                    <p class="floatLeft" data-toggle="modal" data-target="#forgot">Forgot Password</p>
                    <div class="clear"></div>
                  </div>
                  <div id="err-login-ajax" class="alert alert-danger hidden textCenter "></div>
                </div>
              </form>
               <button type="button" id="tab_login_back" class="ip_tab_bottom_btn ip_tab_bottom_btn_back width100">Back</button>
            </div>
          </div>

          <div id="payment" class="tab-pane fade active in">
            <form id="booking-payment-form" data-parsley-validate="" novalidate="">
            <h1>PAYMENT</h1>
            <div class="ip_main_tab_content_inner">
              <p>Description in payment page</p>
              <input data-parsley-required="" data-parsley-minlength="3" data-parsley-maxlength="20" onkeypress="if(this.value.length > 19) return false;" class="ip_content_inner_input payment_firstname" data-parsley-pattern="^[a-zA-Z ]+$" placeholder="First Name" name="firstname">
              <input data-parsley-required="" data-parsley-minlength="3" data-parsley-maxlength="20" onkeypress="if(this.value.length > 19) return false;" data-parsley-pattern="^[a-zA-Z ]+$" class="ip_content_inner_input payment_lastname" placeholder="Last Name" name="lastname">
              <input data-parsley-required="" data-parsley-minlength="13" data-parsley-maxlength="16" onkeypress="if(this.value.length > 15) return false;" data-parsley-pattern="^[0-9]+$" class="ip_content_inner_input payment_cardnum" type="number" placeholder="Card Number" name="cardnumber">
              <div class="ip_card_validity">
                <div class="a1"><span>EXPIRATION DATE</span></div>
                <div class="a1 ip_sel_mm">
                  <select class="ip_validity_select" name="month" data-parsley-required="" data-parsley-error-message="Month is required.">
                    <option selected="" disabled="">MM</option>
                                          <option value="1">1</option>
                                          <option value="2">2</option>
                                          <option value="3">3</option>
                                          <option value="4">4</option>
                                          <option value="5">5</option>
                                          <option value="6">6</option>
                                          <option value="7">7</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                      </select>
                </div>
                <div class="a1 ip_sel_yy">
                  <select class="ip_validity_select" name="year" data-parsley-required="" data-parsley-error-message="Year is required.">
                    <option selected="" disabled="">YY</option>
                                          <option value="13">2018</option>
                                          <option value="13">2019</option>
                                          <option value="13">2020</option>
                                          <option value="13">2021</option>
                                          <option value="13">2022</option>
                                          <option value="13">2023</option>
                                          <option value="13">2024</option>
                                          <option value="13">2025</option>
                                          <option value="13">2026</option>
                                          <option value="13">2027</option>
                                          <option value="13">2028</option>
                                          <option value="13">2029</option>
                                          <option value="13">2030</option>
                                          <option value="13">2031</option>
                                          <option value="13">2032</option>
                                          <option value="13">2033</option>
                                          <option value="13">2034</option>
                                          <option value="13">2035</option>
                                          <option value="13">2036</option>
                                          <option value="13">2037</option>
                                          <option value="13">2038</option>
                                          <option value="13">2039</option>
                                          <option value="13">2040</option>
                                          <option value="13">2041</option>
                                          <option value="13">2042</option>
                                          <option value="13">2043</option>
                                          <option value="13">2044</option>
                                          <option value="13">2045</option>
                                          <option value="13">2046</option>
                                          <option value="13">2047</option>
                                          <option value="13">2048</option>
                                          <option value="13">2049</option>
                                          <option value="13">2050</option>
                                          <option value="13">2051</option>
                                          <option value="13">2052</option>
                                          <option value="13">2053</option>
                                      </select>
                </div>
                <div class="a1 mr0 ip_sel_dd">
                  <input type="number" data-parsley-required="" data-parsley-error-message="CVV is required." data-parsley-minlength="3" data-parsley-maxlength="3" onkeypress="if(this.value.length > 2) return false;" data-parsley-pattern="^[0-9]+$" class="ip_content_inner_input mb0" placeholder="CVV" name="cvv" style="width:60px;">
                </div>
                <div class="ip_date_img">
                  <img src="https://techlabz.in/ipok/assets/images/ip_ques.png">
                </div>
                <div class="clear"></div>
              </div>
              <div class="width100 textCenter">
                <button type="button" id="book_payment_btn" class="ip_makepayment_btn">
                  MAKE PAYMENT                </button>
              </div>
              <div class="width100 textCenter p5">
                <button type="button" id="tab_payment_back" class="ip_tab_bottom_btn ip_tab_bottom_btn_back ip_tab_payment_back">Back</button>
              </div>
              <br>
              <div id="payment-error-div" class="alert alert-danger text-center hidden"></div>
            </div>
            </form>
          </div>

          <div id="confirmation" class="tab-pane fade">
            <h1>Consultation Confirmed</h1>
            <br>
            <div class="ip_main_tab_content_inner">
              <div class="width100 textCenter">
                 <div class="ip_main_tab_pic">
                  <img src="https://techlabz.in/ipok/assets/uploads/profilepic/doctors/32_female_doctor.png">
                </div>
                <h5><strong>Reeba Mathew</strong></h5>
                <h6>Cardiology</h6>
                <p class="ip_booking_confirm_detail">
                  Fortis<br>
                  Pezhakkapilly,
                  Muvattupuzha-686669<br>
                  <br>
                  <span id="book-date-show"> </span><br><span id="book-time-show"> </span>
                </p>
                <br>
                <br>
                <br>
                <a href="https://techlabz.in/ipok/Patient">
                <button type="button" class="ip_makepayment_btn">Done</button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>