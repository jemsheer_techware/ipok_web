
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">
    <div class="ip_edit_record_wrapper">
      <div class="row">
        <div class="col-md-8">
          <div class="ip_edit_record_cover">
            <div class="ip_edit_record_head backgroundnone">
              <?php load_language('add_collaborator');?>
            </div>
              <div class="ip_edit_record_detail">
                <form id="add-colaborator-form" role="form" data-parsley-validate="">

                <div class="ip_edit_row">
                  <div class="ip_bank_detail_frame">
                    <input class="ip_bank_input" data-parsley-required="" name="name" onKeyPress="if(this.value.length > 40) return false;" data-parsley-minlength="5" data-parsley-pattern="^[a-zA-Z ]+$"  placeholder="<?php load_language('name');?>*" data-parsley-required="">
                  </div>
                </div>

                <div class="ip_edit_row">
                    <div class="ip_bank_detail_frame">
                      <input class="ip_bank_input"  placeholder="<?php load_language('email');?>*" data-parsley-required="" data-parsley-emailcolabor="" onKeyPress="if(this.value.length > 75) return false;" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
                  </div>
                </div>

                <div class="ip_edit_row">
                      <div class="ip_bank_detail_frame">
                      <input class="ip_bank_input" name="password" data-parsley-required="" placeholder="Password*" data-parsley-required="" data-parsley-minlength="8" onKeyPress="if(this.value.length > 25) return false;" type="Password" data-parsley-uppercase="1" data-parsley-lowercase="1" data-parsley-number="1" data-parsley-special="1">
                      </div>
                </div>

                <div class="ip_edit_row">
                  <div class="row">
                    <div class="col-md-6">
                      <p class="ip_row_p"><?php load_language('telephone');?>*</p>
                      <div class="ip_bank_detail_frame">
                          <input class="ip_bank_input" data-parsley-required="" type="number" onKeyPress="if(this.value.length > 25) return false;" placeholder="" name="telephone">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <p class="ip_row_p">CPF*</p>
                      <div class="ip_bank_detail_frame">
                          <input class="ip_bank_input" data-parsley-required="" name="cpf" placeholder="" data-parsley-required="" data-parsley-minlength="11" data-parsley-cpf="" data-parsley-cpfunique="" onKeyPress="if(this.value.length > 10) return false;"  type="number">
                      </div>
                    </div>
                  </div>
                </div>

                <div class="ip_edit_row">
                   <div class="col-md-6">
                      <p class="ip_row_p"><?php load_language('add_photo');?>*:</p>
                      <div class="ip_reg_modal_addphoto floatLeft">
                      <img  id="doc-add-colabor-pic">
                      </div>

                      <div class="ip_add_photo floatRight">
                      <input name="image"  data-parsley-required="true" type="file" accept="image/*"   class="ip_reg_form_input"
                      data-parsley-error-message="Choose Profile Photo" onchange="readURL(this,'doc-add-colabor-pic')" id="colabor-pic">

                      </div>
                   </div>
                   <div class="col-md-6"></div>
                 
                <div class="clear"></div>
              </div>
              </form>
              </div>
              <hr>
              <div class="ip_coloborator_btn_bay">
                <a href="javascript:void(0)">
                  <button class="ip_colaborator_btn" type="button" id="add-colaborator-btn"><?php load_language('add_collaborator_save_button');?></button>
                </a>
               <!--  <a href="javascript:void(0)">
                  <button class="ip_colaborator_delete_btn">Delete collaborator</button>
                </a> -->
              </div>
              <div class="alert alert-success alert-dismiss textCenter hidden" id="colabor-add-success"><?php load_language('add_collaborator_success');?></div>
              <div class="alert alert-danger alert-dismiss textCenter hidden" id="colabor-add-error"><?php load_language('add_collaborator_error');?></div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="ip_edit_record_cover hidden" id="authorize-access-div">

            <div class="ip_edit_record_head backgroundnone">
              <?php load_language('collaborator_access_heading');?>
            </div>
              <div class="ip_edit_record_detail">
                <form data-parsley-validate="" role="form" id="colabor-auth-access-form">
                <div class="ip_edit_row">
                  <br>
                  <br>
                    <p class="ip_row_p"><?php load_language('collaborator_access_desc');?>                  
                    </p>
                </div>
                <br>
                <div class="ip_notify_time">
                  <li>
                    <div class="ip_day_time_schedule_details_data p0">
                         <input type="hidden" name="section" value="authorizeaccess">
                         <input id="checkbox-41" class="ip_custom_checkbox1" name="access[]" type="checkbox" value="Doctor/collaborator">
                         <label for="checkbox-41" class="ip_custom_checkbox_label1"><?php load_language('appointment');?></label>
                         <div class="clear"></div>
                     </div>
                  </li>
                  <li>
                    <div class="ip_day_time_schedule_details_data p0">
                         <input id="checkbox-42" class="ip_custom_checkbox1" name="access[]" type="checkbox" value="Doctor/chat" >
                         <label for="checkbox-42" class="ip_custom_checkbox_label1"><?php load_language('messages');?></label>
                         <div class="clear"></div>
                     </div>
                  </li>                  
                  <div class="clearfix"></div>
                </div>
                <br>
              </div>
            </form>
              <hr>
              <div class="ip_coloborator_btn_bay">
                <button class="ip_colaborator_btn floatRight" type="button"  id="colabor-auth-access"><?php load_language('collaborator_access_save_button');?></button>
                <div class="clear"></div>
                <br>
                <div class="alert alert-danger alert-dismiss textCenter hidden" id="colabor-auth-access-error"><?php load_language('please_add_collaborator');?></div>
                <div class="alert alert-success alert-dismiss textCenter hidden" id="colabor-auth-access-success"><?php load_language('collaborator_access_success');?></div>
              </div>

          </div>
        </div>

      </div>
    </div>
  </div>
</div>
