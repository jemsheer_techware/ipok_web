<script>

 
setTimeout(function()
{
    $('#doc_consult_duration').val('<?php echo $doctor_data['dr_consult_duration'];?>').trigger('change');
    $('#doc_accept_return').val('<?php echo $doctor_data['dr_accept_return'];?>').trigger('change');
    $('#doc_return_timeperiod').val('<?php echo $doctor_data['dr_return_timeperiod'];?>').trigger('change');
},500)

$('#doc_accept_return').change(function()
{
	var elem = $('#doc_accept_return').val();
	if(elem=="1")
	{
		$('#doc_return_timeperiod').attr('data-parsley-required','true');
	}
	else
	{
		$('#doc_return_timeperiod').removeAttr('data-parsley-required');
	}
	//console.log($('#doc_accept_return').val())
})

/*CUSTOM EVENT TRIGGERING FOR PARSLEY*/
/*----------------------------------*/
$('#sec_clinic_day_mon').parsley().on('field:error', function() 
{
	console.log('parsley eror on secondary schedule')
	$('#doc_dash_agenda_error').removeClass('hidden');
	setTimeout(function()
	{
		$('#doc_dash_agenda_error').addClass('hidden');
	},10000)
});

$('#clinic_day_mon').parsley().on('field:error', function() 
{
	console.log('parsley eror on secondary schedule')
	$('#doc_dash_agenda_error').removeClass('hidden');
	setTimeout(function()
	{
		$('#doc_dash_agenda_error').addClass('hidden');
	},10000)
});

/*----------------------------------*/


</script>

