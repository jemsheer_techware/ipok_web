<script>

     /*CUSTOM SCRIPT FOR LOCATION ON SEARCH RESULT PAGE*/
     /*-------------------------------------------------------------*/
     var filter_input = document.getElementById('filter_dr_srch_loc');
     var filter_options = 
     {
          componentRestrictions: {
          country: 'br'
          },
          types: ['(cities)']
     };

     var filter_autocomplete = new google.maps.places.Autocomplete(filter_input, filter_options);
     google.maps.event.addListener(filter_autocomplete, 'place_changed', function () 
     {
          var place = filter_autocomplete.getPlace();
          console.log(place.formatted_address, place.geometry.location.lat(),place.geometry.location.lng());   
          $("#filter_dr_srch_lat" ).val(place.geometry.location.lat());
          $("#filter_dr_srch_lng" ).val(place.geometry.location.lng());
     });
     /*-------------------------------------------------------------*/


     var low_price =  Number('<?php echo $filter_autoload['price_min'];?>');
     var high_price = Number('<?php echo $filter_autoload['price_max'];?>');

     var start_distance = Number('<?php echo $filter_autoload['distance_min'];?>');
     var end_distance = Number('<?php echo $filter_autoload['distance_max'];?>');

     <?php
     if(!empty($searchdata['filter_dr_srch_price_high'])&&!empty($searchdata['filter_dr_srch_price_low']))
     {
     ?>
          pr1 = '<?php echo $searchdata['filter_dr_srch_price_low'];?>';
          pr2 = '<?php echo $searchdata['filter_dr_srch_price_high'];?>';
          pr1 = pr1.replace('R$ ','');
          pr2 = pr2.replace('R$ ','');
          setTimeout(function() {
               $( "#ip_price_slider" ).slider("option", {
               values: [pr1, pr2]
               });   
               $("#ip_filter_price_low" ).val('<?php echo $searchdata['filter_dr_srch_price_low'];?>');
               $("#ip_filter_price_high" ).val('<?php echo $searchdata['filter_dr_srch_price_high'];?>');  
          }, 1000);
     <?php
     }
     else
     {
     ?>
          setTimeout(function() {
               $( "#ip_price_slider" ).slider("option", {
               values: [low_price, high_price]
               });   
               $("#ip_filter_price_low" ).val("R$"+low_price);
               $("#ip_filter_price_high" ).val("R$"+high_price);  
          }, 1000); 
     <?php
     }
     ?>


     <?php
     if(!empty($searchdata['filter_dr_srch_distance_start'])&&!empty($searchdata['filter_dr_srch_distance_end']))
     {
     ?>   
     
     dis1 = '<?php echo $searchdata['filter_dr_srch_distance_start'];?>';
     dis2 = '<?php echo $searchdata['filter_dr_srch_distance_end'];?>';

     dis1 = dis1.replace(' km','');
     dis2 = dis2.replace(' km','');

     setTimeout(function(){
          $( "#ip_filter_distance_range" ).slider("option", {
          values: [dis1, dis2]
          });   
          $( "#ip_filter_distance_start" ).val('<?php echo $searchdata['filter_dr_srch_distance_start'];?>');
          $( "#ip_filter_distance_end" ).val('<?php echo $searchdata['filter_dr_srch_distance_end'];?>');  
     },1000);

     <?php
     }
     else
     {
     ?>
          setTimeout(function(){
               $( "#ip_filter_distance_range" ).slider("option", {
               values: [start_distance, end_distance]
               });   
               $( "#ip_filter_distance_start" ).val(start_distance+" km");
               $( "#ip_filter_distance_end" ).val(end_distance+" km");  
          },1000);
     <?php 
     }
     ?>
 

     <?php if(!empty($searchdata['doctor-search-location']))
     {?>
          $("#filter_dr_srch_loc").val("<?php echo $searchdata['doctor-search-location'];?>");
          $("#filter_dr_srch_lat" ).val("<?php echo $searchdata['doctor-search-latitude'];?>");
          $("#filter_dr_srch_lng" ).val("<?php echo $searchdata['doctor-search-longitude'];?>");
          $('#search-data-location').html('<?php echo $searchdata['doctor-search-location'];?>');
     <?php
     }
     ?>

     <?php if(!empty($searchdata['doctor-search-date']))
     {?>
     $("#ip_datepicker_srch" ).val("<?php echo date('d-m-Y',$searchdata['doctor-search-date']);?>");
       
     <?php
     }
     ?> 

     <?php if(!empty($searchdata['filter_dr_gender']))
     {
     ?>   var filter_gender = '<?php echo $searchdata['filter_dr_gender']?>';
          if(filter_gender=="MALE")
          { 
          id =  '#dctr-filter-male'
          }          
          else if(filter_gender=="FEMALE")
          {  
               id = '#dctr-filter-female'
          }
          $(id).prop('checked', true); 
          setTimeout(function() {
               $(id).trigger("change");
          }, 1000)
          
          
     <?php
     }
     ?> 

     <?php if(!empty($searchdata['doctor-search-speciality']))
     {?>
          $('#filter_dr_srch_speciality')
        .val('<?php echo $searchdata['doctor-search-speciality'];?>')
        .trigger('change');
     <?php
     }
     ?>

      <?php if(!empty($searchdata['filter_dr_domicilar']))
     {
     ?>  
          $('#ip_filter_dr_domicilar').prop('checked', true); 
          setTimeout(function() {
               $('#ip_filter_dr_domicilar').trigger("change");
          }, 1000)
          
          
     <?php
     }
     ?> 

     function load_filterdata(data)
     {
          var searchdata = JSON.parse(data);
          //console.log(searchdata)
          if(searchdata['doctor-search-location'])
          {
               $("#filter_dr_srch_loc").val(searchdata['doctor-search-location']);
               $("#filter_dr_srch_lat" ).val(searchdata['doctor-search-latitude']);
               $("#filter_dr_srch_lng" ).val(searchdata['doctor-search-longitude']);
               $("#search-data-location").html(searchdata['doctor-search-location']);
               
          }

          if(searchdata['doctor-search-speciality'])
          {
               $("#search-data-speciality").html(searchdata['doctor-search-speciality']);
          }
         
     }
     </script>
