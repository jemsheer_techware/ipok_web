<div class="ip_bank_detail_frame">
  <select class="ip_bank_input reset-redemption-form" data-parsley-required="" name="redemption_bank">
    <option disabled selected><?php load_language('select_bank');?></option>
    <?php 
    if(!empty($banks))
    {
      foreach ($banks as $key => $value) 
      {
      ?>
       <option value="<?php echo $value['id'];?>"><?php echo $value['bank_name'];?> - <?php echo $value['account_no'];?></option>
      <?php
      }
    }
    ?>                            
  </select>
</div>