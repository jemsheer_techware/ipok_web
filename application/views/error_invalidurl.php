<div class="ip_set_two_wrapper">

	<div  class="">
	  <div class="modal-dialog" style=" box-shadow: 0px 0px 5px 0px #888888;border-top-right-radius: 8px;border-top-left-radius: 8px;">
	      <div class="ip_patient_delete_pop_wrapper">
	        <div class="ip_paitent_delete_header uppercase">
	          <?php load_language('error');?>
	        </div>
	        <div class="ip_patient_delete_content">
	        	<div class="textCenter">
	        		<img src="<?php echo base_url();?>assets/images/ip_error.png">
	        	</div>
	          <h5><?php load_language('url_error');?>!</h5>
	          <hr>
	          <p><?php load_language('invalid_url');?>!</p>
	          <div class="ip_patient_delete_form">
	            <div class="ip_patient_delete_row textCenter">
	             <!--  <button class="ip_paitent_dark_btn">OKAY</button> -->
	              <div class="clear"></div>
	            </div>
	          </div>
	        </div>
	      </div>
	  </div>
	</div>

</div>