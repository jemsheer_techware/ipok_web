
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">
    <div class="ip_top_dash_bay">
      <div class="row">
        <div class="col-md-3">
          <div class="ip_top_dash_list">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_appointments.png">
            </div>
            <div class="ip_top_dash_detail">
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_attendance']?>">0</strong>
              <p><?php load_language('attendance_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="ip_top_dash_list">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_feature.png">
            </div>
            <div class="ip_top_dash_detail" >
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_billed']?>">0</strong>
              <p><?php load_language('billed_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="ip_top_dash_list">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_paintences.png">
            </div>
            <div class="ip_top_dash_detail">
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_patients']?>">0</strong>
              <p><?php load_language('patients_visited_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="ip_top_dash_list bordernone">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_vistors.png">
            </div>
            <div class="ip_top_dash_detail">
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_profileview']?>">0</strong>
              <p><?php load_language('profile_views_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>

    <div class="ip_grid_cols">
      <div class="row">
        <div class="col-md-8">
          <div class="ip_bio_tab_div">
            <div class="ip_bio_head">
              <?php load_language('notification_center');?>
            </div>
            <div class="ip_bio_detail"  id="doc-noti-view">
            <?php
                $this->load->view('doctor_notifications_list')
            ?>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="ip_bio_tab_div">
            <div class="ip_bio_head">
              <?php load_language('anniversaries');?>
            </div>
            <div class="ip_bio_detail">
              <div class="ip_bio_message_list">

                <ul>

                  <?php
                  if(!empty($aniversaries))
                  {
                    foreach ($aniversaries as $key => $value) 
                    {
                  ?>  
                      <li>
                        
                          <div class="ip_bio_message_pic">
                            <img src="<?php echo base_url(); echo $value['profile_photo'];?>">
                          </div>
                          <div class="ip_bio_messages">
                            <h5><?php echo $value['name'];?></h5>
                            <div class="clear"></div>
                            <p><?php echo date('d F',$value['dob']);?></p>
                            <!-- <p>22th September</p> -->
                          </div>
                          <a href="<?php echo base_url();?>Doctor/Chat/<?php echo $value['id'];?>">
                            <div class="ip_message_not">
                              <img src="<?php echo base_url();?>assets/images/ip_menu4.png"><br>
                              <?php load_language('messages');?>
                            </div>
                          </a>
                          <div class="clear"></div>
                        
                      </li>

                  <?php
                    }
                  }
                  ?>

                  

                 
                  
                </ul>
              </div>
              <div class="ip_notification_btm_btn_bay textCenter" style="padding:30px;">
                  <!-- <button button="type" class="ip_notification_btn" style="width: 40%;">View more</button> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  </div>
</div>

