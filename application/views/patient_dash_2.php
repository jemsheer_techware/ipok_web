
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">
    <div class="ip_bio_tab_div">
      <div class="row m0">
        <div class="col-md-2 p0 height100">
          <div class="ip_bio_tab_bay height100">
            <ul>
              <li class="active" data-toggle="tab" href="#profile">Profile</li>
              <li data-toggle="tab" href="#bio">Address</li>
              <li data-toggle="tab" href="#photo">Photos</li>
              <li class="arrow" data-toggle="tab" href="#special">Support</li>
              <li data-toggle="tab" href="#more" class="arrow">More</li>
            </ul>
          </div>
        </div>
        <div class="col-md-10 p0">
          <div class="ip_bio_tab_content">
            <div class="tab-content">
              <div id="profile" class="tab-pane fade in active">
                <div class="ip_profile_tab_detail p0">
                  <div class="ip_profile_tab_top">
                    <div class="ip_profile_tab_circle">
                      <img src="<?php echo base_url();echo $patient_data['pt_pic']?>">
                    </div>
                    <div class="ip_profile_tab_name">
                      <h3><?php echo $patient_data['pt_name']?></h3>
                    </div>
                    <div class="ip_profile_tab_button">
                      <div class="ip_profile_tab_button_circle"><img src="<?php echo base_url();?>assets/images/ip_edit.png"></div>
                      <div class="ip_profile_tab_button_circle"><img src="<?php echo base_url();?>assets/images/ip_delete.png"></div>
                      <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <ul>
                        <li>
                          <div class="child1">Email :</div>
                          <div class="child2"><?php echo $patient_data['pt_email']?></div>
                          <div class="clear"></div>
                        </li>
                        <li>
                          <div class="child1">Phone :</div>
                          <div class="child2"><?php echo $patient_data['pt_number']?></div>
                          <div class="clear"></div>
                        </li>
                        <li>
                          <div class="child1">BloodGroup :</div>
                          <div class="child2"><?php echo $patient_data['pt_blood_group']?></div>
                          <div class="clear"></div>
                        </li>
                      
                      </ul>
                    </div>
                    <div class="col-md-6">
                      <ul>
                        <li>
                          <div class="child1">Birthday :</div>
                          <div class="child2"><?php echo date('d F Y',$patient_data['pt_dob']);?></div>
                          <div class="clear"></div>
                        </li>
                        <li>
                          <div class="child1">Weight :</div>
                          <div class="child2"><?php echo $patient_data['pt_weight']?>Kg</div>
                          <div class="clear"></div>
                        </li>
                        <li>
                          <div class="child1">Height :</div>
                          <div class="child2"><?php echo $patient_data['pt_height']?>cm</div>
                          <div class="clear"></div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div id="bio" class="tab-pane fade">
                <div class="ip_profile_tab_detail">
                  <h3>Address</h3>
                  <p><?php echo $patient_data['pt_street_add']?></p>
                  <p><?php echo $patient_data['pt_locality']?></p>
                  <p><?php echo $patient_data['pt_zip_code']?></p>

                </div>
              </div>
              <div id="special" class="tab-pane fade">
                <div class="ip_profile_tab_detail p0">
                  <div class="ip_profile_tab_top p0">
                    <div class="ip_profile_contato">
                      <h5><strong>What is the reason for your contact?</strong></h5>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                      <div class="clear"></div>
                      <div class="row">
                        <div class="col-md-3">
                          <div class="ip_day_time_schedule_details_data p0">
                               <input id="checkbox-11" class="ip_custom_checkbox1" name="checkbox-11" type="checkbox" checked="">
                               <label for="checkbox-11" class="ip_custom_checkbox_label1">Problems Attendent</label>
                               <div class="clear"></div>
                           </div>
                           <div class="clear"></div>
                        </div>
                        <div class="col-md-3">
                          <div class="ip_day_time_schedule_details_data p0">
                               <input id="checkbox-12" class="ip_custom_checkbox1" name="checkbox-12" type="checkbox" checked="">
                               <label for="checkbox-12" class="ip_custom_checkbox_label1">Difficulty to schedule</label>
                               <div class="clear"></div>
                           </div>
                           <div class="clear"></div>
                        </div>
                        <div class="col-md-3">
                          <div class="ip_day_time_schedule_details_data p0">
                               <input id="checkbox-13" class="ip_custom_checkbox1" name="checkbox-13" type="checkbox" checked="">
                               <label for="checkbox-13" class="ip_custom_checkbox_label1">Problems with payment</label>
                               <div class="clear"></div>
                           </div>
                           <div class="clear"></div>
                        </div>
                        <div class="col-md-3">
                          <div class="ip_day_time_schedule_details_data p0">
                               <input id="checkbox-13" class="ip_custom_checkbox1" name="checkbox-13" type="checkbox" checked="">
                               <label for="checkbox-13" class="ip_custom_checkbox_label1">Others</label>
                               <div class="clear"></div>
                           </div>
                           <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                      </div>
                    </div>
                    </div>
                    <div>
                      <div class="ip_edit_record_detail">
                        <div class="ip_edit_text_bay">
                          <div class="ip_edit_record_text">
                            <ul>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit1.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit2.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit3.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit4.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit5.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit6.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit7.png"></li>
                              <li><img src="<?php echo base_url();?>assets/images/ip_edit8.png"></li>
                              <div class="clear"></div>
                            </ul>
                          </div>
                          <textarea class="ip_edit_record_content_textarea" rows="4"></textarea>
                        </div>
                        <div class="ip_edit_bottom_btn_bay">
                          <button class="ip_edit_save_btn floatRight">SEND</button>
                          <div class="clear"></div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
              <div id="photo" class="tab-pane fade">
                <div class="ip_profile_tab_detail">
                  <h3>Photos</h3>
                </div>
              </div>
              <div id="more" class="tab-pane fade">
                <div class="ip_profile_tab_detail">
                  <h3>More</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="ip_grid_cols">
      <div class="row">
        <div class="col-md-4">
          <div class="ip_bio_tab_div">
            <div class="ip_bio_head">
              Notification
              <div class="ip_bio_more">
              </div>
            </div>
            <div class="ip_bio_detail">
              <div class="ip_bio_notification_list">
                <ul>
                  <li>
                    <h5>Nyla Augusta
                      <div class="ip_notification_time">12:56</div>
                    </h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                  </li>
                  <li>
                    <h5>Nyla Augusta
                      <div class="ip_notification_time">12:56</div>
                    </h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                  </li>
                  <li>
                    <h5>Nyla Augusta
                      <div class="ip_notification_time">12:56</div>
                    </h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                  </li>
                  <li>
                    <h5>Nyla Augusta
                      <div class="ip_notification_time">12:56</div>
                    </h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                  </li>
                  <li>
                    <h5>Nyla Augusta
                      <div class="ip_notification_time">12:56</div>
                    </h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                  </li>
                  <li>
                    <h5>Nyla Augusta
                      <div class="ip_notification_time">12:56</div>
                    </h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                  </li>
                  <li>
                    <h5>Nyla Augusta
                      <div class="ip_notification_time">12:56</div>
                    </h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                  </li>
                  <li>
                    <h5>Nyla Augusta
                      <div class="ip_notification_time">12:56</div>
                    </h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="ip_bio_tab_div">
            <div class="ip_bio_head">
              Messages
              <div class="ip_bio_more">
              </div>
            </div>
            <div class="ip_bio_detail">
              <div class="ip_bio_message_list">
                <ul>
                  <li>
                    <div class="ip_bio_message_pic">
                    </div>
                    <div class="ip_bio_messages">
                      <h5>Nyla Augusta</h5><div class="ip_message_time">12:56</div>
                      <div class="clear"></div>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li>
                    <div class="ip_bio_message_pic">
                    </div>
                    <div class="ip_bio_messages">
                      <h5>Nyla Augusta</h5><div class="ip_message_time">12:56</div>
                      <div class="clear"></div>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li>
                    <div class="ip_bio_message_pic">
                    </div>
                    <div class="ip_bio_messages">
                      <h5>Nyla Augusta</h5><div class="ip_message_time">12:56</div>
                      <div class="clear"></div>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li>
                    <div class="ip_bio_message_pic">
                    </div>
                    <div class="ip_bio_messages">
                      <h5>Nyla Augusta</h5><div class="ip_message_time">12:56</div>
                      <div class="clear"></div>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li>
                    <div class="ip_bio_message_pic">
                    </div>
                    <div class="ip_bio_messages">
                      <h5>Nyla Augusta</h5><div class="ip_message_time">12:56</div>
                      <div class="clear"></div>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li>
                    <div class="ip_bio_message_pic">
                    </div>
                    <div class="ip_bio_messages">
                      <h5>Nyla Augusta</h5><div class="ip_message_time">12:56</div>
                      <div class="clear"></div>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li>
                    <div class="ip_bio_message_pic">
                    </div>
                    <div class="ip_bio_messages">
                      <h5>Nyla Augusta</h5><div class="ip_message_time">12:56</div>
                      <div class="clear"></div>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li>
                    <div class="ip_bio_message_pic">
                    </div>
                    <div class="ip_bio_messages">
                      <h5>Nyla Augusta</h5><div class="ip_message_time">12:56</div>
                      <div class="clear"></div>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                    </div>
                    <div class="clear"></div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="ip_bio_tab_div">
            <div class="ip_bio_head">
              promotions
              <div class="ip_bio_more">
              </div>
            </div>
            <div class="ip_bio_detail textCenter">
              <div class="ip_bio_message_list">
                <ul>
                  <li>
                    <div class="ip_bio_messages width100">
                      <div class="ip_promo_image">
                      </div>
                      <h5>Nyla Augusta</h5>
                      <div class="clear"></div>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li>
                    <div class="ip_bio_messages width100">
                      <div class="ip_promo_image">
                      </div>
                      <h5>Nyla Augusta</h5>
                      <div class="clear"></div>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li>
                    <div class="ip_bio_messages width100">
                      <div class="ip_promo_image">
                      </div>
                      <h5>Nyla Augusta</h5>
                      <div class="clear"></div>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li>
                    <div class="ip_bio_messages width100">
                      <div class="ip_promo_image">
                      </div>
                      <h5>Nyla Augusta</h5>
                      <div class="clear"></div>
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>
                    </div>
                    <div class="clear"></div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<hr>
    <div class="row">
      <div class="col-md-12">
        <div class="ip_full_calender_div">
          <div class="ip_full_calender_head">
            <div class="ip_full_calender_nav">
              <div class="btn-group">
                <button type="button" class="btn"><img src="<?php echo base_url();?>assets/images/ip_arw_left.png"></button>
                <button type="button" class="btn"><img src="<?php echo base_url();?>assets/images/ip_arw_right.png"></button>
              </div>
              <div class="btn-group">
                <button type="button" class="btn ip_apppointment_btn_custom"><a>SEPTEMBER</a></button>
              </div>
            </div>
            <h3>Appointment of Dr. Jinu Samuel</h3>
          </div>
          <div class="ip_full_calender_content">
            <div class="ip_table_head">
              <ul>
                  <li class="time_slot"></li>
                  <li>MON, 3</li>
                  <li>TUES, 4</li>
                  <li>WED, 5</li>
                  <li>THUR, 6</li>
                  <li>FRI, 7</li>
                  <li>SAT, 8</li>
                  <li class="borderrightnone">SUN, 9</li>
                  <div class="clear"></div>
              </ul>
            </div>
            <div class="ip_table_head_divide">
              <ul>
                  <li class="time_slot"></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li class="borderrightnone"></li>
                  <div class="clear"></div>
              </ul>
            </div>
            <div class="ip_table_days">
              <ul>
                  <li class="time_slot">
                    <div class="ip_time_interval">
                      <ul>
                        <li><p>13:00</p></li>
                        <li><p>14:00</p></li>
                        <li><p>15:00</p></li>
                        <li><p>16:00</p></li>
                        <li><p>17:00</p></li>
                        <li><p>18:00</p></li>
                        <li><p>19:00</p></li>
                        <li><p>20:00</p></li>
                        <li><p>21:00</p></li>
                        <li><p>22:00</p></li>
                        <li><p>23:00</p></li>
                        <li><p>00:00</p></li>
                        <li><p>01:00</p></li>
                        <li><p>02:00</p></li>
                        <li><p>03:00</p></li>
                        <li><p>04:00</p></li>
                        <li><p>05:00</p></li>
                        <li><p>06:00</p></li>
                        <li><p>07:00</p></li>
                        <li><p>08:00</p></li>
                        <li><p>09:00</p></li>
                        <li><p>10:00</p></li>
                        <li><p>11:00</p></li>
                        <li><p>12:00</p></li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <div class="ip_time_avialability">
                      <ul>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <div class="ip_time_avialability">
                      <ul>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <div class="ip_time_avialability">
                      <ul>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <div class="ip_time_avialability">
                      <ul>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <div class="ip_time_avialability">
                      <ul>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_not_avialable">
                            Not Available
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <div class="ip_time_avialability">
                      <ul>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability ip_avialable">
                            Avialable
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li class="borderrightnone">
                    <div class="ip_time_avialability">
                      <ul>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                        <li>
                          <div class="ip_avialability">
                          </div>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <div class="clear"></div>
              </ul>
            </div>

          </div>

        </div>
      </div>
    </div>
  </div>

  </div>
</div>
