

<div class="container">
  <div class="row">
    <div class="col-md-8">
      <div class="ip_edit_record_cover">
  <div class="ip_edit_record_head">
    <?php load_language('future_releases');?>
  </div>
  <div>
    <hr>
    <div class="ip_edit_record_detail">
      <div class="ip_edit_row">
        <div class="ip_budject_list">
          <?php if(!empty($futurereleases))
          {
            foreach ($futurereleases as $key => $value) 
            {
          ?>
              <li>
                <h6 class="m0"><?php echo date('d-m-Y',$value['date']);?></h6>
                <div class="child1"><h5><strong class="uppercase"><?php echo $value['consultation_type'];?>  <?php load_language('consultation');?></strong></h5>
                  <p class="select"><?php echo $value['doctor_name'];?> | <?php echo date('h:i a',$value['time_start']);?></p>
                </div>
                <div class="child2"><strong><b>R$ <?php echo $value['release_amount'];?></b></strong></div>
                <div class="clear"></div>
              </li>
              <hr>
          <?php
            }
          } 
          else
          {
          ?>
            <li>
              
                <div class="child1"><h5><?php load_language('no_future_releases');?></h5>
                </div>
                <div class="clear"></div>
              </li>
              <hr>
          <?php
          }
          ?>
         

        </div>
      </div>
      </div>
    </div>
</div>
    </div>
    <div class="col-md-4">
      
    </div>
    
  </div>
  
</div>

