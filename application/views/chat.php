
<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
<script>
 var config = {
    apiKey: "AIzaSyDTlmsPVmyS_Ca2ODQ-nLBXaMEcMSsuD7Q",
    authDomain: "ipok-178210.firebaseapp.com",
    databaseURL: "https://ipok-178210.firebaseio.com",
    projectId: "ipok-178210",
    storageBucket: "ipok-178210.appspot.com",
    messagingSenderId: "1043527648730"
  };
  firebase.initializeApp(config);
</script>

<div ng-app='myApp'>
<div class="ip_set_two_wrapper"  ng-controller="mainCtrl">
  <div class="container ip_custom_container">
    <div class="ip_main_path_stream">
      <ul>
        <li><?php load_language('dashboard');?><span><img src="<?php echo base_url();?>assets/images/ip_tab_list_arw.png"></span></li>
        <li><?php load_language('messages');?><span><img src="<?php echo base_url();?>assets/images/ip_tab_list_arw.png"></span></li>
        <!-- <li>Bertie Potter<span><img src="<?php echo base_url();?>assets/images/ip_tab_list_arw.png"></span></li> -->
      </ul>
    </div>
    <div class="tab-content">
          <div class="ip_message_tabs_body">
            <h3 ng-if="opponent">{{opponent.name}}</h3>
            <div class="ip_messages_wrapper">
              <div class="row m0">
                <div class="col-md-3 p0">
                  <div class="ip_messages_header">
                    <input class="ip_messages_search_input" ng-model="namefilter" placeholder="Search Contact">
                  </div>
                  <div class="ip_message_sender_list">
                    <div class="ip_bio_message_list" >
                      <ul>
                        <h2><?php load_language('recent_messages');?></h2>
                        <li ng-if="recentUsers&&session.type=='PATIENT'" ng-repeat="recent in recentUsers | filter : {doc_name: namefilter}" ng-click="load_chat(recent)">
                          <div class="ip_bio_message_pic">
                            <img src="<?php echo base_url();?>{{recent.doc_pic}}" ng-if="recent.doc_pic">
                          </div>
                          <div class="ip_bio_messages">
                            <h5 ng-if="recent.doc_name">{{recent.doc_name}}</h5>
                            <div class="ip_message_time">{{recent.time | date: "hh:mm a" }}</div>
                            <div class="clear"></div>
                            <p ng-bind-html="trustAsHtml(recent.msg)"><!-- {{recent.msg}} --></p>
                          </div>
                          <div class="clear"></div>
                        </li>

                        <li ng-if="recentUsers&&(session.type=='DOCTOR'||session.type=='COLLABORATOR')" ng-repeat="recent in recentUsers | filter : {pat_name: namefilter}" ng-click="load_chat(recent)">
                          <div class="ip_bio_message_pic">
                            <img src="<?php echo base_url();?>{{recent.pat_pic}}" ng-if="recent.pat_pic">
                          </div>
                          <div class="ip_bio_messages">
                            <h5 ng-if="recent.pat_name">{{recent.pat_name}}</h5>
                            <div class="ip_message_time">{{recent.time | date: "hh:mm a" }}</div>
                            <div class="clear"></div>
                            <p ng-bind-html="trustAsHtml(recent.msg)"><!-- {{recent.msg}} --></p>
                          </div>
                          <div class="clear"></div>
                        </li>

                        <li ng-if="recentUsers.length==0">
                          <div class="ip_bio_messages">
                            <h5><?php load_language('no_recent_messages');?></h5>
                          </div>
                          <div class="clear"></div>
                        </li>
                       
                      </ul>
                      
                      <ul>
                        <h2><?php load_language('all_messages');?></h2>
                         <li ng-if="allUsers&&(session.type=='DOCTOR'||session.type=='COLLABORATOR')" ng-repeat="users in allUsers | filter : {pat_name: namefilter}" ng-click="load_chat(users)">
                          <div class="ip_bio_message_pic">
                            <img src="<?php echo base_url();?>{{users.pat_pic}}" ng-if="users.pat_pic">
                          </div>
                          <div class="ip_bio_messages">
                            <h5 ng-if="users.pat_pic">{{users.pat_name}}</h5>
                           <!--  <div class="ip_message_time">{{recent.time | date: "hh:mm a" }}</div> -->
                            <div class="clear"></div>
                           <!--  <p>{{recent.msg}}</p> -->
                          </div>
                          <div class="clear"></div>
                        </li>

                         <li ng-if="allUsers&&session.type=='PATIENT'" ng-repeat="users in allUsers | filter : {doc_name: namefilter}" ng-click="load_chat(users)">
                          <div class="ip_bio_message_pic">
                            <img src="<?php echo base_url();?>{{users.doc_pic}}" ng-if="users.doc_pic">
                          </div>
                          <div class="ip_bio_messages">
                            <h5 ng-if="users.doc_name">{{users.doc_name}}</h5>
                           <!--  <div class="ip_message_time">{{recent.time | date: "hh:mm a" }}</div> -->
                            <div class="clear"></div>
                           <!--  <p>{{recent.msg}}</p> -->
                          </div>
                          <div class="clear"></div>
                        </li>

                      </ul>
                    </div>
                  </div>
                </div>

                <div class="col-md-9 p0">
                  <div class="ip_messages_header">
                    <div class="ip_message_text_header" ng-if="opponent">
                      {{opponent.name}}
                    </div>
                    <div class="ip_message_text_header textCenter ip_select_message_chat" ng-if="!opponent">
                      <?php load_language('select_conversation_text');?>
                    </div>
                    <div class="ip_message_more_btn"></div>
                    <!-- <div class="ip_message_search_btn"></div> -->
                    <div class="clear"></div>
                    <!-- <div class="ip_month_date">02 Mar 2017</div> -->
                  </div>
                  <div class="ip_messages_content_div" id="chat_autoscroll">
                    <ul>
                      
                      <li ng-repeat="msg in messages">
                        <div class="ip_month_date" ng-if="msg.show_date"><!-- 02 Mar 2017 -->{{msg.time | date: 'dd MMM yyyy'}}</div>

                        <div class="ip_receive" ng-if="(session.type=='DOCTOR'||session.type=='COLLABORATOR')&&msg.sender_type==0">
                          <div class="ip_message_chat" ng-bind-html="trustAsHtml(msg.message)">
                            <div class="ip_receive_quote"></div>
                            <!-- {{msg.message}} -->
                          </div>
                          <span>{{msg.time | date:'hh:mm a'}}</span>
                        </div>

                        <div class="ip_send" ng-if="(session.type=='DOCTOR'||session.type=='COLLABORATOR')&&msg.sender_type==1">
                          <div class="ip_message_chat" ng-bind-html="trustAsHtml(msg.message)">
                            <div class="ip_send_quote"></div>
                           <!--  {{msg.message}} -->
                          </div>
                          <span>{{msg.time | date:'hh:mm a'}}</span>
                        </div>

                        <div class="ip_receive" ng-if="session.type=='PATIENT'&&msg.sender_type==1">
                          <div class="ip_message_chat" ng-bind-html="trustAsHtml(msg.message)">
                            <div class="ip_receive_quote"></div>
                            <!-- {{msg.message}} -->
                          </div>
                          <span>{{msg.time | date:'hh:mm a'}}</span>
                        </div>
                        <div class="ip_send" ng-if="session.type=='PATIENT'&&msg.sender_type==0">
                          <div class="ip_message_chat" ng-bind-html="trustAsHtml(msg.message)">
                            <div class="ip_send_quote"></div>
                            <!-- {{msg.message}} -->
                          </div>
                          <span>{{msg.time | date:'hh:mm a'}}</span>
                        </div>

                        <div class="clear"></div>
                      </li>

                      <li ng-if="(opponent)&&(!messages)">
                        <div class="ip_no_message_info" >
                           <?php load_language('no_messages');?> 
                          </div>
                        <div class="clear"></div>
                      </li>

                     <!--  <li>
                        <div class="ip_send">
                          <div class="ip_message_chat">
                            <div class="ip_send_quote"></div>
                            type specimen book. It has survived not only five centuries,
                            Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                          </div>
                          <span>01:00 pm</span>
                        </div>
                        <div class="clear"></div>
                      </li> -->
                      
                    </ul>
                  </div>
                  <div class="ip_message_typing_bay" ng-if="opponent">
                      <div class="ip_message_typing_pic">
                        <img src="<?php echo base_url();?>{{session.profile_photo}}">
                      </div>
                      <input class="ip_message_typing_input" ng-enter="sentmsg(variables.texttosent,opponentname)" ng-model="variables.texttosent" placeholder="Enter your message here">
                      <button type="button" class="ip_send_btn uppercase" ng-disabled="!variables.texttosent.length>0" ng-click="sentmsg(variables.texttosent,opponentname)" ><?php load_language('send');?> </button>
                      <!-- <div class="ip_emoji_btn">
                      </div> -->
                      <div class="clear"></div>
                  </div>
                </div>

              </div>
            </div>
          </div>
      </div>
    </div>
</div>
</div>


  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
  <script src="https://cdn.firebase.com/libs/angularfire/2.3.0/angularfire.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-sanitize.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/chat/dist/ng-scrollbar.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/chat/jquery.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/chat/chat-page.js"></script>
