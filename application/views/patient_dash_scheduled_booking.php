      <?php
      if(!empty($confirmed_consultation))
      {
        foreach ($confirmed_consultation as $key => $element) 
        {
      ?>
           
        <li>
            <div class="row m0 height100">
              <div class="col-md-2 p0 height100"><div class="ip_record_main_head_data"><strong><?php load_language('consultation');?>: </strong><?php echo date('d M Y',$element['book_date']);?></div></div>
              <div class="col-md-2 p0 height100"><div class="ip_record_main_head_data"><?php echo $element['book_time']?></div></div>
              <div class="col-md-3 p0 height100"><div class="ip_record_main_head_data">Dr.<?php echo $element['doc_name']?></div></div>
              <!-- onclick="change_consult(<?php echo $element['book_id']?>)" -->

              <?php if($element['status']==1) 
              {
              ?>
              <div class="col-md-2 p0 height100"><div class="ip_record_main_head_data"><button class="ip_reader_btn" ><?php load_language('confirmed');?></button></div></div>
              <?php
              }
              ?>

              <?php if($element['status']==0) 
              {
              ?>
              <div class="col-md-2 p0 height100"><div class="ip_record_main_head_data"><button class="ip_reader_btn" ><?php load_language('waiting_list');?></button></div></div>
              <?php
              }
              ?>


              <div class="col-md-3 p0 height100"><div class="ip_record_main_head_data"><button class="ip_reader_btn" bookingid="<?php echo $element['book_id']?>" onclick="cancel_consult(<?php echo $element['book_id']?>)"><a><?php load_language('cancel_consultation');?></a></button></div></div>
            </div>
        </li>
      <?php 
        }
      }
      else
      {
      ?>
        <li>
            <div class="row m0 height100">
              <div class="col-md-12 p0 height100"><div class="ip_record_main_head_data"><?php load_language('no_scheduled_consultations');?></div></div>
            </div>
        </li>
      <?php
      }
      ?>