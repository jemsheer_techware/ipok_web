
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">
    <div class="ip_edit_record_wrapper">
          <div class="ip_edit_record_cover">

          	<?php
				if($this->session->flashdata('message')) {
				$message = $this->session->flashdata('message');
			?>
				<div class="alert alert-<?php echo $message['class']; ?> alert-dismissible flash-msg">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><strong> <?php echo $message['title']; ?></strong></h4>
				<?php echo $message['message']; ?>
				</div>

			<?php
				}
			?>


          	<form role="form" data-parsley-validate="" id="pat-edt-form" method="POST" action="<?php echo base_url();?>Patient/saveProfile" enctype="multipart/form-data">
            <div class="ip_edit_record_head backgroundnone">
              <?php load_language('edit_your_profile');?>
            </div>
              <div class="ip_edit_record_detail">
				<div class="row">
					<div class="col-md-7">
						<div class="ip_edit_row">
							  <div class="ip_bank_detail_frame">
								  <input name="name" class="ip_bank_input" data-parsley-minlength="5" onKeyPress="if(this.value.length > 40) return false;" data-parsley-pattern="^[a-zA-Z ]+$" type="text" data-parsley-required="true" value="<?php echo decrypt_data($patient_data['pt_name']);?>" placeholder="<?php load_language('name');?>">
							  </div>
						</div>
						<div class="ip_edit_row">
							  <div class="ip_bank_detail_frame">
								  <input class="ip_bank_input" disabled placeholder="<?php load_language('email');?>" value="<?php echo $patient_data['pt_email'];?>">
							  </div>
						</div>
						<div class="ip_edit_row">
							<div class="row">
								<div class="col-md-6">
									<p class="ip_row_p">RG</p>
									<div class="ip_bank_detail_frame">
										<input name="rg" class="ip_bank_input" placeholder="" onKeyPress="if(this.value.length > 25) return false;" data-parsley-maxlength="25"  value="<?php echo decrypt_data($patient_data['pt_rg']);?>">
									</div>
								</div>
								<div class="col-md-6">
									<p class="ip_row_p">CPF</p>
									<div class="ip_bank_detail_frame">
										<input name="cpf" class="ip_bank_input" placeholder="" data-parsley-required="true" data-parsley-minlength="11" data-parsley-cpf="" onKeyPress="if(this.value.length > 10) return false;"  type="number" value="<?php echo $patient_data['pt_cpf'];?>">
									</div>
								</div>
							</div>
						</div>
						<div class="ip_edit_row">
							<div class="row">
								<div class="col-md-6">
									<p class="ip_row_p"><?php load_language('date_of_birth');?></p>
									<div class="ip_bank_detail_frame" id="edit-patient">
										<input name="dob" readonly class="ip_bank_input" placeholder="" data-parsley-required="true" >
									</div>
								</div>
								<div class="col-md-6">
									<p class="ip_row_p"><?php load_language('gender');?></p>
									<div class="ip_bank_detail_frame">
										<select name="gender" class="ip_bank_input" id="pat-edt-gender" data-parsley-required="true" placeholder="">
											<option value="MALE"><?php load_language('male');?></option>
											<option value="FEMALE"><?php load_language('female');?></option>
											<option value="OTHERS"><?php load_language('others');?></option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="ip_edit_row">
							<div class="row">
								<div class="col-md-6">
									<p class="ip_row_p"><?php load_language('username');?></p>
									<div class="ip_bank_detail_frame">
										<input name="username" class="ip_bank_input" placeholder="" data-parsley-required  data-parsley-usernamepatedit=""  data-parsley-minlength="5" onKeyPress="if(this.value.length > 25) return false;" data-parsley-maxlength="25" type="text" data-parsley-pattern="^[a-zA-Z0-9]+$" value="<?php echo $patient_data['pt_username'];?>" >
									</div>
								</div>
								<div class="col-md-6">
									<p class="ip_row_p"><?php load_language('profile_photo');?></p>

									<div class="ip_reg_add_phot_div">
										<button id="pat-edt-pic-btn" class="ip_add_photo_doc"><?php load_language('edit_photo');?>
											<input name="profile_photo" id="pat-edt-pic-inp"  type="file" accept="image/*"   class="ip_reg_form_input reset-form-custom "
											data-parsley-error-message="Choose Profile Photo" onchange="pat_edit_loadthumbnail(this)" placeholder="" >
										</button>
										<div class="ip_reg_modal_addphoto">
											<img src="<?php echo base_url(); echo $patient_data['pt_pic'];?>" id="pat-edt-pic">
										</div>
								
									</div>

									

								</div>
							</div>
						</div>
					
					</div>
					<div class="col-md-5">
						<div class="ip_edit_row">
							  <div class="ip_bank_detail_frame">
								  <input name="zip_code" class="ip_bank_input"  data-parsley-required placeholder="CEP" value="<?php echo decrypt_data($patient_data['pt_zip_code']);?>" onKeyPress="if(this.value.length > 7) return false;" type="number" data-parsley-minlength="8" data-parsley-cep="" data-parsley-pattern="^[0-9]+$">
							  </div>
						</div>
						<div class="ip_edit_row">
							  <div class="ip_bank_detail_frame">
								  <input name="street_address" class="ip_bank_input" maxlength="100" placeholder="Rua" data-parsley-required value="<?php echo decrypt_data($patient_data['pt_street_add']);?>" data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-minlength="5" maxlength="100" type="text">
							  </div>
						</div>
						<div class="ip_edit_row">
							<div class="row">
								<div class="col-md-7">
									<p class="ip_row_p"><?php load_language('neighbourhood');?></p>
									<div class="ip_bank_detail_frame">
										<input name="locality" class="ip_bank_input" data-parsley-required maxlength="100" type="text" placeholder="" data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-minlength="5"  value="<?php echo decrypt_data($patient_data['pt_locality']);?>" >
									</div>
								</div>
								<div class="col-md-5">
									<p class="ip_row_p"><?php load_language('number');?></p>
									<div class="ip_bank_detail_frame">
										<input name="number" class="ip_bank_input" data-parsley-required maxlength="100" onKeyPress="if(this.value.length > 15) return false;" data-parsley-pattern="^[0-9]+$"  type="number" data-parsley-minlength="5" placeholder="" value="<?php echo decrypt_data($patient_data['pt_number']);?>">
									</div>
								</div>
							</div>
						</div>
						<div class="ip_edit_row">
							<div class="row">
								<div class="col-md-6">
									<p class="ip_row_p"><?php load_language('weight');?></p>
									<div class="ip_bank_detail_frame">
										<input name="weight" class="ip_bank_input" placeholder="" data-parsley-type="digits" type="number" onKeyPress="if(this.value.length > 2) return false;" data-parsley-maxlength="3" value="<?php echo decrypt_data($patient_data['pt_weight']);?>">
									</div>
								</div>
								<div class="col-md-6">
									<p class="ip_row_p"><?php load_language('height');?></p>
									<div class="ip_bank_detail_frame">
										<input name="height" class="ip_bank_input" placeholder="" data-parsley-type="digits" onKeyPress="if(this.value.length > 2) return false;" data-parsley-maxlength="3" type="number" value="<?php echo decrypt_data($patient_data['pt_height']);?>">
									</div>
								</div>
							</div>
						</div>
						<div class="ip_edit_row">
							<div class="row">
								<div class="col-md-6">
									<p class="ip_row_p"><?php load_language('bloodgroup');?></p>
									<div class="ip_bank_detail_frame">
										<!-- <select class="ip_bank_input" placeholder="" value="<?php echo $patient_data['pt_blood_group'];?>">
											<option>A +</option>
											<option>A -</option>
										</select> -->
											<input name="blood_group" class="ip_bank_input" placeholder="" value="<?php echo decrypt_data($patient_data['pt_blood_group']);?>" data-parsley-bloodgroup="" onKeyPress="if(this.value.length > 10) return false;" maxlength="100" type="text">
									</div>
								</div>
								<div class="col-md-6">
									
								</div>
							</div>
						</div>
					</div>
				</div>
              </div>
              <hr>
              <div class="ip_coloborator_btn_bay">
                <button class="ip_colaborator_btn" id="pat-edt-savebtn"><?php load_language('update_and_save');?></button>
              </div>
          </div>
    </div>
  </div>
</div>

