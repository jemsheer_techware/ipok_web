
<style>
.ip_reg_modal_addphoto img{width:100%;height:100%;object-fit:cover;object-position:center;border-radius:50%;}
</style>
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">
    <div class="ip_edit_record_wrapper">
          <div class="ip_edit_record_cover">
          	
			<?php
				if($this->session->flashdata('message')) {
				$message = $this->session->flashdata('message');
			?>
				<div class="alert alert-<?php echo $message['class']; ?> alert-dismissible flash-msg">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><strong> <?php echo $message['title']; ?></strong></h4>
				<?php echo $message['message']; ?>
				</div>

			<?php
				}
			?>


          	<form role="form" data-parsley-validate="" id="doc-edt-form" method="POST" action="<?php echo base_url();?>Doctor/saveProfile" enctype="multipart/form-data">
            <div class="ip_edit_record_head backgroundnone">
              <?php load_language('edit_your_profile');?>
            </div>
              <div class="ip_edit_record_detail">
				<div class="row">
					<div class="col-md-7">
						<div class="ip_edit_row">
							  <div class="ip_bank_detail_frame">
								  <input class="ip_bank_input" name="name"  data-parsley-required="true" onKeyPress="if(this.value.length > 40) return false;" data-parsley-minlength="5" data-parsley-pattern="^[a-zA-Z ]+$"  placeholder="<?php load_language('name');?>" value="<?php echo $this->encrypt->decode($doctor_data['dr_name']);?>">
							  </div>
						</div>
						<div class="ip_edit_row">
							  <div class="ip_bank_detail_frame">
								  <input class="ip_bank_input" disabled placeholder="<?php load_language('email');?>" value="<?php echo $doctor_data['dr_email']?>">
							  </div>
						</div>
						<div class="ip_edit_row">
							<div class="row">
								<div class="col-md-6">
									<p class="ip_row_p">RG</p>
									<div class="ip_bank_detail_frame">
										<input class="ip_bank_input" name="rg" maxlength="25" onKeyPress="if(this.value.length > 25) return false;" placeholder="" value="<?php echo $this->encrypt->decode($doctor_data['dr_rg']);?>">
									</div>
								</div>
								<div class="col-md-6">
									<p class="ip_row_p">CPF</p>
									<div class="ip_bank_detail_frame">
										<input class="ip_bank_input" name="cpf" placeholder="" data-parsley-required="true" data-parsley-minlength="11" data-parsley-cpf="" onKeyPress="if(this.value.length > 10) return false;"  type="number" value="<?php echo $doctor_data['dr_cpf'];?>">
									</div>
								</div>
							</div>
						</div>
						<div class="ip_edit_row">
							<div class="row">
								<div class="col-md-6">
									<p class="ip_row_p">CRM</p>
									<div class="ip_bank_detail_frame">
										<input class="ip_bank_input" name="crm" placeholder="" onKeyPress="if(this.value.length > 25) return false;" value="<?php echo $this->encrypt->decode($doctor_data['dr_crm']);?>">
									</div>
								</div>
								<div class="col-md-6">
									<p class="ip_row_p"><?php load_language('telephone');?></p>
									<div class="ip_bank_detail_frame">
										<input class="ip_bank_input" name="telephone"  placeholder="" data-parsley-pattern="^[0-9]+$"  type="number" data-parsley-minlength="5"  onKeyPress="if(this.value.length > 30) return false;" value="<?php echo $this->encrypt->decode($doctor_data['dr_telephone']);?>">
									</div>
								</div>
							</div>
						</div>
						<div class="ip_edit_row">
							<div class="row">
								<div class="col-md-6">
									<p class="ip_row_p"><?php load_language('username');?></p>
									<div class="ip_bank_detail_frame">
										<input class="ip_bank_input" name="username" data-parsley-usernamedocedit=""  data-parsley-minlength="5" onKeyPress="if(this.value.length > 25) return false;" data-parsley-required="true" data-parsley-pattern="^[a-zA-Z0-9]+$" placeholder="" value="<?php echo $doctor_data['dr_username']?>">
									</div>
								</div>
						
								<div class="col-md-6">
									<p class="ip_row_p"><?php load_language('date_of_birth');?></p>
									<div class="ip_bank_detail_frame" id="edit-doctor">
										<!-- <input class="ip_reg_form_input" type="text" form-control" placeholder=""> -->
										<input name="dob" readonly  class="ip_reg_form_input form-control reset-form-custom background_transparent" data-parsley-required="true">
									</div>
								</div>
							</div>
						</div>
						
						<div class="ip_edit_row">
							<div class="row">
							<div class="col-md-6">
							<p class="ip_row_p"><?php load_language('gender');?></p>
									<div class="ip_bank_detail_frame">
										<select name="gender" class="ip_bank_input" id="doc-edt-gender" data-parsley-required="true" placeholder="">
											<option value="MALE"><?php load_language('male');?></option>
											<option value="FEMALE"><?php load_language('female');?></option>
											<option value="OTHERS"><?php load_language('others');?></option>
										</select>
									</div>
							</div>
							<div class="col-md-6">
								<p class="ip_row_p"><?php load_language('profile_photo');?></p>
									<div class="ip_reg_add_phot_div">
										<button class="ip_add_photo_doc"><?php load_language('add_photo');?>
										<input id="doc-edt-pic-inp" type="file" accept="image/*" name="profile_photo" data-parsley-error-message="Choose Profile Photo"  onchange="doc_edit_loadthumbnail(this)">
										</button>
										<div class="ip_reg_modal_addphoto">
											<img src="<?php echo base_url();echo $doctor_data['dr_pic']?>" id="doc-edt-pic">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="ip_edit_row">
							<div class="row">
								<div class="col-md-6">
									<p class="ip_row_p"><?php load_language('price');?></p>
									  <div class="ip_bank_detail_frame">
										  <input data-parsley-required onKeyPress="if(this.value.length > 5) return false;" data-parsley-minlength="2" data-parsley-maxlength="5" class="ip_bank_input" type="number" name="price" value="<?php echo $doctor_data['dr_price']?>" >
									  </div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="ip_edit_row">
							  <div class="ip_bank_detail_frame">
								  <input class="ip_bank_input" name="cep" data-parsley-required placeholder="CEP" onKeyPress="if(this.value.length > 7) return false;" type="number" data-parsley-minlength="8" data-parsley-cep="" data-parsley-pattern="^[0-9]+$" value="<?php echo $this->encrypt->decode($doctor_data['dr_cep']);?>">
							  </div>
						</div>
						<div class="ip_edit_row">
							  <div class="ip_bank_detail_frame">
								  <input class="ip_bank_input" name="street_address" data-parsley-required placeholder="<?php load_language('rua');?>" data-parsley-maxlength="50" onKeyPress="if(this.value.length > 50) return false;" data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-minlength="5" value="<?php echo $this->encrypt->decode($doctor_data['dr_rua']);?>">
							  </div>
						</div>
						<div class="ip_edit_row">
							<div class="row">
								<div class="col-md-7">
									<div class="ip_bank_detail_frame">
										<input class="ip_bank_input" name="locality" data-parsley-required placeholder="<?php load_language('neighbourhood');?>"  onKeyPress="if(this.value.length > 50) return false;" data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-minlength="5"  data-parsley-maxlength="50" value="<?php echo $this->encrypt->decode($doctor_data['dr_neighbourhood']);?>">
									</div>
								</div>
								<div class="col-md-5">
									<div class="ip_bank_detail_frame">
										<input  class="ip_bank_input" name="number" data-parsley-required placeholder="<?php load_language('number');?>"  data-parsley-pattern="^[0-9]+$"  type="number" data-parsley-minlength="5" data-parsley-maxlength="30" onKeyPress="if(this.value.length > 30) return false;" value="<?php echo $this->encrypt->decode($doctor_data['dr_number']);?>">
									</div>
								</div>
							</div>
						</div>
						<div class="ip_edit_row">
							  <div class="ip_bank_detail_frame">
								  <input class="ip_bank_input" name="complement" placeholder="<?php load_language('complement');?>" onKeyPress="if(this.value.length > 50) return false;" data-parsley-maxlength="50" value="<?php echo $this->encrypt->decode($doctor_data['dr_complement']);?>">
							  </div>
						</div>
						<div class="ip_edit_row">
							<p class="ip_row_p"><?php load_language('biography');?></p>
							  <div class="ip_bank_detail_frame" style="height:auto;">
								  <textarea class="ip_bank_input" name="about" placeholder="BIOGRAPHY" data-parsley-required rows="8" data-parsley-maxlength="1000" data-parsley-minlength="15" onKeyPress="if(this.value.length > 1000) return false;"><?php echo $this->encrypt->decode($doctor_data['dr_bio']);?></textarea>
							  </div>
						</div>
						<div class="ip_edit_row">
							<p class="ip_row_p"><?php load_language('specialization');?></p>
							  <div class="ip_bank_detail_frame">
								 <!--  <input class="ip_bank_input" placeholder="Specialization"> -->
								  <select class="ip_bank_input" placeholder="" data-parsley-required name="specialization" id="doc-edt-specialization">
					               	<option disabled selected><?php load_language('speciality');?></option>
								    <?php foreach ($speciality_list as $key => $value) {
								    ?>
								    	<option value="<?php echo $value['id']?>"><?php echo $value['specialization_name']?></option>
							        <?php	
							        }
							        ?>
			  						</select>
							  </div>
						</div>
					
						
					</div>
				</div>
              </div>
              <hr>
              <div class="ip_coloborator_btn_bay">
                <button class="ip_colaborator_btn" type="submit"><?php load_language('update_and_save');?></button>
              </div>
          </form>
          </div>
    </div>
  </div>
</div>

