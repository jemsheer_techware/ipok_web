<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">
    <div class="ip_top_dash_bay">
      <div class="row">
        <div class="col-md-3">
          <div class="ip_top_dash_list">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_appointments.png">
            </div>
            <div class="ip_top_dash_detail">
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_attendance']?>">0</strong>
              <p><?php load_language('attendance_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="ip_top_dash_list">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_feature.png">
            </div>
            <div class="ip_top_dash_detail" >
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_billed']?>">0</strong>
              <p><?php load_language('billed_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="ip_top_dash_list">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_paintences.png">
            </div>
            <div class="ip_top_dash_detail">
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_patients']?>">0</strong>
              <p><?php load_language('patients_visited_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="ip_top_dash_list bordernone">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_vistors.png">
            </div>
            <div class="ip_top_dash_detail">
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_profileview']?>">0</strong>
              <p><?php load_language('profile_views_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="ip_bio_tab_div">
      <div class="row m0">
        <div class="col-md-2 p0 height100">
          <div class="ip_bio_tab_bay height100">
            <ul>
              <li class="active" data-toggle="tab" href="#profile"><?php load_language('profile');?></li>
              <li data-toggle="tab" href="#bio"><?php load_language('biography');?></li>
              <li class="arrow" data-toggle="tab" href="#special"><?php load_language('specialization');?></li>
             <!--  <li data-toggle="tab" href="#photo">Photos</li>
              <li data-toggle="tab" href="#more" class="arrow">More</li> -->
            </ul>
          </div>
        </div>
        <div class="col-md-10 p0">
          <div class="ip_bio_tab_content">
            <div class="tab-content">
              <div id="profile" class="tab-pane fade in active">
                <div class="ip_profile_tab_top">
                  <div class="ip_profile_tab_circle">
                    <img src="<?php echo base_url();echo $doctor_data['dr_pic'];?>">
                  </div>
                  <div class="ip_profile_tab_name">
                    <h3>Dr. <?php echo $doctor_data['dr_name']; ?></h3>
                    
                  </div>
                  <div class="ip_profile_tab_button">
                    <div class="ip_profile_tab_button_circle"><a href="<?php echo base_url();?>doctor/editprofile"><img src="<?php echo base_url();?>assets/images/ip_edit.png"></a></div>
                    <a href="javascript:void(0)" class="doc-delete-profile">
                    <div class="ip_profile_tab_button_circle"><img src="<?php echo base_url();?>assets/images/ip_delete.png">
                    </div>  
                    </a>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                </div>
                <div class="ip_profile_tab_detail">
                  <div class="row">
                    <div class="col-md-6">
                      <ul>
                        <li>
                          <div class="child1"><?php load_language('email');?> :</div>
                          <div class="child2"><?php echo $doctor_data['dr_email'] ?></div>
                          <div class="clear"></div>
                        </li>
                        <li>
                          <div class="child1"><?php load_language('phone');?> :</div>
                          <div class="child2"><?php echo $this->encrypt->decode($doctor_data['dr_telephone']); ?></div>
                          <div class="clear"></div>
                        </li>
                        <!-- <li>
                          <div class="child1">Site :</div>
                          <div class="child2">www.dummy.com</div>
                          <div class="clear"></div>
                        </li>
                        <li>
                          <div class="child1">Company :</div>
                          <div class="child2">Dummy</div>
                          <div class="clear"></div>
                        </li> -->
                        <li>
                          <div class="child1"><?php load_language('job_title');?> :</div>
                          <div class="child2"><?php echo $doctor_data["dr_specialization"];?></div>
                          <div class="clear"></div>
                        </li>
                      </ul>
                    </div>
                    <div class="col-md-6">
                      <ul>
                        <?php if(!empty($doctor_data['dr_dob']))
                        {?>
                          <li>
                          <div class="child1"><?php load_language('birthday');?> :</div>
                          <div class="child2"><?php echo date('d F Y',$doctor_data["dr_dob"]);?></div>
                          <div class="clear"></div>
                        </li>
                        <?php
                        }
                        ?>
                        <li>
                          <div class="child1"><?php load_language('current_city');?> :</div>
                          <div class="child2"><?php echo $this->encrypt->decode($doctor_data["dr_neighbourhood"]);?></div>
                          <div class="clear"></div>
                        </li>
                        <!-- <li>
                          <div class="child1">Studied at :</div>
                          <div class="child2">Harward University</div>
                          <div class="clear"></div>
                        </li> -->
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div id="bio" class="tab-pane fade">
                <div class="ip_profile_tab_top">
                  <div class="ip_profile_tab_circle">
                    <img src="<?php echo base_url();echo $doctor_data['dr_pic'];?>">
                  </div>
                  <div class="ip_profile_tab_name">
                    <h3>Dr. <?php echo $doctor_data['dr_name']; ?></h3>
                    
                  </div>
                  <div class="ip_profile_tab_button">
                    <div class="ip_profile_tab_button_circle"><a href="<?php echo base_url();?>doctor/editprofile"><img src="<?php echo base_url();?>assets/images/ip_edit.png"></a></div>
                    <a href="javascript:void(0)" class="doc-delete-profile">
                    <div class="ip_profile_tab_button_circle"><img src="<?php echo base_url();?>assets/images/ip_delete.png"></div> 
                    </a>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                </div>
                <div class="ip_profile_tab_detail">
                  <p class="ip_row_p"><?php echo $this->encrypt->decode($doctor_data["dr_bio"]);?></p>
                </div>
              </div>
              <div id="special" class="tab-pane fade">
                  <div class="ip_profile_tab_top">
                  <div class="ip_profile_tab_circle">
                    <img src="<?php echo base_url();echo $doctor_data['dr_pic'];?>">
                  </div>
                  <div class="ip_profile_tab_name">
                    <h3>Dr. <?php echo $doctor_data['dr_name']; ?></h3>
                    
                  </div>
                  <div class="ip_profile_tab_button">
                    <div class="ip_profile_tab_button_circle"><a href="<?php echo base_url();?>doctor/editprofile"><img src="<?php echo base_url();?>assets/images/ip_edit.png"></a></div>
                    <a href="javascript:void(0)" class="doc-delete-profile">
                    <div class="ip_profile_tab_button_circle"><img src="<?php echo base_url();?>assets/images/ip_delete.png"></div> 
                    </a>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                </div>
                <div class="ip_profile_tab_detail">
                  <div class="row">
                    <div class="col-md-10">
                       <p class="ip_row_p"><?php load_language('specialization');?></p>
                      <div class="ip_bank_detail_frame">
                        <input class="ip_bank_input" value="<?php echo $doctor_data["dr_specialization"];?>" readonly="">
                      </div>
                    </div>
                    
                  </div>
                 <!--  <h3><?php echo $doctor_data["dr_specialization"];?></s></h3> -->
                </div>
              </div>
             <!--  <div id="photo" class="tab-pane fade">
                <div class="ip_profile_tab_detail">
                  <h3>Photos</h3>
                </div>
              </div>
              <div id="more" class="tab-pane fade">
                <div class="ip_profile_tab_detail">
                  <h3>More</h3>
                </div>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
             
    <div class="ip_grid_cols">
      <div class="row">
        <div class="col-md-4">
          <div class="ip_bio_tab_div">
            <div class="ip_bio_head">
              <?php load_language('attendance_text');?>
              <div class="ip_bio_more">
              </div>
            </div>
            <div class="ip_bio_detail textCenter">
              <div class="ip_attendence_circle">
              <div class="c100 p25">
                    <span><strong><?php echo $attendence['attendence_today'];?></strong></span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <div class="clear"></div>
              </div>
              <p><?php load_language('total_attendence_today');?></p>
              <div class="ip_bio_bottom_bay">
                <li>
                  <strong><?php echo $attendence['attendence_week'];?></strong>
                  <p><?php load_language('week');?></p>
                </li>
                <li>
                  <strong><?php echo $attendence['attendence_month'];?></strong>
                  <p><?php load_language('month');?></p>
                </li>
                <li>
                  <strong><?php echo $attendence['attendence_year'];?></strong>
                  <p><?php load_language('year');?></p>
                </li>
                <div class="clear"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="ip_bio_tab_div">
            <div class="ip_bio_head">
              <?php load_language('notification');?>
              <div class="ip_bio_more">
              </div>
            </div>

            <div class="ip_bio_detail">
              <div class="ip_bio_notification_list">
                <ul>
                  <?php 
                  if(!empty($notifications))
                  {
                    foreach ($notifications as $key => $value) 
                    {
                  ?>
                      <li>
                        <h5><?php echo $value['type_desc'];?>
                        <div class="ip_notification_time"><?php echo change_time_to_local($value['time'])?></div>
                        </h5>
                        <p><?php echo $value['message'];?></p>
                      </li>
                  <?php
                    }
                  }
                  else
                  {
                  ?>
                    <li>
                        <p><?php load_language('no_notification');?>!</p>
                    </li>
                  <?php
                  }
                  ?>
                          
                </ul>
              </div>
            </div>

          </div>
        </div>
        <div class="col-md-4">
          <div class="ip_bio_tab_div">
            <div class="ip_bio_head">
              <?php load_language('messages');?>
              <div class="ip_bio_more">
              </div>
            </div>
            <div class="ip_bio_detail">
              <div class="ip_bio_message_list">
                <ul>

                  
                    <?php if(!empty($recent))
                    {
                      foreach ($recent as $key => $elm) 
                      {
                    ?>
                          <li>
                         <a href="<?php echo base_url();?>Doctor/chat">
                          <div class="ip_bio_message_pic">
                            <img src="<?php echo base_url(); echo $elm['pat_pic']?>">
                          </div>
                          <div class="ip_bio_messages">
                            <h5><?php echo $this->encrypt->decode($elm['pat_name']);?></h5><div class="ip_message_time"><?php echo change_time_to_local($elm['time'])?></div>
                            <div class="clear"></div>
                            <p><?php echo $this->encrypt->decode($elm['msg']);?></p>
                          </div>
                          <div class="clear"></div>
                          </a>
                          </li>
                    <?php  
                      }
                    } ?>

                 
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="ip_full_calender_div">
          <div class="ip_full_calender_head">
            <div class="ip_full_calender_nav">
              <div class="btn-group">
                <button type="button" class="btn" disabled id="appointments_day_prevbtn"><img src="<?php echo base_url();?>assets/images/ip_arw_left.png"></button>
                <button type="button" class="btn" id="appointments_day_nextbtn"><img src="<?php echo base_url();?>assets/images/ip_arw_right.png"></button>
              </div>
              <div class="btn-group">
                <button type="button" class="btn ip_apppointment_btn_custom" id="appointments_day_todaybtn"><a><?php load_language('today');?></a></button>
              </div>
            </div>
            <h3><?php load_language('appointment');?></h3>
            <div class="ip_record_settings">
              <div class="btn-group ip_custom_tabs_menu">
                <button type="button" class="btn ip_apppointment_btn_custom current dctr_dash_appoint_day"><a class="uppercase" href="#tab-1"><?php load_language('day');?></a></button>
                <button type="button" class="btn ip_apppointment_btn_custom dctr_dash_appoint_week"><a class="uppercase" href="#tab-2"><?php load_language('week');?></a></button>
                <button type="button" class="btn ip_apppointment_btn_custom dctr_dash_appoint_month"><a class="uppercase" href="#tab-3"><?php load_language('month');?></a></button>
              </div>
                <span class="settings"><img src="<?php echo base_url();?>assets/images/ip_settings.png"></span>
              </div>
          </div>
          <div class="ip_full_calender_content">
            <div class="ip_custom_tab">

              <div id="tab-1"  class="ip_period_section ip_custom_tab_content">
                <div class="row m0">
                  <div class="col-md-9 p0">
                    <div class="ip_day_scheduleler">
                      <ul>
                        <li class="ip_current_date">08</li>
                        <li class="ip_current_month">September</li>
                        <div class="clear"></div>
                      </ul>
                      <div class="ip_day_space"></div>

                      <ul class="ip_day_listing" id="ip-appointments-day">
                        <?php $this->load->view('doctor_dash_appointments_day'); ?>
                      </ul>

                    </div>
                  </div>
                  <div class="col-md-3 p0">
                    <div class="ip_appointment_calender">
                      <div class="ip_current_day_frame">
                       <!-- value="<?php echo date('m/d/Y');?>"  -->
                        <!-- <input class="ip_current_day" id="ip_appointment_calender" value="<?php echo date('m/d/Y');?>"  placeholder="Select Date"  /> -->
                       <input type="hidden" id="ip_appointment_calender" value="<?php echo date('m/d/Y');?>" />
                       <div id="ip_appointment_calender_div"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div id="tab-2" class="ip_period_section ip_custom_tab_content">
                <div class="ip_table_head">
                  <ul>
                      <!-- <li class="time_slot"></li>
                      <li>MON, 3</li>
                      <li>TUES, 4</li>
                      <li>WED, 5</li>
                      <li>THUR, 6</li>
                      <li>FRI, 7</li>
                      <li>SAT, 8</li>
                      <li class="borderrightnone">SUN, 9</li> -->
                      <li class="time_slot"></li>
                      <?php 
                        //$today =date('N',time());
                        for ($i=0; $i < 7; $i++) { 
                          $day = date('D',strtotime('+'.$i.'day'));
                          $dayno = date('d',strtotime('+'.$i.'day'));

                          ?>
                          <li><?php echo $day.','. $dayno;?></li>
                          <?php
                        }
                        
                      ?>
                     
                      <div class="clear"></div>
                  </ul>
                </div>
                <div class="ip_table_head_divide">
                  <ul>
                      <li class="time_slot"></li>
                      <li></li>
                      <li></li>
                      <li></li>
                      <li></li>
                      <li></li>
                      <li></li>
                      <li class="borderrightnone"></li>
                      <div class="clear"></div>
                  </ul>
                </div>
                <div class="ip_table_days">
                  <ul id="dctr_week_appointment">
                     <?php $this->load->view('doctor_dash_appointments_week');?>
                  </ul>
                </div>
              </div>
              <div id="tab-3" class="ip_period_section ip_custom_tab_content">
                <div class="ip_month_schedule">
                  <div class="ip_month_schedule_head">
                    <ul>
                      <li class="uppercase"><?php load_language('monday');?></li>
                      <li class="uppercase"><?php load_language('tuesday');?></li>
                      <li class="uppercase"><?php load_language('wednesday');?></li>
                      <li class="uppercase"><?php load_language('thursday');?></li>
                      <li class="uppercase"><?php load_language('friday');?></li>
                      <li class="uppercase"><?php load_language('saturday');?></li>
                      <li class="uppercase"><?php load_language('sunday');?></li>
                      <div class="clear"></div>
                    </ul>
                  </div>
                  <div class="ip_month_schedule_dates">
                    <ul id="dctr_month_appointment">
                       <?php $this->load->view('doctor_dash_appointments_month'); ?>
                      <div class="clear"></div>
                    </ul>
                  </div>
                </div>
              </div>
          </div>
        </div>
        </div>
      </div>
    </div>
    <div class="ip_grid_cols">
      <div class="row">
        <div class="col-md-8">
          <div class="ip_schedule_div">
            <form data-parsley-validate="" role="form" id="doc_sch_sub_form">
            <!--  <div id="loading" style="display: none;">
                <img id="loading-image" src="<?php echo base_url();?>assets/images/ipok-loading.gif" alt="Loading..." />
              </div> -->
            <div class="ip_schedule_head">
              <div class="ip_bio_head bordernone floatLeft">
                <?php load_language('main_schedule');?>
              </div>
              <div class="ip_head_button floatRight">
                <div class="floatLeft mr5">
                  <select class="ip_select_clinic_input" data-parsley-consultduration="" data-parsley-required=""  name="dct_sch_clinic" id="doc_sel_clinic" >
                      <option disabled selected><?php load_language('select_clinic');?></option>
                      <?php foreach ($clinic_list as $key => $value) {
                        //if($value['clinic_name'] == 'default') { ?>
                        <!--  <option disabled selected value="<?php echo $value['clinic_id']?>"><?php echo $value['clinic_name']?></option> -->
                      <option value="<?php echo $value['clinic_id']?>"><?php echo $value['clinic_name']?></option>
                      <?php 
                      }
                      ?>
                  </select>
                </div>
                <div class="floatLeft hidden"> <!-- DIV FOR NEXT AND PREV IN CLINIC SELECT -->
                  <div class="btn-group btn-group-sm">
                      <button type="button" class="btn ip_bio_head_btn"><img src="<?php echo base_url();?>assets/images/ip_arw_left.png"></button>
                      <button type="button" class="btn ip_bio_head_btn" id="doc_sel_clinic_next"><img src="<?php echo base_url();?>assets/images/ip_arw_right.png"></button>
                  </div>
                </div>
                <div class="clear"></div>
              </div>
              <div class="clear"></div>
            </div>
            <div class="ip_schedule_week">
                <div class="ip_day_time_schedule_details_data p0">
                      <div class="textLeft">
                       <input disabled="" id="choose-schedule-primary" name="active_schedule_type" class="ip_custom_checkbox1 ip_gender_check_checkbox" type="radio" value="0"  >
                       <label for="choose-schedule-primary" class="ip_custom_checkbox_label1 ip_gender_check_label t0"><?php load_language('primary');?></label>
              
                       <input disabled="" id="choose-schedule-secondary" name="active_schedule_type" class="ip_custom_checkbox1 ip_gender_check_checkbox " required="" value="1"  type="radio" data-parsley-required="" >
                       <label for="choose-schedule-secondary" class="ip_custom_checkbox_label1 ip_gender_check_label t0"><?php load_language('secondary');?></label>
                     
                      <div class="clear"></div>
                    </div>
                  </div>
              </div>
              <div class="ip_doc_dash_tab">
                <ul>
                  <li class="active" data-toggle="tab" href="#primary"><?php load_language('primary_schedule');?></li>
                  <li  data-toggle="tab" href="#secondary"><?php load_language('secondary_schedule');?></li>
                  <div class="clear"></div>
                </ul>
                <div id="doc_dash_agenda_error" class="ip_parley_validation_error hidden">
                  <?php load_language('configure_schedule_error');?>
                </div>
              </div>



          <div class="tab-content">
            <div id="primary" class="tab-pane fade in active">
              <div class="ip_schedule_week">
                <li>
                  <input id="clinic_day_mon"  disabled="" type="checkbox" name="dct_sch_day[]" value="mon">
                  <label for="clinic_day_mon"><?php load_language('monday');?></label>
                </li>
                <li>
                  <input id="clinic_day_tue" disabled="" type="checkbox" name="dct_sch_day[]" value="tue">
                  <label for="clinic_day_tue"><?php load_language('tuesday');?></label>
                </li>
                <li>
                  <input id="clinic_day_wed" disabled="" type="checkbox" name="dct_sch_day[]" value="wed">
                  <label for="clinic_day_wed"><?php load_language('wednesday');?></label>
                </li>
                <li>
                  <input id="clinic_day_thu" disabled="" type="checkbox" name="dct_sch_day[]" value="thu">
                  <label for="clinic_day_thu"><?php load_language('thursday');?></label>                  
                </li>
                <li>
                  <input id="clinic_day_fri" disabled="" type="checkbox" name="dct_sch_day[]" value="fri">
                  <label for="clinic_day_fri"><?php load_language('friday');?></label>     
                </li>
                <li>
                  <input id="clinic_day_sat" disabled="" type="checkbox" name="dct_sch_day[]" value="sat">
                  <label for="clinic_day_sat"><?php load_language('saturday');?></label>     
                </li>
                <li>
                  <input id="clinic_day_sun" data-parsley-mincheck="1" data-parsley-required=""  disabled="" type="checkbox" name="dct_sch_day[]" value="sun">
                  <label for="clinic_day_sun"><?php load_language('sunday');?></label>     
                </li>
                <div class="clear"></div>
              </div>

              <div class="ip_schedule_timing">
                <li id="clinic_day_mon_div" class="inp-dis">
                  <div class="row">
                    <div class="col-md-6">
                      <h6><?php load_language('monday');?></h6>
                      <!-- <select class="ip_schedule_timing_input floatLeft">
                        <option>05:25 PM</option>   </select>-->
                        <input disabled="" readonly="" id="sch_mon_start"  class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " name="dct_sch_mon_start"    placeholder="" >
                        <input disabled="" readonly="" data-parsley-mintime= "#sch_mon_start" name="dct_sch_mon_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sch_mon_end">
                    
                     <!--  <select class="ip_schedule_timing_input floatRight">
                        <option>05:25 PM</option>
                      </select> -->
                      <div class="clear"></div>
                    </div>
                    <div class="col-md-6">
                      <h6 class="ip_schedule_check">
                          <!-- <input id="checkbox-1" class="ip_custom_checkbox" name="checkbox-1" type="checkbox" checked> -->
                          <!-- <p class="ip_custom_checkbox_label">Interval</p> -->
                           <input disabled id="intr_chkbx_mon" class="ip_custom_checkbox"  type="checkbox" >
                           <label for="intr_chkbx_mon" class="ip_custom_checkbox_label"><?php load_language('interval');?></label>
                      </h6>
                   <!--    <select  disabled="" id="sch_mon_int" name="dct_sch_mon_int" class="ip_schedule_timing_input floatLeft">
                        <option disabled selected>Time</option>
                        <?php 
                        for ($i = 1; $i <= 59; $i++) {
                          ?>
                         <option value="<?php echo $i?>"><?php echo $i?> min(s)</option>
                        <?php
                        }
                        ?>
                      </select> -->
                      <input disabled="" readonly="" id="intr_mon_start"  class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " data-parsley-mintime ="#sch_mon_start" name="dct_intr_mon_start"    placeholder="" >
                      <input disabled="" readonly=""  data-parsley-mintime="#intr_mon_start" data-parsley-maxtime = "#sch_mon_end" name="dct_intr_mon_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="intr_mon_end">

                      <div class="clear"></div>
                    </div>
                  </div>
                </li>
                <li id="clinic_day_tue_div" class="inp-dis">
                  <div class="row">
                    <div class="col-md-6">
                      <h6><?php load_language('tuesday');?></h6>
                    <!--   <select class="ip_schedule_timing_input floatLeft">
                        <option>05:25 PM</option>
                      </select>
                      <select class="ip_schedule_timing_input floatRight">
                        <option>05:25 PM</option>
                      </select> -->
                        <input disabled="" readonly="" id="sch_tue_start" name="dct_sch_tue_start" class="ip_time floatLeft  ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" >
                        <input disabled="" readonly="" data-parsley-mintime = "#sch_tue_start" name="dct_sch_tue_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sch_tue_end">
                      <div class="clear"></div>
                    </div>
                    <div class="col-md-6">
                      <h6 class="ip_schedule_check">
                          <!-- <input id="checkbox-2" class="ip_custom_checkbox" name="checkbox-2" type="checkbox" checked> -->
                         <!--  <p class="ip_custom_checkbox_label">Interval</p> -->
                          <input disabled id="intr_chkbx_tue" class="ip_custom_checkbox" type="checkbox" >
                          <label for="intr_chkbx_tue" class="ip_custom_checkbox_label"><?php load_language('interval');?></label>
                      </h6>

                     <!--  <select disabled="" id="sch_tue_int" name="dct_sch_tue_int" class="ip_schedule_timing_input floatLeft">
                        <option disabled selected>Time</option>
                        <?php 
                        for ($i = 1; $i <= 59; $i++) {
                          ?>
                         <option value="<?php echo $i?>"><?php echo $i?> min(s)</option>
                        <?php
                        }
                        ?>
                      </select> -->
                      <input disabled="" readonly="" id="intr_tue_start"  class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " data-parsley-mintime ="#sch_tue_start" name="dct_intr_tue_start"    placeholder="" >
                      <input disabled="" readonly="" data-parsley-mintime = "#intr_tue_start" data-parsley-maxtime = "#sch_tue_end" name="dct_intr_tue_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="intr_tue_end">
                      <div class="clear"></div>
                    </div>
                  </div>
                </li>
                <li id="clinic_day_wed_div" class="inp-dis">
                  <div class="row">
                    <div class="col-md-6">
                      <h6><?php load_language('wednesday');?></h6>
                     <!--  <select class="ip_schedule_timing_input floatLeft">
                        <option>05:25 PM</option>
                      </select>
                      <select class="ip_schedule_timing_input floatRight">
                        <option>05:25 PM</option>
                      </select> -->
                        <input disabled="" readonly="" id="sch_wed_start"  name="dct_sch_wed_start" class="ip_time floatLeft  ip_schedule_timing_input dctr_dsh_timepicker" placeholder="">
                        <input disabled="" readonly="" data-parsley-mintime="#sch_wed_start" name="dct_sch_wed_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sch_wed_end">
                      <div class="clear"></div>
                    </div>
                    <div class="col-md-6">
                      <h6 class="ip_schedule_check">
                          <!-- <input id="checkbox-3" class="ip_custom_checkbox" name="checkbox-3" type="checkbox" checked> -->
                          <!-- <p class="ip_custom_checkbox_label">Interval</p> -->
                          <input disabled id="intr_chkbx_wed" class="ip_custom_checkbox" type="checkbox" >
                          <label for="intr_chkbx_wed" class="ip_custom_checkbox_label"><?php load_language('interval');?></label>
                      </h6>
        
                    <!--   <select id="sch_wed_int" disabled=""  name="dct_sch_wed_int" class="ip_schedule_timing_input floatLeft">
                         <option disabled selected>Time</option>
                        <?php 
                        for ($i = 1; $i <= 59; $i++) {
                          ?>
                         <option value="<?php echo $i?>"><?php echo $i?> min(s)</option>
                        <?php
                        }
                        ?>
                      </select> -->
                      <input disabled="" readonly="" id="intr_wed_start" data-parsley-mintime ="#sch_wed_start"  class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " name="dct_intr_wed_start"    placeholder="" >
                      <input disabled="" readonly="" data-parsley-mintime = "#intr_wed_start" data-parsley-maxtime = "#sch_wed_end" name="dct_intr_wed_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="intr_wed_end">
                      <div class="clear"></div>
                    </div>
                  </div>
                </li>
                <li id="clinic_day_thu_div" class="inp-dis">
                  <div class="row">
                    <div class="col-md-6">
                      <h6><?php load_language('thursday');?></h6>
                      <!-- <select class="ip_schedule_timing_input floatLeft">
                        <option>05:25 PM</option>
                      </select>
                      <select class="ip_schedule_timing_input floatRight">
                        <option>05:25 PM</option>
                      </select> -->
                        <input disabled="" readonly="" id="sch_thu_start" name="dct_sch_thu_start" class="ip_time floatLeft  ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" >
                        <input disabled="" readonly="" data-parsley-mintime="#sch_thu_start" name="dct_sch_thu_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sch_thu_end">
                      <div class="clear"></div>
                    </div>
                    <div class="col-md-6">
                      <h6 class="ip_schedule_check">
                          <!-- <input id="checkbox-4" class="ip_custom_checkbox" name="checkbox-4" type="checkbox" checked> -->
                          <!-- <p class="ip_custom_checkbox_label">Interval</p> -->
                          <input disabled id="intr_chkbx_thu" type="checkbox" class="ip_custom_checkbox">
                          <label for="intr_chkbx_thu" class="ip_custom_checkbox_label"><?php load_language('interval');?></label>
                      </h6>
      
                     <!--  <select disabled="" id="sch_thu_int"  name="dct_sch_thu_int" class="ip_schedule_timing_input floatLeft">
                         <option disabled selected>Time</option>
                        <?php 
                        for ($i = 1; $i <= 59; $i++) {
                          ?>
                         <option value="<?php echo $i?>"><?php echo $i?> min(s)</option>
                        <?php
                        }
                        ?>
                      </select> -->
                       <input disabled="" readonly="" id="intr_thu_start" data-parsley-mintime ="#sch_thu_start"  class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " name="dct_intr_thu_start"    placeholder="" >
                      <input disabled="" readonly="" data-parsley-mintime = "#intr_thu_start" data-parsley-maxtime = "#sch_thu_end" name="dct_intr_thu_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="intr_thu_end">
                      <div class="clear"></div>
                    </div>
                  </div>
                </li>
                <li id="clinic_day_fri_div" class="inp-dis">
                  <div class="row">
                    <div class="col-md-6">
                      <h6><?php load_language('friday');?></h6>
                      <!-- <select class="ip_schedule_timing_input floatLeft">
                        <option>05:25 PM</option>
                      </select>
                      <select class="ip_schedule_timing_input floatRight">
                        <option>05:25 PM</option>
                      </select> -->
                        <input disabled="" readonly="" id="sch_fri_start" name="dct_sch_fri_start" class="ip_time floatLeft  ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" >
                        <input disabled="" readonly="" data-parsley-mintime="#sch_fri_start" name="dct_sch_fri_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sch_fri_end">
                      <div class="clear"></div>
                    </div>
                    <div class="col-md-6">
                      <h6 class="ip_schedule_check">
                          <!-- <input id="checkbox-5" class="ip_custom_checkbox" name="checkbox-5" type="checkbox" checked> -->
                          <!-- <p class="ip_custom_checkbox_label">Interval</p> -->
                          <input disabled id="intr_chkbx_fri" class="ip_custom_checkbox" type="checkbox" >
                          <label for="intr_chkbx_fri" class="ip_custom_checkbox_label"><?php load_language('interval');?></label>
                      </h6>
        
                     <!--  <select disabled="" id="sch_fri_int"  name="dct_sch_fri_int" class="ip_schedule_timing_input floatLeft">
                         <option disabled selected>Time</option>
                        <?php 
                        for ($i = 1; $i <= 59; $i++) {
                          ?>
                         <option value="<?php echo $i?>"><?php echo $i?> min(s)</option>
                        <?php
                        }
                        ?>
                      </select> -->
                      <input disabled="" readonly="" id="intr_fri_start" data-parsley-mintime ="#sch_fri_start"  class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " name="dct_intr_fri_start"    placeholder="" >
                      <input disabled="" readonly="" data-parsley-mintime = "#intr_fri_start" data-parsley-maxtime = "#sch_fri_end" name="dct_intr_fri_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="intr_fri_end">
                      <div class="clear"></div>
                    </div>
                  </div>
                </li>
                <li id="clinic_day_sat_div" class="inp-dis">
                  <div class="row">
                    <div class="col-md-6">
                      <h6><?php load_language('saturday');?></h6>
                    <!--   <select class="ip_schedule_timing_input floatLeft">
                        <option>05:25 PM</option>
                      </select>
                      <select class="ip_schedule_timing_input floatRight">
                        <option>05:25 PM</option>
                      </select> -->
                        <input disabled="" readonly="" id="sch_sat_start" name="dct_sch_sat_start" class="ip_time floatLeft  ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" >
                        <input disabled="" readonly="" data-parsley-mintime="#sch_sat_start" name="dct_sch_sat_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sch_sat_end">
                      <div class="clear"></div>
                    </div>
                    <div class="col-md-6">
                      <h6 class="ip_schedule_check">
                          <!-- <input id="checkbox-6" class="ip_custom_checkbox" name="checkbox-6" type="checkbox" checked> -->
                          <!-- <p class="ip_custom_checkbox_label">Interval</p> -->
                          <input disabled id="intr_chkbx_sat" class="ip_custom_checkbox" type="checkbox" >
                          <label for="intr_chkbx_sat" class="ip_custom_checkbox_label"><?php load_language('interval');?></label>
                      </h6>
 
                      <!-- <select disabled="" id="sch_sat_int" name="dct_sch_sat_int" class="ip_schedule_timing_input floatLeft">
                         <option disabled selected>Time</option>
                        <?php 
                        for ($i = 1; $i <= 59; $i++) {
                          ?>
                         <option value="<?php echo $i?>"><?php echo $i?> min(s)</option>
                        <?php
                        }
                        ?>
                      </select> -->
                       <input disabled="" readonly="" id="intr_sat_start" data-parsley-mintime ="#sch_sat_start" class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " name="dct_intr_sat_start"    placeholder="" >
                      <input disabled="" readonly="" data-parsley-mintime ="#intr_sat_start" data-parsley-maxtime = "#sch_sat_end" name="dct_intr_sat_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="intr_sat_end">
                      <div class="clear"></div>
                    </div>
                  </div>
                </li>
                <li id="clinic_day_sun_div" class="inp-dis">
                  <div class="row">
                    <div class="col-md-6">
                      <h6><?php load_language('sunday');?></h6>
                      <!-- <select class="ip_schedule_timing_input floatLeft">
                        <option>05:25 PM</option>
                      </select>
                      <select class="ip_schedule_timing_input floatRight">
                        <option>05:25 PM</option>
                      </select> -->
                      <input disabled="" readonly="" id="sch_sun_start" name="dct_sch_sun_start" class="ip_time floatLeft  ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" >
                        <input disabled="" readonly="" data-parsley-mintime="#sch_sun_start" name="dct_sch_sun_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sch_sun_end">
                      
                      <div class="clear"></div>
                    </div>
                    <div class="col-md-6">
                      <h6 class="ip_schedule_check">
                          <!-- <input id="checkbox-7" class="ip_custom_checkbox" name="checkbox-7" type="checkbox" checked> -->
                          <!-- <p class="ip_custom_checkbox_label">Interval</p> -->
                          <input disabled id="intr_chkbx_sun" class="ip_custom_checkbox" type="checkbox" >
                          <label for="intr_chkbx_sun" class="ip_custom_checkbox_label"><?php load_language('interval');?></label>
                      </h6>
                    <!--   <select disabled="" id="sch_sun_int"  name="dct_sch_sun_int" class="ip_schedule_timing_input floatLeft">
                         <option disabled selected>Time</option>
                        <?php 
                        for ($i = 1; $i <= 59; $i++) {
                          ?>
                         <option value="<?php echo $i?>"><?php echo $i?> min(s)</option>
                        <?php
                        }
                        ?>
                      </select> -->
                       <input disabled="" readonly="" id="intr_sun_start" data-parsley-mintime ="#sch_sun_start" class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " name="dct_intr_sun_start"    placeholder="" >
                      <input disabled="" readonly="" data-parsley-mintime = "#intr_sun_start" data-parsley-maxtime = "#sch_sun_end" name="dct_intr_sun_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="intr_sun_end">
                      <div class="clear"></div>
                    </div>
                  </div>
                </li>
              </div>
            </div>

            <div id="secondary" class="tab-pane fade">
                <div class="ip_schedule_week">
                  <li>
                    <input id="sec_clinic_day_mon"  disabled="" type="checkbox" name="sec_dct_sch_day[]" value="mon">
                    <label for="sec_clinic_day_mon"><?php load_language('monday');?></label>
                  </li>
                  <li>
                    <input id="sec_clinic_day_tue" disabled="" type="checkbox" name="sec_dct_sch_day[]" value="tue">
                    <label for="sec_clinic_day_tue"><?php load_language('tuesday');?></label>
                  </li>
                  <li>
                    <input id="sec_clinic_day_wed" disabled="" type="checkbox" name="sec_dct_sch_day[]" value="wed">
                    <label for="sec_clinic_day_wed"><?php load_language('wednesday');?></label>
                  </li>
                  <li>
                    <input id="sec_clinic_day_thu" disabled="" type="checkbox" name="sec_dct_sch_day[]" value="thu">
                    <label for="sec_clinic_day_thu"><?php load_language('thursday');?></label>                  
                  </li>
                  <li>
                    <input id="sec_clinic_day_fri" disabled="" type="checkbox" name="sec_dct_sch_day[]" value="fri">
                    <label for="sec_clinic_day_fri"><?php load_language('friday');?></label>     
                  </li>
                  <li>
                    <input id="sec_clinic_day_sat" disabled="" type="checkbox" name="sec_dct_sch_day[]" value="sat">
                    <label for="sec_clinic_day_sat"><?php load_language('saturday');?></label>     
                  </li>
                  <li>
                    <input id="sec_clinic_day_sun" data-parsley-mincheck="1" data-parsley-required="" disabled="" type="checkbox" name="sec_dct_sch_day[]" value="sun">
                    <label for="sec_clinic_day_sun"><?php load_language('sunday');?></label>     
                  </li>
                  <div class="clear"></div>
                </div>

                <div class="ip_schedule_timing">

                  <li id="sec_clinic_day_mon_div" class="inp-dis">
                    <div class="row">
                      <div class="col-md-6">
                        <h6><?php load_language('monday');?></h6>
                          <input disabled="" readonly="" id="sec_sch_mon_start"  class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " name="sec_dct_sch_mon_start"    placeholder="" >
                          <input disabled="" readonly="" data-parsley-mintime = "#sec_sch_mon_start" name="sec_dct_sch_mon_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sec_sch_mon_end">
                        <div class="clear"></div>
                      </div>
                      <div class="col-md-6">
                        <h6 class="ip_schedule_check">
                           <!--  <p class="ip_custom_checkbox_label">Interval</p>  -->
                            <input disabled id="sec_intr_chkbx_mon" class="ip_custom_checkbox"   type="checkbox" >
                            <label for="sec_intr_chkbx_mon" class="ip_custom_checkbox_label"><?php load_language('interval');?></label>
                        </h6>

                     <!--    <h6 class="ip_schedule_check">
                          <input id="checkbox-1" class="ip_custom_checkbox" name="checkbox-1" type="checkbox" checked>
                          <label for="checkbox-1" class="ip_custom_checkbox_label">Interval</label>
                      </h6> -->


                        <input disabled="" readonly="" id="sec_intr_mon_start"  class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " data-parsley-mintime ="#sec_sch_mon_start" name="sec_dct_intr_mon_start"    placeholder="" >
                        <input disabled="" readonly="" data-parsley-mintime = "#sec_intr_mon_start" data-parsley-maxtime = "#sec_sch_mon_end" name="sec_dct_intr_mon_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sec_intr_mon_end">
                        <div class="clear"></div>
                      </div>
                    </div>
                  </li>

                  <li id="sec_clinic_day_tue_div" class="inp-dis">
                    <div class="row">
                      <div class="col-md-6">
                        <h6><?php load_language('tuesday');?></h6>
                          <input disabled="" readonly="" id="sec_sch_tue_start" name="sec_dct_sch_tue_start" class="ip_time floatLeft  ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" >
                          <input disabled="" readonly="" data-parsley-mintime = "#sec_sch_tue_start" name="sec_dct_sch_tue_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sec_sch_tue_end">
                        <div class="clear"></div>
                      </div>
                      <div class="col-md-6">
                        <h6 class="ip_schedule_check">
                            <!--  <p class="ip_custom_checkbox_label">Interval</p> -->
                            <input disabled id="sec_intr_chkbx_tue" class="ip_custom_checkbox" type="checkbox" >
                            <label for="sec_intr_chkbx_tue" class="ip_custom_checkbox_label"><?php load_language('interval');?></label>
                        </h6>
                        <input disabled="" readonly="" id="sec_intr_tue_start"  class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " data-parsley-mintime ="#sec_sch_tue_start" name="sec_dct_intr_tue_start"    placeholder="" >
                        <input disabled="" readonly="" data-parsley-mintime = "#sec_intr_tue_start" data-parsley-maxtime = "#sec_sch_tue_end" name="sec_dct_intr_tue_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sec_intr_tue_end">
                        <div class="clear"></div>
                      </div>
                    </div>
                  </li>

                  <li id="sec_clinic_day_wed_div" class="inp-dis">
                    <div class="row">
                      <div class="col-md-6">
                        <h6><?php load_language('wednesday');?></h6>
                          <input disabled="" readonly="" id="sec_sch_wed_start"  name="sec_dct_sch_wed_start" class="ip_time floatLeft  ip_schedule_timing_input dctr_dsh_timepicker" placeholder="">
                          <input disabled="" readonly="" data-parsley-mintime="#sec_sch_wed_start" name="sec_dct_sch_wed_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sec_sch_wed_end">
                        <div class="clear"></div>
                      </div>
                      <div class="col-md-6">
                        <h6 class="ip_schedule_check">
                            <!-- <p class="ip_custom_checkbox_label">Interval</p> -->
                            <input disabled id="sec_intr_chkbx_wed" class="ip_custom_checkbox"  type="checkbox" >
                            <label for="sec_intr_chkbx_wed" class="ip_custom_checkbox_label"><?php load_language('interval');?></label>
                        </h6>
                        <input disabled="" readonly="" id="sec_intr_wed_start" data-parsley-mintime ="#sec_sch_wed_start"  class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " name="sec_dct_intr_wed_start"    placeholder="" >
                        <input disabled="" readonly="" data-parsley-mintime = "#sec_intr_wed_start" data-parsley-maxtime = "#sec_sch_wed_end" name="sec_dct_intr_wed_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sec_intr_wed_end">
                        <div class="clear"></div>
                      </div>
                    </div>
                  </li>
                  <li id="sec_clinic_day_thu_div" class="inp-dis">
                    <div class="row">
                      <div class="col-md-6">
                        <h6><?php load_language('thursday');?></h6>
                          <input disabled="" readonly="" id="sec_sch_thu_start" name="sec_dct_sch_thu_start" class="ip_time floatLeft  ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" >
                          <input disabled="" readonly="" data-parsley-mintime="#sec_sch_thu_start" name="sec_dct_sch_thu_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sec_sch_thu_end">
                        <div class="clear"></div>
                      </div>
                      <div class="col-md-6">
                        <h6 class="ip_schedule_check">
                            <!-- <p class="ip_custom_checkbox_label">Interval</p> -->
                            <input disabled id="sec_intr_chkbx_thu" class="ip_custom_checkbox" type="checkbox" >
                            <label for="sec_intr_chkbx_thu" class="ip_custom_checkbox_label"><?php load_language('interval');?></label>
                        </h6>
                         <input disabled="" readonly="" id="sec_intr_thu_start" data-parsley-mintime ="#sec_sch_thu_start"  class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " name="sec_dct_intr_thu_start"    placeholder="" >
                        <input disabled="" readonly="" data-parsley-mintime = "#sec_intr_thu_start" data-parsley-maxtime = "#sec_sch_thu_end" name="sec_dct_intr_thu_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sec_intr_thu_end">
                        <div class="clear"></div>
                      </div>
                    </div>
                  </li>
                  <li id="sec_clinic_day_fri_div" class="inp-dis">
                    <div class="row">
                      <div class="col-md-6">
                        <h6><?php load_language('friday');?></h6>
                          <input disabled="" readonly="" id="sec_sch_fri_start" name="sec_dct_sch_fri_start" class="ip_time floatLeft  ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" >
                          <input disabled="" readonly="" data-parsley-mintime="#sec_sch_fri_start" name="sec_dct_sch_fri_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sec_sch_fri_end">
                        <div class="clear"></div>
                      </div>
                      <div class="col-md-6">
                        <h6 class="ip_schedule_check">
                            <!-- <p class="ip_custom_checkbox_label">Interval</p> -->
                            <input disabled id="sec_intr_chkbx_fri" class="ip_custom_checkbox" type="checkbox" >
                            <label for="sec_intr_chkbx_fri" class="ip_custom_checkbox_label"><?php load_language('interval');?></label>
                        </h6>
                        <input disabled="" readonly="" id="sec_intr_fri_start" data-parsley-mintime ="#sec_sch_fri_start"  class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " name="sec_dct_intr_fri_start"    placeholder="" >
                        <input disabled="" readonly="" data-parsley-mintime = "#sec_intr_fri_start" data-parsley-maxtime = "#sec_sch_fri_end" name="sec_dct_intr_fri_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sec_intr_fri_end">
                        <div class="clear"></div>
                      </div>
                    </div>
                  </li>
                  <li id="sec_clinic_day_sat_div" class="inp-dis">
                    <div class="row">
                      <div class="col-md-6">
                        <h6><?php load_language('saturday');?></h6>
                          <input disabled="" readonly="" id="sec_sch_sat_start" name="sec_dct_sch_sat_start" class="ip_time floatLeft  ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" >
                          <input disabled="" readonly="" data-parsley-mintime="#sec_sch_sat_start" name="sec_dct_sch_sat_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sec_sch_sat_end">
                        <div class="clear"></div>
                      </div>
                      <div class="col-md-6">
                        <h6 class="ip_schedule_check">
                            <!-- <p class="ip_custom_checkbox_label">Interval</p> -->
                            <input disabled id="sec_intr_chkbx_sat" class="ip_custom_checkbox" type="checkbox" >
                            <label for="sec_intr_chkbx_sat" class="ip_custom_checkbox_label"><?php load_language('interval');?></label>
                        </h6>
                         <input disabled="" readonly="" id="sec_intr_sat_start" data-parsley-mintime ="#sec_sch_sat_start" class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " name="sec_dct_intr_sat_start"    placeholder="" >
                        <input disabled="" readonly="" data-parsley-mintime ="#sec_intr_sat_start" data-parsley-maxtime = "#sec_sch_sat_end" name="sec_dct_intr_sat_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sec_intr_sat_end">
                        <div class="clear"></div>
                      </div>
                    </div>
                  </li>
                  <li id="sec_clinic_day_sun_div" class="inp-dis">
                    <div class="row">
                      <div class="col-md-6">
                        <h6><?php load_language('sunday');?></h6>
                        <input disabled="" readonly="" id="sec_sch_sun_start" name="sec_dct_sch_sun_start" class="ip_time floatLeft  ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" >
                          <input disabled="" readonly="" data-parsley-mintime="#sec_sch_sun_start" name="sec_dct_sch_sun_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sec_sch_sun_end">
                        
                        <div class="clear"></div>
                      </div>
                      <div class="col-md-6">
                        <h6 class="ip_schedule_check">
                            <!-- <p class="ip_custom_checkbox_label">Interval</p> -->
                            <input disabled id="sec_intr_chkbx_sun" class="ip_custom_checkbox" type="checkbox" >
                            <label for="sec_intr_chkbx_sun" class="ip_custom_checkbox_label"><?php load_language('interval');?></label>
                        </h6>
                         <input disabled="" readonly="" id="sec_intr_sun_start" data-parsley-mintime ="#sec_sch_sun_start" class="ip_time floatLeft ip_schedule_timing_input dctr_dsh_timepicker " name="sec_dct_intr_sun_start"    placeholder="" >
                        <input disabled="" readonly="" data-parsley-mintime = "#sec_intr_sun_start" data-parsley-maxtime = "#sec_sch_sun_end" name="sec_dct_intr_sun_end" class="ip_time floatRight ip_schedule_timing_input dctr_dsh_timepicker" placeholder="" id="sec_intr_sun_end">
                        <div class="clear"></div>
                      </div>
                    </div>
                  </li>
                </div>
            </div>

            <div class="ip_schedule_button_bay">
                  <button class="ip_schedule_btn "  type="button" id="doc_sch_sub"><?php load_language('add_schedule');?></button>
            </div>
            
            <div id="add_schedule_success" class="alert alert-success hidden">
               <?php load_language('schedule_add_success');?>
            </div>
            <div id="add_schedule_fail" class="alert alert-danger hidden">
            </div>
          </form>
        </div>
        </div>
        </div>
        <div class="col-md-4">
          <div class="ip_schedule_div">
            <form data-parsley-validate="" role="form" id="doc_consult_config_sub_form">
            <!-- <div id="loading" style="display: none;">
                <img id="loading-image" src="<?php echo base_url();?>assets/images/ipok-loading.gif" alt="Loading..." />
            </div> -->
            <div class="ip_schedule_head">
              <div class="ip_bio_head bordernone floatLeft">
                <?php load_language('configuring_consultation_heading');?>
              </div>
              <div class="clear"></div>
            </div>
            <div class="ip_schedule_detail">
              <li>
                <div class="child1"><?php load_language('duration_of_consultation');?></div>
                <div class="child2">
                  <select name="consultation_duration" id="doc_consult_duration" class="ip_schedule_input_duration " placeholder="" data-parsley-required="">
                      <option value="0" disabled selected><?php load_language('time');?></option>
                      <option value="10" >10 MIN</option>
                      <option value="15" >15 MIN</option>
                      <option value="30" >30 MIN</option>
                      <option value="45" >45 MIN</option>
                      <option value="60" >60 MIN</option>
                      <option value="90" >90 MIN</option>
                      <option value="120" >120 MIN</option>
                  
                      
                       <!--  <?php 
                        for ($i = 1; $i <= 60; $i++) {
                          ?>
                      <option value="<?php echo $i?>"><?php echo $i?> MIN</option>
                        <?php
                        }
                        ?> -->
                  </select>
                </div>
                <div class="clear"></div>
              </li>
              <hr>
              <li>
                <div class="child1"><?php load_language('value_of_consultation');?></div>
                <div class="child2">
                 <input class="ip_schedule_input_value" onKeyPress="if(this.value.length > 5) return false;" data-parsley-minlength="2" data-parsley-maxlength="5" data-parsley-required="" type="number" name="price" value="<?php echo $doctor_data['dr_price']?>">
                </div>
                <div class="clear"></div>
              </li>
              <hr>
               <li>
                <div class="child1"><?php load_language('inquiry_including_return');?></div>
                <div class="child2">
                  <select id="doc_accept_return" name="accept_return" class="ip_schedule_input_return" placeholder="" data-parsley-required="">
                    <option disabled selected><?php load_language('select_any');?></option>
                    <option value="1"><?php load_language('yes');?></option>
                    <option value="0"><?php load_language('no');?></option>
                  </select>
                </div>
                <div class="clear"></div>
              </li>

               <li>
                <div class="child1"><?php load_language('limit_period');?></div>
                <div class="child2">
                  <select id="doc_return_timeperiod"  name="return_timeperiod" class="ip_schedule_input_period " placeholder="">
                    <option value="0" disabled selected><?php load_language('select_any');?></option>
                        <?php 
                        for ($i = 1; $i <= 60; $i++) {
                          ?>
                      <option value="<?php echo $i?>"><?php echo $i?> <?php load_language('days');?></option>
                        <?php
                        }
                        ?>
                  </select>
                </div>
                <div class="clear"></div>
              </li>

            </div>
            <div class="ip_schedule_button_bay">
              <button class="ip_schedule_btn uppercase" type="button" id="doc_consult_config_sub" ><?php load_language('save');?></button>
            </div>

           
        

        </form>
        </div>
          <div class="alert alert-danger hidden" id="add_consult_config_fail">
            <strong><?php load_language('error');?>!</strong><?php load_language('configuring_consultation_failed');?>!.
          </div>
          <div class="alert alert-success hidden" id="add_consult_config_success">
            <strong><?php load_language('success');?>!</strong><?php load_language('configuring_consultation_success');?>.
          </div>

          <div class="ip_schedule_div">
            <form data-parsley-validate="" role="form" id="doc_leave_sub_form">
           
            <div class="ip_schedule_head">
              <div class="ip_bio_head bordernone floatLeft">
                <?php load_language('vacation_heading');?>
              </div>
              <div class="ip_head_button bordernone floatRight">
                 <select class="ip_select_clinic_input" data-parsley-required="true" name="doc-leave-clinic"  id="doc_leave_clinic" >
                    <option disabled selected><?php load_language('select_clinic');?></option>
                    <?php foreach ($clinic_list as $key => $value) {
                    ?>
                    <option value="<?php echo $value['clinic_id']?>"><?php echo $value['clinic_name']?></option>
                    <?php 
                    }
                    ?>
                </select>
              </div>
              <div class="clear"></div>
            </div>
            <div class="ip_schedule_detail">
              <li>
                <div class="child1"><?php load_language('start_of_vacation');?></div>
                <div class="child2" id="doc-leave-container">
                  <input data-parsley-required="true" class="ip_schedule_input" name="dctr-leave-start" id="dctr_leave_start" placeholder="">
                </div>
                <div class="clear"></div>
              </li>
              <li>
                <div class="child1"><?php load_language('end_of_vacation');?></div>
                <div class="child2" id="doc-leave-container">
                  <input data-parsley-required="true" class="ip_schedule_input" name="dctr-leave-end" id="dctr_leave_end" data-parsley-mindate="#dctr_leave_start" placeholder="">
                </div>
                <div class="clear"></div>
              </li>
            </div>
            <div class="ip_schedule_button_bay">
              <button class="ip_schedule_btn" type="button" id="doc_leave_sub" ><?php load_language('activate_vacation');?></button>
            </div>
            <div class="alert alert-success hidden" id="add_vacation_success">
            <strong><?php load_language('success');?>!</strong><?php load_language('add_vacation_success');?>.
          </div>
          <div class="alert alert-success hidden" id="add_vacation_fail">
            <strong><?php load_language('error');?>!</strong><?php load_language('add_vacation_error');?>.
          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


<!-- DELETE-POP-UP -->

<div id="doc-delete" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
      <div class="ip_patient_delete_pop_wrapper">
        <div class="ip_paitent_delete_header">
          <?php load_language('delete_user_account');?>
        </div>
        <div class="ip_patient_delete_content">
          <div class="ip_delete_pic_circle">
            <img src="<?php echo base_url();?>/assets/images/ip_delete_user_pic.png">
          </div>
          <h5><?php load_language('delete_user_desc');?></h5>
          <hr>
          <p><?php load_language('delete_user_message');?></p>
          <form data-parsley-validate="" role="form" id="doc_profile_delete">
            <div class="ip_patient_delete_form">
              <div class="ip_patient_delete_row">
                <input class="ip_patient_input ip_paitent_delete_user reset-form" data-parsley-required="" name="username" placeholder="<?php load_language('login');?>" onKeyPress="if(this.value.length > 25) return false;">
              </div>
              <div class="ip_patient_delete_row">
                <input class="ip_patient_input ip_paitent_delete_pass reset-form" data-parsley-required="" name="password" placeholder="<?php load_language('password');?>" onKeyPress="if(this.value.length > 25) return false;" type="Password">
              </div>
              <div id="doc_profile_delete_error" class="alert alert-danger hidden">
            </div>
             
              <div class="ip_patient_delete_row">
                <button type="button" id="doc_del_check_login_sub" class="ip_paitent_dark_btn floatLeft"><?php load_language('accept');?></button>
                <button class="ip_paitent_delete_btn floatRight" data-dismiss="modal"><?php load_language('cancel');?></button>
                <div class="clear"></div>
              </div>
            </div>
          </form>
        </div>
      </div>
  </div>
</div>


<!-- DELETE-CONFIRMATION POP-UP -->

<div id="doc-delete-con" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
      <div class="ip_patient_delete_pop_wrapper">
        <div class="ip_paitent_delete_header">
          <?php load_language('delete_user_account');?>
        </div>
        <div class="ip_patient_delete_content">
          <div class="ip_delete_pic_circle">
            <img src="<?php echo base_url();?>/assets/images/ip_delete_user_pic.png">
          </div>
          <h5><?php load_language('delete_user_desc');?></h5>
          <hr>
          <p><?php load_language('delete_user_otp_desc');?>.<br>
           <?php load_language('enter_confirm_code');?>.</p>
          <div class="ip_patient_delete_form">
            <form data-parsley-validate="" role="form" id="doc_profile_delete_confirmation">
           <!--  <div class="ip_patient_delete_row">
              <input class="ip_patient_input ip_paitent_delete_user" placeholder="Login">
            </div>
            <div class="ip_patient_delete_row">
              <input class="ip_patient_input ip_paitent_delete_pass" placeholder="Password">
            </div> -->
            <div class="ip_patient_delete_row">
              <input class="ip_patient_input ip_paitent_delete_pass uppercase reset-form" name="confirmation_code" data-parsley-minlength="8" data-parsley-required="" data-parsley-maxlength="8" onKeyPress="if(this.value.length > 7) return false;" placeholder="<?php load_language('confirmation_code');?>">
            </div>
            <div id="doc_profile_delete_code_error" class="alert alert-danger hidden"></div>
            <div class="ip_patient_delete_row">
              <button type="button" class="ip_paitent_dark_btn floatLeft" id="doc_del_check_code_sub"><?php load_language('accept');?></button>
              <button class="ip_paitent_delete_btn floatRight" data-dismiss="modal"><?php load_language('cancel');?></button>
              <div class="clear"></div>
            </div>
          </form>
          </div>
        </div>
      </div>
  </div>
</div>


<!-- DELETED POP-UP -->

<div id="doc-delete-complete" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
      <div class="ip_patient_delete_pop_wrapper">
        <div class="ip_paitent_delete_header">
          <?php load_language('delete_user_account');?>
        </div>
        <div class="ip_patient_delete_content">
          <div class="ip_delete_pic_circle">
            <img src="<?php echo base_url();?>/assets/images/ip_delete_user_pic.png">
          </div>
          <h5><?php load_language('account_deleted');?></h5>
          <hr>
           <div class="ip_patient_delete_row textCenter">
             <a href="javascript:void(0)">
                <button type="button" class="ip_paitent_dark_btn" data-dismiss="modal"><?php load_language('log_out');?></button>
             </a>
              <div class="clear"></div>
            </div>
         
        </div>
      </div>
  </div>
</div>




