<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url();?>assets/images/ip_logo.png">
     <title>iPoK</title>
  <?php
   $this->load->view('template/header-script');
  ?>
  <body>

	  <?php
    if($page!="home" and $page!="aboutus"){ $this->load->view('template/header');}
	  //$this->load->view('Templates/left-menu');
	 // $this->load->view('template/left-menu-old');
	  $this->load->view($page);
    //if($page!="home"){ $this->load->view('template/footer');}
    $this->load->view('template/footer');
      ?>    
 
  <?php
  $this->load->view('template/footer-script');
  if($page=="home"){$this->load->view('home_custom_script');}
  if($page=="search_doctor"){$this->load->view('search_doctor_custom_script');}
  if($page=="patient_editprofile"){$this->load->view('patient_editprofile_custom_script');}
  if($page=="doctor_editprofile"){$this->load->view('doctor_editprofile_custom_script');}
  if($page=="doctor_dash"){$this->load->view('doctor_dash_custom_script');}
  if($page=="patient_edit_dependent"){$this->load->view('patient_edit_dependent_custom_script');}
  if($page=="doctor_dash_start_service"){$this->load->view('doctor_dash_start_service_custom_script');}
  ?>
  </body>
</html>
