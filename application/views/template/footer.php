
</div>

<div class="full_screen_loader hidden">
  <div class="inner_div">
   <img src="<?php echo base_url();?>assets/images/ipok-loading.gif">
  </div>
</div>

<!-- <footer class="ip_footer_main">
  <div class="container">
    <div class="ip_footer_sub">
    <div class="row">
      <div class="col-md-2">
        <ul>
          <h5>CATEGORIES</h5>
          <li>Physiotherapy</li>
          <li>Clinics</li>
          <li>Laboratories</li>
        </ul>
      </div>
      <div class="col-md-2">
        <ul>
          <h5>PARTNERS</h5>
          <li>First company title</li>
          <li>Second company title</li>
          <li>Third company title</li>
          <li>Fourth company title</li>
        </ul>
      </div>
      <div class="col-md-4">
        <div class="ip_footer_contact">
          <div class="ip_footer_contact_inner">
            <h5>CONTACT US</h5>
            <p>Montergade 19 1140</p>
            <p>Copenhagen, Denmark</p>
            <h6>+45 878-78-14</h6>
            <div class="ip_footer_social">
              <li><img src="<?php echo base_url();?>assets/images/ip_facebook.png"></li>
              <li><img src="<?php echo base_url();?>assets/images/ip_twitter.png"></li>
              <li><img src="<?php echo base_url();?>assets/images/ip_base.png"></li>
              <div class="clear"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="ip_footer_subscription">
          <h5>SUBSCRIBE</h5>
          <div class="ip_footer_email_div">
            <input class="ip_footer_input" placeholder="Enter your e-mail">
            <div class="ip_footer_send">
            </div>
            <div class="clear"></div>
          </div>
          <p>&copy&nbsp;&nbsp;Copyright basement wireframe 2017</p>
        </div>
      </div>
    </div>
  </div>
</div>
</footer>
 -->

 <footer class="ip_footer_main">
  <div class="container">
    <div class="ip_footer_sub">
    <div class="row">
      <div class="col-md-1">&nbsp;</div>
      <div class="col-md-2">
        <ul>
          <h4 class="titulorodape">FAÇA SUA BUSCA POR</h4>
          <li>MÉDICOS</li>
          <li>ClÍNICAS</li>
          <li>LABORATÓRIOS</li>
        </ul>
      </div>
      <div class="col-md-2">
        <ul>
          <h4 class="titulorodape">SEJA IPOK</h4>
          <li>FAÇA DOWNLOAD</li>
          <li>ABRIR UMA CONTA</li>
          <li>FAZER LOGIN</li>
        </ul>
      </div>
      <div class="col-md-2">
        <ul>
          <h4 class="titulorodape">IPOK</h4>
          <li><a  class="uppercase" href="<?php echo base_url()?>Home/about">SOBRE NÓS</a></li>
          <li>TERMOS E CONDIÇÕES</li>
          <li>SUPORTE</li>
        </ul>
      </div>
      <div class="col-md-4">
        <div class="ip_footer_contact">
          <div class="ip_footer_contact_inner">
            <div class="row">
              <div class="col-md-12">
                <img src="<?php echo base_url();?>assets/images/rodape-logo.png" alt="">
              </div>
            </div>
            <div class="row" style="margin-top: 10px;">
              <div class="col-md-6"><img src="<?php echo base_url();?>assets/images/rodape-apple.png" alt="Faça donwload na Apple" width="120%" ></div>
              <div class="col-md-6"><img src="<?php echo base_url();?>assets/images/rodape-android.png" alt="Faça donwload na Android Play Store" width="120%" style="margin-left:25px;" ></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-1">&nbsp;</div>
    </div>
  </div>
</div>
</footer>
