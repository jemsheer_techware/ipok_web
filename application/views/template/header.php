
<?php if($this->session->userdata('UserData'))
    {$userdata = $this->session->userdata('UserData');}
    if($this->session->userdata('DependentData'))
      {$dependentdata = $this->session->userdata('DependentData');}
    if($this->session->userdata('CollaboratorData'))
      {$collaboratordata = $this->session->userdata('CollaboratorData');}
    
     if(auto_logout("user_time"))
    {
        $this->session->set_userdata('user_time', time());
        if($this->session->userdata('UserData'))
          {
            $this->session->set_userdata('logout', 'autologoff');
            redirect(base_url().'Home/logout');
          }
    }     
?>
  <div class="ip_main_wrapper">
    <nav class="navbar  navbar-fixed-top">

		<!-- PRIMARY-HEADER -->

   <!--  <div class="ip_header_primary">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar_primary" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar_primary" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
          <div class="row">
            <div class="col-md-6">
            <div class="ip_primary_left">
              <ul>
                <li><a>Download ipok</a></li>
                <li><a>Blog</a></li>
                <li><a>Contact us</a></li>
              </ul>
            </div>
            </div>
            <div class="col-md-6">
            <div class="ip_primary_right">
              <div class="ip_right_plane">
                <div class="ip_search floatLeft">
                  <div id="search-bar"> 
                      <form id="search-form">  
                        <input autocomplete="off" class="search-field" id="search-text" name="search" placeholder="Search…" type="text" />  
                        <input style="display: none;" type="submit" value="" />
                      </form>
                  </div>
                  <div class="clear"></div>
                </div>
                <div class="ip_account">
                MY ACCOUNT
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->

		<!-- SECONDARY-HEADER-LOGEDOUT-->

		 <!-- <div class="ip_header_secondary">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
				<div class="ip_logo" href="http://getbootstrap.com/examples/starter-template/#"><img src="../assets/images/ip_logo.png"></div>
				</div>
				<div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
					<ul class="nav navbar-nav ip_navbar_nav">
						<li class="active"><a>HOME</a></li>
						<li><a>	ABOUT</a></li>
						<li><a>FAQ</a></li>
						<li><a>BLOG</a></li>
						<li><a>CONTACT US</a></li>
					</ul>
					<ul class="nav navbar-right ip_nav_bar_right">
          <div class="ip_right_nav_loged_out">
            <li><a>Login/Register</a></li>
          </div>
					</ul>
				</div>
			</div>
		</div>

    <!-- SECONDARY-HEADER-LOGEDIN -->

  <div class="ip_header_secondary">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
				<a href="<?php echo base_url();?>"><div class="ip_logo"><img src="<?php echo base_url();?>assets/images/ip_logo.png"></div></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">

					<ul class="nav navbar-nav ip_navbar_nav">
            <?php if(empty($userdata))
            {
            ?>
              <li class="active "><a href="<?php echo base_url()?>"><?php load_language('home');?></a></li>
              <li ><a href="<?php echo base_url()?>Home/about"><?php load_language('about_us');?></a></li>
              <li ><a href=""><?php load_language('supports');?></a></li>
              <li ><a href=""><?php load_language('blog');?></a></li>
              <li ><a class="capitalize" href="<?php echo base_url()?>Home/contact"><?php load_language('contact_us');?></a></li>
            <?php } elseif (!empty($userdata)&&($userdata['type']=="PATIENT")) {           
            ?>
              <li class="active"><a href="<?php echo base_url()?>Home/Dashboard"><?php load_language('dashboard');?></a></li>
              <li ><a href="<?php echo base_url()?>Home/about"><?php load_language('about_us');?></a></li>
              <li ><a href=""><?php load_language('supports');?></a></li>
              <li ><a href=""><?php load_language('blog');?></a></li>
              <li ><a href="<?php echo base_url()?>Home/contact"><?php load_language('contact_us');?></a></li>
            <?php } elseif (!empty($userdata)&&($userdata['type']=="DOCTOR")) {
            ?>
              <li class="active"><a href="<?php echo base_url()?>Home/Dashboard"><?php load_language('dashboard');?></a></li>
              <li ><a href=""><?php load_language('agenda');?></a></li>
              <li ><a href="<?php echo base_url();?>Doctor/wallet""><?php load_language('wallet');?></a></li>
              <li ><a href="<?php echo base_url();?>Doctor/records/"><?php load_language('records');?></a></li>
              <!-- <li ><a href="">About Us</a></li>
              <li ><a href="">FAQ</a></li>
              <li ><a href="">Blog</a></li>
              <li ><a href="">Contact Us</a></li> -->
            <?php   } ?>


<!-- 
						<li class="active">
              <?php if(!empty($userdata))
              {
              ?>
                <a href="<?php echo base_url()?>Home/Dashboard">Dashboard</a>
              <?php
              }
              else
              {
              ?>
                <a>Dashboard</a>
              <?php 
              }
              ?>
             
            </li>

						<li><a>Appointment Book</a></li>
            <li><a>Wallet</a></li>

            <?php if(!empty($this->session->userdata('UserData'))&&$this->session->userdata('UserData')['type']=="DOCTOR")
            {
            ?>
						<li onclick="location.href='<?php echo base_url();?>Doctor/records/'"><a>Records</a></li>
            <?php
            } 
            ?> -->
            <!-- <li class="dropdown">
                <a class="dropdown-toggle"  data-toggle="dropdown"><img src="<?php echo base_url();?>assets/images/ip_more.png"></a>
                <ul class="dropdown-menu ip_more_list_menu">
                  <li><a href="#">Menu 1</a></li>
                  <li><a href="#">Menu 2</a></li>
                  <li><a href="#">Menu 3</a></li>
                  <li><a href="#">Menu 5</a></li>
                  <li><a href="#">Menu 6</a></li>
                </ul>
            </li> -->
					</ul>
					<ul class="nav navbar-right ip_nav_bar_right">
  <div class="ip_right_nav_loged_in">
					<!-- <li>
            <div class="ip_nav_search_bar">
              <input class="ip_nav_search_text" type="text" placeholder="Search">
            </div>
          </li> -->
          <?php if(!empty($this->session->userdata('UserData') ) and ($this->session->userdata('UserData')['type']=='DOCTOR') or $this->session->userdata('UserData')['type']=='PATIENT')
          {?>
          <li class="dropdown">
              <div class="ip_nav_notification dropdown-toggle" data-toggle="dropdown">
              </div>
                <ul class="dropdown-menu ip_nav_notification_listing">
                  <div class="ip_bio_tab_div">
                    <div class="ip_bio_head">
                      <div class="arrow-up"></div>
                      <?php load_language('notification');?>
                    </div>
                    <div class="ip_bio_detail">

          <?php 
            $notifications = get_notification($userdata['id'],$userdata['type']);
          if(!empty($notifications))
          {
            //$notifications = $this->session->userdata('notifications');
          ?>
            <div class="ip_bio_notification_list">
                        <ul>
                          <?php
                          foreach ($notifications as $key => $value) 
                          {
                          ?>
                            <li>
                              <h5><?php echo $value['type_desc'];?>
                              <div class="ip_notification_time"><?php echo change_time_to_local($value['time'])?></div>
                              </h5>
                              <p><?php echo $value['message'];?></p>
                            </li>
                          <?php
                          }
                          ?>
                        </ul>
                        <div class="clear"></div>
                        <div class="ip_notification_btm_btn_bay">
                          <?php 
                          if($userdata['type']=='PATIENT')
                          {
                          ?>
                            <a class="p0" href="<?php echo base_url();?>Patient/notification">
                            <button class="ip_notification_btn"><?php load_language('see_all');?></button>
                            </a>
                          <?php
                          }
                          elseif($userdata['type']=='DOCTOR')
                          {
                          ?>
                            <a class="p0" href="<?php echo base_url();?>Doctor/notification">
                            <button class="ip_notification_btn"><?php load_language('see_all');?></button>
                            </a>
                          <?php
                          }
                          ?>
                          
                        </div>
                      </div>
          <?php 
          }
          else
          {
          ?>
             <div class="ip_bio_notification_list">
                <ul>
                  <li>
                    </p><?php load_language('no_notification');?>!</li>
                  </li>
                </ul>
              </div>
          <?php
          }
          ?>



                    </div>
                  </div>
                </ul>
          </li>
          <?php }?>

          <li>
            <div class="ip_nav_account_details dropdown">
              <?php if(!empty($userdata))
              {?>
              <div class="ip_nav_account_profile_pic dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url();echo $userdata['profile_photo'];?>"> 
              </div>
              <?php 
              } 
              ?>

              <?php if(!empty($userdata)&&($userdata['type']=="DOCTOR"))
              {
                $clinic_list = get_clinic_list($userdata['id']);
                ?>
              <ul class="dropdown-menu ip_nav_profile_listing">
                <div class="ip_arrow_up"></div>

                <?php
                if(!empty($clinic_list))
                {
                ?>
                  <li>
                    <p><?php load_language('clinics');?></p>
                  </li>

                  <div class="ip_listing_scroll">
                  <?php 

                  foreach ($clinic_list as $key => $elm) 
                  {
                  ?>
                    <li>
                      <div class="ip_drop_pic">
                        <img src="<?php echo base_url(); echo $elm['clinic_pic']?>">
                      </div>
                      <div class="ip_drop_detail"><?php echo $elm['clinic_name']?></div>
                      <div class="clear"></div>
                    </li>

                  <?php
                  }
                  ?>

                  </div>
                  <?php
                }
                ?>
                

                <!-- CODE FOR SHOWING COLLABORATOR -->
                <?php if(!empty($collaboratordata)){
                  ?>
                  <li>
                  <p><?php load_language('your_collaborators');?></p>
                </li>
                <div class="ip_listing_scroll">
                <?php
                foreach ($collaboratordata as $key => $value) 
                {
                ?>
                  <a href="<?php echo base_url();?>Doctor/editColaborator/<?php echo $value['id']?>">
                    <li>
                      <div class="ip_drop_pic">
                        <img src="<?php echo base_url(); echo $value['image']?>">
                      </div>
                      <div class="ip_drop_detail"><?php echo $value['name']?></div>
                      <div class="clear"></div>
                    </li>
                  </a>
                <?php
                }
                ?>
                </div>
                <?php
                }
                ?>

                <li>
                  <a href="<?php echo base_url();?>Doctor/addColaborator"><?php load_language('add_collaborator');?></a>
                </li>
                <!-- <li>
                  <a href="">Edit Collaborator</a>
                </li> -->
                <li  class="bordernone">
                  <a href="<?php echo base_url()?>Home/logout"><?php load_language('sign_out');?></a>
                </li>
              </ul>
              <?php } ?>
              

               <?php if(!empty($userdata)&&($userdata['type']=="COLLABORATOR"))
              {?>
              <ul class="dropdown-menu ip_nav_profile_listing">
                <div class="ip_arrow_up"></div>
                <li  class="bordernone">
                  <a href="<?php echo base_url()?>Home/logout"><?php load_language('sign_out');?></a>
                </li>
              </ul>
              <?php } ?>



              <?php if(!empty($userdata)&&($userdata['type']=="PATIENT"))
              {?>
              <ul class="dropdown-menu ip_nav_profile_listing">
                <div class="ip_arrow_up"></div>
                <div class="ip_listing_scroll">

                <!-- CODE FOR SHOWING DEPENDENT -->
                <?php if(!empty($dependentdata)){
                foreach ($dependentdata as $key => $value) 
                  {
                ?>

                    <li>
                      <div class="ip_drop_pic">
                        <img src="<?php echo base_url(); echo $value['image']?>">
                      </div>
                      <div class="ip_drop_detail"><?php echo$value['dependent_name']?></div>
                      <div class="clear"></div>
                    </li>
                <?php
                  }
                }
                ?>
                </div>
               <!-- ................................. -->
               <a href="<?php echo base_url();?>Patient/addDependent">
                <li><?php load_language('add_dependent');?></li>
                </a>
                <a href="<?php echo base_url();?>Patient/editDependent">
                <li><?php load_language('edit_dependent');?></li>
                </a>
                <a href="<?php echo base_url()?>Home/logout">
                <li  class="bordernone">
                  <?php load_language('sign_out');?>
                </li>
                </a>
              </ul>
              <?php } ?>

              <div class="ip_nav_account_profile_name">
               <?php if(!empty($userdata)&&($userdata['type']=="DOCTOR" or $userdata['type']=="COLLABORATOR"))
               {?>Dr.<?php echo $userdata['name'];}

              else if(!empty($userdata)&&($userdata['type']=="PATIENT")){echo decrypt_data($userdata['name']);}

              else{?><a href="<?php echo base_url()?>"><?php load_language('login/register');?></a> <?php } ?>
              
              </div>

              <div class="clear"></div>
            </div>
          </li>

          <?php 
        $langVal=$this->session->userdata('language');
      ?> 
           <li >
            <div class="ip_nav_account_details">
              <select class="nav_select_dark" onchange="langChange(this)">
                <option value="en" <?php echo ($langVal == 'en') ? "selected" : "";?>>EN</option>
                <option value="pr" <?php echo ($langVal == 'pr') ? "selected" : "";?>>PR</option>
              </select>
               </div>
            </li>

          <div class="clear"></div>
        </div>
					</ul>
				</div>
			</div>
		</div>
    </nav>
