<?php
if($this->session->userdata('language') == 'en'){
	$genderphoto = 'Choose Profile Photo';
	$gender = 'Choose Gender';
}else{
	$genderphoto = "Escolha a foto do perfil";
	$gender = "Escolha o sexo";
}

?>
<style>
.ip_reg_modal_addphoto img{width:100%;height:100%;object-fit:cover;object-position:center;border-radius:50%;}
</style>
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">
    <div class="ip_edit_record_wrapper">
          <div class="ip_edit_record_cover">

			<?php
				if($this->session->flashdata('message')) {
				$message = $this->session->flashdata('message');
			?>
				<div class="alert alert-<?php echo $message['class']; ?> alert-dismissible flash-msg">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><strong> <?php echo $message['title']; ?></strong></h4>
				<?php echo $message['message']; ?>
				</div>

			<?php
				}
			?>

<div id="loading" style="display: none;">
  <img id="loading-image" src="<?php echo base_url();?>assets/images/ipok-loading.gif" alt="Loading..." />
</div>

			<input type="hidden" name="language" value="<?php echo $this->session->userdata('language');?>" id="language">
          	<form role="form" data-parsley-validate="" id="reg-form-doctor" method="POST" action="<?php echo base_url();?>Home/doRegister" enctype="multipart/form-data">
            <div class="ip_edit_record_head backgroundnone">
              <?php load_language('create_a_medical_account');?>
            </div>
              <div class="ip_edit_record_detail">
              	<p><?php load_language('personal_details');?></p>
				<div class="row">
					<div class="col-md-7">
						<div class="ip_edit_row">
							  <div class="ip_bank_detail_frame">
								  <input class="ip_bank_input" name="name"  data-parsley-required="true" onKeyPress="if(this.value.length > 40) return false;" data-parsley-minlength="5" data-parsley-pattern="^[a-zA-Z ]+$"  placeholder="<?php load_language('name');?>*">
							  </div>
						</div>
						<div class="ip_edit_row">
							  <div class="ip_bank_detail_frame">
								  <input class="ip_bank_input" data-parsley-emaildoc="" onKeyPress="if(this.value.length > 75) return false;" name="email"  data-parsley-required="true"  
 								pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="<?php load_language('email');?>*">
							  </div>
						</div>
						<div class="ip_edit_row">
							<div class="ip_bank_detail_frame">
								<input class="ip_bank_input" name="telephone" onKeyPress="if(this.value.length > 30) return false;" data-parsley-pattern="^[0-9]+$"  type="number" data-parsley-minlength="5"  placeholder="<?php load_language('telephone');?>">
							</div>
						</div>
						<div class="ip_edit_row">
							<div class="ip_day_time_schedule_details_data p0  ip_gender_check">
								<div>
									<label for="reg-form-doc-male" class="ip_custom_checkbox_label1 ip_gender_check_label" style="margin-right:10px;">Gender</label>
									<input id="reg-form-doc-male" class="ip_custom_checkbox1 ip_gender_check_checkbox " name="gender" type="radio" required data-parsley-error-message="<?php echo $gender;?>" value="MALE">
									<label for="reg-form-doc-male" class="ip_custom_checkbox_label1 ip_gender_check_label"><?php load_language('male');?></label>
									<input id="reg-form-doc-female" class="ip_custom_checkbox1 ip_gender_check_checkbox "  name="gender" type="radio" value="FEMALE">
									<label for="reg-form-doc-female" class="ip_custom_checkbox_label1 ip_gender_check_label"><?php load_language('female');?></label>
									<input id="reg-form-doc-others" class="ip_custom_checkbox1 ip_gender_check_checkbox "  name="gender" type="radio" value="OTHERS">
									<label for="reg-form-doc-others" class="ip_custom_checkbox_label1 ip_gender_check_label"><?php load_language('others');?></label>
									<div class="clear"></div>
								</div>
							</div>
						</div>
						
					</div>
					<div class="col-md-5">
						<div class="ip_edit_row">
							<div class="ip_bank_detail_frame">
								<input class="ip_bank_input" maxlength="25" onKeyPress="if(this.value.length > 25) return false;" name="rg" placeholder="RG">
							</div>
						</div>
						<div class="ip_edit_row">
							<div class="ip_bank_detail_frame">
								<input class="ip_bank_input" name="cpf" placeholder="CPF" data-parsley-required="true" data-parsley-minlength="11" data-parsley-cpf="" data-parsley-cpfunique="" onKeyPress="if(this.value.length > 10) return false;"  type="number">
							</div>
						</div>
						<!-- <div class="ip_edit_row">
							<div class="ip_bank_detail_frame" id="doc-registration-container">
							
							<input name="dob" readonly  class="ip_reg_form_input form-control reset-form-custom background_transparent" data-parsley-required="true" placeholder="Date of Birth">
							</div>
						</div> -->

						<div class="ip_edit_row">

								<div class="row">
									<div class="col-md-4">
										<label class="ip_custom_checkbox_label1 ip_gender_check_label"><?php load_language('date_of_birth');?><!-- Data de Nascimento* --></label>	
									</div>
									<div class="col-md-2">
										<div class="ip_bank_detail_frame">
											<select class="ip_bank_input" name="day" style="background: #fff;" onclick="datebirth_function()" id="birth_day" data-parsley-required="true" data-parsley-datebirthdoc="">
												<option value=""></option>
												<?php 
												 for($i = 01; $i <= 31 ; $i++){?>
												 <option value="<?php echo $i;?>"><?php echo $i;?></option>
											<?php }
												?>
											</select>
										</div>
									</div>
									<div class="col-md-3">
										<div class="ip_bank_detail_frame">
											<select class="ip_bank_input" name="month" style="background: #fff;" onclick="datebirth_function()" id="birth_month" data-parsley-required="true" data-parsley-datebirthdoc="">
												<option value=""></option>
												<option value="01">JAN</option>
												<option value="02">FEV</option>
												<option value="03">MAR</option>
												<option value="04">ABR</option>
												<option value="05">MAI</option>
												<option value="06">JUN</option>
												<option value="07">JUL</option>
												<option value="08">AGO</option>
												<option value="09">SET</option>
												<option value="10">OUT</option>
												<option value="11">NOV</option>
												<option value="12">DEZ</option>
											</select>
										</div>
									</div>
									<div class="col-md-3">
										<div class="ip_bank_detail_frame">
											<select class="ip_bank_input" name="year" style="background: #fff;" onclick="datebirth_function()" id="birth_year" data-parsley-datebirthdoc="" data-parsley-required="true">
												<option value=""></option>
											<?php
												 $now = date('Y');
												 $max_year = $now - 18;
												 $min_year = $max_year - 120;
												for($i = $min_year; $i <= $max_year ; $i++){?>
													<option value="<?php echo $i;?>"><?php echo $i;?></option>
												<?php } 
											 ?>
												
											</select>
										</div>
									</div>
								</div>

							</div>

							<div id="error_date"></div>
					</div>
				</div>
			</div>
			<hr>
			<div class="ip_edit_record_detail">
				<p><?php load_language('customer_service');?></p>
				<div class="row">
					<div class="col-md-7">
						<div class="ip_edit_row">
							  <div class="ip_bank_detail_frame">
								  <input class="ip_bank_input" name="cep" data-parsley-required placeholder="CEP*" onKeyPress="if(this.value.length > 7) return false;" type="number" data-parsley-minlength="8" data-parsley-maxlength="8" data-parsley-cep="doctor" onkeyup="loadaddress_cep(this,'doctor')" data-parsley-pattern="^[0-9]+$">
								  <input type="hidden" name="default_latitude" id="default_latitude" value="">
								  <input type="hidden" name="default_longitude" id="default_longitude" value="">
							  </div>
						</div>
						<div class="ip_edit_row">
							<div class="row">
								<div class="col-md-7">
									<div class="ip_bank_detail_frame">
										<input class="ip_bank_input" name="locality" data-parsley-required placeholder="<?php load_language('neighbourhood');?>*" data-parsley-maxlength="50" id="doc-reg-locality" data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-minlength="5"  onKeyPress="if(this.value.length > 50) return false;">
									</div>
								</div>
								<div class="col-md-5">
									<div class="ip_bank_detail_frame">
										<input  class="ip_bank_input" name="number" data-parsley-required placeholder="<?php load_language('number');?>*" data-parsley-maxlength="30" id="doc-reg-number"  data-parsley-pattern="^[0-9]+$"  type="number" data-parsley-minlength="5"  onKeyPress="if(this.value.length > 30) return false;">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="ip_edit_row">
							  <div class="ip_bank_detail_frame">
								  <input class="ip_bank_input" name="street_address" data-parsley-required placeholder="Rua*" data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-minlength="5" onKeyPress="if(this.value.length > 50) return false;" id="doc-reg-rua" data-parsley-maxlength="50">
							  </div>
						</div>
						<div class="ip_edit_row">
							  <div class="ip_bank_detail_frame">
								  <input class="ip_bank_input" data-parsley-maxlength="50" name="complement" onKeyPress="if(this.value.length > 50) return false;" id="doc-reg-complement" placeholder="<?php load_language('complement');?>">
							  </div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="ip_edit_record_detail">
				<p><?php load_language('medical_data');?></p>
				<div class="row">
					<div class="col-md-12">
						<div class="ip_edit_row">
							  <div class="ip_bank_detail_frame" style="height:auto;">
								  <textarea class="ip_bank_input" name="about" placeholder="BIOGRAPHY*" data-parsley-required rows="5" data-parsley-maxlength="500" data-parsley-minlength="15" onKeyPress="if(this.value.length > 500) return false;"></textarea>
							  </div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-7">
						<div class="ip_edit_row">
							<div class="row">
								<div class="col-md-7">
									<div class="ip_bank_detail_frame">
										<input class="ip_bank_input" onKeyPress="if(this.value.length > 25) return false;" name="crm" data-parsley-required="true" placeholder="CRM">
									</div>
								</div>
								<div class="col-md-5">
									<div class="ip_bank_detail_frame">
								 <!--  <input class="ip_bank_input" placeholder="Specialization"> -->
								  <select class="ip_bank_input" placeholder="" data-parsley-required name="specialization">
					               	<option disabled selected><?php load_language('speciality');?>*</option>
								    <?php foreach ($speciality_list as $key => $value) {
								    ?>
								    	<option value="<?php echo $value['id']?>"><?php echo $value['specialization_name']?></option>
							        <?php	
							        }
							        ?>
			  						</select>
							  </div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-5">
					</div>
				</div>
			</div>
			<hr>
			<div class="ip_edit_record_detail">
				<p>Login</p>
				<div class="row">
					<div class="col-md-3">
						<div class="ip_edit_row">
							<div class="ip_bank_detail_frame">
										<input class="ip_bank_input" name="username" data-parsley-usernamedoc="" data-parsley-minlength="5" onKeyPress="if(this.value.length > 25) return false;" data-parsley-required="true" data-parsley-pattern="^[a-zA-Z0-9]+$" placeholder="<?php load_language('username');?>">
									</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="ip_edit_row">
							<div class="ip_bank_detail_frame">
										<input id="reg-doctor-pass" name="password" class="ip_bank_input"  data-parsley-minlength="8" data-parsley-required="true" onKeyPress="if(this.value.length > 25) return false;" type="Password" placeholder="<?php load_language('password');?>" data-parsley-uppercase="1" data-parsley-lowercase="1" data-parsley-number="1" data-parsley-special="1">
									</div>
						</div>
						
					</div>
					<div class="col-md-3">
						<div class="ip_edit_row">
							<div class="ip_bank_detail_frame">
										<input  data-parsley-equalto="#reg-doctor-pass" data-parsley-required type="Password" onKeyPress="if(this.value.length > 25) return false;"  class="ip_bank_input" placeholder="<?php load_language('confirm_password');?>">
									</div>
						</div>
						
					</div>
					<div class="col-md-3">
						<div class="ip_reg_add_phot_div">
							<button id="add_photo_pat" class="ip_add_photo_doc"><?php load_language('add_photo');?>
							<input class="" type="file" accept="image/*" name="profile_pic" data-parsley-error-message="<?php echo $genderphoto;?>" data-parsley-required onchange="doc_loadthumbnail(this)">
							</button>
							<div class="ip_reg_modal_addphoto">
								<img src="" id="reg-doc-temppic">
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>
<!-- 			<div class="ip_edit_row">
							<p class="ip_row_p"><?php load_language('price');?>*</p>
							  <div class="ip_bank_detail_frame">
								  <input data-parsley-required onKeyPress="if(this.value.length > 5) return false;" data-parsley-minlength="2" data-parsley-maxlength="5" class="ip_bank_input" type="number" name="price" placeholder="R$" >
							  </div>
						</div>
 -->

              <div class="ip_coloborator_btn_bay textCenter">
                <button class="ip_colaborator_btn" type="submit"><?php load_language('create_an_account');?></button>
              </div>
          </form>
          </div>
    </div>
  </div>
</div>

