

    <div class="container ip_custom_container" >
		<div class="col-md-3">
			<form id="searchfilter_form">
			<div class="ip_filter_div">
				<div class="dropdown ip_filter_dropdown pl15">
				<div class="dropdown-toggle ip_filter_drop_toggle" data-toggle="collapse" data-target="#ip_filter"><?php load_language('filters');?></div>
				</div>
				<div id="ip_filter" class="collapse in">
				<!-- <hr>
				<h5 class="pl15">AVALIACOES</h5>
				<div class="ip_star_rate pl15">
					<label onclick="" class="ip_star_rate_toggle_btn">
					<input type="radio" value="1" class="filter-change" name="filter_dr_rating"/>1</label>
					<label onclick="" class="ip_star_rate_toggle_btn">
					<input type="radio" value="2" class="filter-change" name="filter_dr_rating"/>2</label>
					<label onclick="" class="ip_star_rate_toggle_btn">
					<input type="radio" value="3" class="filter-change" name="filter_dr_rating"/>3</label>
					<label onclick="" class="ip_star_rate_toggle_btn">
					<input type="radio" value="4" class="filter-change" name="filter_dr_rating"/>4</label>
					<label onclick="" class="ip_star_rate_toggle_btn">
					<input type="radio" value="5" class="filter-change" name="filter_dr_rating"/>5</label>
				</div> -->
			<!-- 	<hr>
				<h5 class="pl15">DISTANCIA</h5>
				<div class="ip_box_drop_down pl15">
				<div class="dropdown ip_dropdown">
				<div class="dropdown-toggle ip_drop_toggle" type="button" data-toggle="dropdown">Centro da Cidade</div>
					<ul class="dropdown-menu ip_dropdown_menu">
					  <li><a href="#">Dummy 1</a></li>
					  <li><a href="#">Dummy 2</a></li>
					  <li><a href="#">Dummy 3</a></li>
					  <li><a href="#">Dummy 4</a></li>
					  <li><a href="#">Dummy 5</a></li>
					  <li><a href="#">Dummy 6</a></li>
					</ul>
				</div>
				</div> -->
				<hr>
				<div class="ip_distance_slider pl15">
					<div id="ip_filter_distance_range"></div>
					<div class="ip_filter_range_count">
						<input type="text" id="ip_filter_distance_start" name="filter_dr_srch_distance_start" class="ip_distance_count floatLeft filter-change" readonly >
						<input type="text" id="ip_filter_distance_end" name="filter_dr_srch_distance_end" class="ip_distance_count floatRight filter-change" readonly >
						<div class="clear"></div>
					</div>
				</div>
				<hr>
				<select class="ip_box_drop_down ip_dropdown pl15 filter-change" placeholder="" name="doctor-search-speciality" id="filter_dr_srch_speciality">
               	<option disabled selected><?php load_language('speciality');?></option>
			    <?php foreach ($speciality_list as $key => $value) {
			    ?>
			    	<option value="<?php echo $value['specialization_name']?>"><?php echo $value['specialization_name']?></option>
		        <?php	
		        }
		        ?>
			  	</select>
			  	<input type="hidden" name="selected_doctor_type" value="speciality" id="selected_doctor_type">
				<hr>
				<h5 class="pl15 uppercase"><?php load_language('location');?></h5>
				<div class="ip_box_drop_down pl15">
				<input class="ip_dropdown filter-change" type="text" placeholder="<?php load_language('enter_location');?>" id="filter_dr_srch_loc" name="doctor-search-location">
				<input  type="hidden"  id="filter_dr_srch_lat" name="doctor-search-latitude">
				<input  type="hidden"  id="filter_dr_srch_lng" name="doctor-search-longitude">
				
				</div>			
				<hr>
				<h5 class="pl15"><?php load_language('return_included');?></h5>
				<div class="ip_return pl15">
					<label onclick="" class="ip_return_option_toggle_btn">
					<input type="radio" class="filter-change uppercase" name="filter_dr_return_included" value="1" /><?php load_language('yes');?></label>
					<label onclick="" class="ip_return_option_toggle_btn">
					<input type="radio" class="filter-change uppercase" name="filter_dr_return_included" value="0"/><?php load_language('no');?></label>
				</div>
				<hr>
				</div>
				<div class="dropdown ip_filter_dropdown pl15">
				<div class="dropdown-toggle ip_filter_drop_toggle" data-toggle="collapse" data-target="#ip_filter1">VER NO MAPA</div>
				</div>
				<div id="ip_filter2" class="collapse in">
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="ip_result_div">
				<div class="ip_result_settings_bay">
					<div class="row">
						<div class="col-md-3">
            
             <div class="ip_calender_div">
              <input class="ip_calender ip_datentime filter-change" readonly id="ip_datepicker_srch" name="doctor-search-date">
             </div>
						<!-- 	<input class="ip_time ip_datentime" id="ip_timepicker"> -->
							<div class="clear"></div>
						</div>
						<div class="col-md-9 p0">
							<div class="ip_other_settings">
							<p>
								<?php if(!empty($searchdata['doctor-search-speciality'])){?><span><?php load_language('search_result_for') ?> <a id="search-data-speciality"><?php echo $searchdata['doctor-search-speciality'];?></a></span><?php }?>
								<?php if(!empty($searchdata['doctor-search-location'])){?><span> localiza em <strong><img src="<?php echo base_url();?>assets/images/ip_location.png"></strong><a id="search-data-location"><?php echo $searchdata['doctor-search-location'];?></a></span><?php }?>
							</p>
							</div>
						</div>
					</div>
				</div>
				<div class="ip_filter_settings">
					<div class="row">
						<div class="col-md-7">
							<div class="ip_price_filter">
								<h5><?php load_language('filter_on_value');?></h5>
								<div id="ip_price_slider"></div>
								<div class="ip_filter_price_count">
									<input type="text" id="ip_filter_price_low" name="filter_dr_srch_price_low" class="ip_price_count floatLeft filter-change" readonly >
									<input type="text" id="ip_filter_price_high" name="filter_dr_srch_price_high" class="ip_price_count floatRight filter-change" readonly >
									<div class="clear"></div>
								</div>
							</div>
						</div>
						<div class="col-md-5">
							<div class="floatLeft">
								<label onclick="" class="ip_filter_more_list_toggle_btn">
									<input id="ip_filter_dr_domicilar" type="checkbox" class="filter-change" value="true" name="filter_dr_domicilar"/><?php load_language('home_visit');?></label>
							</div>
							<div class="ip_filter_more_list floatLeft widthAuto">
									

									<label onclick=""  class="ip_filter_more_list_toggle_btn">
									<input type="radio" id="dctr-filter-male" value="MALE" class="filter-change" name="filter_dr_gender"/><?php load_language('men');?></label>

									<label onclick="" class="ip_filter_more_list_toggle_btn">
									<input type="radio" id="dctr-filter-female" value="FEMALE" class="filter-change" name="filter_dr_gender"/><?php load_language('women');?></label>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</div>


				<!-- <div class="ip_sort_more_list">
					<input type="checkbox" name="group3"/>Order by</label>
				</div>
                <div class="ip_sort_more_list">
					<input type="checkbox" name="group3"/>Price</label>
				</div>
                <div class="ip_sort_more_list">
					<input type="checkbox" name="group3"/>Availabilities</label>
				</div> -->
				<div class="ip_sort_settings">
					<!-- <li><?php load_language('order_on');?></li>
					<li>
						<input type="radio" id="ip_filter_price_orderby_inc" value="ASC" class="filter-change" name="filter_dr_order_price"/>
						<label for="ip_filter_price_orderby_inc"><p><span><?php load_language('price_up');?></span></p></label>
					</li>
					<li>
						<input type="radio" id="ip_filter_price_orderby_desc" value="DESC" class="filter-change" name="filter_dr_order_price"/>
						<label for="ip_filter_price_orderby_desc"><p><span><?php load_language('price_down');?></span></p></label>
					</li> -->
				</div>
			</form>
				<div class="ip_result_listing">
					<!-- <div class="ip_result_listing_loader hidden" id="search_filter_loader"></div> -->
					<ul id="searchresult">
						
					<?php 

					$this->load->view('search_doctor_result'); ?>
						
					</ul>
				</div>
				<div>
					<!-- <p id="load-more">LOAD MORE</p> -->
				</div>
			</div>
		</div>
    </div>

    

    