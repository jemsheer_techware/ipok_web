
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">
    <div class="ip_edit_record_wrapper">
      <div class="row">
        <div class="col-md-8">
          <div class="ip_edit_record_cover">
            <div class="ip_edit_record_head backgroundnone">
              <?php load_language('edit_collaborator');?>
            </div>
              <div class="ip_edit_record_detail">
                <form id="edit-colaborator-form" role="form" data-parsley-validate="">

                <div class="ip_edit_row">
                  <div class="ip_bank_detail_frame">
                    <input class="ip_bank_input" data-parsley-required="" name="name" onKeyPress="if(this.value.length > 40) return false;" data-parsley-minlength="5" data-parsley-pattern="^[a-zA-Z ]+$"  placeholder="<?php load_language('name');?>" data-parsley-required="" value="<?php echo $collaborator_data['name'];?>">
                  </div>
                </div>

                <div class="ip_edit_row">
                    <div class="ip_bank_detail_frame">
                      <input class="ip_bank_input" disabled placeholder="<?php load_language('email');?>"  onKeyPress="if(this.value.length > 75) return false;" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="<?php echo $collaborator_data['email'];?>">
                  </div>
                </div>

               <!--  <div class="ip_edit_row">
                      <div class="ip_bank_detail_frame">
                      <input class="ip_bank_input" name="password" data-parsley-required="" placeholder="Password" data-parsley-required="" data-parsley-minlength="8" onKeyPress="if(this.value.length > 25) return false;" type="Password">
                      </div>
                </div> -->

                <div class="ip_edit_row">
                  <div class="row">
                    <div class="col-md-6">
                      <p class="ip_row_p"><?php load_language('telephone');?></p>
                      <div class="ip_bank_detail_frame">
                          <input class="ip_bank_input" data-parsley-required="" type="number" onKeyPress="if(this.value.length > 25) return false;" placeholder="" name="telephone" value="<?php echo $collaborator_data['telephone'];?>">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <p class="ip_row_p">CPF</p>
                      <div class="ip_bank_detail_frame">
                          <input class="ip_bank_input" data-parsley-required="" name="cpf" placeholder="" data-parsley-required="" data-parsley-minlength="11" data-parsley-cpf="" onKeyPress="if(this.value.length > 10) return false;"  type="number" value="<?php echo $collaborator_data['cpf'];?>">
                      </div>
                    </div>
                  </div>
                </div>

                <div class="ip_edit_row">
                   <div class="col-md-6">
                      <p class="ip_row_p"><?php load_language('add_photo');?>:</p>
                      <div class="ip_reg_modal_addphoto floatLeft">
                      <img  id="doc-edit-colabor-pic" src="<?php echo base_url(); echo $collaborator_data['image']; ?>">
                      </div>

                      <div class="ip_add_photo floatRight">
                      <input name="image"   type="file" accept="image/*"   class="ip_reg_form_input"
                      data-parsley-error-message="<?php load_language('profile_photo_error_text');?>" onchange="readURL(this,'doc-edit-colabor-pic')" id="colabor-pic-edit">

                      </div>
                   </div>
                   <div class="col-md-6"></div>
                 
                <div class="clear"></div>
              </div>
              </form>
              </div>
              <hr>
              <div class="ip_coloborator_btn_bay">
                <a href="javascript:void(0)">
                  <button class="ip_colaborator_btn" type="button" id="edit-colaborator-btn" collabor="<?php echo $collaborator_data['id'];?>"><?php load_language('update_and_save');?></button>
                </a>
                <a href="javascript:void(0)">
                  <button class="ip_colaborator_delete_btn" type="button" id="delete-colaborator-btn" colabor="<?php echo $collaborator_data['id'];?>" ><?php load_language('delete_collaborator');?></button>
                </a>
              </div>
              <div class="alert alert-success alert-dismiss textCenter hidden" id="colabor-edit-success"><?php load_language('collaborator_update_success');?></div>
              <div class="alert alert-danger alert-dismiss textCenter hidden" id="colabor-edit-error"><?php load_language('collaborator_update_error');?></div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="ip_edit_record_cover " id="authorize-access-div">

            <div class="ip_edit_record_head backgroundnone">
              <?php load_language('collaborator_access_heading');?>
            </div>
              <div class="ip_edit_record_detail">
                <form data-parsley-validate="" role="form" id="colabor-auth-access-form">
                <div class="ip_edit_row">
                  <br>
                  <br>
                    <p class="ip_row_p"><?php load_language('collaborator_access_desc');?>
                      
                    </p>
                </div>
                <br>
                <div class="ip_notify_time">

                  <li>
                    <div class="ip_day_time_schedule_details_data p0">
                         <input type="hidden" name="section" value="authorizeaccess">
                         <input id="checkbox-access-appoint" class="ip_custom_checkbox1" name="access[]" type="checkbox" value="Doctor/collaborator">
                         <label for="checkbox-access-appoint" class="ip_custom_checkbox_label1"><?php load_language('appointment');?></label>
                         <div class="clear"></div>
                     </div>
                  </li>

                  <li>
                    <div class="ip_day_time_schedule_details_data p0">
                         <input id="checkbox-access-msg" class="ip_custom_checkbox1" name="access[]" type="checkbox" value="Doctor/chat" >
                         <label for="checkbox-access-msg" class="ip_custom_checkbox_label1"><?php load_language('messages');?></label>
                         <div class="clear"></div>
                     </div>
                  </li>                  
                  <div class="clearfix"></div>
                </div>
                <br>
              </div>
            </form>
              <hr>
              <div class="ip_coloborator_btn_bay">
                <button class="ip_colaborator_btn floatRight" type="button" colabor="<?php echo $collaborator_data['id'];?>"  id="colabor-auth-access"><?php load_language('collaborator_access_save_button');?></button>
                <div class="clear"></div>
                <br>
                <div class="alert alert-danger alert-dismiss textCenter hidden" id="colabor-auth-access-error"><?php load_language('collaborator_not_selected');?></div>
                <div class="alert alert-success alert-dismiss textCenter hidden" id="colabor-auth-access-success"><?php load_language('collaborator_access_success');?></div>
              </div>

          </div>
        </div>

      </div>
    </div>
  </div>
</div>


<div id="success-collaborator-del" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="ip_custom_modal" style="min-height:200px !important;">
      <div class="ip_custom_modal_content">
        <div class="ip_main_tab_content ">
              <div class="ip_main_tab_pic">
                <img src="<?php echo base_url();?>assets/images/tick.png">
              </div>
              <div class="success_content">
                <h1><strong><?php load_language('success');?></strong></h1>
                <h5><strong><?php load_language('collaborator_excluded');?></strong></h5>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>

<script>
  var checkbox_appoint = document.getElementById('checkbox-access-appoint');
  var checkbox_msg = document.getElementById('checkbox-access-msg');

  <?php if(!empty($collaborator_data['capabilities'])){
    $permissions = explode(',', $collaborator_data['capabilities']);
    foreach ($permissions as $key => $value)
     {
        if($value== 'Doctor/collaborator')
        {
        ?>
          checkbox_appoint.checked = true;
        <?php
        }
        if($value== 'Doctor/chat')
        {
        ?>
          checkbox_msg.checked = true;
        <?php
        }
      }  
    }

  ?>


  
  
</script>
