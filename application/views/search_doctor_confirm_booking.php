
<?php
if($this->session->userdata('language') == 'en'){
  //$genderphoto = 'Choose Profile Photo';
  $year_lang = 'Year is required.';
  $month_lang  = 'Month is required.';
  $cvv_lang  = 'CVV is required.';
}else{
  //$genderphoto = "Escolha a foto do perfil";
   $year_lang = 'O ano é obrigatório.';
  $month_lang  = 'Mês é obrigatório.';
  $cvv_lang  = 'O CVV é obrigatório.';
}
?>
  <div class="container ip_custom_container">
    <div class="ip_main_tabs_div">
      <div class="ip_main_tab_head">
        <ul>
          <li class="active confirm-tab-1" ><?php load_language('review_information');?></li>
          <li class="confirm-tab-2 uppercase"  ><?php load_language('login');?></li>
          <li class="confirm-tab-3"  ><?php load_language('payment');?></li>
          <li class="confirm-tab-4" ><?php load_language('confirmation');?></li>
        </ul>
        <div class="clear"></div>
        <button class="hidden" id="btnTrigger-review" data-toggle="tab" href="#review">
        <button class="hidden" id="btnTrigger-login" data-toggle="tab" href="#login">
        <button class="hidden" id="btnTrigger-payment" data-toggle="tab" href="#payment">
        <button class="hidden" id="btnTrigger-confirmation" data-toggle="tab" href="#confirmation">
      </div>
      <div class="ip_main_tab_content">
        <div class="tab-content">

          <div id="review" class="tab-pane fade in active">
            <div class="ip_main_tab_pic">
              <img src="<?php echo base_url();echo $doctor_data['dr_pic']?>">
            </div>
            <h5><strong><?php echo $doctor_data['dr_name']?></strong></h5>
            <p><?php echo $doctor_data['dr_specialization']?></p>
            <p><?php echo $doctor_data['clinic_name']?>,<?php echo $doctor_data['clinic_street_address']?>,<?php echo $doctor_data['clinic_locality']?>-<?php echo $doctor_data['clinic_cep']?></p>
            <div class="ip_profile_ratting">
             <!--  <fieldset class="ip_rating">
                <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
              </fieldset> -->
              <div class="clear"></div>
            </div>
           <!--  <h4>14th december 2017</h4>
            <h6>16:00 hours</h6> -->
            <form role="form" id="confirm_book_form" data-parsley-validate="">
            <input type="hidden" name="confirm-book-clinic" id="confirm_book_clinic" value="<?php echo $doctor_data['clinic_id']?>">
            <input type="hidden" name="confirm-book-doctor" id="confirm_book_doctor" value="<?php echo $doctor_data['doctorid']?>">
            <input type="hidden" name="book-status" id="book_status" value="1" drprice="<?php echo $doctor_data['dr_price'];?>">
            <input type="hidden" id="entered_book_id" value="">
            <input type="hidden" name="promocode-status" id="promocode_status" value="0">
            <input type="hidden" name="promocode-name" id="promocode_name" value="0">

            <div class="ip_profile_datetime ip_booking_date" >
              <div class="row">
              <div id="booking-container">
                <input type="text" class="ip_calender floatLeft" data-parsley-required="" readonly="" id="confirm_book_date" name="confirm-book-date" >
              </div>
            </div>
            <div class="row">
              
            
              <select data-parsley-required="" id="schedule-consult-timeslot" class="ip_time floatRight" placeholder="" name="confirm-book-time">
                <option disabled selected><?php load_language('time_slot');?></option>
              </select>
              </div>
              <div class="clear"></div>
            </div>
          </form>
			<div class="ip_profile_list_div hidden" id="waitinglist-div">
				<ul>
				<li class="floatLeft" id="enter_waiting_list_btn"><?php load_language('enter_waiting_list_text');?></li>
				<li class="floatRight uppercase"  data-toggle="modal" data-target="#waitinglistmodal"><?php load_language('know_more');?></li>
				<div class="clear"></div>
				</ul>       
			</div>
      <div id="err_confirm_booking" class="alert alert-danger ip_profile_book_error hidden"></div>
      <div id="info_confirm_booking" class="alert alert-info ip_profile_book_error hidden"></div>
     
            <hr>
            <div class="row ip_promo_div">
              <div class="col-md-6">
                <form role="form" id="confirm_book_form_promo" data-parsley-validate="">
                  <input type="hidden" name="doctorid" value="<?php echo $doctor_data['doctorid']?>">
                  <input class="ip_coupon mr15" style="text-transform:uppercase" name="code" placeholder="<?php load_language('coupon_heading');?>" data-parsley-required="" data-parsley-minlength="8" data-parsley-maxlength="8" id="promocode_inp" onKeyPress="if(this.value.length > 7) return false;">
                   <button type="button" id="promocode_submit_btn" class="ip_tab_bottom_btn ip_tab_bottom_btn_back floatLeft mr15" style="width: 20%;"><?php load_language('apply');?></button>
                   <button type="button" id="promocode_cancel_btn" class="ip_tab_bottom_btn ip_tab_bottom_btn_back floatLeft " style="width: 20%;"><?php load_language('clear');?></button>
                </form>
              <div id="promo_success_error" class="alert ip_profile_book_error floatLeft mt30 hidden"></div>
              <div class="clear"></div>
              </div>
              <div class="col-md-6">
                <div class="ip_total_price">
                  <div class="ip_price floatLeft uppercase">
                    <?php load_language('amount');?>
                  </div>
                  <div class="ip_amount floatRight">
                    R$ <?php echo $doctor_data['dr_price'];?>
                  </div>
                  <div class="clear"></div>
                </div>
                <div class="clear"></div>   

                <div class="ip_total_price hidden mt10" id="show_offer_div">
                  <div class="ip_price floatLeft">
                    <?php load_language('offer_price');?>
                  </div>
                  <div class="ip_amount floatRight">
                    R$ <span></span>
                  </div>
                  <div class="clear"></div>
                </div>
                <div class="clear"></div>

                 <div class="ip_total_price mt10" id="show_total_div">
                  <div class="ip_price floatLeft">
                    <?php load_language('total_price');?>
                  </div>
                  <div class="ip_amount floatRight">
                    R$ <span><?php echo $doctor_data['dr_price'];?></span>
                  </div>
                  <div class="clear"></div>
                </div>
                  <div class="clear"></div>
              </div>
            </div>
            <div class="ip_bottom_tab_btn_bay">
            <div class="row">
              <div class="col-md-6">
                <a href="<?php echo base_url();?>Searchdoctor">
                  <button type="button" class="ip_tab_bottom_btn ip_tab_bottom_btn_back floatLeft uppercase"><?php load_language('back');?></button>
                </a>
            
                <div class="clear"></div>
              </div>
              <div class="col-md-6">
                <a href="javascript:void(0)">
                  <button class="ip_tab_bottom_btn ip_tab_bottom_btn_continue floatRight uppercase" id="confirm_booking_continue_btn"><?php load_language('continue');?></button>
                </a>

                <div class="clear"></div>
              </div>
            </div>
          </div>
          </div>

          <div id="login" class="tab-pane fade">
              <h1 class="capitalize"><?php load_language('login');?></h1>
            <div class="ip_main_tab_content_inner">
                   <form role="form" data-parsley-validate="" id="confirm-book-login-form">
                <div class="ip_login_input_form">
                 <input type="hidden" name="login_type" value="PATIENT">
                  <div class="ip_login_input_row">
                    <input name="login-form-username" data-parsley-required class="ip_login_input ip_login_user clear-login-data" placeholder="<?php load_language('login');?>">
                  </div>
                  <div class="ip_login_input_row">
                    <input name="login-form-password" data-parsley-required class="ip_login_input ip_login_pass clear-login-data" placeholder="<?php load_language('password');?>" type="password">
                  </div>
                  <input type="hidden" name="latitude" id="" >
                  <input type="hidden" name="longitude" >
                  <input type="hidden" name="address" >
                  <div class="">
                    <button type="button" class="ip_login_modal_signin floatLeft  capitalize" id="confirm-book-login_submit"><?php load_language('login');?></button>

                    <p class="floatLeft" data-toggle="modal" data-target="#forgot"><?php load_language('forgot_password');?></p>
                    <div class="clear"></div>
                  </div>
                  <div id="err-login-ajax" class="alert alert-danger hidden textCenter "></div>
                </div>
              </form>
               <button type="button" id="tab_login_back" class="ip_tab_bottom_btn ip_tab_bottom_btn_back width100"><?php load_language('back');?></button>
            </div>
          </div>

          <div id="payment" class="tab-pane fade">
            <form id="booking-payment-form" data-parsley-validate="">
            <h1><?php load_language('payment');?></h1>
            <div class="ip_main_tab_content_inner">
              <p><?php load_language('payment_tab_desc');?></p>
              <input data-parsley-required="" data-parsley-minlength="3" data-parsley-maxlength="20" onKeyPress="if(this.value.length > 19) return false;"  class="ip_content_inner_input payment_firstname" data-parsley-pattern="^[a-zA-Z ]+$" placeholder="<?php load_language('first_name');?>" name="firstname">
              <input  data-parsley-required="" data-parsley-minlength="3" data-parsley-maxlength="20" onKeyPress="if(this.value.length > 19) return false;" data-parsley-pattern="^[a-zA-Z ]+$" class="ip_content_inner_input payment_lastname" placeholder="<?php load_language('last_name');?>" name="lastname">
              <input  data-parsley-required="" data-parsley-minlength="13" data-parsley-maxlength="16" onKeyPress="if(this.value.length > 15) return false;" data-parsley-pattern="^[0-9]+$" class="ip_content_inner_input payment_cardnum" type="number" placeholder="<?php load_language('card_number');?>" name="cardnumber">
              <div class="ip_card_validity">
                <div class="a1"><span><?php load_language('expiration_date');?></span></div>
                <div class="a1 ip_sel_mm">
                  <select class="ip_validity_select" name="month" data-parsley-required="" data-parsley-error-message="<?php echo $month_lang;?>">
                    <option selected disabled >MM</option>
                    <?php for ($i=01; $i <13 ; $i++) { 
                    ?>
                      <option value="<?php echo $i?>"><?php echo $i?></option>
                    <?php 
                    }
                    ?>
                  </select>
                </div>
                <div class="a1 ip_sel_yy">
                  <select class="ip_validity_select" name="year" data-parsley-required="" data-parsley-error-message="<?php echo $year_lang;?>">
                    <option selected disabled>YY</option>
                    <?php
                    $this_year = date("Y"); // Run this only once
                    for ($year = $this_year; $year <= $this_year + 35; $year++) {
                    ?>
                      <option value="<?php echo $i?>"><?php echo $year?></option>
                    <?php 
                    }
                    ?>
                  </select>
                </div>
                <div class="a1 mr0 ip_sel_dd">
                  <input  type="number"  data-parsley-required="" data-parsley-error-message="<?php echo $cvv_lang;?>" data-parsley-minlength="3" data-parsley-maxlength="3" onKeyPress="if(this.value.length > 2) return false;" data-parsley-pattern="^[0-9]+$" class="ip_content_inner_input mb0" placeholder="<?php load_language('cvv');?>" name="cvv" style="width:60px;">
                </div>
                <div class="ip_date_img">
                  <img src="<?php echo base_url();?>assets/images/ip_ques.png">
                </div>
                <div class="clear"></div>
              </div>
              <div class="width100 textCenter">
                <button type="button" id="book_payment_btn" class="ip_makepayment_btn">
                  <?php load_language('make_payment');?>
                </button>
              </div>
              <div  class="width100 textCenter p5">
                <button type="button" id="tab_payment_back" class="ip_tab_bottom_btn ip_tab_bottom_btn_back ip_tab_payment_back"><?php load_language('back');?></button>
              </div>
              <br>
              <div id="payment-error-div" class="alert alert-danger text-center hidden"></div>
            </div>
            </form>
          </div>

          <div id="confirmation" class="tab-pane fade">
            <h1><?php load_language('consultation_confirmed');?></h1>
            <br>
            <div class="ip_main_tab_content_inner">
              <div class="width100 textCenter">
                 <div class="ip_main_tab_pic">
                  <img src="<?php echo base_url();echo $doctor_data['dr_pic']?>">
                </div>
                <h5><strong><?php echo $doctor_data['dr_name']?></strong></h5>
                <h6><?php echo $doctor_data['dr_specialization']?></h6>
                <p class="ip_booking_confirm_detail">
                  <?php echo $doctor_data['clinic_name']?><br>
                  <?php echo $doctor_data['clinic_street_address']?>,
                  <?php echo $doctor_data['clinic_locality']?>-<?php echo $doctor_data['clinic_cep']?><br>
                  <br>
                  <span id="book-date-show"> </span><br><span id="book-time-show"> </span>
                </p>
                <br>
                <br>
                <br>
                <a href="<?php echo base_url();?>Patient">
                <button type="button"  class="ip_makepayment_btn"><?php load_language('done');?></button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!-- WAITING-LIST-MODAL -->

<div id="waitinglistmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="ip_waiting_list_modal_wrapper">
      <div class="ip_waiting_list">
        <div class="ip_waiting_list_head capitalize">
          <?php load_language('waiting_list');?>
        </div>
        <div class="ip_waiting_list_body">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <p> <?php echo $policy;?></p>
           
        </div>
      </div>
    </div>
  </div>
</div>

<div id="select_cash_mode" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="ip_custom_modal_wrapper">
      <div class="ip_custom_modal_head2">
       <?php load_language('form_of_payment');?>
      </div>
      <div class="ip_custom_modal_outter" style="border-radius: 0px;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="ip_custom_modal_content">
          <div class="ip_main_tab_content textCenter">
            <button class="ip_modal_gen_btn ip_gen_btn1" id="money_cash" value="1"><?php load_language('money');?></button>
            <input type="hidden"  value="1" name="bkid">
            <button class="ip_modal_gen_btn ip_gen_btn2" id="credit_card_cash" value="2"><?php load_language('credit_card');?></button>
            <input type="hidden"  value="2" name="bkidd">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
