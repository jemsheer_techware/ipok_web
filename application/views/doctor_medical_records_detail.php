
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">
    <div class="ip_top_dash_bay">
      <div class="row">
        <div class="col-md-3">
          <div class="ip_top_dash_list">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_appointments.png">
            </div>
            <div class="ip_top_dash_detail">
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_attendance']?>">0</strong>
              <p><?php load_language('attendance_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="ip_top_dash_list">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_feature.png">
            </div>
            <div class="ip_top_dash_detail" >
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_billed']?>">0</strong>
              <p><?php load_language('billed_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="ip_top_dash_list">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_paintences.png">
            </div>
            <div class="ip_top_dash_detail">
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_patients']?>">0</strong>
              <p><?php load_language('patients_visited_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="ip_top_dash_list bordernone">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_vistors.png">
            </div>
            <div class="ip_top_dash_detail">
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_profileview']?>">0</strong>
              <p><?php load_language('profile_views_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="ip_main_path_stream">
      <ul>
        <li><?php load_language('dashboard');?><span><img src="<?php echo base_url();?>assets/images/ip_tab_list_arw.png"></span></li>
        <li><?php load_language('medical_records');?><span><img src="<?php echo base_url();?>assets/images/ip_tab_list_arw.png"></span></li>
      </ul>
    </div>
    <hr>
    <div class="ip_message_tabs_body">
      <h3><?php echo $patient_data['pt_name']?></h3>
    </div>
    <div class="ip_record_main_div">
      <div class="ip_record_main_head">
        <div class="row m0 height100">
          <div class="col-md-3 p0 height100"><div class="ip_record_main_head_title"><?php load_language('date');?></div></div>
          <div class="col-md-3 p0 height100"><div class="ip_record_main_head_title"><?php load_language('time');?></div></div>
          <div class="col-md-3 p0 height100"><div class="ip_record_main_head_title"><?php load_language('consultation_value');?></div></div>
          <!-- <div class="col-md-2 p0 height100"><div class="ip_record_main_head_title">Ratting</div></div> -->
          <div class="col-md-3 p0 height100"><div class="ip_record_main_head_title"><div class="ip_message_more_btn"></div></div></div>
        </div>
      </div>
      <ul>
        <?php 
        if(!empty($consultation_list))
        {
          foreach ($consultation_list as $key => $value) 
          {
        ?>

         <li>
            <div class="row m0 height100">
              <div class="col-md-3 p0 height100"><div class="ip_record_main_head_data"><strong><?php load_language('consultation');?>: </strong><?php echo date('d M - Y',$value['book_date']);?></div></div>
              <div class="col-md-3 p0 height100"><div class="ip_record_main_head_data"><?php echo $value['book_time'];?></div></div>
              <?php if(!empty($value['promo']))
              {
              ?>
                <div class="col-md-3 p0 height100"><div class="ip_record_main_head_data"><?php load_language('promotional_consultation');?> : R$ <?php echo $value['book_total'];?></div></div>
              <?php
              }
              else
              {
              ?>
                <div class="col-md-3 p0 height100"><div class="ip_record_main_head_data"><?php load_language('normal_consultation');?> : R$ <?php echo $value['book_total'];?></div></div>
              <?php
              } 
              ?>
              
             <!--  <div class="col-md-2 p0 height100">
                <div class="ip_record_main_head_data">
                  <form id="ip_user_rating_form">
                    <div id="ip_selected_rating" class="ip_selected_rating floatLeft">5.0</div>
                    <span class="ip_user_rating floatLeft">
                      <input type="radio" name="rating" value="5.0"><span class="star"></span>
                      <input type="radio" name="rating" value="4.0"><span class="star"></span>
                      <input type="radio" name="rating" value="3.0"><span class="star"></span>
                      <input type="radio" name="rating" value="2.0"><span class="star"></span>
                      <input type="radio" name="rating" value="1.0"><span class="star"></span>
                    </span>
                    <div class="clear"></div>
                  </form>
                </div>
              </div> -->
              <div class="col-md-3 p0 height100">
                <div class="ip_record_main_head_data">
                  <a href="javascript:void(0)"><button book="<?php echo $value['book_id'];?>" class="ip_reader_btn show_record_recordview_btn" type="button"><?php load_language('open_record');?></button></a>
                </div>
              </div>
            </div>
        </li>

        <?php 
          }
        }
        else
        {
        ?>

        <li>
        <div class="row m0 height100">
          <div class="col-md-2 p0 height100"></div>
          <div class="col-md-2 p0 height100"></div>
          <div class="col-md-3 p0 height100">
            <div class="ip_record_main_head_data"><?php load_language('no_consultations');?></div>
          </div>
          <div class="col-md-2 p0 height100"> </div>
              <div class="col-md-3 p0 height100"></div>
        </div>
        </li>
        <?php
        }
        ?>
       
      </ul>
    </div>

    <!-- RECORDS DIV STARTS -->
    <!-- <div class="ip_records_tab" id="med_rec_view">
      <div class="row m0">
        <div class="col-md-2 p0">
          <div class="ip_record_section_header">
            08 Sept, 13.00 - 14:00
          </div>
        </div>
        <div class="col-md-10 p0">
          <div class="ip_record_header1">
            <div class="ip_record_back">
              <span class="settings"><img src="<?php echo base_url();?>assets/images/ip_arw_left.png"></span>
            </div>
            <div class="ip_record_search_box">
              <input class="ip_record_search_box_input" type="text" placeholder="Search">
            </div>
            <div class="ip_record_settings">
              <span class="settings"><img src="<?php echo base_url();?>assets/images/ip_settings.png"></span>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
      <div class="ip_bio_tab_div m0">
        <div class="row m0">
          <div class="col-md-2 p0 height100">
            <div class="ip_bio_tab_bay height100">
              <ul>
                <li class="active" data-toggle="tab" href="#record">Records</li>
                <li data-toggle="tab" href="#prescription">Prescription</li>
                <li data-toggle="tab" href="#exam">Exams</li>
                <li data-toggle="tab" href="#other">Others</li>
                <li data-toggle="tab" href="#ratting">Ratting</li>
              </ul>
            </div>
          </div>
          <div class="col-md-10 p0">
            <div class="ip_bio_tab_content">
              <div class="tab-content">
                <div class="ip_profile_tab_top">
                  <div class="ip_profile_tab_circle">
                    <img src="<?php echo base_url();?>assets/images/ip_pic.png">
                  </div>
                  <div class="ip_profile_tab_name">
                    <h3>Dr. Anna Alexander</h3>
                  </div>
                  <div class="ip_profile_tab_button">
                    <div class="ip_profile_tab_button_circle" onclick="location.href='<?php echo base_url();?>Doctor/recordsedit'"><img src="<?php echo base_url();?>assets/images/ip_edit.png"></div>
                    <div class="ip_profile_tab_button_circle"><img src="<?php echo base_url();?>assets/images/ip_delete.png"></div>
                    <div class="ip_profile_tab_button_circle"><img src="<?php echo base_url();?>assets/images/ip_msg.png"></div>
                    <div class="clear"></div>
                  </div>
                  <div class="clear"></div>
                </div>
                <div id="record" class="tab-pane fade in active">

                  <div class="ip_profile_tab_detail">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text
                       ever since the 1500s, when an unknown printer took a galley of type and scrambled it to when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing </p>
                  </div>
                </div>
                <div id="prescription" class="tab-pane fade">
                  <div class="ip_profile_tab_detail">
                    <div class="ip_prescription_div">
                      <ul>
                          <li>
                            <h5>Broncoflux 30mg</h5>
                            <p>been the industry's standard </p>
                            <h6>since the 1500s, when an unknown printer took a</h6>
                          </li>
                          <li>
                            <h5>Broncoflux 30mg</h5>
                            <p>been the industry's standard </p>
                            <h6>since the 1500s, when an unknown printer took a</h6>
                          </li>
                          <li>
                            <h5>Broncoflux 30mg</h5>
                            <p>been the industry's standard </p>
                            <h6>since the 1500s, when an unknown printer took a</h6>
                          </li>
                          <li>
                            <h5>Broncoflux 30mg</h5>
                            <p>been the industry's standard </p>
                            <h6>since the 1500s, when an unknown printer took a</h6>
                          </li>
                          <li>
                            <h5>Broncoflux 30mg</h5>
                            <p>been the industry's standard </p>
                            <h6>since the 1500s, when an unknown printer took a</h6>
                          </li>
                          <li>
                            <h5>Broncoflux 30mg</h5>
                            <p>been the industry's standard </p>
                            <h6>since the 1500s, when an unknown printer took a</h6>
                          </li>
                          <li>
                            <h5>Broncoflux 30mg</h5>
                            <p>been the industry's standard </p>
                            <h6>since the 1500s, when an unknown printer took a</h6>
                          </li>
                          <div class="clear"></div>
                      </ul>
                    </div>
                  </div>
                </div>
                <div id="exam" class="tab-pane fade">
                  <div class="ip_profile_tab_detail">
                    <h3>Specialization</s></h3>
                  </div>
                </div>
                <div id="other" class="tab-pane fade">
                  <div class="ip_profile_tab_detail">
                    <h3>Photos</h3>
                  </div>
                </div>
                <div id="ratting" class="tab-pane fade">
                  <div class="ip_profile_tab_detail">
                    <div class="ip_ratting_tab_content">
                      <div class="row m0">
                          <div class="ip_profile_tab_circle">
                            <img src="<?php echo base_url();?>assets/images/ip_pic.png">
                          </div>
                          <div class="ip_profile_tab_name">
                            <h3>Dr. Anna Alexander</h3>
                            <form id="ip_user_rating_form">
                              <div id="ip_selected_rating" class="ip_selected_rating floatLeft">5.0</div>
                              <span class="ip_user_rating floatLeft">
                                <input type="radio" name="rating" value="5.0"><span class="star"></span>
                                <input type="radio" name="rating" value="4.0"><span class="star"></span>
                                <input type="radio" name="rating" value="3.0"><span class="star"></span>
                                <input type="radio" name="rating" value="2.0"><span class="star"></span>
                                <input type="radio" name="rating" value="1.0"><span class="star"></span>
                              </span>
                              <div class="clear"></div>
                            </form>
                          </div>
                          <div class="ip_profile_tab_name_detail">
                            <p> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>
                          </div>
                          <div class="clear"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->
    <div id="load-med-record-view">
      <?php $this->load->view('doctor_medical_records_recordview') ?>
    </div>
    <!-- RECORD DIV ENDS -->

  </div>
</div>



<div id="sentmail-dialog-success" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="sentmail-dialog-head"><?php load_language('medical_record');?></h4>
      </div>
      <div class="modal-body">
        <p id="sentmail-dialog-content"><?php load_language('medical_record_sent_success');?>.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php load_language('close');?></button>
      </div>
    </div>

  </div>
</div>

<div id="sentmail-dialog-error" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="sentmail-dialog-head"><?php load_language('medical_record');?></h4>
      </div>
      <div class="modal-body">
        <p id="sentmail-dialog-content"><?php load_language('medical_record_sent_error');?>.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php load_language('close');?></button>
      </div>
    </div>

  </div>
</div>

