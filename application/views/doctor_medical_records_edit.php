
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">

    <div class="ip_main_path_stream">
      <ul>
        <li>Dashboard<span><img src="../assets/images/ip_tab_list_arw.png"></span></li>
        <li>Medical Records<span><img src="../assets/images/ip_tab_list_arw.png"></span></li>
      </ul>
    </div>
    <hr>
    <div class="ip_edit_record_wrapper">
      <div class="ip_edit_record_name">
        <h5>Mariya Samie</h5>
        <p>08 Sept, 13:00 - 14:00<img src="../assets/images/ip_menu5.png"></p>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="ip_edit_record_cover">
            <div class="ip_edit_record_head" data-toggle="collapse" data-target="#records">
              Record
            </div>
            <div id="records" class="collapse in">
            <div class="ip_edit_record_detail">
              <div class="ip_edit_text_bay">
                <div class="ip_edit_record_text">
                  <ul>
                    <li><img src="../assets/images/ip_edit1.png"></li>
                    <li><img src="../assets/images/ip_edit2.png"></li>
                    <li><img src="../assets/images/ip_edit3.png"></li>
                    <li><img src="../assets/images/ip_edit4.png"></li>
                    <li><img src="../assets/images/ip_edit5.png"></li>
                    <li><img src="../assets/images/ip_edit6.png"></li>
                    <li><img src="../assets/images/ip_edit7.png"></li>
                    <li><img src="../assets/images/ip_edit8.png"></li>
                    <div class="clear"></div>
                  </ul>
                </div>
                <textarea class="ip_edit_record_content_textarea" rows="4"></textarea>
              </div>
              <div class="ip_edit_bottom_btn_bay">
                <button class="ip_edit_save_btn floatRight">SAVE</button>
                <button class="ip_edit_cancel_btn floatRight">CANCEL</button>
                <div class="clear"></div>
              </div>
            </div>
          </div>
          </div>
          <div class="ip_edit_record_cover">
            <div class="ip_edit_record_head" data-toggle="collapse" data-target="#receipt">
              Receipts
            </div>
            <div id="receipt" class="collapse in">
              <div class="ip_edit_record_detail">
                <div class="ip_edit_row">
                  <div class="ip_bank_detail_frame">
                      <select class="ip_bank_input">
                          <option>Medication</option>
                      </select>
                  </div>
                </div>
                <div class="ip_edit_row">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="ip_bank_detail_frame">
                          <select class="ip_bank_input">
                              <option>	Amount</option>
                          </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="ip_bank_detail_frame">
                          <select class="ip_bank_input">
                              <option>Dosage and Administration</option>
                          </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="ip_edit_bottom_btn_bay">
                  <button class="ip_edit_save_btn floatRight">SAVE</button>
                  <button class="ip_edit_cancel_btn floatRight">CANCEL</button>
                  <div class="clear"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="ip_edit_record_cover">
            <div class="ip_edit_record_head" data-toggle="collapse" data-target="#examination">
              Examination
            </div>
            <div id="examination" class="collapse in">
              <div class="ip_edit_record_detail">
                <div class="ip_edit_row">
                  <div class="ip_bank_detail_frame">
                      <select class="ip_bank_input">
                          <option>	Examination or procedure</option>
                      </select>
                  </div>
                </div>
                <div class="ip_edit_row">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="ip_bank_detail_frame">
                          <select class="ip_bank_input">
                              <option>Observations</option>
                          </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="ip_edit_bottom_btn_bay">
                        <button class="ip_edit_save_btn floatRight">SAVE</button>
                        <button class="ip_edit_cancel_btn floatRight">CANCEL</button>
                        <div class="clear"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="ip_edit_record_cover">
            <div class="ip_edit_record_head" data-toggle="collapse" data-target="#other">
              Others
            </div>
            <div id="other" class="collapse in">
              <div class="ip_edit_record_detail">
                <div class="ip_edit_row">
                  <div class="ip_bank_detail_frame">
                      <select class="ip_bank_input">
                          <option>Other observation</option>
                      </select>
                  </div>
                </div>
                <div class="ip_edit_text_bay">
                  <div class="ip_edit_record_text">
                    <ul>
                      <li><img src="../assets/images/ip_edit1.png"></li>
                      <li><img src="../assets/images/ip_edit2.png"></li>
                      <li><img src="../assets/images/ip_edit3.png"></li>
                      <li><img src="../assets/images/ip_edit4.png"></li>
                      <li><img src="../assets/images/ip_edit5.png"></li>
                      <li><img src="../assets/images/ip_edit6.png"></li>
                      <div class="clear"></div>
                    </ul>
                  </div>
                  <textarea class="ip_edit_record_content_textarea" rows="3"></textarea>
                </div>
              </div>
            </div>
            <hr>
            <div class="ip_edit_attachement">
              <div class="ip_edit_record_detail pt0">
              <h6><img src="../assets/images/ip_attachment.png">Attach images</h6>
              <ul>
                <li>
                  <div class="ip_attach_file_name">67asd4hbhb.psd</div>
                  <div class="ip_attach_file_size">657 KB</div>
                  <div class="ip_attach_close"><img src="../assets/images/ip_attach_close.png"></div>
                  <div class="clear"></div>
                </li>
                <li>
                  <div class="ip_attach_file_name">67asd4hbhb.psd</div>
                  <div class="ip_attach_file_size">657 KB</div>
                  <div class="ip_attach_close"><img src="../assets/images/ip_attach_close.png"></div>
                  <div class="clear"></div>
                </li>
                <li>
                  <div class="ip_attach_file_name">ui3464778.psd</div>
                  <div class="ip_attach_file_size">784 KB</div>
                  <div class="ip_attach_close"><img src="../assets/images/ip_attach_close.png"></div>
                  <div class="clear"></div>
                </li>
                <li>
                  <div class="ip_attach_file_name">y32578u.psd</div>
                  <div class="ip_attach_file_size">352 KB</div>
                  <div class="ip_attach_close"><img src="../assets/images/ip_attach_close.png"></div>
                  <div class="clear"></div>
                </li>
              </ul>
            </div>
            </div>
            <div class="ip_image_attach">
              <ul>
                <li><img src="../assets/images/ip_image1.jpg"></li>
                <li><img src="../assets/images/ip_image2.jpg"></li>
                <li><img src="../assets/images/ip_upload.png"></li>
              </ul>
            </div>
          </div>
          <button class="ip_circle_btn bal_btn">END OF SERVICE</button>
        </div>
      </div>
    </div>
  </div>
</div>

