
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">
    <div class="ip_top_dash_bay">
      <div class="row">
        <div class="col-md-3">
          <div class="ip_top_dash_list">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_appointments.png">
            </div>
            <div class="ip_top_dash_detail">
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_attendance']?>">0</strong>
              <p><?php load_language('attendance_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="ip_top_dash_list">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_feature.png">
            </div>
            <div class="ip_top_dash_detail" >
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_billed']?>">0</strong>
              <p><?php load_language('billed_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="ip_top_dash_list">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_paintences.png">
            </div>
            <div class="ip_top_dash_detail">
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_patients']?>">0</strong>
              <p><?php load_language('patients_visited_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="ip_top_dash_list bordernone">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_vistors.png">
            </div>
            <div class="ip_top_dash_detail">
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_profileview']?>">0</strong>
              <p><?php load_language('profile_views_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
    <br>    
    
   

<?php 

if(!empty($my_capabilities['capabilities']))
{
 $capability = explode(',', $my_capabilities['capabilities']);
 foreach ($capability as $key => $value) 
 {
    if($value=="Doctor/collaborator")
    {
    ?>
      <div class="row">
      <div class="col-md-12">
        <div class="ip_full_calender_div">
          <div class="ip_full_calender_head">
            <div class="ip_full_calender_nav">
              <div class="btn-group">
                <button type="button" class="btn" disabled id="appointments_day_prevbtn"><img src="<?php echo base_url();?>assets/images/ip_arw_left.png"></button>
                <button type="button" class="btn" id="appointments_day_nextbtn"><img src="<?php echo base_url();?>assets/images/ip_arw_right.png"></button>
              </div>
              <div class="btn-group">
                <button type="button" class="btn ip_apppointment_btn_custom" id="appointments_day_todaybtn"><a><?php load_language('today');?></a></button>
              </div>
            </div>
            <h3><?php load_language('appointment');?></h3>
            <div class="ip_record_settings">
              <div class="btn-group ip_custom_tabs_menu">
                <button type="button" class="btn ip_apppointment_btn_custom current dctr_dash_appoint_day"><a class="uppercase" href="#tab-1"><?php load_language('day');?></a></button>
                <button type="button" class="btn ip_apppointment_btn_custom dctr_dash_appoint_week"><a class="uppercase" href="#tab-2"><?php load_language('week');?></a></button>
                <button type="button" class="btn ip_apppointment_btn_custom dctr_dash_appoint_month"><a class="uppercase" href="#tab-3"><?php load_language('month');?></a></button>
              </div>
                <span class="settings"><img src="<?php echo base_url();?>assets/images/ip_settings.png"></span>
              </div>
          </div>
          <div class="ip_full_calender_content">
            <div class="ip_custom_tab">

              <div id="tab-1"  class="ip_period_section ip_custom_tab_content">
                <div class="row m0">
                  <div class="col-md-9 p0">
                    <div class="ip_day_scheduleler">
                      <ul>
                        <li class="ip_current_date">08</li>
                        <li class="ip_current_month">September</li>
                        <div class="clear"></div>
                      </ul>
                      <div class="ip_day_space"></div>

                      <ul class="ip_day_listing" id="ip-appointments-day">
                        <?php $this->load->view('doctor_dash_appointments_day'); ?>
                      </ul>

                    </div>
                  </div>
                  <div class="col-md-3 p0">
                    <div class="ip_appointment_calender">
                      <div class="ip_current_day_frame">
                       <!-- value="<?php echo date('m/d/Y');?>"  -->
                        <!-- <input class="ip_current_day" id="ip_appointment_calender" value="<?php echo date('m/d/Y');?>"  placeholder="Select Date"  /> -->
                       <input type="hidden" id="ip_appointment_calender" value="<?php echo date('m/d/Y');?>" />
                       <div id="ip_appointment_calender_div"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div id="tab-2" class="ip_period_section ip_custom_tab_content">
                <div class="ip_table_head">
                  <ul>
                      <li class="time_slot"></li>
                      <?php 
                        //$today =date('N',time());
                        for ($i=0; $i < 7; $i++) { 
                          $day = date('D',strtotime('+'.$i.'day'));
                          $dayno = date('d',strtotime('+'.$i.'day'));

                          ?>
                          <li><?php echo $day.','. $dayno;?></li>
                          <?php
                        }
                        
                      ?>
                     
                      <div class="clear"></div>
                  </ul>
                </div>
                <div class="ip_table_head_divide">
                  <ul>
                      <li class="time_slot"></li>
                      <li></li>
                      <li></li>
                      <li></li>
                      <li></li>
                      <li></li>
                      <li></li>
                      <li class="borderrightnone"></li>
                      <div class="clear"></div>
                  </ul>
                </div>
                <div class="ip_table_days">
                  <ul id="dctr_week_appointment">
                     <?php $this->load->view('doctor_dash_appointments_week');?>
                  </ul>
                </div>
              </div>
              <div id="tab-3" class="ip_period_section ip_custom_tab_content">
                <div class="ip_month_schedule">
                  <div class="ip_month_schedule_head">
                    <ul>
                      <li class="uppercase"><?php load_language('monday');?></li>
                      <li class="uppercase"><?php load_language('tuesday');?></li>
                      <li class="uppercase"><?php load_language('wednesday');?></li>
                      <li class="uppercase"><?php load_language('thursday');?></li>
                      <li class="uppercase"><?php load_language('friday');?></li>
                      <li class="uppercase"><?php load_language('saturday');?></li>
                      <li class="uppercase"><?php load_language('sunday');?></li>
                      <div class="clear"></div>
                    </ul>
                  </div>
                  <div class="ip_month_schedule_dates">
                    <ul id="dctr_month_appointment">
                       <?php $this->load->view('doctor_dash_appointments_month'); ?>
                      <div class="clear"></div>
                    </ul>
                  </div>
                </div>
              </div>
          </div>
        </div>
        </div>
      </div>
    </div>

    <?php

    }
    if($value=='Doctor/chat')
    {
?>

      <div class="ip_grid_cols">
            <div class="row">
              <div class="col-md-4">
                <div class="ip_bio_tab_div">
                  <div class="ip_bio_head">
                    <?php load_language('messages');?>
                    <div class="ip_bio_more">
                    </div>
                  </div>
                  <div class="ip_bio_detail">
                    <div class="ip_bio_message_list">
                      <ul>
                          <?php if(!empty($recent))
                          {
                            foreach ($recent as $key => $elm) 
                            {
                          ?>
                                <li>
                               <a href="<?php echo base_url();?>Doctor/chat">
                                <div class="ip_bio_message_pic">
                                  <img src="<?php echo base_url(); echo $elm['pat_pic']?>">
                                </div>
                                <div class="ip_bio_messages">
                                  <h5><?php echo decrypt_data($elm['pat_name']);?></h5><div class="ip_message_time"><?php echo change_time_to_local($elm['time'])?></div>
                                  <div class="clear"></div>
                                  <p><?php echo $elm['msg'];?></p>
                                </div>
                                <div class="clear"></div>
                                </a>
                                  </li>
                            <?php  
                              }
                            } ?>                 
                        </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>

<?php

   }
 }
}
else
{
?>
  <div class="ip_grid_cols">
    <div class="row">
      <div class="ip_bio_tab_div ip_top_dash_detail textCenter">
      <p><?php load_language('collaborator_permission_denied');?></p>
    </div>
    </div>
  </div>

<?php
}
?>
    

  </div>
</div>










