
<div class="ip_set_two_wrapper">

  <div class="container ip_custom_container">
    <div class="ip_main_path_stream">
      <ul>
        <li><?php load_language('dashboard');?><span><img src="<?php echo base_url();?>assets/images/ip_tab_list_arw.png"></span></li>
        <li><?php load_language('wallet');?><span><img src="<?php echo base_url();?>assets/images/ip_tab_list_arw.png"></span></li>
      
      </ul>
    </div>
    <div class="tab-content">
        <div>
          <div class="ip_message_tabs_body">
            <div class="ip_graph_wrapper">
            <div class="row">
              <div class="col-md-4">
                <div class="ip_bio_tab_div">
                  <div class="ip_bio_head">
                    <?php load_language('balance_for_redemption');?>
                    <div class="ip_bio_more">
                    </div>
                  </div>
                  <div class="ip_bio_detail textCenter">
                    <div class="ip_bal_circle">
                    <div class="c100 p25">
                          <span><strong class="ip_counter" data-count="<?php echo $this->encrypt->decode($wallet['reedem_earn']);?>"><?php echo $this->encrypt->decode($wallet['reedem_earn']);?></strong></span>
                          <div class="slice">
                              <div class="bar"></div>
                              <div class="fill"></div>
                          </div>
                    </div>
                      <div class="clear"></div>
                    </div>
                    <p><?php load_language('available_for_redemption');?></p>
                    <div class="ip_bio_bottom_bay">
                      <div class="ip_circle_left">
                        <strong>R$ <?php echo $last_redemption;?></strong>
                        <p><?php load_language('last_redemption');?></p>
                      </div>
                      <div class="ip_circle_right">
                        <a href="<?php echo base_url();?>Doctor/wallet/redemptionhistory">
                          <button class="ip_circle_btn bal_btn" type="button"><?php load_language('history');?></button>
                        </a>
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="ip_bio_tab_div">
                  <div class="ip_bio_head">
                    <?php load_language('future_releases');?>
                    <div class="ip_bio_more">
                    </div>
                  </div>
                  <div class="ip_bio_detail textCenter">
                    <div class="ip_future_circle">
                    <div class="c100 p25">
                          <span><strong class="ip_counter" data-count="<?php echo $this->encrypt->decode($wallet['future_earn']);?>"><?php echo $this->encrypt->decode($wallet['future_earn']);?></strong></span>
                          <div class="slice">
                              <div class="bar"></div>
                              <div class="fill"></div>
                          </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                    <p><?php load_language('launches_of_today');?></p>
                    <div class="ip_bio_bottom_bay">
                      <div class="ip_circle_left">
                        <strong>R$ <?php echo $next_release_amount;?></strong>
                        <p><?php load_language('next_release');?></p>
                      </div>
                      <div class="ip_circle_right">
                       <a href="<?php echo base_url();?>Doctor/wallet/futurereleases">
                         <button class="ip_circle_btn future_btn" type="button"><?php load_language('view_more');?></button>
                       </a> 
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="ip_bio_tab_div">
                  <div class="ip_bio_head">
                    <?php load_language('total_balance');?>
                    <div class="ip_bio_more">
                    </div>
                  </div>
                  <div class="ip_bio_detail textCenter">
                    <div class="ip_total_circle">
                    <div class="c100 p25">
                          <span><strong class="ip_counter" data-count="<?php echo $this->encrypt->decode($wallet['total_earn']);?>"><?php echo $this->encrypt->decode($wallet['total_earn']);?></strong></span>
                          <div class="slice">
                              <div class="bar"></div>
                              <div class="fill"></div>
                          </div>
                      </div>
                      <div class="clear"></div>
                    </div>
                    <p><?php load_language('total_balance');?></p>
                    <!-- <div class="ip_bio_bottom_bay">
                      <div class="ip_circle_left">
                        <strong>R$ 350</strong>
                        <p>Last Release</p>
                      </div>
                      <div class="ip_circle_right">
                        <button class="ip_circle_btn total_btn">VIEW MORE</button>
                      </div>
                      <div class="clear"></div>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
            <br>

            <div class="row">
              <div class="col-md-4">
                <div class="ip_bio_tab_div">
                  <div class="ip_bio_head">
                    <?php load_language('add_bank_account');?>
                    <div class="ip_bio_more">
                    </div>
                  </div>

                  <form data-parsley-validate="" role="form" id="add-bank-form">
                  <div class="ip_bio_detail textCenter">
                    <div class="ip_bank_form">
                      <div class="ip_bank_row">
                        <div class="ip_bank_detail_frame">
                          <input class="ip_bank_input reset-bank-form" data-parsley-required="" placeholder="<?php load_language('bank');?>" data-parsley-pattern="^[a-zA-Z ]+$" onKeyPress="if(this.value.length > 40) return false;" data-parsley-maxlength="40" name="bank">
                        </div>
                      </div>
                      <div class="ip_bank_row">
                        <div class="ip_bank_detail_frame">
                          <div class="row m0 height100">
                            <div class="col-md-4 p0 height100">
                             <!--  <input class="ip_bank_input"  placeholder="Agency" style="border-right:2px solid #f5f5f5;"> -->
                             <p class="p12" style="border-right:2px solid #f5f5f5;"><?php load_language('agency');?></p>
                            </div>
                            <div class="col-md-8 p0 height100"><input class="ip_bank_input bordernone reset-bank-form" placeholder="xxxx-xx" data-parsley-required="" name="agency" data-parsley-pattern="^[a-zA-Z ]+$" onKeyPress="if(this.value.length > 40) return false;" data-parsley-maxlength="40"></div>
                          </div>
                        </div>
                      </div>
                      <div class="ip_bank_row">
                        <div class="ip_bank_detail_frame">
                          <div class="row m0 height100">
                            <div class="col-md-4 p0 height100">
                             <!--  <select class="ip_bank_input" style="border-right:2px solid #f5f5f5;">
                                <option>Account</option>
                              </select> -->
                              <p class="p12" style="border-right:2px solid #f5f5f5;"><?php load_language('account');?></p>
                            </div>
                            <div class="col-md-8 p0 height100"><input class="ip_bank_input bordernone reset-bank-form" placeholder="xxxx-xx" data-parsley-required="" type="number" name="account" onKeyPress="if(this.value.length > 30) return false;" data-parsley-maxlength="30"></div>
                          </div>
                        </div>
                      </div>
                      <div class="ip_bank_row">
                        <div class="ip_bank_detail_frame">
                          <input class="ip_bank_input reset-bank-form" placeholder="<?php load_language('name');?>" data-parsley-required="" onKeyPress="if(this.value.length > 40) return false;" data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-maxlength="40" name="name">
                        </div>
                      </div>
                      <div class="row m0 height100">
                        <div class="col-md-4 p0 height100">
                          <div class="ip_bank_detail_frame">
                            <button class="ip_bank_input uppercase" type="button" id="add-bank-reg-btn"><?php load_language('register');?></button>
                          </div>
                        </div>
                      </div>
                      <hr>
                      <div id="add-bank-success-error" class="alert alert-dismiss hidden"></div>
                    </div>
                  </div>
                  </form>

                </div>
              </div>
              <div class="col-md-4">
                <div class="ip_bio_tab_div">
                  <div class="ip_bio_head">
                    <?php load_language('your_banks');?>
                    <div class="ip_bank_setting_btn">
                    </div>
                  </div>
                  <div class="ip_bio_detail textCenter">
                    <div class="ip_bank_form">
                      <div class="ip_bank_row" id="show_all_saved_banks">
                        <?php 
                        $this->load->view('wallet_show_banks');
                        ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="ip_bio_tab_div">
                  <div class="ip_bio_head">
                    <?php load_language('perform_redemption');?>
                    <div class="ip_bio_more">
                    </div>
                  </div>
                  <div class="ip_bio_detail textCenter">
                    <div class="ip_bank_form">
                      <div class="ip_bank_row">
                        <div class="ip_requested">
                          <div class="ip_requested_circle">
                            <img src="<?php echo base_url();?>assets/images/ip_dollar.png">
                          </div>
                          <div class="ip_requested_detail">
                            <strong>R$ <?php echo $wallet['reedem_earn'];?></strong>
                            <p><?php load_language('requested_value');?></p>
                          </div>
                          <div class="clear"></div>
                        </div>
                      </div>
                      <form data-parsley-validate="" role="form" id="redempetion-req-form">
                      <div class="ip_bank_row" id="show_bank_for_redemption">
                        <?php $this->load->view('wallet_show_bank_redemption') ?>
                      </div>
                      <div class="ip_bank_row">
                        <div class="ip_bank_detail_frame">
                          <div class="row m0 height100">
                            <div class="col-md-6 p0 height100">
                              <!-- <input class="ip_bank_input"style="border-right:2px solid #f5f5f5;" placeholder="Redemption Value"> -->
                              <p class="p12" style="border-right:2px solid #f5f5f5;"><?php load_language('redemption_value');?></p>
                            </div>
                            <div class="col-md-6 p0 height100"><input class="ip_bank_input bordernone reset-redemption-form" placeholder="R$ " type="number" data-parsley-required="" name="redemption_amount" onKeyPress="if(this.value.length > 6) return false;" data-parsley-maxlength="6" > </div>
                          </div>
                        </div>
                      </div>
                      <div class="ip_bank_row">
                          <button class="ip_circle_btn bal_btn" type="button" id="request-redemption-btn"><?php load_language('request_redemption');?></button>
                      </div>
                      <div class="hidden alert alert-dismiss" id="redemption-success-error"></div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
</div>

