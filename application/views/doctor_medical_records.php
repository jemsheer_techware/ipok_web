
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">
    <div class="ip_top_dash_bay">
      <div class="row">
        <div class="col-md-3">
          <div class="ip_top_dash_list">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_appointments.png">
            </div>
            <div class="ip_top_dash_detail">
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_attendance']?>">0</strong>
              <p><?php load_language('attendance_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="ip_top_dash_list">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_feature.png">
            </div>
            <div class="ip_top_dash_detail" >
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_billed']?>">0</strong>
              <p><?php load_language('billed_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="ip_top_dash_list">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_paintences.png">
            </div>
            <div class="ip_top_dash_detail">
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_patients']?>">0</strong>
              <p><?php load_language('patients_visited_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="ip_top_dash_list bordernone">
            <div class="ip_top_dash_circle">
              <img src="<?php echo base_url();?>assets/images/ip_vistors.png">
            </div>
            <div class="ip_top_dash_detail">
              <strong class="ip_counter" data-count="<?php echo $dash_view['no_of_profileview']?>">0</strong>
              <p><?php load_language('profile_views_text');?></p>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="ip_main_path_stream">
      <ul>
        <li><?php load_language('dashboard');?><span><img src="<?php echo base_url();?>assets/images/ip_tab_list_arw.png"></span></li>
        <li><?php load_language('medical_records');?><span><img src="<?php echo base_url();?>assets/images/ip_tab_list_arw.png"></span></li>
      </ul>
    </div>
    <div class="ip_records_wrapper">
      <div class="row m0">
        <div class="col-md-2 p0">
          <div class="ip_record_section">
            <div class="ip_record_section_header">
              <?php load_language('medical_records');?>
            </div>
            <div class="ip_record_section_detail p0">
              <ul class="p0 m0">
                <li class="active p12" data-toggle="tab" href="#attended">
                  <span><img src="<?php echo base_url();?>assets/images/ip_record1.png"></span><?php load_language('patience_attended_filter');?>
                </li>
                <li class="p12" data-toggle="tab" href="#schedulled">
                  <span><img src="<?php echo base_url();?>assets/images/ip_record2.png"></span><?php load_language('patience_scheduled_filter');?></li>
              </ul>
               <!-- <li><span><img src="<?php echo base_url();?>assets/images/ip_record1.png"></span>Filter Month</li>

               <li><input id="med_rec_filter_month" class="ip_dropdown" type="text" ></li> -->

            </div>
          </div>
        </div>
        <div class="col-md-10 p0">

          <div class="tab-content">
          <div id="attended" class="ip_paitent_tab tab-pane fade in active">
            <?php $this->load->view('doctor_medical_records_attended'); ?>
          </div>

          <div id="schedulled" class="ip_paitent_tab tab-pane fade">
            <?php $this->load->view('doctor_medical_records_scheduled'); ?>
          </div>
        </div>
      </div>
    </div>
    </div>
</div>
</div>


<!-- <div id="sent-broadcast" class="modal fade" role="dialog">
  <div class="modal-dialog">


    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="sentbroad-users">Medical Record</h4>
      </div>
      <div class="modal-body">
        <p>Test</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div> -->

<div id="sent-broadcast" class="modal fade" role="dialog">
  <div class="modal-dialog">
      <div class="ip_patient_delete_pop_wrapper">
        <div class="ip_message_pop_header">
          <?php load_language('send_message');?>
        </div>
        <hr>
        <div class="ip_message_pop_content">
          <ul>
            <p><?php load_language('send_message_to');?></p>
            <li id="sentbroad-users"></li>
          </ul>
          <hr>
          <div class="ip_message_pop_inside">
            <textarea id="broadcasttext"></textarea>
          </div>
          <hr>
          <p class="ip_message_pop_header hidden" id="broad-users-error"><?php load_language('send_message_user_error');?></p>
          <p class="ip_message_pop_header hidden" id="broad-msg-error"><?php load_language('send_message_text_error');?></p>
          <div class="ip_message_bottom">
            <div class="row">
              <div class="col-md-2">
                <button class="ip_edit_set_btn floatLeft uppercase" type="button" id="broadcast-msg-sent-btn"><?php load_language('send');?></button>
              </div>
              <div class="col-md-10">
                <button class="ip_edit_save_btn floatLeft uppercase" data-dismiss="modal"><?php load_language('cancel');?></button>
              </div>
            </div>
            <div class="clear"></div>
        </div>

      </div>
  </div>
</div>
</div>

<div id="success" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="ip_custom_modal" style="min-height:200px !important;">
      <div class="ip_custom_modal_content">
        <div class="ip_main_tab_content ">
              <div class="ip_main_tab_pic">
                <img src="<?php echo base_url();?>assets/images/tick.png">
              </div>
              <div class="success_content">
                <h1><strong><?php load_language('success');?></strong></h1>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>

<script>
    setTimeout(function() {
      CKEDITOR.replace('broadcasttext');
    },1800)
</script>

