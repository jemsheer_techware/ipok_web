
<div class="ip_set_two_wrapper">
  <div class="container ip_custom_container">

      <?php
        if($this->session->flashdata('message')) {
        $message = $this->session->flashdata('message');
      ?>
        <div class="alert alert-<?php echo $message['class']; ?> alert-dismissible flash-msg">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><strong> <?php echo $message['title']; ?></strong></h4>
        <?php echo $message['message']; ?>
        </div>

      <?php
        }
      ?>

    <div class="ip_edit_record_wrapper">
     <!--  <pre>
        <?php print_r($dependent_data);?>
      </pre> -->
      <div class="row">
        <?php
        if(!empty($dependent_data))
          {
            foreach ($dependent_data as $key => $element) 
            {
        ?>
               <div class="col-md-6">
                <div class="ip_edit_record_cover">
                  <div class="ip_edit_record_detail">
                    <div class="ip_add_dependent_photo">
                      <img src="<?php echo base_url();echo$element['image'];?> ">
                    </div>
                    <div class="ip_add_dependent_detail">
                      <h5><?php echo$element['dependent_name'];?></h5>
                      <p><?php echo$element['age'];?> Years / <?php echo$element['relation'];?></p>
                    </div>
                    <div class="clear"></div>
                  </div>
                  <hr>
                  <div class="ip_edit_record_detail">
                  </div>
                </div>
              </div>
        <?php
            }
          }
        ?>
       

      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="ip_edit_record_cover">
            <form role="form" data-parsley-validate="" id="pat-add-depend-form" method="POST" action="<?php echo base_url();?>Patient/saveDependent" enctype="multipart/form-data">
            <div class="ip_edit_record_head backgroundnone">
             <?php load_language('add_dependent');?>
            </div>
              <div class="ip_edit_record_detail">
                <div class="ip_edit_row">
                  <div class="ip_bank_detail_frame">
                      <input name="dependent_name" class="ip_bank_input" data-parsley-required="true" data-parsley-minlength="5" onKeyPress="if(this.value.length > 40) return false;" data-parsley-maxlength="40" data-parsley-pattern="^[a-zA-Z ]+$" type="text" placeholder="<?php load_language('name');?>*">
                  </div>
                </div>
                <div class="ip_edit_row">
                  <div class="row">
                    <div class="col-md-3">
                      <p class="ip_row_p"><?php load_language('date_of_birth');?>*</p>
                      <div class="ip_bank_detail_frame" id="edit-patient"> <!-- id for datepicker -->
                          <input name="dob" readonly data-parsley-required="true" class="ip_bank_input" placeholder="">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <p class="ip_row_p"><?php load_language('family_bond');?>*</p>
                      <div class="ip_bank_detail_frame">
                          <input name="relation" data-parsley-required="true" data-parsley-minlength="5" data-parsley-maxlength="25" onKeyPress="if(this.value.length > 25) return false;" data-parsley-pattern="^[a-zA-Z ]+$" class="ip_bank_input" placeholder="">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <p class="ip_row_p">CPF*</p>
                      <div class="ip_bank_detail_frame">
                          <input class="ip_bank_input" placeholder="" name="cpf"
                            data-parsley-required="true" data-parsley-minlength="11" data-parsley-cpf="" onKeyPress="if(this.value.length > 10) return false;"  type="number">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <p class="ip_row_p"><?php load_language('add_photo');?>*:</p>
                       <div class="ip_reg_modal_addphoto floatLeft">
                        <img  id="pat-add-depend-pic">
                      </div>

                      <div class="ip_add_photo floatRight">
                        <input name="image"  data-parsley-required="true" type="file" accept="image/*"   class="ip_reg_form_input"
                      data-parsley-error-message="Choose Profile Photo" onchange="pat_add_depend_loadthumbnail(this,'pat-add-depend-pic')" >
                      
                      </div>
                      <div class="clear"></div>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <div class="ip_coloborator_btn_bay">
                <button class="ip_colaborator_btn"><?php load_language('add_dependent_save_button');?></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

