<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() 
	{
	    parent::__construct();
	    $this->load->model('Doctor_model');
	    global $default_date ;
	    $this->default_date = '01/01/1970';

	    $this->load->library('encrypt');
	 
	    //print_r(date_default_timezone_get());die();
	}
	public function index()
	{
		$this->load->view('payment');
	}

	public function encrypt_data(){
		$new_data = '{"anamnese":{"Kidney_Problem":"Abdominal Compartment Syndrome","Breathing_Problem":"Lung Cancer","Gastric_Problem":"Hemorrhoids","others":["hepatitis","diabetis"]}}';
		$data = $this->encrypt->encode($new_data); 
		//print_r($data);echo"<br>";
		$data2 = $this->encrypt->decode($data);
		print_r($data2);
		exit(); 
	}
}