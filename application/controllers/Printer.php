<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Printer extends CI_Controller {

	function __construct() 
	{
        parent::__construct();
        $this->load->model('Home_model');
        $this->load->model('Patient_model');
        $this->load->model('Doctor_model');
        $this->load->model('Printer_model');
        $this->load->library("Pdf");
    }

    public function index()
    {
    	redirect(base_url());
    }

	/*PRINT CONTROLLER  - MEDICAL RECORD SUMMARY PRINT*/
	public function record()
	{
		if($this->session->userdata('UserData') and !empty($this->uri->segment(3)))
		{
			$record_id = $this->uri->segment(3);
			$userdata = $this->session->userdata('UserData');
			
			$record_data = $this->Printer_model->authentic_record($record_id,$userdata['id'],$userdata['type']);
			//print_r($record_data);die();
			if(!empty($record_data))
			{
				$patient_data = $this->Patient_model->get_single_patient($record_data['patient_id']);
				$doctor_data = $this->Doctor_model->get_single_doctor($record_data['doctor_id']);
				$booking_data = $this->Patient_model->get_Booking($record_data['booking_id']);
				// /print_r($record_data);die();
				$diseases = json_decode($record_data['diseases']);
				$prescription = json_decode($record_data['prescribtions']);
				$exams = json_decode($record_data['exams']);
				$budget = json_decode($record_data['budget']);
				//print_r($budget);die();
			
				
				//print_r($html);die();
					// Set some content to print
		$html = '	

		<div style="width:700px;margin:0 auto;border:1px solid #a8a8a8;padding: 15px;">
			<table style="width:100%">
				<thead>
					<tr>
						<th style="border-bottom:1px solid #a8a8a8;">
							<h3 style="color:#144b6d;text-align: center;padding-left: 10px;padding-right: 10px;margin:0px;padding-bottom: 20px;padding-top: 0px;">MEDICAL RECORD</h3>
						</th>
					</tr>
				</thead>
			</table>
			<table style="width:100%">
				<tbody>
					<tr>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;"><b>Name</b></td>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;">'.$patient_data['pt_name'].'</td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;"><b>Vist Date</b></td>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;">'.date('d-m-Y',$booking_data['book_date']).'</td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;"><b>Provider</b></td>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;">Dr.'.$doctor_data['dr_name'].'</td>
					</tr>
				</tbody>
			</table>
			<table style="width:100%">
				<thead>
					<tr>
						<th>
							<h4 style="color:#606161;text-align:left;padding-left:0px;padding-right:5px;margin:0px;padding-top:10px;padding-bottom: 10px;border-bottom:1px solid #a8a8a8;text-transform: uppercase;">anamnesis</h4>
						</th>
					</tr>
				</thead>
			</table>
			<table style="width:100% ; border-bottom:1px solid #a8a8a8;">
				<tbody>
					<tr>
						<td style="padding:5px;font-size: 15px;padding-left: 0px;">
							<p style="margin:0px;padding-top: 10px;"><b>'.$record_data['main_complaint'].'</b></p>
						</td>
					</tr>';
					if(!empty($record_data['description']))
					{
						$html = $html.'<tr>
						<td style="padding:8px;padding-left: 0px;">
							<h5 style="margin:0px;font-size: 14px;"><b>Description</b></h5>
							<p style="margin:0px;padding-top: 10px;padding-bottom:10px;font-size: 13px;">'.$record_data['description'].'</p>
						</td>
						</tr>';
					}
					
				$html=$html.'</tbody>
			</table>';

			if(!empty($diseases->anamnese))
			{
				$html = $html.'<table style="width:100% ;border-bottom:1px solid #a8a8a8;">
								<tbody>';

								foreach ($diseases->anamnese as $key => $value) 
								{
									if($key!='Medications' and $key!='others')
									{

										$key = str_replace('_', ' ', $key);
										$html = $html.'<tr>
										
											<td style="padding:15px;padding-left: 0px;border-bottom:1px solid #a8a8a8;">
												<h5 style="margin:0px;font-size: 14px;padding-top:10px;"><b>'.$key.'</b></h5>
												<p style="margin:0px;padding-top: 10px;padding-bottom:10px;font-size: 13px;">
													'.$value.'</p> 
											</td>
										</tr>';
									}
									if($key=='Medications' or $key=='others')
									{
										$medicine = implode(",",$value);
										$medicine = str_replace('_', ' ', $medicine);
										$html = $html.'<tr>
										
											<td style="padding:15px;padding-left: 0px;border-bottom:1px solid #a8a8a8;">
												<h5 style="margin:0px;font-size: 14px;padding-top:10px;"><b>'.$key.'</b></h5>
												<p style="margin:0px;padding-top: 10px;padding-bottom:10px;font-size: 13px;">
													'.$medicine.'</p> 
											</td>
										</tr>';
									}
									
								}

								$html = $html.'</tbody>
							</table>';
			}
			

			if(!empty($prescription))
            {

			$html = $html.'<table style="width:100%">
				<thead>
					<tr>
						<th>
							<h4 style="color:#606161;text-align:left;padding-left:0px;padding-right:5px;margin:0px;padding-top:10px;padding-bottom: 10px;border-bottom:1px solid #a8a8a8;text-transform: uppercase;">PRESCRIPTION</h4>
						</th>
					</tr>
				</thead>
			</table>
			<table style="width:100% ;border-bottom:1px solid #a8a8a8;">
				<tbody>';

				
                        	
                          foreach ($prescription as $key => $value) 
                          {

                          $html = $html.'<tr>
								<td style="padding:15px;padding-left: 0px;border-bottom:1px solid #a8a8a8;">
									<h5 style="margin:0px;font-size: 14px;"><b>'.$value->name.' '.$value->quantity.'</b></h5>
									<p style="margin:0px;padding-top: 10px;padding-bottom:10px;font-size: 13px;">
										'.$value->procedure.' </p>
								</td>
						  </tr>';
		                       
                          }
                        
			$html = $html.'</tbody>
			</table>';

			}

			if(!empty($exams))
			{
				$html = $html.'			
				<table style="width:100%">
					<thead>
						<tr>
							<th>
								<h4 style="color:#606161;text-align:left;padding-left:0px;padding-right:5px;margin:0px;padding-top:10px;padding-bottom: 10px;border-bottom:1px solid #a8a8a8;text-transform: uppercase;">EXAMS</h4>
							</th>
						</tr>
					</thead>
				</table>
				<table style="width:100% ;border-bottom:1px solid #a8a8a8;">
					<tbody>
						<tr>
							<td style="padding:15px;padding-left: 0px;">
								<h5 style="margin:0px;font-size: 14px;"><b>'.$exams[0]->procedure.'</b></h5>
								<p style="margin:0px;padding-top: 10px;padding-bottom:10px;font-size: 13px;">
									'.$exams[0]->observation.'</p>
							</td>
							
						</tr>
					</tbody>
				</table>';
			}

			if(!empty($record_data['letters']))
			{

			$html = $html.'

			<table style="width:100%">
				<thead>
					<tr>
						<th>
							<h4 style="color:#606161;text-align:left;padding-left:0px;padding-right:5px;margin:0px;padding-top:10px;padding-bottom: 10px;border-bottom:1px solid #a8a8a8;text-transform: uppercase;">CERTIFICATE</h4>
						</th>
					</tr>
				</thead>
			</table>
			<table style="width:100% ; border-bottom:1px solid #a8a8a8;">
				<tbody>
					<tr>
						<td style="padding:8px;padding-left: 0px;">
					
							<p style="margin:0px;padding-top: 10px;padding-bottom:10px;font-size: 13px;">
								'.$record_data['letters'].'
							</p>
						</td>
					</tr>
				</tbody>
			</table>';

			}

			if(!empty($budget))
			{

			
			$html = $html.'
			<table style="width:100%">
				<thead>
					<tr>
						<th>
							<h4 style="color:#606161;text-align:left;padding-left:0px;padding-right:5px;margin:0px;padding-top:10px;padding-bottom: 10px;border-bottom:1px solid #a8a8a8;text-transform: uppercase;">Budget</h4>
						</th>
					</tr>
				</thead>
			</table>
			<table style="width:100% ;border-bottom:1px solid #a8a8a8;">
				<tbody>';
				foreach ($budget as $key => $value) 
				{
				$bud_procedure = $value->procedure;
				$bud_quantity = $value->quantity;
				$bud_amount = $value->amount * $bud_quantity;
				
				$html = $html.'
					<tr>
						<td style="padding:15px;padding-left: 0px;border-bottom:1px solid #a8a8a8;">
							<h5 style="margin:0px;font-size: 14px;"><b>'.$bud_quantity.' - '.$bud_procedure.'</b></h5>
						</td>
						<td style="padding:15px;padding-left: 0px;border-bottom:1px solid #a8a8a8;">
							<h5 style="margin:0px;font-size: 14px;text-align: right;"><b>R$'.$bud_amount.'</b></h5>

						</td>
					</tr>';
					}
				$html = $html.'	

				</tbody>
			</table>';
			}

			if(!empty($record_data['patient_review']))
			{

			$html = $html.'
			<table style="width:100%">
				<thead>
					<tr>
						<th>
							<h4 style="color:#606161;text-align:left;padding-left:0px;padding-right:5px;margin:0px;padding-top:10px;padding-bottom: 10px;border-bottom:1px solid #a8a8a8;text-transform: uppercase;">evaluation</h4>
						</th>
					</tr>
				</thead>
			</table>
			<table style="width:100% ; border-bottom:1px solid #a8a8a8;">
				<tbody>
					<tr>
						<td style="padding:8px;padding-left: 0px;">
							<p style="margin:0px;padding-top: 10px;padding-bottom:10px;font-size: 13px;">
								'.$record_data['patient_review'].'
							</p>
						</td>
					</tr>
				</tbody>
			</table>';

		}

		$html = $html.'
		</div>
		';
				$this->print_page($html,$record_id);
			}
			else
			{
				redirect(base_url());
			}

		}
		else
		{
			redirect(base_url());
		}

	}

	public function medicine()
	{
		if($this->session->userdata('UserData') and !empty($this->uri->segment(3)))
		{
			$record_id = $this->uri->segment(3);
			$userdata = $this->session->userdata('UserData');
			
			$record_data = $this->Printer_model->authentic_record($record_id,$userdata['id'],$userdata['type']);
			//print_r($record_data);die();
			if(!empty($record_data['prescribtions']))
			{
				$patient_data = $this->Patient_model->get_single_patient($record_data['patient_id']);
				$doctor_data = $this->Doctor_model->get_single_doctor($record_data['doctor_id']);
				$booking_data = $this->Patient_model->get_Booking($record_data['booking_id']);
				// /print_r($record_data);die();
				
				$prescription = json_decode($record_data['prescribtions']);
					// Set some content to print
		$html = '	

		<div style="width:700px;margin:0 auto;border:1px solid #a8a8a8;padding: 15px;">
			<table style="width:100%">
				<thead>
					<tr>
						<th style="border-bottom:1px solid #a8a8a8;">
							<h3 style="color:#144b6d;text-align: center;padding-left: 10px;padding-right: 10px;margin:0px;padding-bottom: 20px;padding-top: 0px;">MEDICAL RECORD</h3>
						</th>
					</tr>
				</thead>
			</table>
			<table style="width:100%">
				<tbody>
					<tr>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;"><b>Name</b></td>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;">'.$patient_data['pt_name'].'</td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;"><b>Vist Date</b></td>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;">'.date('d-m-Y',$booking_data['book_date']).'</td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;"><b>Provider</b></td>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;">Dr.'.$doctor_data['dr_name'].'</td>
					</tr>
				</tbody>
			</table>';
			
			if(!empty($prescription))
            {

			$html = $html.'<table style="width:100%">
				<thead>
					<tr>
						<th>
							<h4 style="color:#606161;text-align:left;padding-left:0px;padding-right:5px;margin:0px;padding-top:10px;padding-bottom: 10px;border-bottom:1px solid #a8a8a8;text-transform: uppercase;">PRESCRIPTION</h4>
						</th>
					</tr>
				</thead>
			</table>
			<table style="width:100% ;border-bottom:1px solid #a8a8a8;">
				<tbody>';

				
                        	
                          foreach ($prescription as $key => $value) 
                          {

                          $html = $html.'<tr>
								<td style="padding:15px;padding-left: 0px;border-bottom:1px solid #a8a8a8;">
									<h5 style="margin:0px;font-size: 14px;"><b>'.$value->name.' '.$value->quantity.'</b></h5>
									<p style="margin:0px;padding-top: 10px;padding-bottom:10px;font-size: 13px;">
										'.$value->procedure.' </p>
								</td>
						  </tr>';
		                       
                          }
                        
			$html = $html.'</tbody>
			</table>';

			}

		$html = $html.'
		</div>
		';
				$this->print_page($html,$record_id);
			}
			else
			{
				redirect(base_url());
			}
		}
		else
		{
			redirect(base_url());
		}
			
	}

	public function exam()
	{
		if($this->session->userdata('UserData') and !empty($this->uri->segment(3)))
		{
			$record_id = $this->uri->segment(3);
			$userdata = $this->session->userdata('UserData');
			
			$record_data = $this->Printer_model->authentic_record($record_id,$userdata['id'],$userdata['type']);
			//print_r($record_data);die();
			if(!empty($record_data['exams']))
			{
				$patient_data = $this->Patient_model->get_single_patient($record_data['patient_id']);
				$doctor_data = $this->Doctor_model->get_single_doctor($record_data['doctor_id']);
				$booking_data = $this->Patient_model->get_Booking($record_data['booking_id']);
				// /print_r($record_data);die();
				
				
				$exams = json_decode($record_data['exams']);
					// Set some content to print
		$html = '	

		<div style="width:700px;margin:0 auto;border:1px solid #a8a8a8;padding: 15px;">
			<table style="width:100%">
				<thead>
					<tr>
						<th style="border-bottom:1px solid #a8a8a8;">
							<h3 style="color:#144b6d;text-align: center;padding-left: 10px;padding-right: 10px;margin:0px;padding-bottom: 20px;padding-top: 0px;">MEDICAL RECORD</h3>
						</th>
					</tr>
				</thead>
			</table>
			<table style="width:100%">
				<tbody>
					<tr>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;"><b>Name</b></td>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;">'.$patient_data['pt_name'].'</td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;"><b>Vist Date</b></td>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;">'.date('d-m-Y',$booking_data['book_date']).'</td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;"><b>Provider</b></td>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;">Dr.'.$doctor_data['dr_name'].'</td>
					</tr>
				</tbody>
			</table>';
			
			if(!empty($exams))
				{
					$html = $html.'			
					<table style="width:100%">
						<thead>
							<tr>
								<th>
									<h4 style="color:#606161;text-align:left;padding-left:0px;padding-right:5px;margin:0px;padding-top:10px;padding-bottom: 10px;border-bottom:1px solid #a8a8a8;text-transform: uppercase;">EXAMS</h4>
								</th>
							</tr>
						</thead>
					</table>
					<table style="width:100% ;border-bottom:1px solid #a8a8a8;">
						<tbody>
							<tr>
								<td style="padding:15px;padding-left: 0px;">
									<h5 style="margin:0px;font-size: 14px;"><b>'.$exams[0]->procedure.'</b></h5>
									<p style="margin:0px;padding-top: 10px;padding-bottom:10px;font-size: 13px;">
										'.$exams[0]->observation.'</p>
								</td>
								
							</tr>
						</tbody>
					</table>';
				}

		$html = $html.'
		</div>
		';
				$this->print_page($html,$record_id);
			}
			else
			{
				redirect(base_url());
			}
				
		}
		else
		{
			redirect(base_url());
		}

	}

	public function budget()
	{
		if($this->session->userdata('UserData') and !empty($this->uri->segment(3)))
		{
			$record_id = $this->uri->segment(3);
			$userdata = $this->session->userdata('UserData');
			
			$record_data = $this->Printer_model->authentic_record($record_id,$userdata['id'],$userdata['type']);
			//print_r($record_data);die();
			if(!empty($record_data['budget']))
			{
				$patient_data = $this->Patient_model->get_single_patient($record_data['patient_id']);
				$doctor_data = $this->Doctor_model->get_single_doctor($record_data['doctor_id']);
				$booking_data = $this->Patient_model->get_Booking($record_data['booking_id']);
				// /print_r($record_data);die();

				$budget = json_decode($record_data['budget']);
					// Set some content to print
		$html = '	

		<div style="width:700px;margin:0 auto;border:1px solid #a8a8a8;padding: 15px;">
			<table style="width:100%">
				<thead>
					<tr>
						<th style="border-bottom:1px solid #a8a8a8;">
							<h3 style="color:#144b6d;text-align: center;padding-left: 10px;padding-right: 10px;margin:0px;padding-bottom: 20px;padding-top: 0px;">MEDICAL RECORD</h3>
						</th>
					</tr>
				</thead>
			</table>
			<table style="width:100%">
				<tbody>
					<tr>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;"><b>Name</b></td>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;">'.$patient_data['pt_name'].'</td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;"><b>Vist Date</b></td>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;">'.date('d-m-Y',$booking_data['book_date']).'</td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;"><b>Provider</b></td>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;">Dr.'.$doctor_data['dr_name'].'</td>
					</tr>
				</tbody>
			</table>';
			
			if(!empty($budget))
			{

			
			$html = $html.'
			<table style="width:100%">
				<thead>
					<tr>
						<th>
							<h4 style="color:#606161;text-align:left;padding-left:0px;padding-right:5px;margin:0px;padding-top:10px;padding-bottom: 10px;border-bottom:1px solid #a8a8a8;text-transform: uppercase;">Budget</h4>
						</th>
					</tr>
				</thead>
			</table>
			<table style="width:100% ;border-bottom:1px solid #a8a8a8;">
				<tbody>';
				foreach ($budget as $key => $value) 
				{
				$bud_procedure = $value->procedure;
				$bud_quantity = $value->quantity;
				$bud_amount = $value->amount * $bud_quantity;
				
				$html = $html.'
					<tr>
						<td style="padding:15px;padding-left: 0px;border-bottom:1px solid #a8a8a8;">
							<h5 style="margin:0px;font-size: 14px;"><b>'.$bud_quantity.' - '.$bud_procedure.'</b></h5>
						</td>
						<td style="padding:15px;padding-left: 0px;border-bottom:1px solid #a8a8a8;">
							<h5 style="margin:0px;font-size: 14px;text-align: right;"><b>R$'.$bud_amount.'</b></h5>

						</td>
					</tr>';
					}
				$html = $html.'	

				</tbody>
			</table>';
			}

		$html = $html.'
		</div>
		';
				$this->print_page($html,$record_id);
			}
			else
			{
				redirect(base_url());
			}
		}
		else
		{
			redirect(base_url());
		}
			
	}


	public function letter()
	{
		if($this->session->userdata('UserData') and !empty($this->uri->segment(3)))
		{
			$record_id = $this->uri->segment(3);
			$userdata = $this->session->userdata('UserData');
			
			$record_data = $this->Printer_model->authentic_record($record_id,$userdata['id'],$userdata['type']);
			//print_r($record_data);die();
			if(!empty($record_data['letters']))
			{
				$patient_data = $this->Patient_model->get_single_patient($record_data['patient_id']);
				$doctor_data = $this->Doctor_model->get_single_doctor($record_data['doctor_id']);
				$booking_data = $this->Patient_model->get_Booking($record_data['booking_id']);
				// /print_r($record_data);die();

				
					// Set some content to print
		$html = '	

		<div style="width:700px;margin:0 auto;border:1px solid #a8a8a8;padding: 15px;">
			<table style="width:100%">
				<thead>
					<tr>
						<th style="border-bottom:1px solid #a8a8a8;">
							<h3 style="color:#144b6d;text-align: center;padding-left: 10px;padding-right: 10px;margin:0px;padding-bottom: 20px;padding-top: 0px;">MEDICAL RECORD</h3>
						</th>
					</tr>
				</thead>
			</table>
			<table style="width:100%">
				<tbody>
					<tr>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;"><b>Name</b></td>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;">'.$patient_data['pt_name'].'</td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;"><b>Vist Date</b></td>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;">'.date('d-m-Y',$booking_data['book_date']).'</td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;"><b>Provider</b></td>
						<td style="border-bottom:1px solid #a8a8a8;padding:8px;font-size: 14px;padding-left: 0px;">Dr.'.$doctor_data['dr_name'].'</td>
					</tr>
				</tbody>
			</table>';
			

			
			if(!empty($record_data['letters']))
			{

			$html = $html.'

			<table style="width:100%">
				<thead>
					<tr>
						<th>
							<h4 style="color:#606161;text-align:left;padding-left:0px;padding-right:5px;margin:0px;padding-top:10px;padding-bottom: 10px;border-bottom:1px solid #a8a8a8;text-transform: uppercase;">CERTIFICATE</h4>
						</th>
					</tr>
				</thead>
			</table>
			<table style="width:100% ; border-bottom:1px solid #a8a8a8;">
				<tbody>
					<tr>
						<td style="padding:8px;padding-left: 0px;">
							
							<p style="margin:0px;padding-top: 10px;padding-bottom:10px;font-size: 13px;">
								'.$record_data['letters'].'
							</p>
						</td>
					</tr>
				</tbody>
			</table>';

			}

		$html = $html.'
		</div>
		';
				$this->print_page($html,$record_id);
			}
			else
			{
				redirect(base_url());
			}
		}
		else
		{
			redirect(base_url());
		}
		
	}

	public function print_page($html,$record_id)
	{
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Ipok');
		$pdf->SetTitle('Medical Record');
		$pdf->SetSubject('Summary');
		$pdf->SetKeywords('PDF, record, Summary');

		/*// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
		$pdf->setFooterData(array(0,64,0), array(0,64,128));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));*/

		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('times', '', 10, '', true);

		// Add a page
		// This method has several options, check the source code documentation for more information.
		$pdf->AddPage();

		// set text shadow effect
		$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

		
		//print_r($html);die();
		// Print text using writeHTMLCell()
			$pdf->writeHTML($html, true, false, true, false, '');
		//$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

		// Close and output PDF document
		ob_end_clean();
		$pdf->Output('Record.pdf', 'I');
	}



}

?>
	
	

