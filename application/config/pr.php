<?php defined("BASEPATH") OR exit("No direct script access allowed");



/******ALERT*******/
$config ['alert_success'] = "Sucesso!";
$config['alert_error']="erro!";
$config ['patient_account_register_desc'] = "Conta registrada, por favor faça o login.";

/* HOME PAGE
==================================================*/

/*******HEADER*******/

/*top menu*/
$config ['home'] = "Início";
$config ['about'] = "Sobre";
$config ['register_consulting'] = "Cadastre-se";
$config ['contact_us'] = "Fale Conosco";
$config ['log_in'] = "Login";
$config ['log_out'] = "Sair";
$config['logout'] = "Sair";
$config ['download_app'] = "Baixar IPOK";
$config ['home_main_heading'] = "Prometemos praticidade";
$config ['session_invalid_error'] = "Sessão Inválida, por favor faça o Login novamente";
$config ['support']="SUPORTE";
$config ['supports']="Suporte";
$config['registers']="Registrar";

/*search bar*/
$config ['speciality'] = "Especialidade";
$config	['speciality | name_of_doctor_or_clinic']="Especialidade | nome do Médico ou Clínica";
$config ['location'] = "Localização";
$config ['date'] = "Data";

/* login model & forgot password */
$config ['doctor'] = "MÉDICO";
$config ['patient'] = "PACIENTE";
$config ['collaborator'] = "COLABORADOR";
$config ['login'] = "Login";
$config ['password'] = "Senha";
$config ['confirm_password'] = "Confirme a senha";
$config ['forgot_password'] = "Esqueceu a senha?";
$config ['not_registered'] = "Ainda não é cadastrado?";
$config ['register_now'] = "Cadestre-se agora!";
$config ['sent_mail'] = "Email enviado";
$config ['forgot_password_desc'] = "Enviaremos as informações em seu email";
$config ['check_your_email'] = "Verifique seu email";
$config ['check_your_email_desc'] = "Enviamos as informações para";
$config ['password_recovery'] = "recuperação de senha";
$config ['choose_type'] = "Escolha o tipo";
$config ['enter_mail_id'] = "Digite o email";
$config ['account_creation_condition'] = "Ao criar minha conta, eu concordo com o";
$config ['terms_and_conditions'] = "TERMOS E CONDIÇÕES";

/* register-choose model */
$config ['register_as'] = "Cadastrar-se como";
$config ['register_as_doctor'] = "Cadastrar-se como médico / clínica";
$config ['register_as_patient'] = "Cadastrar-se como paciente";

/*patient & doctor - registration model*/
$config ['create_patient_account'] = "Criar conta de paciente";
$config ['register_as_doctor_desc'] = "Gostaria de registrar sua clínica ou consultório,";
$config ['click_here'] = "clique aqui";
$config ['enter_with_facebook'] = "Entrar com Facebook";
$config ['personal_data'] = "Dados pessoais";
$config ['date_of_birth'] = "Data de nascimento";
$config ['gender'] = "Gênero";
$config ['male'] = "Masculino";
$config['personal_details']="Dados pessoais";
$config['customer_service']="Serviço ao cliente";
$config['medical_data']="Dados médicos";
$config['access_as_doctor'] = "Acessar como médico";
$config ['female'] = "Feminino";
$config ['others'] = "Outros";
$config ['next'] = "Próximo";
$config ['basic_medical_data'] = "Dados médicos básicos";
$config ['weight'] = "Peso";
$config ['height'] = "Altura";
$config ['blood_type'] = "Tipo sanguíneo";
$config ['previous'] = "Anterior";
$config ['address'] = "Endereço";
$config ['login_and_password'] = "LOGIN E SENHA";
$config ['add_photo_to_profile'] = "Adicionar foto de perfil";
$config ['finish'] = "Concluir";
$config ['patient_registration_failed'] = "Falha no cadastro, tente novamente.";
$config ['create_a_medical_account'] = "Criar uma conta médica";
$config ['add_profile_photo'] = "Adicionar foto do perfil";
$config ['create_an_account'] = "CRIAR UMA CONTA";
$config ['image_upload_error'] = "Desculpe! Imagens não carregadas ";
$config ['valid_credentials'] = "Credenciais válidas";
$config ['invalid_credentials'] = "Credenciais inválidas";
$config ['valid_code'] = "Código válido";
$config ['invalid_code'] = "Código inválido";
$config ['account_exist_with'] = "Conta existe com";
$config ['check_location_credentials'] = "Verificar as credenciais locais";
$config ['account_disabled'] = "Conta desativada";
$config ['invalid_username_or_password'] = "Nome de usuário ou senha inválidos";
$config ['register_success_message'] = "Cadastrado com sucesso, por favor faça o login";
$config ['register_failed_message'] = "Falha no cadastro, por favor, tente novamente";
$config ['valid_email_address'] = "Endereço de e-mail válido";
$config ['unauthorized_session'] = "Sessão não autorizada";
$config ['dependent_add_success'] = "Dependente adicionado com sucesso";
$config ['dependent_add_error'] = "Dependente adicionado com sucesso";
$config ['no_dependent_found'] = "Nenhum Dependente Encontrado! Por favor, adicione dependentes ";
$config ['dependent_update_failed'] = "Falha na atualização do perfil dependente, tente novamente";


/* PATIENT DASHBOARD PAGE
==================================================*/

$config ['name'] = "Nome";
$config ['username'] = "Nome de usuário";
$config ['profile'] = "Perfil";
$config ['profile_photo'] = "Foto do perfil";
$config ['address'] = "Endereço";
$config ['email'] = "E-mail";
$config ['phone'] = "Telefone";
$config ['bloodgroup'] = "Tipo Sanguíneo ";
$config ['birthday'] = "Data de nascimento";
$config ['bloodgroup'] = "Tipo Sanguíneo";
$config ['weight'] = "Peso";
$config ['height'] = "Altura";
$config ['CEP'] = "CEP";
$config ['Rua'] = "Rua";
$config ['number'] = "Número";
$config ['neighbourhood'] = "Ponto de referência";
$config ['complement'] = "Complemento";
$config ['occupation'] = "Profissão";
$config ['notification'] = "Notificação";
$config ['no_notification'] = "Nenhuma notificação";
$config ['messages'] = "Mensagens";
$config ['promotions'] = "Promoções";
$config ['no_promotions_available'] = "Nenhuma promoção disponível";
$config ['consultation'] = "Consulta";
$config ['consultation_tab'] = "CONSULTA";
$config ['scheduled_consultation_tab'] = "CONSULTA AGENDADA";
$config ['canceled_consultation_tab'] = "CONSULTA CANCELADA";
$config ['completed_tab'] = "COMPLETO";
$config ['open_medical_records'] = "HISTÓRICO MÉDICO ABERTO";
$config ['no_consultations'] = "SEM AGENDAMENTOS";
$config ['cancelation'] = "Cancelar";
$config ['no_canceled_consultations'] = "NÃO HÁ CONSULTAS CANCELADAS";
$config ['no_scheduled_consultations'] = "NÃO HÁ CONSULTAS AGENDADAS";
$config ['cancel_consultation'] = "CANCELAR CONSULTA";
$config ['waiting_list'] = "LISTA DE ESPERA";
$config ['confirmed'] = "CONFIRMADO";


/* HEADER PAGE
==================================================*/
$config ['dashboard'] = "Painel de controle";
$config ['about_us'] = "Sobre nós";
$config ['faq'] = "FAQ";
$config ['blog'] = "Blog";
$config ['agenda'] = "Agenda";
$config ['wallet'] = "Carteira";
$config ['records'] = "Registros";
$config ['notification'] = "Notificação";
$config ['see_all'] = "Ver tudo";
$config ['clinics'] = "Clínicas";
$config ['your_collaborators'] = "Seu (s) colaborador (es)";
$config ['add_collaborator'] = "Adicionar colaborador";
$config ['sign_out'] = "Sair";
$config ['add_dependent'] = "Adicionar Dependente";
$config ['edit_dependent'] = "Editar Dependente";
$config ['login/register'] = "Login / Registro";
$config['doctors']="MÉDICOS";
$config['clinis']="CLÍNICAS";
$config['specialities']="ESPECIALIDADE";


/* PATIENT PROFILE DELETE MODEL
==================================================*/
$config ['delete_user_account'] = "APAGAR CONTA DE USUÁRIO";
$config ['delete_user_desc'] = "VOCÊ QUER REALMENTE EXCLUIR SUA CONTA?";
$config ['delete_user_message'] = "Para confirmar esta ação, você receberá um código de verificação por e-mail para confirmar a exclusão de sua conta";
$config ['accept'] = "ACEITAR";
$config ['cancel'] = "CANCELAR";
$config ['delete_user_otp_desc'] = "Ao confirmar esta ação, sua conta será excluída permanentemente";
$config ['enter_confirm_code'] = "Digite o código de confirmação recebido por email";
$config ['account_deleted'] = "CONTA APAGADA";
$config ['confirmation_code'] = "Código de confirmação";



/* PATIENT PROFILE EDIT PAGE
==================================================*/
$config ['edit_your_profile'] = "Edite seu perfil";
$config ['edit_photo'] = "Editar foto";
$config ['update_and_save'] = "ATUALIZAR E SALVAR";

/* PATIENT PROMOCODE PAGE
==================================================*/
$config ['know_more'] = "Mais informações";
$config ['biography'] = "Biografia";
$config ['specialization'] = "Especialização";
$config ['location'] = "Localização";
$config ['view_complete_profile'] = "VER PERFIL COMPLETO";
$config ['mark_consultation'] = "MARCAR CONSULTA";
$config ['no_records_found'] = "NENHUM REGISTRO ENCONTRADO";


/* DOCTOR SEARCH RESULT PAGE
==================================================*/
$config ['filters'] = "FILTROS";
$config ['return_included'] = "RETORNO INCLUSO";
$config ['yes'] = "Sim";
$config ['no'] = "Não";
$config ['filter_on_value'] = "FILTRO POR VALOR";
$config ['home_visit'] = "VISITA DOMICILIAR";
$config ['men'] = "HOMENS";
$config ['women'] = "MULHERES";
$config ['order_on'] = "Pedido ativado";
$config ['price_up'] = "Maior Valor";
$config ['price_down'] = "Menor Valor";
$config ['enter_location'] = "Inserir local";
$config ['clinic'] = "Clínica";
$config ['search_result_for'] = "Resultado da pesquisa para";


/* DOCTOR SEARCH MARK CONSULTATION PAGE
==================================================*/
$config ['appointment'] = "Agendamento";
$config ['review_information'] = "REVISAR INFORMAÇÃO";
$config ['payment'] = "PAGAMENTO";
$config ['confirmation'] = "CONFIRMAÇÃO";
$config ['time_slot'] = "Horário";
$config ['enter_waiting_list_text'] = "ENTRAR NA LISTA DE ESPERA";
$config ['apply'] = "APLICAR";
$config ['clear'] = "LIMPAR";
$config ['money'] = "DINHEIRO";
$config ['credit_card'] = "CARTAO DE CREDITO";
$config ['form_of_payment'] = "Forma de pagemento";
$config ['amount'] = "Valor";
$config ['offer_price'] = "PREÇO DA OFERTA";
$config ['total_price'] = "PREÇO TOTAL";
$config ['back'] = "Voltar";
$config ['continue'] = "Continuar";
$config ['consultation_confirmed'] = "Consulta confirmada";
$config ['done'] = "Feito";
$config ['coupon_heading'] = "CUPOM";
$config ['no_time_slot_available'] = "Nenhum horário disponível";
$config ['booking_full_waiting_list_available'] = "Sem Reservas, apenas lista de espera disponível";
$config ['booking_slot_unavailable'] = "Reserva não disponível, escolha outro";
$config ['doctor_unavailable'] = "Médico indisponível, escolha outra data";
$config ['invalid_booking_slot'] = "Escolha um horário válido";
$config ['invalid_promocode'] = "Código promocional inválido!";
$config ['promotion_success'] = "Promoção aplicada";


/* PATIENT RECORD SUMMARY PAGE
==================================================*/

$config ['service_summary_heading'] = "Resumo da consulta";
$config ['anamnesis'] = "Anamnese";
$config ['prescription'] = "Prescrição";
$config ['exams'] = "Exames";
$config ['letters_and_certificate'] = "Documentos e atestados";
$config ['budget'] = "Orçamento";
$config ['evaluation'] = "Avaliação";
$config ['total'] = "TOTAL";
$config ['send_by_email'] = "ENVIAR POR E-MAIL";
$config ['print_out'] = "IMPRIMIR";
$config ['send_by_email_heading'] = "Registro Médico";
$config ['send_by_email_success'] = "O registro foi enviado com sucesso.";
$config ['send_by_email_error'] = "Erro no envio do registro.";
$config ['close'] = "Fechar";
$config ['search'] = "Pesquisar";

/* DOCTOR DASHBOARD PAGE
==================================================*/

$config ['attendance_text'] = "Comparecimento";
$config ['billed_text'] = "Faturamento";
$config ['patients_visited_text'] = "Pacientes";
$config ['profile_views_text'] = "Visualizar perfil";
$config ['job_title'] = "Cargo";
$config ['current_city'] = "Cidade";
$config ['total_attendence_today'] = "Total de atendimento hoje";
$config ['week'] = "Semana";
$config ['month'] = "Mês";
$config ['year'] = "Ano";
$config ['day'] = "Dia";
$config ['today'] = "HOJE";
$config ['sunday'] = "domingo";
$config ['monday'] = "segunda-feira";
$config ['tuesday'] = "terça-feira";
$config ['wednesday'] = "quarta-feira";
$config ['thursday'] = "quinta-feira";
$config ['friday'] = "sexta-feira";
$config ['saturday'] = "Sábado";
$config ['main_schedule'] = "Agenda principal";
$config ['select_clinic'] = "Selecionar clínica";
$config ['primary'] = "Primário";
$config ['secondary'] = "Secundário";
$config ['primary_schedule'] = "Programação Primária";
$config ['secondary_schedule'] = "Agenda Secundária";
$config ['configure_schedule_error'] = "Por favor, configure a Agenda (PRIMÁRIA e SECUNDÁRIA)";
$config ['interval'] = "Intervalo";
$config ['add_schedule'] = "ADICIONAR AGENDA";
$config ['schedule_add_success'] = "Agenda adicionada com sucesso";
$config ['schedule_add_failed'] = "As agendas não estão disponíveis";
$config ['schedule_add_mismatch'] = "As agendas estão com problema";
$config ['configuring_consultation_heading'] = "Configurando Consulta";
$config ['duration_of_consultation'] = "Duração da Consulta";
$config ['time'] = "Hora";
$config ['value_of_consultation'] = "Valor da Consulta";
$config ['inquiry_including_return'] = "Consulta com retorno";
$config ['select_any'] = "Selecione alguma";
$config ['limit_period'] = "Período limite";
$config ['days'] = "DIAS";
$config ['save'] = "Salvar";
$config ['error'] = "Erro";
$config ['success'] = "Sucesso";
$config ['configuring_consultation_failed'] = "Configuração da consulta falhou";
$config ['configuring_consultation_success'] = "Consulta Configurada";
$config ['vacation_heading'] = "Férias";
$config ['start_of_vacation'] = "Início das Férias";
$config ['end_of_vacation'] = "Fim das férias";
$config ['activate_vacation'] = "ATIVAR FÉRIAS";
$config ['add_vacation_success'] = "Férias adicionadas";
$config ['add_vacation_error'] = "Férias não adicionadas";


/* DOCTOR PROFILE EDIT PAGE
==================================================*/
$config ['telephone'] = "Telefone";
$config ['add_photo'] = "Adicionar foto";
$config ['price'] = "Preço";

/* DOCTOR RECORDS PAGE
==================================================*/
$config ['medical_records'] = "Registros Médicos";
$config ['patience_attended_filter'] = "Pacientes atendidos";
$config ['patience_scheduled_filter'] = "Paciente agendado";
$config ['send_message'] = "Enviar mensagem";
$config ['send_message_to'] = "Para";
$config ['send_message_user_error'] = "Por favor, selecione o usuários";
$config ['send_message_text_error'] = "Por favor, escreva uma mensagem";
$config ['send'] = "Enviar";
$config ['last_consultation'] = "Última consulta";
$config ['not_available'] = "Não disponível";
$config ['next_consultation'] = "Próxima consulta";
$config ['message'] = "Mensagem";
$config ['no_medical_records_found'] = "Nenhum registro médico encontrado";
$config ['time'] = "Hora";
$config ['consultation_value'] = "Valor";
$config ['consultation'] = "Consulta";
$config ['promotional_consultation'] = "Consulta promocional";
$config ['normal_consultation'] = "Consulta Normal";
$config ['open_record'] = "ABRIR REGISTRO";
$config ['medical_record'] = "REGISTRO MÉDICO";
$config ['medical_record_sent_success'] = "O registro foi enviado com sucesso";
$config ['medical_record_sent_error'] = "Erro.O envio do registro falhou ";

/* DOCTOR WALLET PAGE
==================================================*/
$config ['wallet'] = "Carteira";
$config ['balance_for_redemption'] = "Saldo para resgate";
$config ['available_for_redemption'] = "Disponível para resgate";
$config ['last_redemption'] = "Último resgate";
$config ['history'] = "HISTÓRICO";
$config ['future_releases'] = "Liberações futuras";
$config ['launches_of_today'] = "Lançamentos de hoje";
$config ['next_release'] = "Próximo lançamento";
$config ['view_more'] = "VER MAIS";
$config ['total_balance'] = "Saldo total";
$config ['add_bank_account'] = "Adicionar conta bancária";
$config ['bank'] = "Banco";
$config ['agency'] = "Agência";
$config ['account'] = "Conta";
$config ['register'] = "Cadastrar";
$config ['your_banks'] = "Seus bancos";
$config ['perform_redemption'] = "Executar resgate";
$config ['requested_value'] = "Valor solicitado";
$config ['redemption_value'] = "Valor de resgate";
$config ['request_redemption'] = "Pedido de resgate";
$config ['no_future_releases'] = "Nenhuma Realização Futura";
$config ['redemption_history'] = "Histórico de resgate";
$config ['no_redemption_request'] = "Nenhuma solicitação de resgate";
$config ['you_have_not_added_any_banks'] = "Você não adicionou nenhum banco";
$config ['error_insufficient_balance'] = "Erro, saldo insuficiente";
$config ['invalid_bank_account'] = "Credenciais de conta bancária inválidas";
$config ['facing_technical_issues'] = "Enfrentando questões técnicas";
$config ['select_bank'] = "Selecionar banco";
$config ['bank_added'] = "Banco adicionado";
$config ['redemption_requested'] = "Pedido de resgate";


/* DOCTOR SERVICE PAGE
==================================================*/

$config ['start_service'] = "Iniciar atendimento";
$config ['cancelled_consultation'] = "Consulta cancelada";
$config ['no_appointments'] = "NENHUM AGENDAMENTO";
$config ['appointments'] = "AGENDAMENTOS";
$config ['main_complaint'] = "Queixa principal";
$config ['kidney_problems'] = "Problema nos rins";
$config ['joint_problems_or_rheumatism'] = "Problema nas articulações ou reumatismo";
$config ['heart_problems'] = "Problemas cardíacos";
$config ['breathing_problems'] = "Problemas respiratórios";
$config ['gastric_problems'] = "Problemas gástricos";
$config ['allergies'] = "Alergias";
$config ['use_of_medicines'] = "Medicação de uso continuado";
$config ['hepatitis'] = "Hepatite";
$config ['pregnancy'] = "Gravidez";
$config ['diabetis'] = "Diabetes";
$config ['healing_problems'] = "Problema de cicatrização";
$config ['anamnese_updated_message'] = "Anamnese atualizada";
$config ['updation_failed'] = "Falha na atualização";
$config ['insertion_failed'] = "Inserção falhou";
$config ['prescriptions'] = "Receitas";
$config ['select_quantity'] = "Selecionar quantidade";
$config ['select_dosage_and_administration'] = "Selecione Dosagem e administração";
$config ['add_medicine'] = "Adicionar medicamento";
$config ['edit_medicine'] = "Editar medicamento";
$config ['print'] = "IMPRIMIR";
$config ['medicine_updated_message'] = "Medicamento atualizado";
$config ['exams_desc'] = "Exame ou procedimento";
$config ['exams_observation_note'] = "Nota";
$config ['exams_updated_message'] = "Exames atualizados";
$config ['budget_heading'] = "Item ou procedimento";
$config ['procedure_desc'] = "Item ou procedimento";
$config ['procedure_value'] = "Valor";
$config ['procedure_amount'] = "Valor";
$config ['procedure_add_button'] = "Adicionar item ou procedimento";
$config ['procedure_updated_message'] = "Procedimentos atualizados";
$config ['attached_letters'] = "Documentos anexados";
$config ['standard_certificate'] = "Atestado padrão";
$config ['standard_certificate_with_cid'] = "Atestado padrão com CID";
$config ['days_of_removal'] = "Dias de afastamento";
$config ['select_day'] = "Selecionar dia";
$config ['diagnostic_cid'] = "Diagnóstico (CID)";
$config ['certificate_minimum_length_error'] = "Por favor, insira os dados do atestado! Mínimo de 20 caracteres ";
$config ['certificate_updated_message'] = "Certificado atualizados";
$config ['review_updated'] = "Revisão adicionada";
$config ['end_of_service'] = "FINALIZAR ATENDIMENTO";
$config ['consultation_duration'] = "Duração da Consulta";
$config ['others_heading'] = "Outros";
$config ['other_observation'] = "Outra observação";
$config ['attach_images'] = "Anexar imagens";
$config ['service_summary'] = "Resumo do serviço";



/* CHAT PAGE
================================================== */
$config ['recent_messages'] = "Mensagens recentes";
$config ['no_recent_messages'] = "Nenhum bate-papo recente";
$config ['all_messages'] = "Todos os chats";
$config ['select_conversation_text'] = "Por favor, selecione uma conversa";
$config ['no_messages'] = "Nenhuma mensagem";

/* COLLABORATOR PAGE
================================================== */
$config ['collaborator_permission_denied'] = "Permissão não autorizada agora, volte mais tarde";
$config ['add_collaborator'] = "Adicionar colaborador";
$config ['add_collaborator_save_button'] = "Adicionar colaborador";
$config ['add_collaborator_success'] = "Colaborador adicionado, autorização de acesso";
$config ['add_collaborator_error'] = "Erro! Por favor, tente novamente ";
$config ['collaborator_access_heading'] = "Autorizar acesso";
$config ['collaborator_access_desc'] = "Escolha as áreas que seu colaborador pode acessar, visualizar, editar e configurar. Essas alterações podem ser alteradas no futuro ";
$config ['collaborator_access_save_button'] = "Autorizar acesso";
$config ['please_add_collaborator'] = "Por favor adicione o colaborador";
$config ['collaborator_access_success'] = "Configurações salvas!";
$config ['edit_collaborator'] = "Editar colaborador";
$config ['profile_photo_error_text'] = "Escolha a foto do perfil";
$config ['delete_collaborator'] = "Excluir colaborador";
$config ['collaborator_update_success'] = "Perfil do Colaborador Atualizado";
$config ['collaborator_update_error'] = "Erro! Por favor, tente novamente ";
$config ['collaborator_not_selected'] = "Colaborador não selecionado";
$config ['collaborator_excluded'] = "Colaborador excluído";
$config ['collaborator_exclusion_failed'] = "Falha na exclusão do colaborador";

/* NOTIFICATON PAGE
================================================== */
$config ['notification_center'] = "Central de notificação";
$config ['anniversaries'] = "Aniversários";

/* ERROR & ACCESS DENIED PAGE
================================================== */
$config ['url_error'] = "ERRO DE URL";
$config ['invalid_url'] = "URL inválido fornecido";
$config ['access_denied'] = "Acesso negado";
$config ['access_denied_desc'] = "Você não está autorizado a acessar esta página";

/* FORGOT PASSWORD PAGE
================================================== */
$config ['want_to_change_password'] = "QUER MUDAR SENHA";
$config ['please_fill_credentials'] = "Por favor preencha os novos dados de acesso";
$config ['new_password'] = "Nova senha";
$config ['confirm_new_password'] = "Confirme a nova senha";
$config ['password_change_success'] = "Sua senha foi atualizada! Por favor, faça o login ";
$config ['invalid_credentials_provided'] = "Dados inválidos!";


/* PATIENT DEPENDENT PAGE
================================================== */

$config ['add_dependent'] = "Adicionar dependente";
$config ['family_bond'] = "Vínculo familiar";
$config ['add_dependent_save_button'] = "Adicionar Dependente";
$config ['edit_dependent'] = "Editar dependente";

/* PAYMENT PAGE

==================================================*/

$config['invalid_credit_card'] = 'Invalid Card Provided';

$config['no_booking_found'] = 'No Booking Found';

$config['make_payment'] = 'MAKE PAYMENT';

$config['cvv'] = 'CVV';

$config['card_number'] = 'Card Number';

$config['last_name'] = 'Last Name';

$config['first_name'] = 'First Name';

$config['payment_tab_desc'] = 'Description in payment page';

$config['expiration_date'] = 'EXPIRATION DATE';


?>