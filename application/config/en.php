<?php defined("BASEPATH") OR exit("No direct script access allowed");





/******ALERT*******/

$config['alert_success']="Success!";
$config['alert_error']="Error!";

$config['patient_account_register_desc']=" Account Registered,Kindly Login.";



/* HOME PAGE

==================================================*/



/*******HEADER*******/



/*top menu*/

$config['home']="Home";

$config['about']="About";

$config['register_consulting']="REGISTER CONSULTING";

$config['contact_us']="Contact Us";

$config['log_in']="LOGIN";

$config['log_out']="LOG OUT";

$config['logout']="Logout";

$config['download_app']="Download App";

$config['home_main_heading']="We promise practicality";

$config['session_invalid_error']="Invalid Session, Kindly Login";

$config['support']="SUPPORT";

$config['supports']="Support";

$config['registers']="REGISTER";



/*search bar*/

$config['speciality']="Speciality";

$config['speciality | name_of_doctor_or_clinic']="Speciality | Name of Doctor or Clinic";

$config['location']="Location";

$config['date']="Date";



/*login model & forgot password*/

$config['doctor']="DOCTOR";

$config['patient']="PATIENT";

$config['collaborator']="COLLABORATOR";

$config['login']="Login";

$config['password']="Password";

$config['confirm_password']="Confirm Password";

$config['forgot_password']="Forgot Password";

$config['not_registered']="Not yet registered?";

$config['register_now']="Register Now!";

$config['sent_mail']="Sent Mail";

$config['forgot_password_desc']="We will send the information in your email";

$config['check_your_email']="Check your email";

$config['check_your_email_desc']="We send the information to";

$config['password_recovery']="password recovery";

$config['choose_type']="Choose Type";

$config['enter_mail_id']="Enter mail ID";

$config['account_creation_condition']="By creating my account I agree to the ";

$config['terms_and_conditions']="TERMS AND CONDITIONS";



/*register-choose model*/

$config['register_as']="REGISTER AS";

$config['register_as_doctor']="REGISTER AS DOCTOR / CLINIC";

$config['register_as_patient']="REGISTER AS PAITENT";



/*patient & doctor - registration model*/

$config['create_patient_account']="CREATE PATIENT ACCOUNT";

$config['register_as_doctor_desc']="I would like to register your clinic or office, ";

$config['click_here']="click here";

$config['enter_with_facebook']="ENTER WITH FACEBOOK";

$config['personal_data']="PERSONAL DATA";

$config['date_of_birth']="Date of Birth";

$config['gender']="Gender";

$config['male']="Male";

$config['access_as_doctor'] = "Access as a doctor";

$config['personal_details']="Personal Data";

$config['customer_service']="Customer Service";

$config['medical_data'] = "Medical data";

$config['female']="Female";

$config['others']="Others";

$config['next']="Next";

$config['basic_medical_data']="BASIC MEDICAL DATA";

$config['weight']="Weight";

$config['height']="Height";

$config['blood_type']="Blood type";

$config['previous']="Previous";

$config['address']="ADDRESS";

$config['login_and_password']="LOGIN AND PASSWORD";

$config['add_photo_to_profile']="Add Photo to profile";

$config['finish']="Finish";

$config['patient_registration_failed']="Account Registration Failed,Try Again.";

$config['create_a_medical_account']="Create a Medical Account";

$config['add_profile_photo']="Add Profile Photo";

$config['create_an_account']="CREATE AN ACCOUNT";

$config['image_upload_error']="Sorry! Images not uploaded";

$config['valid_credentials']="Valid Credentials";

$config['invalid_credentials']="Invalid Credentials";

$config['valid_code']="Valid Code";

$config['invalid_code']="Invalid Code";

$config['account_exist_with']="Account exist with ";

$config['check_location_credentials']="Check Location Credentials";

$config['account_disabled']="Account Disabled";

$config['invalid_username_or_password']="Invalid Username or Password";

$config['register_success_message']="Successfully Registered, Kindly Login";

$config['register_failed_message']="Registration Failed, Kindly Try Again";

$config['valid_email_address']="Valid Email Address";

$config['unauthorized_session']="Unauthorized Session";

$config['dependent_add_success']="Dependent Successfully Added";

$config['dependent_add_error']="Dependent Successfully Added";

$config['no_dependent_found']="No Dependent Found! Please Add Dependent";

$config['dependent_update_failed']="Dependent Profile Update Failed, Try Again";





/* PATIENT DASHBOARD PAGE

==================================================*/



$config['name']="Name";

$config['username']="Username";

$config['profile']="Profile";

$config['profile_photo']="Profile Photo";

$config['address']="Address";

$config['email']="Email";

$config['phone']="Phone";

$config['bloodgroup']="BloodGroup";

$config['birthday']="Birthday";

$config['bloodgroup']="BloodGroup";

$config['weight']="Weight";

$config['height']="Height";

$config['CEP']="CEP";

$config['Rua']="Rua";

$config['number']="Number";

$config['neighbourhood']="Neighbourhood";

$config['complement']="Complement";

$config['occupation']="Occupation";

$config['notification']="Notification";

$config['no_notification']="No Notification";

$config['messages']="Messages";

$config['promotions']="Promotions";

$config['no_promotions_available']="No Promotions Available";

$config['consultation']="Consultation";

$config['consultation_tab']="CONSULTATION";

$config['scheduled_consultation_tab']="SCHEDULED CONSULTATION";

$config['canceled_consultation_tab']="CANCELED CONSULTATION";

$config['completed_tab']="COMPLETED";

$config['open_medical_records']="OPEN MEDICAL RECORDS";

$config['no_consultations']="NO CONSULTATIONS";

$config['cancelation']="Cancelation";

$config['no_canceled_consultations']="NO CANCELED CONSULTATIONS";

$config['no_scheduled_consultations']="NO SCHEDULED CONSULTATIONS";

$config['cancel_consultation']="CANCEL CONSULTATION";

$config['waiting_list']="WAITING LIST";

$config['confirmed']="CONFIRMED";





/* HEADER PAGE

==================================================*/

$config['dashboard']="Dashboard";

$config['about_us']="About Us";

$config['faq']="FAQ";

$config['blog']="Blog";

$config['agenda']="Agenda";

$config['wallet']="Wallet";

$config['records']="Records";

$config['notification']="Notification";

$config['see_all']="See All";

$config['clinics']="Clinics";

$config['your_collaborators']="Your Collaborator(s)";

$config['add_collaborator']="Add Collaborator";

$config['sign_out']="Sign Out";

$config['add_dependent']="Add Dependent";

$config['edit_dependent']="Edit Dependent";

$config['login/register']="Login/Register";

$config['doctors']="DOCTORS";

$config['clinis']="CLINICS";

$config['specialities']="SPECIALITY";





/* PATIENT PROFILE DELETE MODEL

==================================================*/

$config['delete_user_account'] = "DELETE USER ACCOUNT";

$config['delete_user_desc'] = "DO YOU REALLY WANT TO DELETE ACCOUNT?";

$config['delete_user_message'] = "By verifying this action you will receive a verification code via email to confirm deletion of your account";

$config['accept'] = "ACCEPT";

$config['cancel'] = "CANCEL";

$config['delete_user_otp_desc'] = "By confirming this action your account will be permanently deleted";

$config['enter_confirm_code'] = "Enter the confirmation code received by email";

$config['account_deleted'] = "ACCOUNT DELETED";

$config['confirmation_code'] = "Confirmation Code";







/* PATIENT PROFILE EDIT PAGE

==================================================*/

$config['edit_your_profile'] = "Edit Your Profile";

$config['edit_photo'] = "Edit Photo";

$config['update_and_save'] = "UPDATE & SAVE";



/* PATIENT PROMOCODE PAGE

==================================================*/

$config['know_more'] = "Know More";

$config['biography'] = "Biography";

$config['specialization'] = "Specialization";

$config['location'] = "Location";

$config['view_complete_profile'] = "VIEW COMPLETE PROFILE";

$config['mark_consultation'] = "MARK CONSULTATION";

$config['no_records_found'] = "NO RECORDS FOUND";





/* DOCTOR SEARCH RESULT PAGE

==================================================*/

$config['filters'] = "FILTERS";

$config['return_included'] = "RETURN INCLUDED";

$config['yes'] = "Yes";

$config['no'] = "No";

$config['filter_on_value'] = "FILTER ON VALUE";

$config['home_visit'] = "HOME VISIT";

$config['men'] = "MEN";

$config['women'] = "WOMEN";

$config['order_on'] = "Order On";

$config['price_up'] = "Price Up";

$config['price_down'] = "Price Down";

$config['enter_location'] = "Enter Location";

$config['clinic'] = "Clinic";

$config['search_result_for'] = "Search result for";





/* DOCTOR SEARCH MARK CONSULTATION PAGE

==================================================*/

$config['appointment'] = "Appointment";

$config['review_information'] = "REVIEW INFORMATION";

$config['payment'] = "PAYMENT";

$config['confirmation'] = "CONFIRMATION";

$config['time_slot'] = "Time Slot";

$config['enter_waiting_list_text'] = "ENTER THE WAITING LIST";

$config['apply'] = "APPLY";

$config['clear'] = "CLEAR";

$config['amount'] = "Amount";

$config['money'] = "MONEY";

$config['credit_card'] = "CREDIT CARD";

$config['form_of_payment'] = "Form of Payment";

$config['offer_price'] = "OFFER PRICE";

$config['total_price'] = "TOTAL PRICE";

$config['back'] = "Back";

$config['continue'] = "Continue";

$config['consultation_confirmed'] = "Consultation Confirmed";

$config['done'] = "Done";

$config['coupon_heading'] = "COUPON";

$config['no_time_slot_available'] = "No Time Slot Available";

$config['booking_full_waiting_list_available'] = "Booking Full, Only Waiting list Available";

$config['booking_slot_unavailable'] = "Booking Slot Unavailable, Choose Another";

$config['doctor_unavailable'] = "Doctor Unavailable, Choose Another Date";

$config['invalid_booking_slot'] = "Choose a valid Time Slot";

$config['invalid_promocode'] = "Invalid Promotion Code!";

$config['promotion_success'] = "Promotion Applied";





/* PATIENT RECORD SUMMARY PAGE

==================================================*/



$config['service_summary_heading'] = "Service Summary";

$config['anamnesis'] = "Anamnesis";

$config['prescription'] = "Prescription";

$config['exams'] = "Exams";

$config['letters_and_certificate'] = "Letters and certificates";

$config['budget'] = "Budget";

$config['evaluation'] = "Evaluation";

$config['total'] = "TOTAL";

$config['send_by_email'] = "SEND BY EMAIL";

$config['print_out'] = "PRINT OUT";

$config['send_by_email_heading'] = "Medical Record";

$config['send_by_email_success'] = "Record has been Successfully sent.";

$config['send_by_email_error'] = "Record has been Successfully sent.";

$config['close'] = "Close";

$config['search'] = "Search";



/* DOCTOR DASHBOARD PAGE

==================================================*/



$config['attendance_text'] = "Attendance";

$config['billed_text'] = "Billed";

$config['patients_visited_text'] = "Patients";

$config['profile_views_text'] = "Profile Views";

$config['job_title'] = "Job Title";

$config['current_city'] = "Current City";

$config['total_attendence_today'] = "Total attendence today";

$config['week'] = "Week";

$config['month'] = "Month";

$config['year'] = "Year";

$config['day'] = "Day";

$config['today'] = "TODAY";

$config['sunday'] = "Sunday";

$config['monday'] = "Monday";

$config['tuesday'] = "Tuesday";

$config['wednesday'] = "Wednesday";

$config['thursday'] = "Thursday";

$config['friday'] = "Friday";

$config['saturday'] = "Saturday";

$config['main_schedule'] = "Main Schedule";

$config['select_clinic'] = "Select Clinic";

$config['primary'] = "Primary";

$config['secondary'] = "Secondary";

$config['primary_schedule'] = "Primary Schedule";

$config['secondary_schedule'] = "Secondary Schedule";

$config['configure_schedule_error'] = "Please Configure Agenda (PRIMARY and SECONDARY)";

$config['interval'] = "Interval";

$config['add_schedule'] = "ADD SCHEDULE";

$config['schedule_add_success'] = "Schedule added Successfully";

$config['schedule_add_failed'] = "Schedules are Unavailable";

$config['schedule_add_mismatch'] = "Schedules are Corrupted";

$config['configuring_consultation_heading'] = "Configuring Consultation";

$config['duration_of_consultation'] = "Duration of Consultation";

$config['time'] = "Time";

$config['value_of_consultation'] = "Value of Consultation";

$config['inquiry_including_return'] = "Inquiry Including Return";

$config['select_any'] = "Select Any";

$config['limit_period'] = "Limit Period";

$config['days'] = "DAYS";

$config['save'] = "Save";

$config['error'] = "Error";

$config['success'] = "Success";

$config['configuring_consultation_failed'] = "Configuring Consultation Failed";

$config['configuring_consultation_success'] = "Consultation Configured";

$config['vacation_heading'] = "Vacation";

$config['start_of_vacation'] = "Start of Vacation";

$config['end_of_vacation'] = "End of Vacation";

$config['activate_vacation'] = "ACTIVATE VACATION";

$config['add_vacation_success'] = "Vacation Added";

$config['add_vacation_error'] = "Vacation not Added";





/* DOCTOR PROFILE EDIT PAGE

==================================================*/

$config['telephone'] = "Telephone";

$config['add_photo'] = "Add Photo";

$config['price'] = "Price";



/* DOCTOR RECORDS PAGE

==================================================*/

$config['medical_records'] = "Medical Records";

$config['patience_attended_filter'] = "Patience Attended";

$config['patience_scheduled_filter'] = "Patience Scheduled";

$config['send_message'] = "Send Message";

$config['send_message_to'] = "To";

$config['send_message_user_error'] = "Please Select Users";

$config['send_message_text_error'] = "Please Write Message";

$config['send'] = "Send";

$config['last_consultation'] = "Last Consultation";

$config['not_available'] = "Not Available";

$config['next_consultation'] = "Next Consultation";

$config['message'] = "Message";

$config['no_medical_records_found'] = "No Medical Records Found";

$config['time'] = "Time";

$config['consultation_value'] = "Value";

$config['consultation'] = "Consultation";

$config['promotional_consultation'] = "Promotional Consultation";

$config['normal_consultation'] = "Normal Consultation";

$config['open_record'] = "OPEN RECORD";

$config['medical_record'] = "MEDICAL RECORD";

$config['medical_record_sent_success'] = "Record has been Successfully sent";

$config['medical_record_sent_error'] = "Error.Record senting failed";



/* DOCTOR WALLET PAGE

==================================================*/

$config['wallet'] = "Wallet";

$config['balance_for_redemption'] = "Balance for redemption";

$config['available_for_redemption'] = "Available for redemption";

$config['last_redemption'] = "Last Redemption";

$config['history'] = "HISTORY";

$config['future_releases'] = "Future Releases";

$config['launches_of_today'] = "Launches of today";

$config['next_release'] = "Next Release";

$config['view_more'] = "VIEW MORE";

$config['total_balance'] = "Total Balance";

$config['add_bank_account'] = "Add Bank Account";

$config['bank'] = "Bank";

$config['agency'] = "Agency";

$config['account'] = "Account";

$config['register'] = "Register";

$config['your_banks'] = "Your Banks";

$config['perform_redemption'] = "Perform Redemption";

$config['requested_value'] = "Requested value";

$config['redemption_value'] = "Redemption Value";

$config['request_redemption'] = "Request Redemption";

$config['no_future_releases'] = "No Future Realeases";

$config['redemption_history'] = "Redemption History";

$config['no_redemption_request'] = "No Redemption Request";

$config['you_have_not_added_any_banks'] = "You have not added any banks";

$config['error_insufficient_balance'] = "Error, Insufficient Balance";

$config['invalid_bank_account'] = "Invalid Bank Account Credentials";

$config['facing_technical_issues'] = "Facing Technical Issues";

$config['select_bank'] = "Select Bank";

$config['bank_added'] = "Bank Added";

$config['redemption_requested'] = "Redemption Requested";





/* DOCTOR SERVICE PAGE

==================================================*/



$config['start_service'] = "Start Service";

$config['canceled_consultation'] = "Canceled Consultation";

$config['no_appointments'] = "NO APPOINTMENTS";

$config['appointments'] = "APPOINTMENTS";

$config['main_complaint'] = "Main Complaint";

$config['kidney_problems'] = "Kidney Problems";

$config['joint_problems_or_rheumatism'] = "Joint problems or rheumatism";

$config['heart_problems'] = "Heart problems";

$config['breathing_problems'] = "Breathing problems";

$config['gastric_problems'] = "Gastric problems";

$config['allergies'] = "Allergies";

$config['use_of_medicines'] = "Use of medicines";

$config['hepatitis'] = "Hepatitis";

$config['pregnancy'] = "Pregnancy";

$config['diabetis'] = "Diabetis";

$config['healing_problems'] = "Healing problems";

$config['anamnese_updated_message'] = "Anamnese Updated";

$config['updation_failed'] = "Updation Failed";

$config['insertion_failed'] = "Insertion Failed";

$config['prescriptions'] = "Prescriptions";

$config['select_quantity'] = "Select Quantity";

$config['select_dosage_and_administration'] = "Select Dosage and administration";

$config['add_medicine'] = "Add Medicine";

$config['edit_medicine'] = "Edit Medicine";

$config['print'] = "PRINT";

$config['medicine_updated_message'] = "Medicine Updated";

$config['exams_desc'] = "Examination or procedure";

$config['exams_observation_note'] = "Note";

$config['exams_updated_message'] = "Exams Updated";

$config['budget_heading'] = "Item or procedure";

$config['procedure_desc'] = "Item or procedure";

$config['procedure_value'] = "Value";

$config['procedure_amount'] = "Amount";

$config['procedure_add_button'] = "Add item or procedure";

$config['procedure_updated_message'] = "Procedures Updated";

$config['attached_letters'] = "Attached letters";

$config['standard_certificate'] = "Standard certificate";

$config['standard_certificate_with_cid'] = "Standard certificate with CID";

$config['days_of_removal'] = "Days of removal";

$config['select_day'] = "Select Day";

$config['diagnostic_cid'] = "Diagnostic (CID)";

$config['certificate_minimum_length_error'] = "Please enter Certificate Data! Minimum 20 Characters";

$config['certificate_updated_message'] = "Certificate Updated";

$config['review_updated'] = "Review Added";

$config['end_of_service'] = "END OF SERVICE";

$config['consultation_duration'] = "Consultation Duration";

$config['others_heading'] = "Others";

$config['other_observation'] = "Other Observation";

$config['attach_images'] = "Attach images";

$config['service_summary'] = "Service Summary";







/* CHAT PAGE

==================================================*/

$config['recent_messages'] = "Recent Messages";

$config['no_recent_messages'] = "No Recent Chats";

$config['all_messages'] = "All Chats";

$config['select_conversation_text'] = "Please Select a Conversation";

$config['no_messages'] = "No Messages";



/* COLLABORATOR PAGE

==================================================*/

$config['collaborator_permission_denied'] = "No Permissions authorized for you right now, Come back later";

$config['add_collaborator'] = "Add collaborator";

$config['add_collaborator_save_button'] = "Add collaborator";

$config['add_collaborator_success'] = "Collaborator Added, Authorize Access";

$config['add_collaborator_error'] = "Error! Kindly try Again";

$config['collaborator_access_heading'] = "Authorize access";

$config['collaborator_access_desc'] = "Choose the areas that your collaborator can access,view, edit, and configure. These changes can be altered in the future";

$config['collaborator_access_save_button'] = "Authorize access";

$config['please_add_collaborator'] = "Please add Collaborator";

$config['collaborator_access_success'] = "Settings Saved!";

$config['edit_collaborator'] = "Edit collaborator";

$config['profile_photo_error_text'] = "Choose Profile Photo";

$config['delete_collaborator'] = "Delete Collaborator";

$config['collaborator_update_success'] = "Collaborator Profile Updated";

$config['collaborator_update_error'] = "Error! Kindly try Again";

$config['collaborator_not_selected'] = "Collaborator not selected";

$config['collaborator_excluded'] = "Collaborator Excluded";

$config['collaborator_exclusion_failed'] = "Collaborator Exclusion Failed";



/* NOTIFICATON PAGE

==================================================*/

$config['notification_center'] = "Notification center";

$config['anniversaries'] = "Anniversaries";



/* ERROR & ACCESS DENIED PAGE

==================================================*/

$config['url_error'] = "URL ERROR ";

$config['invalid_url'] = "Invalid Url Provided";

$config['access_denied'] = "Access Denied";

$config['access_denied_desc'] = "You are not authorized to access this page";



/* FORGOT PASSWORD PAGE

==================================================*/

$config['want_to_change_password'] = "WANT TO CHANGE PASSWORD";

$config['please_fill_credentials'] = "Please fill your new credential details";

$config['new_password'] = "New Password";

$config['confirm_new_password'] = "Confirm New Password";

$config['password_change_success'] = "Your Password has been Updated! Kindly Login";

$config['invalid_credentials_provided'] = "Invalid Credentials Provided!";





/* PATIENT DEPENDENT PAGE

==================================================*/



$config['add_dependent'] = "Add dependent";

$config['family_bond'] = "Family bond";

$config['add_dependent_save_button'] = "Add Dependent";

$config['edit_dependent'] = "Edit dependent";


/* PAYMENT PAGE

==================================================*/

$config['invalid_credit_card'] = 'Invalid Card Provided';

$config['no_booking_found'] = 'No Booking Found';

$config['make_payment'] = 'MAKE PAYMENT';

$config['cvv'] = 'CVV';

$config['card_number'] = 'Card Number';

$config['last_name'] = 'Last Name';

$config['first_name'] = 'First Name';

$config['payment_tab_desc'] = 'Description in payment page';

$config['expiration_date'] = 'EXPIRATION DATE';


?>