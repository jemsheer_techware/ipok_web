<?php 
   class Search_doctor_model extends CI_Model {
	
function __construct() { 
	 parent::__construct(); 
} 
public function markpayment($data){

	$id = $data['book_id'];
	$datas = array('payment_status' => $data['payment_status'],
					'payment_type' => $data['payment_type']);

	$this->db->update('tbl_booking', $datas, array('id' => $id));
	return true;


}
public function markpayment_viacredit($data){
	$id = $data['book_id'];
	$datas = array('payment_status' => $data['payment_status'],
					'payment_type' => $data['payment_type']);

	$this->db->update('tbl_booking', $datas, array('id' => $id));
	return true;
}

public function doctor_search($post_data)
{
	$limit = 1;
	$page = 1;
	if(isset($post_data['page'])) {
	$page = $post_data['page'];
	}
	$start = ($page-1) * $limit;

	$lat = $post_data["doctor-search-latitude"];
	$lng = $post_data["doctor-search-longitude"];

	$this->db->select("tbl_doctors.profile_pic AS doctor_photo,
			  				   tbl_doctors.about AS biography,
							   tbl_doctors.street_address AS street_address,
							   tbl_doctors.locality AS locality,
							   tbl_specialization.specialization_name AS specialization,
							   tbl_doctors.id as doctorid,
							   tbl_doctors.name,
							   tbl_doctors.price,
							   tbl_clinic.id AS clinic_id, 
							   tbl_clinic.name AS clinic_name,
							   ROUND(( 6371 * acos( cos( radians({$lat}) ) * cos( radians( `location_lattitude` ) ) * cos( radians( `location_longitude` ) - radians({$lng}) ) + sin( radians({$lat}) ) * sin( radians( `location_lattitude` ) ) ) )) AS clinic_distance,
							   tbl_clinic.street_address AS clinic_street_address,
							   tbl_clinic.cep AS clinic_cep,
							   tbl_clinic.locality AS clinic_locality,
							   tbl_clinic.number AS clinic_number,
							   tbl_clinic.location_lattitude AS clinic_lat,
							   tbl_clinic.location_longitude AS clinic_lng"

						);
	$this->db->from('tbl_doctors');

	$this->db->join('tbl_specialization', 'tbl_specialization.id = tbl_doctors.specialization','left');
	$this->db->join('tbl_doctor_leave', 'tbl_doctor_leave.doctor_id = tbl_doctors.id','left');
	$this->db->join('tbl_clinic_doctors', 'tbl_doctors.id = tbl_clinic_doctors.doctor_id','left');
	$this->db->join('tbl_clinic', 'tbl_clinic_doctors.clinic_id = tbl_clinic.id','left');

	if(isset($post_data['doctor-search-speciality']) && !empty($post_data['doctor-search-speciality']))
	{				
	$this->db->where('tbl_specialization.specialization_name',$post_data['doctor-search-speciality']);
	}

	if(isset($post_data['doctor-search-location']) && !empty($post_data['doctor-search-location']))
	{
		/*$where = "ROUND(( 6371 * acos( cos( radians({$post_data['doctor-search-latitude']}) ) * cos( radians( `location_lattitude` ) ) * cos( radians( `location_longitude` ) - radians({$post_data['doctor-search-longitude']}) ) + sin( radians({$post_data['doctor-search-latitude']}) ) * sin( radians( `location_lattitude` ) ) ) )) <=10"; 				
		$this->db->where($where);*/
		
		$where = "ROUND(( 6371 * acos( cos( radians({$post_data['doctor-search-latitude']}) ) * cos( radians( `location_lattitude` ) ) * cos( radians( `location_longitude` ) - radians({$post_data['doctor-search-longitude']}) ) + sin( radians({$post_data['doctor-search-latitude']}) ) * sin( radians( `location_lattitude` ) ) ) ))";

		$this->db->where($where."<=10" );	
	}

	if(isset($post_data['doctor-search-date']) && !empty($post_data['doctor-search-date']))
	{				
		$this->db->where($post_data['doctor-search-date'].'<'.'tbl_doctor_leave.start_date');
		$this->db->or_where($post_data['doctor-search-date'].'>'.'tbl_doctor_leave.end_date');
	}
	/*$this->db->get();
	echo $this->db->last_query();die();*/
	$this->db->limit($limit, $start);
	$data =$this->db->get()->result_array();		
	return $data;



}


public function get_single_doctor_clinic($dctr_id,$clinic_id)
{

	$this->db->select("tbl_doctors.id as doctorid,
	  				   tbl_doctors.name as dr_name,
					   tbl_doctors.profile_pic as dr_pic,
					   tbl_doctors.email as dr_email,
					   tbl_doctors.dob as dr_dob,
					   tbl_doctors.about as dr_bio,
					   tbl_doctors.price as dr_price,
					   tbl_specialization.specialization_name AS dr_specialization,
					   tbl_clinic.id AS clinic_id,
					   tbl_clinic.name AS clinic_name,
					   tbl_clinic.street_address AS clinic_street_address,
					   tbl_clinic.locality AS clinic_locality,
					   tbl_clinic.cep  clinic_cep,
					   tbl_clinic.number AS clinic_number,
					   tbl_clinic.location_lattitude AS clinic_lat,
					   tbl_clinic.location_longitude AS clinic_lng
							 ");
	$this->db->from('tbl_doctors');
		
	$this->db->join('tbl_specialization', 'tbl_specialization.id = tbl_doctors.specialization','left');
	$this->db->join('tbl_clinic_doctors', 'tbl_doctors.id = tbl_clinic_doctors.doctor_id','left');
	$this->db->join('tbl_clinic', 'tbl_clinic_doctors.clinic_id = tbl_clinic.id','left');

	$this->db->where('tbl_doctors.id',$dctr_id);
	$this->db->where('tbl_clinic.id',$clinic_id);

	$data =$this->db->get()->row_array();		
	return $data;
}

public function filter_search($post_data)
{

	//print_r($post_data);echo"<br>";echo"<br>";
	//die();
	//$limit = 10;
	$page = 1;
	if(isset($post_data['page'])) {
	$page = $post_data['page'];
	}
	$limit = $page * 10;
	//$start = ($page-1) * $limit;

	$lat = $post_data["doctor-search-latitude"];
	$lng = $post_data["doctor-search-longitude"];


	$this->db->select("tbl_doctors.profile_pic AS doctor_photo,
			  				   tbl_doctors.about AS biography,
							   tbl_doctors.street_address AS street_address,
							   tbl_doctors.locality AS locality,
							   tbl_specialization.specialization_name AS specialization,
							   tbl_doctors.id as doctorid,
							   tbl_doctors.name,
							   tbl_doctors.price,
							   tbl_doctors.accept_return,
							   tbl_clinic.id AS clinic_id, 
							   tbl_clinic.name AS clinic_name,
							   ROUND(( 6371 * acos( cos( radians({$lat}) ) * cos( radians( `location_lattitude` ) ) * cos( radians( `location_longitude` ) - radians({$lng}) ) + sin( radians({$lat}) ) * sin( radians( `location_lattitude` ) ) ) )) AS clinic_distance,case when tbl_clinic_doctors.clinic_id != '0' then tbl_clinic.street_address else tbl_doctors.street_address end AS clinic_street_address,
							   case when tbl_clinic_doctors.clinic_id != '0' then tbl_clinic.cep else tbl_doctors.cep end AS clinic_cep,
							   case when tbl_clinic_doctors.clinic_id != '0' then tbl_clinic.locality else tbl_doctors.locality end AS clinic_locality,
							   case when tbl_clinic_doctors.clinic_id != '0' then tbl_clinic.number else tbl_doctors.number end AS clinic_number,
							   case when tbl_clinic_doctors.clinic_id != '0' then tbl_clinic.location_lattitude else tbl_doctors.default_latitude end AS clinic_lat,
							   case when tbl_clinic_doctors.clinic_id != '0' then tbl_clinic.location_longitude else tbl_doctors.default_longitude end AS clinic_lng"

						);

	//$this->db->select("tbl_doctors.name");
	$this->db->from('tbl_doctors');

	$this->db->join('tbl_specialization', 'tbl_specialization.id = tbl_doctors.specialization','left');
	$this->db->join('tbl_clinic_doctors', 'tbl_doctors.id = tbl_clinic_doctors.doctor_id','inner');
	$this->db->join('tbl_clinic', 'tbl_clinic_doctors.clinic_id = tbl_clinic.id','left');
	$this->db->where('tbl_doctors.price>0');

	if(isset($post_data['doctor-search-speciality']) && !empty($post_data['doctor-search-speciality']) && isset($post_data['selected_doctor_type']) && !empty($post_data['selected_doctor_type']) && $post_data['selected_doctor_type'] == 'speciality')
	{				
	$this->db->where('tbl_specialization.specialization_name',$post_data['doctor-search-speciality']);
	}

	else if(isset($post_data['doctor-search-speciality']) && !empty($post_data['doctor-search-speciality']) && isset($post_data['selected_doctor_type']) && !empty($post_data['selected_doctor_type']) && $post_data['selected_doctor_type'] == 'clinic')
	{				
	$this->db->where('tbl_clinic.name',$post_data['doctor-search-speciality']);
	}
	else if(isset($post_data['doctor-search-speciality']) && !empty($post_data['doctor-search-speciality']) && isset($post_data['selected_doctor_type']) && !empty($post_data['selected_doctor_type']) && $post_data['selected_doctor_type'] == 'doctor')
	{				
	$this->db->where('tbl_doctors.name',$post_data['doctor-search-speciality']);
	}

	//Initial Search with Location Input
	if( !empty($post_data['doctor-search-location']) && empty($post_data['filter_dr_srch_distance_end'])  && empty($post_data['filter_dr_srch_distance_start']))
	{
		
		$where = "ROUND(( 6371 * acos( cos( radians($lat) ) * cos( radians( `location_lattitude` ) ) * cos( radians( `location_longitude` ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( `location_lattitude` ) ) ) ))";

		$this->db->where($where."<= 15" );
	}

	//Filter Search Location
	if( !empty($post_data['filter_dr_srch_distance_end']) && !empty($post_data['filter_dr_srch_distance_start']))
	{
		$post_data['filter_dr_srch_distance_end']  = preg_replace("/[^0-9,.]/", "", $post_data['filter_dr_srch_distance_end'] );

		$post_data['filter_dr_srch_distance_start']  = preg_replace("/[^0-9,.]/", "", $post_data['filter_dr_srch_distance_start'] );
		
		$where = "ROUND(( 6371 * acos( cos( radians($lat) ) * cos( radians( `location_lattitude` ) ) * cos( radians( `location_longitude` ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( `location_lattitude` ) ) ) ))";

		$this->db->where($where.">= {$post_data['filter_dr_srch_distance_start']}" );
		$this->db->where($where."<= {$post_data['filter_dr_srch_distance_end']}" );
	}


	if(isset($post_data['filter_dr_gender']) && !empty($post_data['filter_dr_gender']))
	{				
	$this->db->where('tbl_doctors.gender',$post_data['filter_dr_gender']);
	}

	if(!empty($post_data['filter_dr_srch_price_high']) && !empty($post_data['filter_dr_srch_price_low']))
	{
		$post_data['filter_dr_srch_price_low']  = preg_replace("/[^0-9,.]/", "", $post_data['filter_dr_srch_price_low'] );

		$post_data['filter_dr_srch_price_high']  = preg_replace("/[^0-9,.]/", "", $post_data['filter_dr_srch_price_high'] );				
	
	$this->db->where('tbl_doctors.price'.">= {$post_data['filter_dr_srch_price_low']}" );
	$this->db->where('tbl_doctors.price'."<= {$post_data['filter_dr_srch_price_high']}" );
	}

	if(isset($post_data['doctor-search-date']) && !empty($post_data['doctor-search-date']))
	{	
		//print_r($post_data['doctor-search-date']);die();
		$passdate = $post_data['doctor-search-date'];
		
		//$this->db->join('tbl_doctor_leave', "tbl_doctor_leave.doctor_id = tbl_doctors.id and ($passdate<tbl_doctor_leave.start_date OR $passdate>tbl_doctor_leave.end_date)",'left');
		$this->db->join('tbl_doctor_leave', "tbl_doctor_leave.doctor_id = tbl_doctors.id and tbl_doctor_leave.clinic_id = tbl_clinic_doctors.clinic_id",'left');
		$this->db->where("(tbl_doctor_leave.start_date IS NULL or ($passdate<tbl_doctor_leave.start_date OR $passdate>tbl_doctor_leave.end_date))");
		//$this->db->or_where($post_data['doctor-search-date'].'>'.'tbl_doctor_leave.end_date');
	}

	if(!empty($post_data['filter_dr_domicilar'])&&($post_data['filter_dr_domicilar']))
	{	
		$this->db->where('tbl_doctors.domiciliary_status',1);
	}

	if(isset($post_data['filter_dr_return_included']))
	{			
		$this->db->where('tbl_doctors.accept_return',$post_data['filter_dr_return_included']);
	}

	if(!empty($post_data['filter_dr_order_price'])&&($post_data['filter_dr_order_price']=='ASC'))
	{	
		//print_r("expression");die();
		$this->db->order_by('tbl_doctors.price','asc');
	}
	else if(!empty($post_data['filter_dr_order_price'])&&($post_data['filter_dr_order_price']=='DESC'))
	{
		$this->db->order_by('tbl_doctors.price','desc');
	}

	//$this->db->limit($limit, $start);
	//$this->db->limit($limit);
	$this->db->where('account_status','0');
	$data =$this->db->get()->result_array();
	//echo $this->db->last_query();echo"<br>";
	//var_dump($data); 	exit();
	return $data;
}
function doctor_availability($doctor_id,$clinic_id)
{
	$this->db->select("tbl_consultation.*");
	$query = $this->db->get_where('tbl_consultation',array('doctor_id' => $doctor_id,'clinic_id' => $clinic_id));
	//print_r($query->row_array());
	if ($query->num_rows() > 0) {
	$return_array = array('status'=>'success','data'=>$query->row_array());
	}
	else{
	$return_array = array('status'=>'fail');
	}
	return $return_array;
}  
function checkDoctorLeave($data)
{
	$date = strtotime($data['confirm-book-date']);
	$this->db->select('count(*) as count');
	$this->db->from('tbl_doctor_leave');
	$this->db->where('tbl_doctor_leave.start_date'."<= {$date}" );
	$this->db->where('tbl_doctor_leave.end_date'.">= {$date}" );
	$this->db->where('tbl_doctor_leave.doctor_id',$data['confirm-book-doctor'] );
	$this->db->where('tbl_doctor_leave.clinic_id',$data['confirm-book-clinic'] );
	$data =$this->db->get()->row_array();	
	//print_r($data);die();	
	return $data;
	
}

function checkDoctorBooking($data)
{
	$date = strtotime($data['confirm-book-date']);
	//print_r($data);die();
	$this->db->select('count(*) as count');
	//$this->db->select('tbl_booking.id');
	$this->db->from('tbl_booking');
	$this->db->where('tbl_booking.time',$data['confirm-book-time']);
	$this->db->where('tbl_booking.doctor_id',$data['confirm-book-doctor'] );
	$this->db->where('tbl_booking.clinic_id',$data['confirm-book-clinic'] );
	$this->db->where('tbl_booking.date',$date);
	$this->db->where('tbl_booking.booking_status<4'); //checked
	//$this->db->or_where('tbl_booking.booking_status','2');
	//$this->db->or_where('tbl_booking.booking_status','0');
	$data =$this->db->get()->row_array();	
	//print_r($data);die();	
	return $data;
	
} 
function getDoctorPrice($doctorid)
{
	
	$this->db->select('tbl_doctors.price as price');
	$this->db->from('tbl_doctors');
	$this->db->where('tbl_doctors.id',$doctorid);
	$data =$this->db->get()->row_array();	
	//print_r($data);die();	
	return $data;
}  
function insertBooking($data)
{
	
	if($this->db->insert('tbl_booking',$data))
	{
		$insert_id = $this->db->insert_id();
	}
	return $insert_id;
} 
function checkBooking($id)
{
	$this->db->select('count(*) as count');
	$this->db->select('tbl_booking.date as booking_date');
	$this->db->select('tbl_booking.id as booking_id');
	$this->db->select('tbl_booking.time as booking_slot');
	$this->db->from('tbl_booking');
	$this->db->where('tbl_booking.id',$id);
	$this->db->where('tbl_booking.payment_status','0');
	$this->db->where('tbl_booking.booking_status!=4');
	$data =$this->db->get()->row_array();	
	//print_r($data);die();	
	return $data;
}
 function checkPromocode($data)
{
	$today = strtotime(Date('y-m-d'));
	
	$this->db->select('*');
	$this->db->from('tbl_promocode');
	$this->db->where('tbl_promocode.status',1);
	$this->db->where('tbl_promocode.promo_name',$data['code']);
	$this->db->where('tbl_promocode.valid_from <='.$today);
	$this->db->where('tbl_promocode.valid_to >='.$today);
	/*$where = "CONCAT(',' , 'doctor_id' , ',') LIKE '%,'".$data['doctorid'].",%' ";
	$this->db->where($where);*/
	$data =$this->db->get()->row_array();	
	//print_r($data);die();	
	return $data;
} 
 function get_promocode_value($name)
{

	
	$this->db->select('amount');
	$this->db->from('tbl_promocode');
	$this->db->where('tbl_promocode.promo_name',$name);
	/*$where = "CONCAT(',' , 'doctor_id' , ',') LIKE '%,'".$data['doctorid'].",%' ";
	$this->db->where($where);*/
	$data =$this->db->get()->row_array();	
	//print_r($data);die();	
	return $data;
} 
function set_payment_status($id)
{
	$update = array('payment_status' => '1');
	$this->db->where('tbl_booking.id',$id);
	$this->db->update('tbl_booking', $update);	
}
 function mark_freevisit_status($id)
{
	$update = array('free_visit_status' => '1');
	$this->db->where('tbl_booking.id',$id);
	$this->db->update('tbl_booking', $update);	
} 
function get_previous_book($data,$days,$myid)
{
	$day = date('Y-m-d',strtotime($data['confirm-book-date']));
	$newdate = date("Y-m-d",strtotime($day." -".$days." day"));
	
	$new_date = Date($data['confirm-book-date']);
	$this->db->select('*');
	$this->db->from("tbl_booking");
	$this->db->where("tbl_booking.doctor_id",$data['confirm-book-doctor']);
	$this->db->where("tbl_booking.clinic_id",$data['confirm-book-clinic']);
	$this->db->where("tbl_booking.patient_id",$myid);
	$this->db->where("tbl_booking.visit_type",0);
	$this->db->where("tbl_booking.free_visit_status",0);
	/*$this->db->where("tbl_booking.date >= ".strtotime($newdate));*/
	$this->db->where("tbl_booking.date <= ".strtotime($data['confirm-book-date']));
	$this->db->where("tbl_booking.date >= ".strtotime($newdate));
	$this->db->order_by("tbl_booking.id","DESC");
	$this->db->limit('1,0');
	$query = $this->db->get();
	return $query->row_array();
}
function get_doc_retrun_inclusive($id)
{
	$this->db->select('accept_return,
						return_timeperiod');
	$this->db->from("tbl_doctors");
	$this->db->where("tbl_doctors.id",$id);
	$query = $this->db->get();
	return $query->row_array();
}
function get_doc_min_price()
{
	$this->db->select_min('price');
	$this->db->from("tbl_doctors");
	$query = $this->db->get();
	return $query->result_array();
}
function get_doc_max_price()
{
	$this->db->select_max('price');
	$this->db->from("tbl_doctors");
	$query = $this->db->get();
	return $query->result_array();
}
function get_doc_min_distance($post_data)
{
	$lat = $post_data["doctor-search-latitude"];
	$lng = $post_data["doctor-search-longitude"];
	
	$this->db->select("MIN(ROUND(( 6371 * acos( cos( radians({$lat}) ) * cos( radians( `location_lattitude` ) ) * cos( radians( `location_longitude` ) - radians({$lng}) ) + sin( radians({$lat}) ) * sin( radians( `location_lattitude` ) ) ) ))) as dist");
	$this->db->from('tbl_doctors');
	$this->db->join('tbl_clinic_doctors', 'tbl_doctors.id = tbl_clinic_doctors.doctor_id','left');
	$this->db->join('tbl_clinic', 'tbl_clinic_doctors.clinic_id = tbl_clinic.id','left');
	$query = $this->db->get();
	return $query->result_array();
}
function get_doc_max_distance($post_data)
{
	$lat = $post_data["doctor-search-latitude"];
	$lng = $post_data["doctor-search-longitude"];
	
	$this->db->select("MAX(ROUND(( 6371 * acos( cos( radians({$lat}) ) * cos( radians( `location_lattitude` ) ) * cos( radians( `location_longitude` ) - radians({$lng}) ) + sin( radians({$lat}) ) * sin( radians( `location_lattitude` ) ) ) ))) as dist");
	$this->db->from('tbl_doctors');
	$this->db->join('tbl_clinic_doctors', 'tbl_doctors.id = tbl_clinic_doctors.doctor_id','left');
	$this->db->join('tbl_clinic', 'tbl_clinic_doctors.clinic_id = tbl_clinic.id','left');
	$query = $this->db->get();
	return $query->result_array();
}

function insert_profile_view_count($doctor_id)
{
	$data['doctor_id'] = $doctor_id;
	if($this->session->userdata('UserData'))
	{
		$data['patient_id'] = $this->session->userdata('UserData')['id'];
	}
	$data['ip_address'] = $_SERVER['REMOTE_ADDR'];
	$this->db->insert('tbl_doctor_profileviewers',$data);
}

function check_profile_view_entry($doctor_id)
{
	$my_ip = $_SERVER['REMOTE_ADDR'];
	$this->db->select('COUNT(id) as count');
	$this->db->from('tbl_doctor_profileviewers');
	$this->db->where('tbl_doctor_profileviewers.doctor_id',$doctor_id);
	$this->db->where('tbl_doctor_profileviewers.ip_address',$my_ip);
	$query = $this->db->get();
	return $query->row_array();
}

function get_doctors_for_promocode($promo_id,$doc_array)
{
	$this->db->select("tbl_doctors.profile_pic AS doctor_photo,
			  				   tbl_doctors.about AS biography,
							   tbl_doctors.street_address AS street_address,
							   tbl_doctors.locality AS locality,
							   tbl_specialization.specialization_name AS specialization,
							   tbl_doctors.id as doctorid,
							   tbl_doctors.name,
							   tbl_doctors.price,
							   tbl_clinic.id AS clinic_id, 
							   tbl_clinic.name AS clinic_name,
							   tbl_clinic.street_address AS clinic_street_address,
							   tbl_clinic.cep AS clinic_cep,
							   tbl_clinic.locality AS clinic_locality,
							   tbl_clinic.number AS clinic_number,
							   tbl_clinic.location_lattitude AS clinic_lat,
							   tbl_clinic.location_longitude AS clinic_lng"

						);
	$this->db->from('tbl_doctors');

	$this->db->join('tbl_specialization', 'tbl_specialization.id = tbl_doctors.specialization','left');
	$this->db->join('tbl_clinic_doctors', 'tbl_doctors.id = tbl_clinic_doctors.doctor_id','inner');
	$this->db->join('tbl_clinic', 'tbl_clinic_doctors.clinic_id = tbl_clinic.id','left');
	$this->db->where_in('tbl_doctors.id', $doc_array);
	$data =$this->db->get()->result_array();		
	return $data;
	/*$this->db->get();
	print_r($this->db->last_query());die();*/

}


function get_booking_details($booking_id)
{
	$this->db->select('*');
	$this->db->from('tbl_booking');
	$this->db->where('id',$booking_id);
	$data =$this->db->get()->row_array();		
	return $data;
}
	  
  
} 
?> 