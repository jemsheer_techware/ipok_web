<?php 

   class Patient_model extends CI_Model {

	

function __construct() { 

	 parent::__construct(); 

} 



public function get_single_patient($id)



{

	$this->db->select("tbl_registration.id as patientid,

	  				   tbl_registration.name as pt_name,

	  				   tbl_registration.username as pt_username,

					   tbl_registration.profile_photo as pt_pic,

					   tbl_registration.gender as pt_gender,

					   tbl_registration.email as pt_email,

					  

					   tbl_registration.number as pt_number,

					   tbl_registration.blood_group as pt_blood_group,

					   tbl_registration.weight as pt_weight,

					   tbl_registration.height as pt_height,					

					   tbl_registration.street_address as pt_street_add,					

					   tbl_registration.locality as pt_locality,					

					   tbl_registration.zip_code as pt_zip_code,					

					   tbl_registration.landmark as pt_complement,					

					   tbl_registration.rg as pt_rg,					

					   tbl_registration.cpf as pt_cpf,								
					   tbl_registration.customer_id as pt_customer_id								

					   					

							 ");

	$this->db->from('tbl_registration');

	//$this->db->join('tbl_specialization', 'tbl_specialization.id = tbl_doctors.specialization','left');

	$this->db->where('tbl_registration.id',$id);


	

	$data =$this->db->get();

	$getsingle_pat = $data->row_array();

	$this->db->select("CAST(AES_DECRYPT(`dob`,'Ptf/PWNWrULQT72syxfaaBRTS9JbiKrj9dfuVEvT3rA') as CHAR) as pt_dob");
	$this->db->where('id',$id);
	$query_date = $this->db->get('tbl_registration')->row();

	// echo $this->db->last_query();
	// die();

	unset($getsingle_pat['dob']);
	$getsingle_pat['pt_dob'] = $query_date->pt_dob;	

	return $getsingle_pat;

}



public function get_doctor_clinic_list($id)

{



	$this->db->select("tbl_clinic.name as clinic_name,

	  				   tbl_clinic.id as clinic_id

							 ");

	$this->db->from('tbl_clinic_doctors');

	$this->db->join('tbl_clinic', 'tbl_clinic.id = tbl_clinic_doctors.clinic_id','inner');

	$this->db->where('tbl_clinic_doctors.doctor_id',$id);

	//$this->db->get();

//	$data = $this->db->last_query();

	$data =$this->db->get()->result_array();		

	//print_r($data);die();

	return $data;

}

public function get_policy()

{



	$this->db->select("*");

	$this->db->from('tbl_policy');

	$data =$this->db->get()->row_array();		

	//print_r($data);die();

	return $data;

}

public function get_patient_completed_consultation($id)

{



	$this->db->select("tbl_booking.id as book_id,

					   tbl_booking.date as book_date,

	  				   tbl_booking.time as book_time,

	  				   tbl_doctors.name as doc_name,

	  				   tbl_doctors.profile_pic as doc_pic

							 ");

	$this->db->from('tbl_booking');

	$this->db->join('tbl_doctors', 'tbl_booking.doctor_id = tbl_doctors.id','inner');

	$this->db->where('tbl_booking.patient_id',$id);

	$this->db->where('tbl_booking.booking_status',3); //checked

	$this->db->where('tbl_booking.payment_status',1); //checked

	$this->db->order_by("tbl_booking.date", "asc"); 

	$this->db->order_by("tbl_booking.time_start", "asc"); 

	//$this->db->get();

//	$data = $this->db->last_query();

	$data =$this->db->get()->result_array();		

	//print_r($data);die();

	return $data;

}

public function get_patient_confirmed_consultation($id)

{



	$this->db->select("tbl_booking.id as book_id,

					   tbl_booking.date as book_date,

	  				   tbl_booking.time as book_time,

	  				   tbl_booking.booking_status as status,

	  				   tbl_doctors.name as doc_name,

	  				   tbl_doctors.profile_pic as doc_pic

							 ");

	$this->db->from('tbl_booking');

	$this->db->join('tbl_doctors', 'tbl_booking.doctor_id = tbl_doctors.id','inner');

	$this->db->where('tbl_booking.patient_id',$id);

	$this->db->where('tbl_booking.booking_status<2'); //checked

	$this->db->where('tbl_booking.payment_status',1);

	$this->db->order_by("tbl_booking.date", "asc"); 

	$this->db->order_by("tbl_booking.time_start", "asc"); 

	//$this->db->get();

//	$data = $this->db->last_query();

	$data =$this->db->get()->result_array();		

	//print_r($data);die();

	return $data;

}

public function get_patient_canceled_consultation($id)

{



	$this->db->select("tbl_booking.id as book_id,

					   tbl_booking.date as book_date,

	  				   tbl_booking.time as book_time,

	  				   tbl_booking.booking_status as status,

	  				   tbl_doctors.name as doc_name,

	  				   tbl_doctors.profile_pic as doc_pic

							 ");

	$this->db->from('tbl_booking');

	$this->db->join('tbl_doctors', 'tbl_booking.doctor_id = tbl_doctors.id','inner');

	$this->db->where('tbl_booking.patient_id',$id);

	$this->db->where('tbl_booking.booking_status',4); //checked

	$this->db->where('tbl_booking.payment_status',1);

	$this->db->order_by("tbl_booking.date", "asc"); 

	$this->db->order_by("tbl_booking.time_start", "asc"); 

	//$this->db->get();

//	$data = $this->db->last_query();

	$data =$this->db->get()->result_array();		

	//print_r($data);die();

	return $data;

}

public function get_Booking($booking_id)

{

 	$this->db->select("tbl_booking.id as book_id,

					   tbl_booking.doctor_id as doc_id,

					   tbl_booking.clinic_id as clinic_id,

					   tbl_booking.date as book_date,

	  				   tbl_booking.time as book_time,

	  				   tbl_booking.time_start ,

	  				   tbl_doctors.name as doc_name,

	  				   tbl_doctors.profile_pic as doc_pic,

	  				   tbl_specialization.specialization_name as doc_specialization

							 ");

	$this->db->from('tbl_booking');



	$this->db->join('tbl_doctors', 'tbl_booking.doctor_id = tbl_doctors.id','inner');

	$this->db->join('tbl_specialization', 'tbl_doctors.specialization = tbl_specialization.id','inner');

	$this->db->where('tbl_booking.id',$booking_id);

	$query = $this->db->get();

	return $query->row_array();

	

}

public function get_all_policy()

{

	$this->db->select("*");

	$this->db->from('tbl_policy');

	$query = $this->db->get();

	return $query->row_array();

}

public function cancel_Booking($booking_id)

{

	$this->db->where('tbl_booking.id',$booking_id);

	$this->db->update('tbl_booking',array('booking_status'=>4)); //checked



	$this->db->select('*');

	$this->db->from('tbl_booking');

	$this->db->where('id',$booking_id);

	$query = $this->db->get();

	return $query->row_array();



}

public function change_waitinglist_to_confirmed($booking_id)

{

	$this->db->where('tbl_booking.id',$booking_id);

	$this->db->update('tbl_booking',array('booking_status'=>1)); //checked

}

public function check_waiting_list($data)

{

	$this->db->select('count(id) as count');

	$this->db->select('id as booking_id');

	$this->db->from('tbl_booking');

	$this->db->where('doctor_id',$data['doctor_id']);

	$this->db->where('clinic_id',$data['clinic_id']);

	$this->db->where('date',$data['date']);

	$this->db->where('time',$data['time']);

	$this->db->where('booking_status',0); //checked

	$this->db->where('payment_status',1);

	$query = $this->db->get();

	return $query->row_array();

}

public function update_Booking($data)

{

	$times = explode('-', $data['confirm-book-time']);	

	$book_start_time = strtotime($data['confirm-book-date'].' '.$times[0]);

	$book_end_time = strtotime($data['confirm-book-date'].' '.$times[1]);

	//print_r($book_end_time);die();

	$this->db->where('tbl_booking.id',$data['reschedule-book-id']);

	$this->db->update('tbl_booking',array('date'=>strtotime($data['confirm-book-date']),'time'=>$data['confirm-book-time'],'time_start'=>$book_start_time,'time_end'=>$book_end_time));

}

public function checkDoctorExist($doc_id)

{

 		$this->db->select("*");

 		$this->db->from("tbl_consultation");

 		$this->db->where_in("tbl_consultation.doctor_id",$doc_id);

 		//$this->db->where("tbl_consultation.clinic_id",$clinicId);

 		$query = $this->db->get();

 		return $query->result_array();

 		

}



public function Schedulelist($clinicId,$docId)

{

 		$this->db->select("date");

 		$this->db->from("tbl_consultation");

 		$this->db->where_in("tbl_consultation.doctor_id",$docId);

 		$this->db->where_in("tbl_consultation.clinic_id",$clinicId);

 		//$this->db->where("tbl_consultation.clinic_id",$clinicId);

 		$query = $this->db->get();

 		return $query->row_array();

 		

}

function set_new_consultation($data,$clinicId,$doctors)

{

	

	$newData = json_encode($data);

	foreach ($doctors as $key => $value) {

		$this->db->where(array('doctor_id'=>$value,'clinic_id'=>$clinicId));

		$this->db->update('tbl_consultation',array('date'=>$newData));

	}

}

function assignDoctors($doctors,$clinicId)

{

	foreach ($doctors as $key => $value) 

	{

		$this->db->insert('tbl_clinic_doctors',array('doctor_id'=>$value,'clinic_id'=>$clinicId));

	}

}

function insertVacation($request)

{

	if($this->db->insert('tbl_doctor_leave', $request))

	{

		return true;

	}

	else

	{

		return false;

	}

}



function update_profile($id,$data)

{
	$date_of_birth = $data['dob'];
    unset($data['dob']);

	$this->db->where('tbl_registration.id',$id);

	if($this->db->update('tbl_registration',$data))

	{



		$this->db->query("update tbl_registration set dob = AES_ENCRYPT(".$date_of_birth.",'Ptf/PWNWrULQT72syxfaaBRTS9JbiKrj9dfuVEvT3rA')where id = ".$id);

		$result = array('status' => 'success');

	}

	else

	{

		$result = array('status' => 'error');

	}

	return $result;

}



function get_single_patient_row($id)

{

	$this->db->select("*");

	$this->db->from("tbl_registration");

	$this->db->where("tbl_registration.id",$id);

	$query = $this->db->get();

	$getsingle_patient = $query->row_array();
	$this->db->select("CAST(AES_DECRYPT(`dob`,'Ptf/PWNWrULQT72syxfaaBRTS9JbiKrj9dfuVEvT3rA') as CHAR) as dob");
	$this->db->where('id',$id);
	$query_date = $this->db->get('tbl_registration')->row();

	// echo $this->db->last_query();
	// die();

	unset($getsingle_patient['dob']);
	$getsingle_patient['dob'] = $query_date->pt_dob;

	return $getsingle_patient;

}



function register_dependent($data)

{



	if($this->db->insert('tbl_patient_dependent', $data))

	{

		$insertid = $this->db->insert_id();

		$query = $this->db->get_where("tbl_patient_dependent",array("id"=>$insertid)); 

		$return_array = array('status'=>'success','data'=>$query->row_array());

	}

	else

	{

		$return_array = array('status'=>'fail');

	}

	return $return_array;

}



function delete_registration_dependent($uid)

{

	 if($this->db->where_in('id', $uid)->delete('tbl_patient_dependent')){ }

}



function updatePic_dependent($data,$id)

{

	$this->db->update('tbl_patient_dependent', $data, array('id' => $id));

}



function get_all_dependent_for_patient($id)

{

	$this->db->select("id,

					patient_id,

					dependent_name,

					relation,dob,

					cpf,

					image,

					floor(datediff (now(), DATE_FORMAT(FROM_UNIXTIME(dob), '%Y-%m-%d'))/365) as age ");

	$this->db->from("tbl_patient_dependent");

	$this->db->where("tbl_patient_dependent.patient_id",$id);

	$query = $this->db->get();

	return $query->result_array();

}



function get_single_dependent($id)

{

	$this->db->select("id,

					patient_id,

					dependent_name,

					relation,dob,

					cpf,

					image,

					floor(datediff (now(), DATE_FORMAT(FROM_UNIXTIME(dob), '%Y-%m-%d'))/365) as age ");

	$this->db->from("tbl_patient_dependent");

	$this->db->where("tbl_patient_dependent.id",$id);

	$query = $this->db->get();

	return $query->row_array();

}



function update_dependent_profile($id,$data)

{

	unset($data['dependent_id']);

	$this->db->where('tbl_patient_dependent.id',$id);

	if($this->db->update('tbl_patient_dependent',$data))

	{

		$result = array('status' => 'success');

	}

	else

	{

		$result = array('status' => 'error');

	}

	return $result;

}



public function get_patient_password($id)

{

	$this->db->select("password");

	$this->db->from("tbl_registration");

	$this->db->where("id",$id);

	$query = $this->db->get();

	return $query->row_array();

}

public function get_patient_confirmation_code($id)

{

	$this->db->select("confirmation_code");

	$this->db->from("tbl_registration");

	$this->db->where("id",$id);

	$query = $this->db->get();

	return $query->row_array();

}



public function disable_patient_account($id)

{

	$data = array('account_status' =>1);

	$this->db->where('tbl_registration.id',$id);

	$this->db->update('tbl_registration',$data);

}



public function get_all_promocodes()

{

	$now = strtotime(date('y-m-d'));

	$this->db->select('id as promo_id,

						promo_name,

						image,

						description');

	$this->db->from('tbl_promocode');

	$this->db->where('valid_from<='.$now);

	$this->db->where('valid_to>='.$now);

	$this->db->where('status',1);

	$this->db->order_by('id','DESC');

	$query = $this->db->get();

	return $query->result_array();

}



public function get_promocode_details($id)

{

	$this->db->select('*');

	$this->db->from('tbl_promocode');

	$this->db->where('id',$id);



	$query = $this->db->get();

	return $query->row_array();

}



public function set_confirmation_code($user,$code)

{

    $data = array('confirmation_code' =>$code);

	$this->db->where('tbl_registration.id',$user['id']);

	$this->db->update('tbl_registration',$data);

}



public function get_notifications($id,$page,$limit = null)

{

	$start = 0;

	if(!isset($limit) or empty($limit))

		{	

			$limit = $page * 5;

		}

	$this->db->select("id,

						type,

						CASE 

							WHEN type = '0' THEN 'New Booking'

							WHEN type = '1' THEN 'Waiting List'

							WHEN type = '2' THEN 'Consultation Reminder'

							WHEN type = '3' THEN 'Consulation Canceled'

						END as type_desc,

						message,

						read_status,

						time");

	$this->db->from('tbl_patient_notification');

	$this->db->where('patient_id',$id);

	$this->db->order_by('time','DESC');

	$this->db->limit($limit, $start);

	$query = $this->db->get();

	return $query->result_array();

}



function get_notifications_total_count($patient_id)

{

	$this->db->select('count(id) as count');

	$this->db->from('tbl_patient_notification');

	$this->db->where('patient_id',$patient_id);

	$query = $this->db->get();

	return $query->result_array();

}



  

  

} 

?> 