<?php 

   class Home_model extends CI_Model {

	

	function __construct() { 

	 parent::__construct(); 

	} 



	public function get_speciality()

	{

		$query = $this->db->get("tbl_specialization");

		if($query->num_rows() > 0)

		{

			$return_array = $query->result_array();

		}

		else

		{

			$return_array = array('message'=>'fail');

		}

		return $return_array;

	}

	/*public function get_speciality_data($data)

	{	$search_word = $data['keyword'];

		$this->db->select('specialization_name,id');
 		$where = 'specialization_name LIKE '."'$search_word%'";
 		$this->db->where($where);
		$querys = $this->db->get("tbl_specialization");

		if($querys->num_rows() > 0)
		{
			$return_array = array('message'=>true,'data'=>$querys->result_array());
		}
		else
		{
			$return_array = array('message'=>'fail');
		}

		return $return_array;

	}*/

	public function get_speciality_data($data)

	{	$search_word = $data['keyword'];
		if($data['keyword'] != ''){
			$this->db->select('specialization_name,id');
	 		$where = 'specialization_name LIKE '."'$search_word%'";
	 		$this->db->where($where);
			$query = $this->db->get("tbl_specialization")->result_array();
			foreach ($query as $key => $value) {
				$query[$key]['type'] = 'speciality';
			}
			//if($querys){
				$this->db->select('name as specialization_name,id,price');
		 		$where = 'name LIKE '."'$search_word%'";
		 		$this->db->where($where);
				$doctors = $this->db->get("tbl_doctors")->result_array();
				if($doctors){

					foreach ($doctors as $key => $value) {
						if($value['price'] > 0){
							unset($doctors[$key]['price']);
							$doctors[$key]['type'] = 'doctor';
						}else{
							array_splice($doctors,$key, 1);
						}
					}	
				}
				if($query){
					$querys = array_merge($query,$doctors);
				}else{
					$querys = $doctors;
				}
			

				$this->db->select('name as specialization_name,id');
		 		$where = 'name LIKE '."'$search_word%'";
		 		$this->db->where($where);
		 		$this->db->where('id !=','0');
				$clinic = $this->db->get("tbl_clinic")->result_array();
				if($clinic){
					foreach ($clinic as $key => $value) {
						$clinic[$key]['type'] = 'clinic';
					}
				}
				if($querys){
					$querys = array_merge($querys,$clinic);
				}else{
					$querys = $clinic;
				}
			//}
			

			if($querys)
			{
				$return_array = array('message'=>true,'data'=>$querys,'doctor'=>$doctors,'clinic'=>$clinic,'speciality'=>$query);
			}
			else
			{
				$return_array = array('message'=>'fail');
			}
		}else{
			$return_array = array('message'=>'fail');
		}

		return $return_array;

	}




	public function emailExist($data)

	{

		$query_email = $this->db->get_where("tbl_registration",array("email"=>$data['email']));

		if($query_email->num_rows() > 0)

		{

			$return_array = array('message'=>'email already exist');

		}

		else

		{

			$return_array = array('message'=>'success');

		}

		return $return_array;

	}



	public function usernameExist($data)

	{

		$query_email = $this->db->get_where("tbl_registration",array("username"=>$data['username']));

		if($query_email->num_rows() > 0)

		{

			$return_array = array('message'=>'username already exist');

		}

		else

		{

			$return_array = array('message'=>'success');

		}

		return $return_array;

	}



	public function usernameExist_doc($data)

	{

		$query_email = $this->db->get_where("tbl_doctors",array("username"=>$data['username']));

		if($query_email->num_rows() > 0)

		{

			$return_array = array('message'=>'username already exist');

		}

		else

		{

			$return_array = array('message'=>'success');

		}

		return $return_array;

	}



	public function emailExist_doc($data)

	{

		$query_email = $this->db->get_where("tbl_doctors",array("email"=>$data['email']));

		if($query_email->num_rows() > 0)

		{

			$return_array = array('message'=>'email already exist');

		}

		else

		{
			$query_email = $this->db->get_where("tbl_doctor_colaborators",array("email"=>$data['email']));

			if($query_email->num_rows() > 0){

				$return_array = array('message'=>'email already exist');

			}else{

				$return_array = array('message'=>'success');
				
			}

		}

		return $return_array;

	}



	public function emailExist_colabor($data)

	{

		$query_email = $this->db->get_where("tbl_doctor_colaborators",array("email"=>$data['email']));

		if($query_email->num_rows() > 0)

		{

			$return_array = array('message'=>'email already exist');

		}

		else

		{

			$return_array = array('message'=>'success');

		}

		return $return_array;

	}



    public function registration($data) 

    {
    	$date_of_birth = $data['dob'];
    	unset($data['dob']);

		if($this->db->insert('tbl_registration', $data)){

			$insertid = $this->db->insert_id();

			$this->db->query("update tbl_registration set dob = AES_ENCRYPT(".$date_of_birth.",'Ptf/PWNWrULQT72syxfaaBRTS9JbiKrj9dfuVEvT3rA')where id = ".$insertid);

			$query = $this->db->get_where("tbl_registration",array("id"=>$insertid))->row_array(); 

			$this->db->select("CAST(AES_DECRYPT(`dob`,'Ptf/PWNWrULQT72syxfaaBRTS9JbiKrj9dfuVEvT3rA') as CHAR) as dob");
			$this->db->where('id',$insertid);
			$query_date = $this->db->get('tbl_registration')->row();

			unset($query['dob']);
			$query['dob'] = $query_date->dob;
			$return_array = array('status'=>'success','userdata'=>$query);

		}

		else{

			$return_array = array('status'=>'fail');

		}

		//print_r($return_array);die();

		return $return_array;

	}



	public function authtoken_registration($authtoken,$userid){

		$data = array('authtoken'=>$authtoken,'userid'=>$userid);

		if($this->db->insert('tbl_authtoken', $data)){

			return true;

		}

		else{

			return false;

		}

	}



	public function updatePic($data,$id){

		if($this->db->update('tbl_registration', $data, array('id' => $id)))

			return true;

		else

			return false;

	}



	public function delete_registration($uid)

	{

	 	if($this->db->where_in('id', $uid)->delete('tbl_registration')){}

	}



	public function login($data)

	{

		//print_r($data['login_type']);die();

		if($data['login_type']=="PATIENT")

		{



		//$this->db->join('tbl_authtoken', 'tbl_authtoken.userid = tbl_registration.id', 'inner');

			$query = $this->db->get_where("tbl_registration",array("username"=>$data['login-form-username'],"password"=>md5($data['login-form-password'])));

			$query_email = $this->db->get_where("tbl_registration",array("email"=>$data['login-form-username'],"password"=>md5($data['login-form-password'])));

			$type = 'PATIENT';

		}

		else if($data['login_type']=="DOCTOR")

		{
			//$this->db->select("tbl_doctors.*,CAST(AES_DECRYPT(name, 'Ptf/PWNWrULQT72syxfaaBRTS9JbiKrj9dfuVEvT3rA') as CHAR) as name");

			$query = $this->db->get_where("tbl_doctors",array("username"=>$data['login-form-username'],"password"=>md5($data['login-form-password'])));

			$query_email = $this->db->get_where("tbl_doctors",array("email"=>$data['login-form-username'],"password"=>md5($data['login-form-password'])));
			
			if($query->num_rows() == 0 && $query_email->num_rows() == 0 ){

				$query = $this->db->get_where("tbl_doctor_colaborators",array("email"=>$data['login-form-username'],"password"=>md5($data['login-form-password'])));

				$query_email = $this->db->get_where("tbl_doctor_colaborators",array("email"=>$data['login-form-username'],"password"=>md5($data['login-form-password'])));

				$type = 'COLLABORATOR';

			}else{
				$type = 'DOCTOR';
			}


		


		}

		

		if($query->num_rows() > 0 || $query_email->num_rows() >0 ){

			if($query->num_rows() > 0)

			{
				$patient_data = $query->row_array();
				if($type != 'COLLABORATOR'){
					$this->db->select("CAST(AES_DECRYPT(`dob`,'Ptf/PWNWrULQT72syxfaaBRTS9JbiKrj9dfuVEvT3rA') as CHAR) as dob");
					$this->db->where('id',$patient_data['id']);
					if($type == 'DOCTOR'){
						$query_date = $this->db->get('tbl_doctors')->row();
					}else if($type == 'PATIENT'){
						$query_date = $this->db->get('tbl_registration')->row();
					}
					$patient_data['dob'] = $query_date->dob;
				}
				

				$return_array = array('status'=>'success','type'=>$type,'userdata'=> $patient_data);

			}

			else if($query_email->num_rows() >0)

			{

				$patient_email = $query_email->row_array();
				if($type != 'COLLABORATOR'){
					$this->db->select("CAST(AES_DECRYPT(`dob`,'Ptf/PWNWrULQT72syxfaaBRTS9JbiKrj9dfuVEvT3rA') as CHAR) as dob");
					$this->db->where('id',$patient_email['id']);
					if($type == 'DOCTOR'){
						$query_date = $this->db->get('tbl_doctors')->row();
					}else if($type == 'PATIENT'){
						$query_date = $this->db->get('tbl_registration')->row();
					}

					$patient_email['dob'] = $query_date->dob;
				}

				$return_array = array('status'=>'success','type'=>$type,'userdata'=>$patient_email);

			}



		}

		else{

			$return_array = array('status'=>'fail');

		}

		//print_r($return_array);die();

		return $return_array;

	}

	/*public function location_update($userdata,$request)

	{

		//print_r($userdata['id']);die();

		$query = $this->db->get_where("tbl_user_location",array("userid"=>$userdata['id']));



		if($query->num_rows() > 0) 

		{

			print_r("location exist");die();

		}

		else

		{

			if($this->db->insert("tbl_user_location",array("userid"=>$userdata['id'],"location_name"=>$request['address'],"location_latitude"=>$request['latitude'],"location_longitude"=>$request['longitude']))){



				$return_array = array('status'=>'success');

			}

			else{

				$return_array = array('status'=>'fail');

			}

		}

		return $return_array;

	}*/



	function location_update($userdata,$request){

		//print_r($request['address']);die();

		

		$sql = $this->db->insert_string('tbl_user_location', array("userid"=>$userdata['id'],"location_name"=>$request['address'],"location_latitude"=>$request['latitude'],"location_longitude"=>$request['longitude'])) . ' ON DUPLICATE KEY UPDATE userid = ' .$userdata['id'].',location_name ='.'"' .$request['address'].'"'.',location_latitude='.$request['latitude'].',location_longitude='.$request['longitude'];

		//print_r($this->db->last_sqlquery());die();

		if($this->db->query($sql)){

			$return_array = array('status'=>'success');

		}

		else{

			$return_array = array('status'=>'fail');

		}

		

		return $return_array;

	}



	function location_update_doctor($userdata,$request){

		//print_r($request['address']);die();

		

		$sql = $this->db->insert_string('tbl_doctors_location', array("doctor_id"=>$userdata['id'],"location_name"=>$request['address'],"location_lattitude"=>$request['latitude'],"location_longitude"=>$request['longitude'])) . ' ON DUPLICATE KEY UPDATE doctor_id = ' .$userdata['id'].',location_name ='.'"' .$request['address'].'"'.',location_lattitude='.$request['latitude'].',location_longitude='.$request['longitude'];

		//print_r($this->db->last_sqlquery());die();

		if($this->db->query($sql)){

			$return_array = array('status'=>'success');

		}

		else{

			$return_array = array('status'=>'fail');

		}

		

		return $return_array;

	}

	  

	function register_doctor($data)

	{



		if($this->db->insert('tbl_doctors', $data))
		{
			$insertid = $this->db->insert_id();

			$key = $this->config->item('encryption_key');
			

			$this->db->query("update tbl_registration set dob = AES_ENCRYPT(".$data['dob'].",'Ptf/PWNWrULQT72syxfaaBRTS9JbiKrj9dfuVEvT3rA')where id = ".$insertid);
			//$this->db->query("Update tbl_doctors SET dob = AES_ENCRYPT(".$data['dob'].",".$key."), name = AES_ENCRYPT('".$data['name']."',".$key.") where id=".$insertid);		

			$this->db->insert('tbl_clinic_doctors',array('doctor_id'=>$insertid,'clinic_id'=> 0));	
			$this->db->insert('tbl_consultation',array('doctor_id'=>$insertid,'clinic_id'=> 0,'date'=>'""','date_secondary'=>'""','active_schedule'=> 0));

			$query = $this->db->get_where("tbl_doctors",array("id"=>$insertid)); 

			$return_array = array('status'=>'success','data'=>$query->row_array());

		}

		else

		{

			$return_array = array('status'=>'fail');

		}

		return $return_array;

	}



	function delete_registration_doctor($uid)

	{

		 if($this->db->where_in('id', $uid)->delete('tbl_doctors')){ }

	}



	function updatePic_doctor($data,$id)

	{

		$this->db->update('tbl_doctors', $data, array('id' => $id));

	}



	function authtoken_registration_doctor($authtoken,$userid)

	{

		$data = array('authtoken'=>$authtoken,'doctor_id'=>$userid);

		if($this->db->insert('tbl_authtoken_doctors', $data)){

			return true;

		}

		else{

			return false;

		}

	}



	public function insert_notification_doctor($data)

	{

		if($this->db->insert('tbl_doctor_notifications',$data))

		{

			$insert_id = $this->db->insert_id();

			return $insert_id;

		}

		

	}



	public function insert_notification_patient($data)

	{

		if($this->db->insert('tbl_patient_notification',$data))

		{

			$insert_id = $this->db->insert_id();

			return $insert_id;

		}

		

	}





	public function check_valid_email_forgot($email,$type)

	{

		$this->db->select('count(id) as count,

							id,

							name');

		if($type=='DOCTOR')

		{

			$this->db->from('tbl_doctors');

		}

		elseif($type=='PATIENT')

		{

			$this->db->from('tbl_registration');

		}



		$this->db->where('email',$email);

		$query = $this->db->get();

		return $query->row_array();

	}



	public function get_all_chat_users($id,$type)

	{

		

		if($type=='DOCTOR' or $type=="COLLABORATOR")

		{

			$this->db->distinct('tbl_registration.id');

			$this->db->select('tbl_registration.id as patient_id,

								tbl_registration.name as pat_name,

								tbl_registration.profile_photo as pat_pic');

			$this->db->where('tbl_booking.doctor_id',$id);

			$this->db->where('tbl_booking.booking_status > 0');

			$this->db->where('tbl_booking.booking_status < 4');

			$this->db->where('tbl_booking.payment_status',1);

			

			$this->db->join('tbl_registration', 'tbl_registration.id = tbl_booking.patient_id', 'inner');

		}

		elseif($type=='PATIENT')

		{

			$this->db->distinct('tbl_doctors.id');

			$this->db->select('tbl_doctors.id as doctor_id,

								tbl_doctors.name as doc_name,

								tbl_doctors.profile_pic as doc_pic');

			$this->db->where('tbl_booking.patient_id',$id);

			$this->db->where('tbl_booking.booking_status > 0');

			$this->db->where('tbl_booking.booking_status < 4');

			$this->db->where('tbl_booking.payment_status',1);

			

			$this->db->join('tbl_doctors', 'tbl_doctors.id = tbl_booking.doctor_id', 'inner');

		}

		

		$this->db->from('tbl_booking');

		$query = $this->db->get();

		return $query->result_array();

	}



	public function get_recent_chat($id,$type)

	{

		

		if($type=='DOCTOR' or $type=='COLLABORATOR')

		{

			$this->db->select('tbl_recent_chats.*,

								tbl_registration.name as pat_name,

								tbl_registration.profile_photo as pat_pic');

			$this->db->where('tbl_recent_chats.doctor_id',$id);

			$this->db->join('tbl_registration', 'tbl_registration.id = tbl_recent_chats.patient_id', 'inner');

		}

		elseif($type=='PATIENT')

		{

			$this->db->select('tbl_recent_chats.*,

								tbl_doctors.name as doc_name,

								tbl_doctors.profile_pic as doc_pic');

			$this->db->where('tbl_recent_chats.patient_id',$id);

			$this->db->join('tbl_doctors', 'tbl_doctors.id = tbl_recent_chats.doctor_id', 'inner');

		}

		$this->db->order_by('tbl_recent_chats.time','desc');

		$this->db->from('tbl_recent_chats');

		

		$query = $this->db->get();

		return $query->result_array();

	}

	  

	public function update_recent_chat($post)

	{


		$sql = $this->db->insert_string('tbl_recent_chats', $post) . ' ON DUPLICATE KEY UPDATE sender_type = ' .$post['sender_type'].',msg ='.'"' .$post['msg'].'"'.',photo_url='.'"' .$post['photo_url'].'"'.',video_url='.'"' .$post['video_url'].'"'.',type='.'"' .$post['type'].'"'.',time='.$post['time'];

		//print_r($this->db->last_sqlquery());die();

		if($this->db->query($sql))

		{

			$return_array = array('status'=>'success');

		}

		else

		{

			$return_array = array('status'=>'fail');

		}

		return $return_array;

	}



	public function check_confirmation_id($id,$code,$type)

	{

		$this->db->select('confirmation_code');

		$this->db->where('id',$id);



		if($type=="1")

		{

			$this->db->from('tbl_registration');

		}

		elseif($type=="2")

		{

			$this->db->from('tbl_doctors');

		}

		$query = $this->db->get();

		return $query->row_array();



	}



	public function update_profile($id,$type,$arr)

	{

		if($type=="1")

		{

			if($this->db->update('tbl_registration', $arr, array('id' => $id)))

				return true;

			else

				return false;

		}

		elseif($type=="2")

		{

			if($this->db->update('tbl_doctors', $arr, array('id' => $id)))

				return true;

			else

				return false;

		}

	}



	public function removeColaborator($id)

	{

		$this->db->where('id', $id);

		if($this->db->delete('tbl_doctor_colaborators'))

		{

			return 1;

		}

		else

		{

			return 0;

		}

	}



	public function insert_bank_account($data)

	{
		//print_r($data);exit();
		if($this->db->insert('tbl_bank_accounts', $data))

		{

			return true;

		}

		else

		{

			return false;

		}

	}



	public function get_all_banks($id,$type)

	{

		$this->db->select('id,

						bank_name,

						agency,

						account_no,

						account_holder');

		$this->db->from('tbl_bank_accounts');

		if($type=="DOCTOR")

		{

			$this->db->where('type',1);

		}

		elseif($type=="PATIENT")

		{

			$this->db->where('type',0);

		}

		$this->db->where('type_id',$id);

		$query = $this->db->get();

		return $query->result_array();

	}



	public function remove_bank($id)

	{

		$this->db->where('id', $id);

		$this->db->delete('tbl_bank_accounts'); 

	}



	public function get_redemption_balance($id)

	{

		$this->db->select('reedem_earn');

		$this->db->from('tbl_wallet_details');

		$this->db->where('doctor_id',$id);

		$query = $this->db->get();

		return $query->row_array();

	}	



	public function add_redemption_request($data)

	{

		$this->db->insert('tbl_withdrawal_history', $data);

	}



	public function get_bank_valid($id,$bank_id)

   	{

   		$this->db->select('count(id) as count');

   		$this->db->from('tbl_bank_accounts');

		$this->db->where('type_id',$id);

		$this->db->where('type',1);

		$this->db->where('id',$bank_id);

		$query = $this->db->get();

		return $query->row_array();

   	}



   	public function get_ipok_settings()

   	{

   		$this->db->select('*');

   		$this->db->from('settings');

		$query = $this->db->get();

		return $query->row_array();

   	}



   	public function get_last_redemption($id)

   	{

   		$this->db->select('*');

   		$this->db->from('tbl_withdrawal_history');

   		$this->db->where('date'.'<'.time());

   		$this->db->where('status',2);

   		$this->db->where('doctor_id',$id);

   		$this->db->limit(1);

		$query = $this->db->get();

		return $query->row_array();

   	}



   	public function get_next_release($id)

   	{

   		$current_time_in_UTC = strtotime(local_time_in_server(time()));

   		//print_r($current_time_in_UTC);die();

   		$this->db->select('*');

   		$this->db->from('tbl_booking');

   		$this->db->where('time_start'.'>'.$current_time_in_UTC);

   		$this->db->where('booking_status',1);

   		$this->db->where('doctor_id',$id);

   		$this->db->limit(1);

		$query = $this->db->get();

		return $query->row_array();

   	}

    

    public function redemptionhistory($id)

    {

    	$this->db->select("tbl_withdrawal_history.id as id,

    					tbl_withdrawal_history.amount as amount,

    					tbl_withdrawal_history.date as date,

    					CASE 

    						WHEN tbl_withdrawal_history.status = '0' THEN 'PENDING'

    						WHEN tbl_withdrawal_history.status = '1' THEN 'INPROGRESS'

    						WHEN tbl_withdrawal_history.status = '2' THEN 'COMPLETED'

    						ELSE 'REJECTED'

    					END as status,

    					tbl_bank_accounts.bank_name as bank_name,

    					tbl_bank_accounts.account_holder as account_holder,

    					tbl_bank_accounts.account_no as account_no ");

    	$this->db->from('tbl_withdrawal_history');

    	$this->db->where('tbl_bank_accounts.type',1);

    	$this->db->where('tbl_withdrawal_history.doctor_id',$id);

    	//$this->db->where('tbl_withdrawal_history.status',2);

    	$this->db->where('tbl_bank_accounts.type_id',$id);

    	$this->db->join('tbl_bank_accounts', 'tbl_bank_accounts.id = tbl_withdrawal_history.bank_id', 'inner');

    	$this->db->order_by('tbl_withdrawal_history.date','DESC');

    	$query = $this->db->get();

		return $query->result_array();

    }



    public function futurereleases($id)

    {

    	$current_time_in_UTC = strtotime(local_time_in_server(time()));

   		//print_r($current_time_in_UTC);die();

   		$this->db->select("tbl_booking.doctor_id,

   			tbl_booking.date as date,

   			tbl_booking.time_start as time_start,

   			CASE 

   				WHEN tbl_booking.promo_name = '' THEN 'NORMAL'

   				WHEN tbl_booking.promo_name != '' THEN 'SPECIAL'

   			END as consultation_type,

   			tbl_booking.promo_name as promocode,

   			tbl_booking.total_sum as total_sum,

   			tbl_booking.ipok_fee as ipok_fee,

   			tbl_doctors.name as doctor_name,

   			(tbl_booking.total_sum - ((tbl_booking.total_sum * tbl_booking.ipok_fee)/100)) as release_amount

   			");

   		$this->db->from('tbl_booking');

   		$this->db->join('tbl_doctors', 'tbl_doctors.id = tbl_booking.doctor_id', 'inner');

   		$this->db->where('tbl_booking.time_start'.'>'.$current_time_in_UTC);

   		$this->db->where('tbl_booking.booking_status',1);

   		$this->db->where('tbl_booking.doctor_id',$id);

   		$this->db->order_by('tbl_booking.time_start','ASC');

   		//$this->db->limit(1);

		$query = $this->db->get();

		return $query->result_array();

    }



    public function get_todays_booking()

    {

    	$todayat12 = strtotime(date('Y-m-d'));

    	//print_r($todayat12);

    	//die();

    	$this->db->select("*");

    	$this->db->from('tbl_booking');

    	$this->db->where('date',$todayat12);

    	$this->db->where('booking_status',1);

    	$this->db->where('payment_status',1);

    	$query = $this->db->get();

		return $query->result_array();

    }



    public function get_patient_fcm($pat_id)

    {

    	$this->db->select('fcm_token');

    	$this->db->from('tbl_authtoken');

    	$this->db->where('userid',$pat_id);

    	$query = $this->db->get();

		return $query->row_array();

    }



    public function get_doctor_fcm($doc_id)

    {

    	$this->db->select('fcm_token');

    	$this->db->from('tbl_authtoken_doctors');

    	$this->db->where('doctor_id',$doc_id);

    	$query = $this->db->get();

		return $query->row_array();

    }



    public function check_cpfunique($cpf)

    {

    	$this->db->select('count(id) as count');

    	$this->db->from('tbl_cpf_number');

    	$this->db->where('cpf',$cpf);

    	$query = $this->db->get();

		return $query->row_array();

    }



    public function insertcpfunique($obj)

    {

    	$this->db->insert('tbl_cpf_number', $obj);

    }



    public function remove_cpfunique($id,$type)

    {

    	if($type=='c')

    	{

    		$this->db->where('user_type', 2);

    	}

    	elseif($type=='d')

    	{

    		$this->db->where('user_type', 1);

    	}

    	elseif($type=='p')

    	{

    		$this->db->where('user_type', 0);

    	}

    	$this->db->where('user_id', $id);

		$this->db->delete('tbl_cpf_number'); 

    }

    public function get_booking_for_payment_cron()
    {
    	$now = strtotime(date('Y-m-d H:i:s'));
    	$this->db->select("id");
    	$this->db->from('tbl_booking');
    	$this->db->where('booking_status',1);
    	$this->db->where('payment_status',0);
    	$where = '('.$now.' - `requested_date`)>300';
    	$this->db->where($where);
    	$query = $this->db->get();
    	//print_r($this->db->last_query());
		return $query->result_array();
    }

	

	   

  

  

   } 

?> 