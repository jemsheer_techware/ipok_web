<?php 
   class Printer_model extends CI_Model {
	
function __construct() { 
	 parent::__construct(); 
} 

public function authentic_record($record_id,$user_id,$type)
{
	
	$this->db->select('tbl_medical_records.id as medical_record_id,
						tbl_medical_records.main_complaint as  main_complaint,
						tbl_medical_records.description as  description,
						tbl_medical_records.diseases as  diseases,
						tbl_medical_records.prescribtions as  prescribtions,
						tbl_medical_records.exams as  exams,
						tbl_medical_records.budget as  budget,
						tbl_medical_records.letters as  letters,
						tbl_medical_records.other_observations as  other_observations,
						tbl_medical_records.images as  images,
						tbl_medical_records.patient_review as  patient_review,
						tbl_booking.id as  booking_id,
						tbl_booking.patient_id as  patient_id,
						tbl_booking.doctor_id as  doctor_id');
	$this->db->from('tbl_medical_records');
	$this->db->where('tbl_medical_records.id',$record_id);
	$this->db->join('tbl_booking', 'tbl_booking.medical_record_id = tbl_medical_records.id','inner');

	if($type=='PATIENT')
		{$this->db->where('tbl_booking.patient_id',$user_id);}
	if($type=='DOCTOR')
		{$this->db->where('tbl_booking.doctor_id',$user_id);}
	
	$query = $this->db->get();
	return $query->row_array();
}
	  
  
} 
?> 